﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace SCA.Automation.Library
{
    public class AgeingWaitingStatusReport
    {
        private readonly List<string> TicketStatusList_One = new List<string>(new string[] { "< 7 days", "7 days to 14 days", "14 days to 30 days", ">= 30 days" });
        private readonly List<string> TicketStatusList_Two = new List<string>(new string[] { "50 - Waiting for Customer", "52 - Waiting for External", "70 - Waiting for Confirmation"});
        private DataTable dtAgeingWaitingReport_One;
        private DataTable dtAgeingWaitingReport_Two;

        public void GenerateAgeingWaitingStatusReport_One(DataTable dtSolutions, DataTable dtExcel, out DataTable dtAgeingWaitingIncidents_One, out DataTable dtAgeingWaitingServiceRequest_One, out DataTable dtAgeingWaitingProblem_One)
        {
            dtAgeingWaitingIncidents_One = dtAgeingWaitingServiceRequest_One = dtAgeingWaitingProblem_One = new DataTable();
            dtSolutions.DefaultView.Sort = "Other Products";
            DataTable temp = dtSolutions.DefaultView.ToTable(true, "Other Products");
            dtAgeingWaitingReport_One = new DataTable();
            dtAgeingWaitingReport_One.Columns.Add("Solutions");
            dtAgeingWaitingReport_One.Columns.Add("< 7 days");
            dtAgeingWaitingReport_One.Columns.Add("7 days to 14 days");
            dtAgeingWaitingReport_One.Columns.Add("14 days to 30 days");
            dtAgeingWaitingReport_One.Columns.Add(">= 30 days");
            dtAgeingWaitingReport_One.Columns.Add("Grand Total", typeof(Int32));
            DataRow datarow;
            string ticketType = "";

            for (int j = 0; j < 3; j++)
            {
                if (j == 0)
                    ticketType = "Incident";
                if (j == 1)
                    ticketType = "Service Request";
                if (j == 2)
                    ticketType = "Problem";

                foreach (DataRow dr in temp.Rows)
                {
                    int counter = 0;
                    int rowTotal = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[0])))
                        continue;
                    datarow = dtAgeingWaitingReport_One.NewRow();
                    datarow[counter] = dr.ItemArray[0].ToString();
                    foreach (var data in TicketStatusList_One)
                    {
                        counter++;
                        DataRow[] result = null;
                        if (ticketType == "Service Request")
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status Cosmos] = '240 - Waiting' and [AgeGroup] = '" + data + "' and [Parent Object] = '" + ticketType + "' and ([Parent SR Fulfillment Type] is null OR [Parent SR Fulfillment Type] = 'Maintenance and Support')");
                        else
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Status Cosmos] = '240 - Waiting' and [AgeGroup] = '" + data + "' and [Parent Object] = '" + ticketType + "'");
                        datarow[counter] = (result.Any()) ? result.Count() : 0;
                        rowTotal += result.Count();

                    }
                    datarow[counter + 1] = rowTotal;

                    if (rowTotal != 0)
                        dtAgeingWaitingReport_One.Rows.Add(datarow);
                }

                //sort by total
                DataView dataview = dtAgeingWaitingReport_One.DefaultView;
                dataview.Sort = "[Grand Total] DESC";
                dtAgeingWaitingReport_One = dataview.ToTable();

                //add column total row
                datarow = dtAgeingWaitingReport_One.NewRow();
                dtAgeingWaitingReport_One.Rows.Add(datarow);
                datarow[0] = "Grand Total";

                for (int i = 1; i < dtAgeingWaitingReport_One.Columns.Count; i++)
                {
                    datarow[i] = 0;
                    datarow[i] = dtAgeingWaitingReport_One.AsEnumerable().Sum(x => Convert.ToInt32(x[dtAgeingWaitingReport_One.Columns[i].ColumnName]));
                }

                if (dtAgeingWaitingReport_One.Rows.Count > 0)
                {
                    if (j == 0)
                        dtAgeingWaitingIncidents_One = dtAgeingWaitingReport_One.Copy();
                    if (j == 1)
                        dtAgeingWaitingServiceRequest_One = dtAgeingWaitingReport_One.Copy();
                    if (j == 2)
                        dtAgeingWaitingProblem_One = dtAgeingWaitingReport_One.Copy();
                }

                dtAgeingWaitingReport_One.Clear();
            }            
        }

        public void GenerateAgeingWaitingStatusReport_Two(DataTable dtSolutions, DataTable dtExcel, out DataTable dtAgeingWaitingIncidents_Two, out DataTable dtAgeingWaitingServiceRequest_Two, out DataTable dtAgeingWaitingProblem_Two)
        {
            dtAgeingWaitingIncidents_Two = dtAgeingWaitingServiceRequest_Two = dtAgeingWaitingProblem_Two = new DataTable();
            dtSolutions.DefaultView.Sort = "Other Products";
            DataTable temp = dtSolutions.DefaultView.ToTable(true, "Other Products");
            dtAgeingWaitingReport_Two = new DataTable();
            dtAgeingWaitingReport_Two.Columns.Add("Solution");
            dtAgeingWaitingReport_Two.Columns.Add("50 - Waiting for Customer");
            dtAgeingWaitingReport_Two.Columns.Add("52 - Waiting for External");
            dtAgeingWaitingReport_Two.Columns.Add("70 - Waiting for Confirmation");
            dtAgeingWaitingReport_Two.Columns.Add("Grand Total", typeof(Int32));
            DataRow datarow;
            string ticketType = "";
            for (int j = 0; j < 3; j++)
            {
                if (j == 0)
                    ticketType = "Incident";
                if (j == 1)
                    ticketType = "Service Request";
                if (j == 2)
                    ticketType = "Problem";
                foreach (DataRow dr in temp.Rows)
                {
                    int counter = 0;
                    int rowTotal = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[0])))
                        continue;
                    datarow = dtAgeingWaitingReport_Two.NewRow();
                    datarow[counter] = dr.ItemArray[0].ToString();
                    foreach (var data in TicketStatusList_Two)
                    {
                        counter++;
                        DataRow[] result = null;
                        if (ticketType == "Service Request")
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Parent Status] = '" + data + "' and [Parent Object] = '" + ticketType + "' and ([Parent SR Fulfillment Type] is null OR [Parent SR Fulfillment Type] = 'Maintenance and Support')");
                        else
                            result = dtExcel.Select("ProductName = '" + dr.ItemArray[0].ToString() + "' and [Parent Status] = '" + data + "' and [Parent Object] = '" + ticketType + "'");
                        datarow[counter] = (result.Any()) ? result.Count() : 0;
                        rowTotal += Convert.ToInt32(datarow[counter].ToString()); 

                    }
                    datarow[counter + 1] = rowTotal;

                    if (rowTotal != 0)
                        dtAgeingWaitingReport_Two.Rows.Add(datarow);
                }

                //sort by total
                DataView dataview = dtAgeingWaitingReport_Two.DefaultView;
                dataview.Sort = "[Grand Total] DESC";
                dtAgeingWaitingReport_Two = dataview.ToTable();

                //add column total row
                datarow = dtAgeingWaitingReport_Two.NewRow();
                dtAgeingWaitingReport_Two.Rows.Add(datarow);
                datarow[0] = "Grand Total";

                for (int i = 1; i < dtAgeingWaitingReport_Two.Columns.Count; i++)
                {
                    datarow[i] = 0;
                    datarow[i] = dtAgeingWaitingReport_Two.AsEnumerable().Sum(x => Convert.ToInt32(x[dtAgeingWaitingReport_Two.Columns[i].ColumnName]));
                }

                if (dtAgeingWaitingReport_Two.Rows.Count > 0)
                {
                    if (j == 0)
                        dtAgeingWaitingIncidents_Two = dtAgeingWaitingReport_Two.Copy();
                    if (j == 1)
                        dtAgeingWaitingServiceRequest_Two = dtAgeingWaitingReport_Two.Copy();
                    if (j == 2)
                        dtAgeingWaitingProblem_Two = dtAgeingWaitingReport_Two.Copy();
                }
                dtAgeingWaitingReport_Two.Clear();
            }
        }

    }
}
