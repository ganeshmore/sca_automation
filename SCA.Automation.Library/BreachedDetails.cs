﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Automation.Library
{
    public class BreachedDetails
    {
        readonly private string ConnectionString;
        public string mailTo;
        public string mailCC;
        public string breachedMailSubject;
        public string logPath;

        //master tables
        private DataTable dtMasterTable;
        private DataTable dtBreachedResults;


        public BreachedDetails(string connectionstring)
        {
            ConnectionString = connectionstring;

        }
        public void LoadMasterData()
        {
            dtMasterTable = PopulateDataTable(ConnectionString);
        }

        public void Process()
        {
            try
            {
                Email.ticketStatusLogPath = logPath;
                Log.write("Populating data table", null, logPath);


                Log.write("Generating reports data table", null, logPath);
                GenerateReports();

                Log.write("Sending Emails", null, logPath);
                SendEmail();
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }

        }

        private void GenerateReports()
        {            
            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).Date;
            var endDate = DateTime.Now.Date;

            /* Get Breached results */
            dtBreachedResults =
                   dtMasterTable.DefaultView.ToTable("dtBreachedResults", false, "Solution", "CreatedDateTime", "Resolution Target",  "Closed Date Time", "Summary", "BreachPassed", "Incident ID");

            var results = from data in dtBreachedResults.AsEnumerable()
                              where (data.Field<DateTime?>("Closed Date Time") >= startDate && data.Field<DateTime?>("Closed Date Time") < endDate) && data.Field<bool>("BreachPassed")
                              orderby data.Field<string>("Solution")
                              select data;

            /* Get Breached results */
                      
            dtBreachedResults = results.Any()? results.CopyToDataTable() : null;
           
            
         
        }
        private void SendEmail()
        {
            Email.SendEmailForBreached(breachedMailSubject, mailTo, mailCC, dtBreachedResults);
        }


        private string GetSheetName(OleDbConnection connExcel)
        {
            try
            {
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
                return SheetName;

            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
            catch (ArgumentException ex)
            {
                Log.write("ArgumentException exception while getting sheet name", ex, logPath);
                return string.Empty;
            }
        }
        private DataTable ReadExcelSheet(string sheetName, OleDbCommand cmdExcel, OleDbConnection connExcel)
        {

            OleDbDataAdapter oda = new OleDbDataAdapter();
            DataTable dt = new DataTable();
            try
            {
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();
            }
            catch (InvalidOperationException ex)
            {
                Log.write("InvalidOperationException exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while ReadExcelSheet", ex, logPath);
                return null;
            }
            return dt;
        }


        private DataTable PopulateDataTable(string connection)
        {
            OleDbConnection connExcel = new OleDbConnection(connection);
            OleDbCommand cmdExcel = new OleDbCommand();

            DataTable dt = null;
            try
            {
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                string SheetName = GetSheetName(connExcel);

                //Read Data from Sheet
                dt = ReadExcelSheet(SheetName, cmdExcel, connExcel);
            }
            catch (OleDbException ex)
            {
                Log.write("OleDbConnection exception while populating the data table", ex, logPath);
                return null;
            }

            return dt;
        }
    }
}
