﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Automation.Library
{
    public class CCBMailData
    {
        public string logPath;
        public void Process()
        {
            Email.CCBMailLogPath = logPath;
            DateTime todayDate = DateTime.Now;

            //Check if it is wednesday of the week
            string ActivityType = ConfigurationManager.AppSettings["Constants"].ToString();
            Email.SendPatchingEmail(todayDate, string.Empty, ActivityType, Email.CCBMailLogPath);
            
             
        }
    }
}
