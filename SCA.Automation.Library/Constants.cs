﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Automation.Library
{
    public static class Constants
    {
        //Environment constants
        public const string envDev3Test3 = "Dev3Test3";
        public const string envUAT = "UAT";
        public const string envProdDev2Test2 = "ProdDev2Test2";

        //Activity constants
        public const string mspatching = "MsPatching";
        public const string nugetpatching = "NugetPatching";
        public const string CCBMail = "CCBMail";
        public const string CCBPublishMail = "CCBPublishMail";
        public const string DeploymentCalenderMail = "DeploymentCalenderMail";

        public const string PackageID = "PackageID";
        public const string PackageName = "Package Name";
        public const string InstalledVersion = "Installed Version";
        public const string LatestVersion = "Latest Version";
        public const string LogPath = "logPath";
        public const string NugetList = "NugetList";
        public const string NumberOfNugetPkgsInSoluion = "NuGet Packages found in the given solution";
        public const string NugetPkgsInSoluion = "/**{0} found in the solution **/";
        public const string CopyToExcel = "/** {0} copy to excel sheet. **/";
        public const string SortingColumn = "PackageID ASC";
        public const string FileNotFoundException = "FileNotFoundException exception in the getting file path method, thus no extract list would be extract";
        public const string PathTooLongException = "PathTooLongException exception in the SendPatchingEmail method, thus no notification email would be sent";
        public const string GeneralException = "Excception occured in exporting excel sheet";
        public const string SelectFolder ="Please select folder for extracting Nuget..";
        public const string SelectedFolder = "You have selected {0} folder for Nuget Extract";
        public const string InitialLogText = "Log File is get created..";
        public const string LoadingText = "Loading....Exporting Nuget package for given solution.";
        public const string ListExtractionStarted = "/*****************NuGet Package List extraction started {0} *****************/";
        public const string NumberOfNugetPkgsText = "From Given Solution {0} Nuget packages exported in Excel file, you can find the file in D drive as filename as {1}";
        public const string ExportNugetFilePath = "ExportNugetFilePath";
        public const string NoNugetPkgsInSolution = "No NuGet Package used in given solution!";
        public const string SelectFile = "Please select a file!";
        public const string FileName = "File Name";
        public const string FilePath = "Path";
        public const string FileError = "Error";
        public const string XMLValidatorPath = "XMLValidatorPath";
        public const string XlFilePath = "Data exported in Excel file, you can find the file in D drive!";
        public const string NoDataToExport = "No data to export!";
        public const string NoItemToClear = "No Items to Clear!";
        public const string Config = ".config";
        public const string XML = ".xml";
        public const string NoXMlNoConfig = "The File/Files : {0} is neither an XML file nor a Config file";
        public const string PackageConfig = "packages.config";
        public const string NoError = "No Error";
        public const string AuthorName = "Run Team";
		public const string NewRelicReport = "NewRelicReport";
		public const string Timesheet = "Timesheet";
		public const string InvalidMailMsg = " while executing Scheduler for Email id--> {0} in teh distribution list is invalid so not excluded from recipient list at time ";
	}
}
