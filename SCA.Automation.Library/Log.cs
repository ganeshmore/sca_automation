﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;


namespace SCA.Automation.Library
{
    public static class Log
    {        
        public static void write(string message, Exception ex, string logPath)
        {            
            string text = ex == null ? message : ex.ToString();           
            using (StreamWriter sw = new StreamWriter(logPath, true))
            {
                sw.WriteLine(text);
            }
        }

        public static void write(string v, object p, object logPath)
        {
            throw new NotImplementedException();
        }
    }
}
