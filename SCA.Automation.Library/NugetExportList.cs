﻿using NuGet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;


namespace SCA.Automation.Library
{
    public class NugetExportList
    {
        readonly string logPath = ConfigurationManager.AppSettings[Constants.LogPath].ToString();
        public DataTable NugetListExtract(string[] fileArray)
        {
            DataTable dataTable = new DataTable(Constants.NugetList);
            dataTable.Columns.Add(Constants.PackageID, typeof(string));
            dataTable.Columns.Add(Constants.InstalledVersion, typeof(string));
            dataTable.Columns.Add(Constants.LatestVersion, typeof(string));
            IPackageRepository packageRepositoryFactory = PackageRepositoryFactory.Default.CreateRepository("https://packages.nuget.org/api/v2");

            foreach (var file in fileArray)
            {
                var pkgRefPath = file;
                if (pkgRefPath != "")
                {
                    PackageReferenceFile nugetPkgConfig = new PackageReferenceFile(pkgRefPath);
                    IEnumerable<PackageReference> allPackages = nugetPkgConfig.GetPackageReferences();
                    Log.write(allPackages.Count() + Constants.NumberOfNugetPkgsInSoluion, null, logPath);
                    foreach (var myPakage in allPackages)
                    {

                        //Connect to the official package repository
                        Log.write(String.Format(Constants.NugetPkgsInSoluion, myPakage.Id), null, logPath);
                        //Get the list of all NuGet packages with ID 'EntityFramework'       
                        var latestPackages = packageRepositoryFactory.FindPackagesById(myPakage.Id).Where(item => item.IsReleaseVersion() == true).Max(p => p.Version);
                        dataTable.Rows.Add(myPakage.Id, myPakage.Version.ToString(), latestPackages);

                        Log.write(String.Format(Constants.CopyToExcel, myPakage.Id), null, logPath);
                    }
                }


            }
            for (int i = 0; i < dataTable.Rows.Count - 1; i++)
            {
                int k = 0;
                for (int j = 0; j < dataTable.Rows.Count - 1; j++)
                {
                    if (dataTable.Rows[i][0].ToString() == dataTable.Rows[j][0].ToString())
                    {
                        if (k != 0)
                        {
                            dataTable.Rows.RemoveAt(j);
                        }
                        k++;

                    }
                }
            }
            dataTable.AcceptChanges();
            DataView dataView = dataTable.DefaultView;
            dataView.Sort = Constants.SortingColumn;
            dataTable = dataView.Table;
            return dataTable;
        }
    }
}
