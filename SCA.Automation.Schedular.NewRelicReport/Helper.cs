﻿using ClosedXML.Excel;
using SCA.Automation.Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SCA.Automation.Schedular.NewRelicReport
{
	class Helper
	{
		/// <summary>
		/// Processes the specified file path.
		/// </summary>
		/// <param name="FilePath">The file path.</param>
		/// <param name="Url">The URL.</param>
		/// <param name="QueryKey">The query key.</param>
		/// <param name="ColumnNames">The column names.</param>
		/// <param name="LogPath">The log path.</param>
		/// <returns></returns>
		public static bool Process(string LogPath)
		{
			string filePath = ConfigurationManager.AppSettings["FilePath"].ToString();
			string url = ConfigurationManager.AppSettings["URL"].ToString();
			string queryKey = ConfigurationManager.AppSettings["QueryKey"].ToString();
			string columnNames = ConfigurationManager.AppSettings["ColumnNames"].ToString();
			DataTable dataTable = GenerateReport(filePath, url, queryKey, columnNames);
			if (dataTable != null)
			{
				if (SendMail(filePath, LogPath, dataTable))
				{
					return true;
				}
			}
			return false;
		}

		private static bool SendMail(string filePath, string logPath, DataTable dataTable)
		{
			string subject = ConfigurationManager.AppSettings["Subject"].ToString();
			string mailTo = ConfigurationManager.AppSettings["MailTo"].ToString();
			string mailCC = ConfigurationManager.AppSettings["MailCC"].ToString();
			dataTable = dataTable.AsEnumerable().Take(10).CopyToDataTable();
			Email.SendEmailForNewRelic(subject, mailTo, mailCC, dataTable, filePath);
			return true;
		}

		/// <summary>
		/// Generates the report. Read https://docs.newrelic.com/docs/insights/insights-api/get-data/query-insights-event-data-api
		/// </summary>
		/// <param name="FilePath">The file path.</param>
		/// <param name="Url">The URL.</param>
		/// <param name="QueryKey">The query key.</param>
		/// <param name="ColumnNames">The column names.</param>
		/// <returns></returns>
		/// <exception cref="System.NullReferenceException">Required parameter(s) are empty, please check.</exception>
		public static DataTable GenerateReport(string FilePath, string Url, string QueryKey, string ColumnNames)
		{
			if (string.IsNullOrEmpty(FilePath) || string.IsNullOrEmpty(Url) || string.IsNullOrEmpty(QueryKey) || string.IsNullOrEmpty(ColumnNames))
				throw new NullReferenceException("Required parameter(s) are empty, please check.");
			HttpClient client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(600);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			client.DefaultRequestHeaders.Add("X-Query-Key", QueryKey);
			HttpResponseMessage response = client.GetAsync(Url).Result;
			if (response.IsSuccessStatusCode)
			{
				string responseJSON = response.Content.ReadAsStringAsync().Result;
				JsonParser jsonParser = JsonParser.FromJson(responseJSON);
				DataTable table = ToDataTable(jsonParser.Facets, ColumnNames);
				XLWorkbook xLWorkbook = File.Exists(FilePath) ? new XLWorkbook(FilePath) : new XLWorkbook();
				string sheetName = DateTime.Now.ToString("dd-MM-yyyy-fff");
				xLWorkbook.Worksheets.Add(table, sheetName).SetTabSelected(true);
				xLWorkbook.SaveAs(FilePath);
				jsonParser = null;
				xLWorkbook.Dispose();
				return table;
			}
			return null;
		}
		public static DataTable ToDataTable(List<Facet> items, string ColumnNames)
		{
			DataTable dataTable = new DataTable();
			string[] headerColumns = ColumnNames.Split(',');
			foreach (string prop in headerColumns)
			{
				dataTable.Columns.Add(prop, typeof(string));
			}
			foreach (Facet thisFacet in items)
			{
				var values = new object[headerColumns.Length];
				int i = 0;
				foreach (string val in thisFacet.Name)
				{
					values[i] = val;
					i++;
				}
				values[headerColumns.Length - 1] = thisFacet.Results[0].Count;
				dataTable.Rows.Add(values);
			}
			return dataTable;
		}
	}
}
