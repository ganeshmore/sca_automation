﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace SCA.Automation.Schedular.NewRelicReport
{

	//Use this https://app.quicktype.io/ to generate class
	public partial class JsonParser
	{
		[JsonProperty("facets")]
		public List<Facet> Facets { get; set; }

		[JsonProperty("totalResult")]
		public TotalResult TotalResult { get; set; }

		[JsonProperty("unknownGroup")]
		public TotalResult UnknownGroup { get; set; }

		[JsonProperty("performanceStats")]
		public Dictionary<string, double> PerformanceStats { get; set; }

		[JsonProperty("metadata")]
		public Metadata Metadata { get; set; }
	}

	public partial class Facet
	{
		[JsonProperty("name")]
		public List<string> Name { get; set; }

		[JsonProperty("results")]
		public List<Result> Results { get; set; }
	}

	public partial class Result
	{
		[JsonProperty("count")]
		public long Count { get; set; }
	}

	public partial class Metadata
	{
		[JsonProperty("eventTypes")]
		public List<string> EventTypes { get; set; }

		[JsonProperty("eventType")]
		public string EventType { get; set; }

		[JsonProperty("openEnded")]
		public bool OpenEnded { get; set; }

		[JsonProperty("beginTime")]
		public DateTimeOffset BeginTime { get; set; }

		[JsonProperty("endTime")]
		public DateTimeOffset EndTime { get; set; }

		[JsonProperty("beginTimeMillis")]
		public long BeginTimeMillis { get; set; }

		[JsonProperty("endTimeMillis")]
		public long EndTimeMillis { get; set; }

		[JsonProperty("rawSince")]
		public string RawSince { get; set; }

		[JsonProperty("rawUntil")]
		public string RawUntil { get; set; }

		[JsonProperty("rawCompareWith")]
		public string RawCompareWith { get; set; }

		[JsonProperty("guid")]
		public string Guid { get; set; }

		[JsonProperty("routerGuid")]
		public string RouterGuid { get; set; }

		[JsonProperty("messages")]
		public List<object> Messages { get; set; }

		[JsonProperty("facet")]
		public List<string> Facet { get; set; }

		[JsonProperty("offset")]
		public long Offset { get; set; }

		[JsonProperty("limit")]
		public long Limit { get; set; }

		[JsonProperty("contents")]
		public Contents Contents { get; set; }
	}

	public partial class Contents
	{
		[JsonProperty("messages")]
		public List<object> Messages { get; set; }

		[JsonProperty("contents")]
		public List<Content> ContentsContents { get; set; }
	}

	public partial class Content
	{
		[JsonProperty("function")]
		public string Function { get; set; }

		[JsonProperty("simple")]
		public bool Simple { get; set; }
	}

	public partial class TotalResult
	{
		[JsonProperty("results")]
		public List<Result> Results { get; set; }
	}

	public partial class JsonParser
	{
		public static JsonParser FromJson(string json) => JsonConvert.DeserializeObject<JsonParser>(json);
	}
	
	public static class Serialize
	{
		public static string ToJson(this JsonParser self) => JsonConvert.SerializeObject(self);
	}

	internal static class Converter
	{
		public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
		{
			MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
			DateParseHandling = DateParseHandling.None,
			Converters = {
		    new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
		},
		};
	}
}
