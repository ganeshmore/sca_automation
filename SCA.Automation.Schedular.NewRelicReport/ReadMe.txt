/##############################################################################################################/
					README - New Relic

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - To send the solution wise New Relic error report by email.
	* Developed By: Anupam Nayek
	* Execution Frequency: everyday for each solution
	* Input for data: solution wise New Relic Report
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
�	New Relic report sent on daily basis on an email to entire run team.


----------------------------------------------------------------------------------------------------------------
File Path:
----------------------------------------------------------------------------------------------------------------
** Daily generated solution wise New Relic report is saved at the location mentioned in config file with respective solution name.  

----------------------------------------------------------------------------------------------------------------
Subject Lines for new relic report:
----------------------------------------------------------------------------------------------------------------
** Subject lines for new relic report emails to be sent to run team are specified in the config file.
** Any changes in email subject lines will need config update.


----------------------------------------------------------------------------------------------------------------
Distribution List for emails:
----------------------------------------------------------------------------------------------------------------
** "TO" and "CC" list are metioned in the config file.
** Any changes in "TO" and "CC" list will need config update.


----------------------------------------------------------------------------------------------------------------
New Relic URL:
----------------------------------------------------------------------------------------------------------------
** New Relic URl is different for each solution. Url contains query for particular solution.
** New Relic URL through which error list is extracted is mentioned in the config file.
** Any changes in the query of new relic will need config update.

----------------------------------------------------------------------------------------------------------------
Query Key:
----------------------------------------------------------------------------------------------------------------
** Query key required to extract error list is mentioned in the config file.
** Query key is important for admin login to New relic application and through which reports are extracted for each solution.
** Any changes in the query key of new relic will need config update. It belongs to particular user(Query key should have New relic admin access).
** Currently we are using Query key of Soubhik Mukherjee's account.

----------------------------------------------------------------------------------------------------------------
Logs:
----------------------------------------------------------------------------------------------------------------
** Path for the Logs file is mentioned in the config file.
** This file should be monitored in case of any issue. It holds every run logging entry as well as errors (if any).