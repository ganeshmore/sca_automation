﻿using ClosedXML.Excel;
using SCA.Automation.Library;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.IO;
using System;
using System.Globalization;

namespace SCA.Automation.Schedular.TimeSheet
{
    /// <summary>
    /// 
    /// </summary>
    internal class Helper
    {
        /// <summary>
        /// Processes the data from the time sheet excel file, and send mail to the defaulters.
        /// </summary>
        /// <param name="logPath">The log path.</param>
        /// <returns> result</returns>
        internal static bool Process(string logPath)
        {

            XLWorkbook workbook = new XLWorkbook(ConfigurationManager.AppSettings["FilePath"].ToString());
            IXLWorksheet workSheet = workbook.Worksheet(1);
            DataTable dtTimeSheet = new DataTable();
            List<string> toList = new List<string>();
            string ccList = ConfigurationManager.AppSettings["MailCC"].ToString();
            string subject = ConfigurationManager.AppSettings["Subject"].ToString();
            string filter = ConfigurationManager.AppSettings["Filter"].ToString();
            string deadlineDate = ConfigurationManager.AppSettings["DeadlineDate"].ToString();
            string valildSupervisorListFile = ConfigurationManager.AppSettings["ValidSupervisors"].ToString();
            int firstRow = 2;//The format Sanish provided is having blank/not required two rows on the top.
            var index = 0;

            try
            {
                foreach (IXLRow row in workSheet.Rows())//Read data from the excel
                {
                    if (index < firstRow)
                    {
                        index++;
                        continue;
                    }
                    //Use the first row to add columns to DataTable.
                    if (firstRow == index)
                    {
                        foreach (IXLCell cell in row.Cells())
                        {
                            dtTimeSheet.Columns.Add(cell.Value.ToString());
                        }
                    }
                    else
                    {
                        //Add rows to DataTable.
                        dtTimeSheet.Rows.Add();
                        int i = 0;
                        foreach (IXLCell cell in row.Cells())
                        {
                            dtTimeSheet.Rows[dtTimeSheet.Rows.Count - 1][i] = cell.Value.ToString();
                            i++;
                        }
                    }
                    index++;
                }

                if ((string.IsNullOrEmpty(filter) == false) || File.Exists(valildSupervisorListFile))
                {
                    DataView dv = new DataView(dtTimeSheet);//Filter out unnecessary data
                    if (File.Exists(valildSupervisorListFile))
                    {
                        List<string> validSupervisors = File.ReadAllLines(valildSupervisorListFile).ToList();
                        validSupervisors = validSupervisors.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
                        if (validSupervisors.Any())
                        {
                            if (string.IsNullOrWhiteSpace(filter) == false)
                            {
                                filter = filter + " AND ";
                            }
                            filter = filter + "[Manager Email] in('" + string.Join("','", validSupervisors) + "')";
                        }
                    }
                    dv.RowFilter = filter;
                    dtTimeSheet = dv.ToTable();
                    dv.Dispose();
                }
                System.DateTime targetDate = System.DateTime.ParseExact(deadlineDate, "d", CultureInfo.InvariantCulture);
                var allDates = dtTimeSheet.AsEnumerable()//Get dates -> weekends
                      .Select(
                         s => s.Field<string>("Week Ending")
                      ).ToList();
                List<string> dates;
                List<string> defaulters;
                if (System.DateTime.Now <= targetDate)
                {
                    dates = allDates.Where(x => (System.DateTime.Parse(x.ToString()) <= DateTime.Now.Subtract(new TimeSpan((int)DateTime.Now.DayOfWeek, 0, 0, 0)).AddDays(7))).Distinct().ToList();
                    defaulters = dtTimeSheet.AsEnumerable().Where(y => (System.DateTime.Parse(y.Field<string>("Week Ending")) <= DateTime.Now.Subtract(new TimeSpan((int)DateTime.Now.DayOfWeek, 0, 0, 0)).AddDays(7)))//Get Defaulters
                  .Select(s =>s.Field<string>("Employee Name")
                  ).Distinct().ToList();
                }
                else
                {
                    dates = allDates.Distinct().ToList();
                    defaulters = dtTimeSheet.AsEnumerable()
                  .Select(s =>

                      s.Field<string>("Employee Name")
                  ).Distinct().ToList();
                }
                
                DataTable dtReport = new DataTable("ReportTable");//Prepare report table format
                dtReport.Columns.Add("Employee Name");
                foreach (var thisdate in dates)
                {
                    dtReport.Columns.Add(thisdate);
                }
                dtReport.Columns.Add("Grand Total");
                foreach (var defaulter in defaulters)//Populate report table
                {
                    int i = 0;
                    dtReport.Rows.Add();
                    DataView dataView = new DataView(dtTimeSheet);
                    string nameFilter = "[Employee Name]='" + defaulter + "'";
                    dataView.RowFilter = nameFilter;
                    dtReport.Rows[dtReport.Rows.Count - 1]["Employee Name"] = dataView.ToTable().Rows[0]["Employee Name"];
                    var thisDefaulterEmail = dataView.ToTable().Rows[0]["Employee Email"].ToString();
                    if (toList.Where(c => c == thisDefaulterEmail).Select(c => c).Any() == false)
                    {
                        toList.Add(thisDefaulterEmail);
                    }
                    foreach (var date in dates)
                    {
                        dataView.RowFilter = nameFilter + " AND [Week Ending]='" + date + "'";
                        if (dataView.Count > 0)
                        {
                            dtReport.Rows[dtReport.Rows.Count - 1][date] = 1;
                            i++;
                        }
                    }
                    dtReport.Rows[dtReport.Rows.Count - 1]["Grand Total"] = i.ToString();
                }
                DataView final = new DataView(dtReport);//Sort as per display name.
                final.Sort = "[Grand Total] desc";
                dtReport = final.ToTable();
                final.Dispose();

                Email.SendEmailForTimeSheet(subject, string.Join(";", toList), ccList, dtReport);//Send mail
            }
            catch (FormatException ex)
            {
                Log.write(string.Format("{0} is not in the correct format.", deadlineDate) + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"+ex.Message), null, logPath);
            }
            return true;

        }
    }
}