﻿using SCA.Automation.Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCA.Automation.Schedular.TimeSheet
{
	class Program
	{
		static void Main(string[] args)
		{
			string logPath = ConfigurationManager.AppSettings["Log"].ToString();
			try
			{
				Log.write("Job started at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);
				var result = Helper.Process(logPath);
				if (result)
					Log.write("Job successfully ends at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);
				else
					Log.write("Job failed at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);
				Log.write("========================================================================", null, logPath);
			}
			catch (Exception ex)
			{
				Log.write("ERROR: ", ex, logPath);
			}
		}
	}
}
