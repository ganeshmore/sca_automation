/##############################################################################################################/
					README - Timesheet

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - To send the pending timesheet notification by email.
	* Developed By: Anupam Nayek
	* Execution Frequency: 3 times/week
	* Input for data: Excel sheet which contains pending timesheets.
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
**Pending timesheet email is sent on every thursday to the employees those who have not submitted the timecards.


----------------------------------------------------------------------------------------------------------------
File Path:
----------------------------------------------------------------------------------------------------------------
** PMO should take pending timesheet records from capgemini timecard application and keep this file at location
  mentioned in filepath of Schedular Configuration file.

----------------------------------------------------------------------------------------------------------------
Supervisor List:
----------------------------------------------------------------------------------------------------------------
** Supervisor list is used to filter the pending timesheets of SCA project from pending timesheets of SOGETI.
** If there are any changes in supervisor names in project,it should be added/removed in supervisor.txt file
** Path of this file is mentioned in the config file.

----------------------------------------------------------------------------------------------------------------
Distribution List for emails:
----------------------------------------------------------------------------------------------------------------
** "CC" list is metioned in the config file of schedular.
** "To" list will be the list of employees who have not submitted or pending timesheets.

----------------------------------------------------------------------------------------------------------------
Logs:
----------------------------------------------------------------------------------------------------------------
** Path for the Logs file is mentioned in the config file.
** This file should be monitored in case of any issue.It holds every run logging entry as well as errors, if any.
