﻿using System;
using System.IO;
using SCA.Automation.Library;
using System.Configuration;

namespace SCA.Automation.Schedular.CCBMail
{
    class Program
    {
        private static string logPath;

        static void Main(string[] args)
        {
            try
            {
             
                logPath = Path.GetFullPath(ConfigurationManager.AppSettings["Log"].ToString());
                //string logPath = @"D:\ganesh\sca_internal\Logs\Log.txt";
                Log.write("/*****************CCB Mail triggered starts at " + DateTime.Now.ToString() + "*****************/", null, logPath);

                CCBMailHelper.logPath = logPath;
                CCBMailHelper.ExecuteCCBReminders();

                Log.write("/*****************   CCB Mail triggered ends at " + DateTime.Now.ToString() + "*****************/", null, logPath);
            }
            catch (Exception ex)
            {
                Log.write("/*****************  CCB Mail got exception at " + DateTime.Now.ToString() + "*****************/ /n " + ex, null, logPath);
            }
        }
    }
}
