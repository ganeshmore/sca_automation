﻿/##############################################################################################################/
					README - CCB Scheduler

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - To send the notification emails for the CCB activity based on CCB process.
	* Developed By: Ganesh More
	* Execution Frequency: Approx. 3 times/week
	* Input for data: As per SDL TRIDION CCB Process mail template
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
•	CCB activity schedule Weekly.
•	Schedule for CCB activity is as per given in CCB Process.
•	As per schedule for CCB, Mail alert need to be send for list of recipients and required authorities
   for CCB maintainace.
•	Mail alert are based on below Email-template which include Date and timeline for CCB activity as given in schedule. 

----------------------------------------------------------------------------------------------------------------
CCB Process Schedule::
----------------------------------------------------------------------------------------------------------------
•	CCB Request      -- To send mail alert to accept CCB request for that week i.e on Wednesday 
•	CCB Publish      -- To send mail alert to publish all CCB request gather/receive from all the stakeholders for that week i.e on Friday
•	Deployment Calender Publish -- To send mail alert to publish status of all CCB request receive from all teams for that week i.e on Monday 

----------------------------------------------------------------------------------------------------------------
Mandatory Folder structure:
----------------------------------------------------------------------------------------------------------------
	1.Templates:
		a.	Should be having HTML email templates 
		b.	Should be having To and CC distribution files
		
	2.Logs:
		Should be present, wihin it logs would be generated.

----------------------------------------------------------------------------------------------------------------
Templates used for sending emails:
----------------------------------------------------------------------------------------------------------------
1.	CCBMailTemplate.html
2.	CCBPublishMailTemplate.html
3. 	DeploymentCalenderPublishEmailTemplate.html

** The above 3 templates are used for sending the emails fr those respective CCB Schedulers.
** If any chnages need to be performed in email content, then the respective HTML template should be updated.
** These HTML files have the placedholders in curly braces (for example, {0}, {1}, {2}) do not replace those
   placeholders while updating any of the HTML template. These placeholders are used for CCB dates.
  
----------------------------------------------------------------------------------------------------------------
Subject Lines for CCB emails:
----------------------------------------------------------------------------------------------------------------
** Email Subject lines for CCB emails to be sent for each mail scheduler are specified in the config file.
** Any changes in email subject lines will need config update.
** These email subject lines have the placedholders in curly braces(for example, {0}, {1}) do not replace those
   placeholders while updating any of subject lines in config files. These placeholders are used for CCB
   dates.

----------------------------------------------------------------------------------------------------------------
Distribution List for emails:
----------------------------------------------------------------------------------------------------------------
** Currently 2 distribution lists are used for patching namely TO and CC lists.
** ToDistributionList.txt files holds emails ids of TO recipients
** CCDistributionList.txt files holds emails ids of CC recipients

In case of any new addition of email id, it should be done on new line of respective distribution list file.
----------------------------------------------------------------------------------------------------------------
Logs:
----------------------------------------------------------------------------------------------------------------

Logs.txt file generated in /Logs folder could be used to monitor every execution of this schedular. this file
should be monitored in case of any issue,it shall be holding every run logging entry as well as errors, if any.
