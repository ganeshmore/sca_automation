﻿using System;
using SCA.Automation.Library;

namespace SCA.Automation.Schedular.NugetPatching
{
    class NugetPatchingHelper
    {
        public static string logPath;
        public static void ExecuteNugetPatchingReminders()
        {
            NugetPatchingData objPatchingData = new NugetPatchingData();
            objPatchingData.logPath = logPath;
            objPatchingData.Process();   
        }
    }
}
