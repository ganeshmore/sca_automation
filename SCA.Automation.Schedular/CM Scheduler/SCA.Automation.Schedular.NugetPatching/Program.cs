﻿using SCA.Automation.Library;
using System;
using System.IO;
using System.Configuration;

namespace SCA.Automation.Schedular.NugetPatching
{
    class Program
    {
        static void Main(string[] args)
        {
            string logPath = Path.GetFullPath(ConfigurationManager.AppSettings["Log"].ToString());
            Log.write("/*****************MS Patching Scheduler execution starts at " + DateTime.Now.ToString() + "*****************/", null, logPath);
            
            NugetPatchingHelper.logPath = logPath;
            NugetPatchingHelper.ExecuteNugetPatchingReminders();
            
            Log.write("/*****************MS Patching Scheduler execution ends at " + DateTime.Now.ToString() + "*****************/", null, logPath);
        }
    }
}
