﻿/##############################################################################################################/
					README - Nuget Patching Scheduler

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - To send the notification emails for the Nuget Patching activity based on Nuget Patching process.
	* Developed By: Ganesh More
	* Execution Frequency: Approx. once in a month
	* Input for data: As per SDL TRIDION Nuget Patching Process mail template
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
•	Nuget Patching activity schedule Weekly.
•	Schedule for Nuget Patching activity is as per given in Nuget Patching Process.
•	As per schedule for Nuget Patching, Mail alert need to be send for list of recipients and required authorities
   for Nuget Patching maintainace.
•	Mail alert are based on below Email-template which include Date and timeline for Nuget Patching activity as given in schedule. 

----------------------------------------------------------------------------------------------------------------
Nuget Patching Process Schedule::
----------------------------------------------------------------------------------------------------------------
•	Nuget Patching Request      -- To send mail alert to accept Nuget Patching request for that week i.e on Friday of 1st week of month

----------------------------------------------------------------------------------------------------------------
Mandatory Folder structure:
----------------------------------------------------------------------------------------------------------------
	1.Templates:
		a.	Should be having HTML email templates 
		b.	Should be having To and CC distribution files
		
	2.Logs:
		Should be present, wihin it logs would be generated.

----------------------------------------------------------------------------------------------------------------
Templates used for sending emails:
----------------------------------------------------------------------------------------------------------------
1.	NugetPatchingMailTemplate.html


** The above template are used for sending the emails fr those respective Nuget Patching Schedulers.
** If any chnages need to be performed in email content, then the respective HTML template should be updated.
** These HTML files have the placedholders in curly braces (for example, {0}, {1}, {2}) do not replace those
   placeholders while updating any of the HTML template. These placeholders are used for Nuget Patching dates.
  
----------------------------------------------------------------------------------------------------------------
Subject Lines for Nuget Patching emails:
----------------------------------------------------------------------------------------------------------------
** Email Subject lines for Nuget Patching emails to be sent for each mail scheduler are specified in the config file.
** Any changes in email subject lines will need config update.
** These email subject lines have the placedholders in curly braces(for example, {0}, {1}) do not replace those
   placeholders while updating any of subject lines in config files. These placeholders are used for Nuget Patching
   dates.

----------------------------------------------------------------------------------------------------------------
Distribution List for emails:
----------------------------------------------------------------------------------------------------------------
** Currently 2 distribution lists are used for patching namely TO and CC lists.
** ToDistributionList.txt files holds emails ids of TO recipients
** CCDistributionList.txt files holds emails ids of CC recipients

In case of any new addition of email id, it should be done on new line of respective distribution list file.
----------------------------------------------------------------------------------------------------------------
Logs:
----------------------------------------------------------------------------------------------------------------

Logs.txt file generated in /Logs folder could be used to monitor every execution of this schedular. this file
should be monitored in case of any issue,it shall be holding every run logging entry as well as errors, if any.
