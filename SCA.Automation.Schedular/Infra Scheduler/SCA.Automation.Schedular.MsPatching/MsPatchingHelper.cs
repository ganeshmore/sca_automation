﻿using System;
using System.Linq;
using System.IO;
using System.Configuration;
using SCA.Automation.Library;

namespace SCA.Automation.Schedular.MsPatching
{
    public class MsPatchingHelper
    {
        public static string logPath;
        public static void ExecuteMSPatchingReminders(string conStr, string directory)
        {            
            string[] files=null;
            string connectionString = string.Empty;
            
            if (Directory.Exists(directory))
            {
                //Fetches Patching calendar for current Year 
                try
                {
                    files = Directory.GetFiles(directory, @"Patching calender-" + DateTime.Now.Year + ".xlsx", SearchOption.TopDirectoryOnly);
                }
                catch(FileNotFoundException) 
                {
                    Log.write("Patching calendar for thecurrent year " + DateTime.Now.Year + "doesnt exist in the directory " + directory,null, logPath);
                }
                catch (UnauthorizedAccessException)
                {
                    Log.write("UnauthorizedAccessException occured while accessing Patching calendar in the directory " + directory, null, logPath);
                }
                catch (PathTooLongException)
                {
                    Log.write("PathTooLongException occured while accessing Patching calendar in the directory " + directory, null, logPath);
                }
                catch (ArgumentException)
                {
                    Log.write("ArgumentException occured while accessing Patching calendar in the directory " + directory, null, logPath);
                }

                if (files != null && files.Count() == 1)
                    connectionString = string.Format(conStr, files[0]);

                if (!string.IsNullOrEmpty(connectionString))
                {
                    MsPatchingData objPatchingData = new MsPatchingData(connectionString)
                    {
                        logPath = logPath
                    };
                    objPatchingData.LoadPatchingCalendarData();
                    objPatchingData.Process();
                }
            }
            else
            {
                Log.write("Directory " + directory + "doesnt exist",null, logPath);
            }
        }
    }
}