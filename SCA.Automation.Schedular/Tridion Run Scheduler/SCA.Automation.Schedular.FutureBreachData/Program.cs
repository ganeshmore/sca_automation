﻿using SCA.Automation.Library;
using System;
using System.Configuration;

namespace SCA.Automation.Schedular.FutureBreachData
{
    class Program
    {
        static void Main(string[] args)
        {
            string logPath = ConfigurationManager.AppSettings["Log"].ToString();
            try
            {
                Log.write("========================================================================", null, logPath);
                Log.write("Job started at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);

                string conStr = ConfigurationManager.AppSettings["XlsxConn"].ToString();
                string directory = ConfigurationManager.AppSettings["FilePath"].ToString();                
                string archiveDir = ConfigurationManager.AppSettings["ArchivePath"].ToString();


                Helper.ExecuteExcelFile(conStr, directory);

                Helper.ArchiveFile(directory, archiveDir);

                Log.write("Job ends at: " + System.DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss"), null, logPath);
                Log.write("========================================================================", null, logPath);
            }
            catch (Exception ex)
            {
                Log.write("ERROR: ", ex, logPath);
            }
        }
    }
}
