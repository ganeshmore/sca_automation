﻿/##############################################################################################################/
					README - RedisCheck Powershell Script

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - Powershell which automatically check the Redis Cache of every catalog of each Solution with the last refreshed date and generate the report accordingly. Email Notification is also integrated in the script.
	* Developed By: Gursewak Singh
	* Execution Frequency: for every monday of week
	* Pre-requisite for Script:
		1.	2 Text file (one for URL & one for Email). - NOT REQUIRED
		2.	The Script.

	NOTE: Please make the HOST Entry of all the Util URL before running the Script. Use this Script on Test/Dev environment because SMTP PORT is not open. 
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------

* 	Open Powershell Script named as below in Powershell ISE (Administartor mode)
		WebsiteStatus

In that Script we need to pass the URL (Eg: http://uatst.cacheutil.com/tork/scauatstage_tork_za_local_en/lastrefreshed) for every catalog of every solution which is very tidies job. 
 
So, to overcome this I am going to release new version of the script (Version 2). Now no need to pass the URL and even no need to add the HOST entry. Below is the Detail how script will work.
 
Before going to start the work flow of the script, we need to understand WWH (What, Why & How).
 
--------------------------------------------------------------------------------------------------------------
*	Why we need Script ?
 --------------------------------------------------------------------------------------------------------------
		There are lot of reason of having this script, 
1.      This script will help you to  investigate PIM related issues.
2.      You can also check when the data in the Redis was updated and what data is updated, 
3.      Get the information which catalog is/are caches 
And many more…
 
-------------------------------------------------------------------------------------------------------------------
*	How Script will Work ?
-------------------------------------------------------------------------------------------------------------------
 
The Below fashion will tell you how the existing script works.
Take the URL from text file  Hit the Redis Cache Util (Which Nitin Develop)  Get the Response from Util  Convert it into Readable format  Generate the Report Solution wise (in csv)  Email notification
                
                Now the question is how the new Script works, Below are the details.
                 
 
One issue which I have noticed in the existing Script is, suppose if Nitin’s Util would down or Nitin Change the URL of his Util, then your script will not behave in an expected manner. So to overcome this, I am directly using the Redis Endpoints to get the response.
 
NOTE: Rest all functionality is same as of existing one (like logging, reporting, Email notification).
 
If you want to know more about this script or have any question/concern please feel free to buzz me.

-------------------------------------------------------------------------------------------------------------------
* 	Process Steps:
-------------------------------------------------------------------------------------------------------------------		
1.	Check the Last Refreshed Date of each particular catalog (which you mention in the text file).
2.	If Redis is not getting Refreshed today we get the notification in the Email.
3.	CSV files are generated for each solution (TORK, TENA & FEMCARE) with all catalog result.
4.	Email Subject line also change accordingly to failure or success of Redis.
5.	Logs are also locked if in case script got failed.

Below are the few more points on which we can work.
•	We have to send emails to  2 groups, one for seniors and second for application RUN team(like Femcare, tork etc).
•	If suppose error comes for Femcare then mail must go to Neeraj, his team and seniors.
•	Email must be triggered when Redis cache doesn't get updated for any catalog. 
•	We have to configure this util on any of the servers so that util will autodetected and send the report to the teams if the cache is not updated.
