﻿##############################################################################
##
## Redis Check
## Created by Gursewak Singh 
## Date : 04 June 2018
## Version : 2.0
## Email: gursewaksingh189@gmail.com 
##############################################################################
clear
$web_client = new-object system.net.webclient;

# ==============  Variable   ========================= #
$EnvRedisURL = "http://prodlvweb001.azurewebsites.net";   #PROD LV
<#
$EnvRedisURL = "http://uatredisstweb02.azurewebsites.net/";  #UAT STG
$EnvRedisURL = "http://prodlvweb001.azurewebsites.net";   #PROD LV
$EnvRedisURL = "http://prodstweb001.azurewebsites.net";   #PROD STG
$EnvRedisURL = "http://uatredisweb01.azurewebsites.net";  #UAT LV
#>
$Solution = @("tork","tpw","tc","femcare","essity","tcw");
$CurrentDir = split-path -parent $MyInvocation.MyCommand.Definition;
$LogFile = "$CurrentDir\Logs.txt";
$a = "{0:dd-MMM-yyyy}" -f (get-date);
$torkDate = "{0:yyyy-MM-dd}" -f (get-date).AddDays(-1);
$csvContentsTork = @();
$csvContentsTena = @();
$csvContentsOther = @();
$TORKerrorstat = "";
$FEMCAREerrorstat = "";
$TENAerrorstat = "";
$errorstat = "";
# ============= END Variable   ======================= #


function SendEmail($errorstatus,$TENAerror,$TORKerror,$FEMCAREerror)
{
    $emailBody = "Dear Team,`r`n" ;
    try{
    $message = new-object Net.Mail.MailMessage;
    $sendMailTo = "$CurrentDir\CCEmail.txt";
    $EmailTo = Get-Content $sendMailTo -ErrorAction SilentlyContinue
    Foreach($EmailID in $EmailTo) {
        Write-Host $EmailID;
        $message.CC.Add("$EmailID");
    }
	
    $emailBody += "Please find the Redis Cache Report in attachment `r`n `r`n";
    if($errorstatus -eq "1")
    {
        $message.Subject = "Redis Failed";
        $sendMailToAddress = "$CurrentDir\";
        if($TENAerror -eq "1")
        {
            $sendMailToAddress = "$CurrentDir\TenaEmail.txt";
            $EmailToTena = Get-Content $sendMailToAddress -ErrorAction SilentlyContinue;
            Foreach($EmailID in $EmailToTena) {
                $message.To.Add("$EmailID");
            }
            $message.Subject += " | TENA";
            $message.Attachments.Add("$CurrentDir\Result\Tena_$today.csv");
        }
        if($TORKerror -eq "1")
        {
            
            $sendMailToAddress = "$CurrentDir\TorkEmail.txt";
            $EmailToTork= Get-Content $sendMailToAddress -ErrorAction SilentlyContinue;
            Foreach($EmailID in $EmailToTork) {
                $message.To.Add("$EmailID");
            }
            $message.Subject += " | TORK";
            $message.Attachments.Add("$CurrentDir\Result\Tork_$today.csv");
        }
        if($FEMCAREerror -eq "1")
        {
            $sendMailToAddress = "$CurrentDir\FemcareEmail.txt";
            $EmailToFemcare= Get-Content $sendMailToAddress -ErrorAction SilentlyContinue;
            Foreach($EmailID in $EmailToFemcare) {
                $message.To.Add("$EmailID");
            }
            $message.Subject += " | FEMCARE";
            $message.Attachments.Add("$CurrentDir\Result\Femcare_$today.csv");
        }
    }
    else
    {
        $message.Subject = "Redis ran successfully!";
    }
	$message.From = "Redis.notification@essity.com";
    $emailBody += "Best Regards, `r`nInfra Run Team";
    $emailBody += "`r`n `r`n `r`n-----------------------------------------------------------------`r`n";
    $emailBody += "THIS EMAIL IS SYSTEM GENERATED; PLEASE DO NOT REPLY TO THIS MAIL. `r`n";
    $emailBody += "-----------------------------------------------------------------";
	#This message has been generated automatically.
    #Write-Host $emailBody;
    $message.Body = $emailBody;
	$smtp = new-object Net.Mail.SmtpClient("smtp.gmail.com", "587");
	$smtp.EnableSSL = $true;
	$smtp.Credentials = New-Object System.Net.NetworkCredential("sogeti.notification@gmail.com", "Cap@12345");
	$smtp.send($message);
    $message.Attachments.Dispose();
    }
    catch{
        Write-Host $_.Exception
        $message = "ERROR - " + $_.Exception
        $message >> $LogFile
	}
}



<#  =======================  STARTING POINT OF SCRIPT  ==============================  #>

$locklogs += "======================================="+ $a + "===================================================== `r`n `r`n";


foreach($SolutionID in $Solution)
{
    $locklogs += "START Checking Redis for $SolutionID `r`n";
    try
    {
        $RedisResponse=$web_client.DownloadString("$EnvRedisURL/$SolutionID/") | ConvertFrom-Json;
        foreach($Catalogid in $RedisResponse)
        {
            $locklogs += "KEY = $Catalogid `r`n";
            try
            {
                $RedisResponseLastUpdate = $web_client.DownloadString("$EnvRedisURL/$SolutionID/$Catalogid/lastrefreshed");#.Replace('"',''); #| ConvertFrom-Json
                $RedisResponseLastUpdate = $RedisResponseLastUpdate.Replace('"','');
                $getdate =  "{0:yyyy-MM-dd}" -f [Datetime] $RedisResponseLastUpdate.ToString();
            }
            catch
            {
                $getdate =  "{0:yyyy-MM-dd}" -f (get-date).AddDays(-2);
            }
            #Write-Host "$Catalogid =  $RedisResponseLastUpdate";
            $today = "{0:yyyy-MM-dd}" -f (get-date);
            $lastRefreshed="";
            Write-Host $Catalogid;
            if($today -eq $getdate)
            {
                Write-Host "Cached Refreshed Today";
                $locklogs += "RESULT = Refreshed Today `r`n";
                $lastRefreshed = "Today";
            }
            else
            {
                #Write-Host "Last Cached Refreshed : " $lastRefreshed;
                
                if($SolutionID.ToLower() -eq "tork" -or $SolutionID.ToLower() -eq "tc")
                {
                    if($torkDate -eq $getdate)
                    {
                        $lastRefreshed = "Today";
                    }
                    else
                    {
                        $TORKerrorstat = "1";
                        $lastRefreshed = "{0:dd-MMM-yyyy}" -f [Datetime] $getdate.ToString();
                        <# ---------------------   STORE Non-Cached Record in CSV for TORK ------------------------------ #>
                        $rowtork = New-Object System.Object; # Create an object to append to the array
                        #$row | Add-Member -MemberType NoteProperty -Solution $passvalue[3] -KeyName $passvalue[4] -LastRefreshed $lastRefreshed # create a property called User. This will be the User column
   
                        $rowtork | Add-Member -MemberType NoteProperty -Name "Solution" -Value $SolutionID; # create a property called User. This will be the User column
                        $rowtork | Add-Member -MemberType NoteProperty -Name "KeyName" -Value $Catalogid; # create a property called UserID. This will be the UserID column
                        $rowtork | Add-Member -MemberType NoteProperty -Name "LastRefreshed" -Value $lastRefreshed; # create a property called PC. This will be the PC column

                        $csvContentsTork += $rowtork; # append the new data to the array

                        $locklogs += "Store Result in CSV For TORK `r`n";
                        <# --------------------- END STORE Non-Cached Record in CSV for TORK ----------------------------- #>
                    }
                }
                else
                {
                    $errorstat = "1";
                    $lastRefreshed =  "{0:dd-MMM-yyyy}" -f [Datetime] $getdate.ToString();
                    if($SolutionID.ToLower() -eq "tcw" -or $SolutionID.ToLower() -eq "tpw")
                    {
                        $TENAerrorstat = "1";
                        <# ---------------------   STORE Non-Cached Record in CSV for TENA ------------------------------ #>
                        $rowtena = New-Object System.Object; # Create an object to append to the array
                        #$row | Add-Member -MemberType NoteProperty -Solution $passvalue[3] -KeyName $passvalue[4] -LastRefreshed $lastRefreshed # create a property called User. This will be the User column
   
                        $rowtena | Add-Member -MemberType NoteProperty -Name "Solution" -Value $SolutionID; # create a property called User. This will be the User column
                        $rowtena | Add-Member -MemberType NoteProperty -Name "KeyName" -Value $Catalogid; # create a property called UserID. This will be the UserID column
                        $rowtena | Add-Member -MemberType NoteProperty -Name "LastRefreshed" -Value $lastRefreshed; # create a property called PC. This will be the PC column

                        $csvContentsTena += $rowtena; # append the new data to the array

                        $locklogs += "Store Result in CSV For TENA `r`n";
                        <# --------------------- END STORE Non-Cached Record in CSV for TENA ---------------------------- #>
                    }
                    elseif($SolutionID.ToLower() -eq "femcare" -or $SolutionID.ToLower() -eq "essity")
                    {
                        $FEMCAREerrorstat = "1";
                        <# ---------------------   STORE Non-Cached Record in CSV for FEMCARE & ESSITY ------------------------------ #>
                        $rowother = New-Object System.Object; # Create an object to append to the array
                        #$row | Add-Member -MemberType NoteProperty -Solution $passvalue[3] -KeyName $passvalue[4] -LastRefreshed $lastRefreshed # create a property called User. This will be the User column
   
                        $rowother | Add-Member -MemberType NoteProperty -Name "Solution" -Value $SolutionID; # create a property called User. This will be the User column
                        $rowother | Add-Member -MemberType NoteProperty -Name "KeyName" -Value $Catalogid; # create a property called UserID. This will be the UserID column
                        $rowother | Add-Member -MemberType NoteProperty -Name "LastRefreshed" -Value $lastRefreshed; # create a property called PC. This will be the PC column

                        $csvContentsOther += $rowother; # append the new data to the array

                        $locklogs += "Store Result in CSV For Others `r`n";
                        <# --------------------- END STORE Non-Cached Record in CSV for FEMCARE & ESSITY ---------------------------- #>
                    }
                    
                    
                    #$lastRefreshed = "{0:dd-MMM-yyyy}" -f $RedisResponseLastUpdate.ToString();
                }
                Write-Host "Last Cached Refreshed : " $lastRefreshed;
                $locklogs += "RESULT = $lastRefreshed `r`n";
            }
            Write-Host "`r`n";
        }
    }
    catch{
        Write-Host $_.Exception;
        $locklogs += "ERROR - " + $_.Exception;
	}
    $locklogs >> $LogFile;
    $locklogs = "";
}
$locklogs += "Save TORK CSV file with name : Tork_$today.csv `r`n" ;
$csvContentsTork | Export-CSV -Path $CurrentDir\Result\Tork_$today.csv;
$locklogs += "Save TENA CSV file with name : Tena_$today.csv `r`n";
$csvContentsTena | Export-CSV -Path $CurrentDir\Result\Tena_$today.csv;
$locklogs += "Save Other CSV file with name : Femcare_$today.csv `r`n";
$csvContentsOther | Export-CSV -Path $CurrentDir\Result\Femcare_$today.csv;

$locklogs += "Send Email";
$locklogs >> $LogFile;
$locklogs = "";
if($errorstat -eq "1" -or $TORKerrorstat -eq "1")
{
    SendEmail -errorstatus $errorstat -TENAerror $TENAerrorstat -TORKerror $TORKerrorstat -FEMCAREerror $FEMCAREerrorstat;
}