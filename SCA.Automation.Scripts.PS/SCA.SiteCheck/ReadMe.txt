﻿/##############################################################################################################/
					README - Site StatusCheck Powershell Script

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - to Check status of Tridion Website whether site is reachable or not
	* Developed By: Gursewak Singh
	* Execution Frequency: for every monday of week
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------

* 	Open Powershell Script named as below in Powershell ISE (Administartor mode)
		WebsiteStatus
*	Run the Script from Powershell ISE (Administartor mode)
		it will give output in Html format. which will be easily readable from any browser.