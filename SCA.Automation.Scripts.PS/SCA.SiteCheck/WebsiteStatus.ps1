﻿##############################################################################
##
## Website Availability
## Created by Gursewak Singh 
## Date : 19 Jan 2018
## Version : 1.0
## Email: gursewaksingh189@gmail.com 
##############################################################################

function sitecheck($sitename)
{
    $time = try{
	    $request = $null
	    # Create the Web Client object
        $Client = New-Object -TypeName System.Net.WebClient
 
        # Tell it to use our default creds for the proxy
        $Client.Proxy.Credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials
        
	    $result1 = Measure-Command { $request = Invoke-WebRequest -Uri $sitename }
	    $result1.TotalMilliseconds
	} 
	catch
	{
	   <# If the request generated an exception (i.e.: 500 server
	   error or 404 not found), we can pull the status code from the
	   Exception.Response property #>
	   $request = $_.Exception.Response
	   $time = -1
	   #Write-Host "ERROR in Checking Site $sitename - "  $_.Exception.Response -ForegroundColor Red
       Write-Host "ERROR in Checking Site $sitename - "  $_.Exception.Message -ForegroundColor Red
	   #$message12 >> $LogFile1
	}  
    $result += [PSCustomObject] @{
	Time = Get-Date;
	Urires = $sitename;
	StatusCode = [int] $request.StatusCode;
	StatusDescription = $request.StatusDescription;
	ResponseLength = $request.RawContentLength;
	TimeTaken =  $time;
  }
  $message >> $LogFile
  $message = ""
  return $result
}




clear
Import-Module WebAdministration
## The URI list to test
$URLListFile = "E:\Sitecheck\URLList.txt" 
$siteculture = @()
$a = "{0:yyyy-MM-dd}" -f (get-date)
$URLList = Get-Content $URLListFile -ErrorAction SilentlyContinue

Foreach($Uri in $URLList) {
    $siteculture += sitecheck -sitename $Uri
}

#Prepare email body in HTML format
if($siteculture -ne $null)
{
    $Outputreport = "<HTML><TITLE>Website Availability Report</TITLE><BODY background-color:peachpuff><font color =""#99000"" face=""Microsoft Tai le""><H2> Website Availability Report </H2></font><Table border=1 cellpadding=0 cellspacing=0><TR bgcolor=gray align=center><TD><B>URL</B></TD><TD><B>StatusCode</B></TD><TD><B>StatusDescription</B></TD><TD><B>ResponseLength</B></TD><TD><B>TimeTaken</B></TD</TR>"
    Foreach($Entry in $siteculture)
    {
        if($Entry.StatusCode -ne "200")
        {
            $Outputreport += "<TR bgcolor=red>"
        }
        else
        {
            $Outputreport += "<TR>"
        }
        $Outputreport += "<TD>$($Entry.Urires)</TD><TD align=center>$($Entry.StatusCode)</TD><TD align=center>$($Entry.StatusDescription)</TD><TD align=center>$($Entry.ResponseLength)</TD><TD align=center>$($Entry.timetaken)</TD></TR>"
    }
    $Outputreport += "</Table></BODY></HTML>"
}

$Outputreport | out-file E:\Sitecheck\Result_$a.htm
Invoke-Expression E:\Sitecheck\Result_$a.htm  