﻿/##############################################################################################################/
					README - TCW Deployment Script

/##############################################################################################################/

###############################################################################################################
	* Project - Essity - Sogeti Sweden
	* Purpose - to perform Deployment Activity for TCW solution
	* Developed By: Gursewak Singh
	* Execution Frequency: for every UAT/PROD Deployment
	* Input for data: List of site URL's on which deplouyment need to be done and Deployment Package(Config,Binaries)
	
###############################################################################################################
----------------------------------------------------------------------------------------------------------------
Process decription:
----------------------------------------------------------------------------------------------------------------
•	There are two txt file maintain in repository named as 
		URLList - Copy  --Contains all list of site url's of TCW
		URLList 	--Need to mention list of site url's where deployment has to be done.
* 	Open Powershell Script named as below in Powershell ISE (Administartor mode)
		TCWDeployment
*	Run the Script from Powershell ISE (Administartor mode)
		it will asked user input(Yes/No) like below queries:
			1.Do you want to take back Up
			2.Do you want to Deploy web.config
			3.Do you want to Deploy Package