﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressSelectionStep.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.AddressSelectionStep" %>
<asp:HiddenField ID="hdnAddressFromString" ClientIDMode="Static" runat="server" />
<script type="text/javascript">
    var currentStepNo = '<%=currentStepNo %>';
    var currentStoryType = '<%=StoryType %>';
    //Modified by Remaining target group | Added variable to check the second last step 
    var isSecondLastStep = '<%=IsSecondLastStep %>';

    document.addEventListener("keydown", keyDownTextField, false);

    function keyDownTextField(e) {
        var keyCode = e.keyCode;
        if (keyCode == 13) {
            var btnSubmit = document.getElementById("ContentPlaceHolder1_ucAddressStep2_btnAddress");
            btnSubmit.click();
        } else {
            return false;
        }
    }
</script>
<!-- Modified by Remaining target group | Added hidden field to store the value of selected size. -->
<asp:HiddenField ID="hdnSelectedSizeValue" EnableViewState="true" ClientIDMode="Static" runat="server" />
<% if (IsActiveStep == true)
   { %>
<input type="hidden" id="currenet-step-no" value="<%=currentStepNo %>/<%= totalStepCount %>" />
<% if (isFirst)
   { %>
<!-- start component: btn -->
<div class="component component-btn">
    <a href="<%= Request.Url.ToString() %>" id="btnPrevious" class="btn btn-transparent btn-no-icon align-left vspace-xs-xs" runat="server" onserverclick="btnPrevious_ServerClick">
        <span><%=SamplePreviousBtnText%></span>
        <i class="icon-button-right"></i>
    </a>
</div>
<!-- end component: btn -->
<%} %>
<button type="submit" runat="server" class="btn btn-no-icon field-submit" onclick="if (!ValidatePageItems()) return false;" id="btnAddress" OnServerClick="SubmitAddressForm" data-sampleorcoupon="yes">
    <span><%=SubmitBtnText%></span>
</button>
<%} %>
