﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AskDoctor.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.AskDoctor" %>

<script type="text/javascript">

    $(document).ready(function () {
        //$("#btnSendMail").click(function () {
        //    //getPageItems();
        //    return false;

        //});

    });
    
    function getPageItems() {
      
        var msg = $('#AskDoctor_txtMessage').val()
        var Mail = $('#AskDoctor_txtEmail').val()
        var Param1 = msg;
        var Param2 = Mail;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=UTF-8;",
            url: 'SendMail.aspx/SendMail',
            dataType: "json",
            async: false,
            data:    JSON.stringify({ Message:  Param1  ,Email:Param2 }), //"{ Message: '" + Param1 + "',Email: '" + Param2 + "'}",           
            success: function (data) {
               
                if (data.d = 'true') {
                   
                    $("#Confirmationmessage").css("display", "block")[0].innerText = $('.msgText').html();
                   // alert($('.msgText').html());
                    $("#form").css('display', 'none')[0].innerHTML = "";                  
                     
                }
                else {
                    alert('45785');
                }
            },
            error: function (result) {
                debugger;
                alert("error");
                //alert("Error occured" + result.error);
            }
        });
    }

    </script>

<div id="Confirmationmessage" class="Confirmationmessage" style="display:none">
   
</div>

<div   class="form" id="form">

    <div class="row vspace-sm">
        <div class="col-12">
            <strong class="form-required">*Required fields</strong>
        </div>
    </div>

    <div class="row">
        <div class="form-field  col-12 validate-required form-field-textarea">
            <label class="field-label" for="form-1-0">
                Your message*												
            </label>
            <textarea class="field field-textarea" runat="server" name="message" id="txtMessage" rows="3"></textarea>


            <div class="validate-message validate-message-required">
                This field can not be empty				
            </div>
        </div>
    </div>
    <div class="row  validate- form-field-input">
        <div class="form-field  col-12 validate-required validate-email form-field-input">

            <label class="field-label" for="form-1-2">
                Your email*												
            </label>
            <input class="field field-input" runat="server" type="text" name="email" id="txtEmail">
            <div class="validate-message validate-message-required">
                This field can not be empty				
            </div>
            <div class="validate-message validate-message-email">
                This is not a valid email address				
            </div>
            <div class="field-description text-content">
                We will not share or release your private details.			
            </div>

        </div>

    </div>
    <div class="row  validate- form-field-input">

        <div class="form-field  col-12 validate- form-field-submit">
            
            <asp:Button ID="btnSendMail" class="btn btn-no-icon field-submit"  runat="server" Text="Send" OnClick="Button1_Click" />
        </div>

    </div>

</div>
