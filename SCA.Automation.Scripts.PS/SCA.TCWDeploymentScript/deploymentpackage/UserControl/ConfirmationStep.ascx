﻿<%@ control language="C#" autoeventwireup="true" codebehind="ConfirmationStep.ascx.cs" inherits="SCA.TCW.Men.Web.UserControl.ConfirmationStep" %>
<%@ import namespace="SCA.TCW.Men.Utility" %>
<%@ import namespace="SCA.TCW.Men.Data" %>
<script type="text/javascript">
    var currentStepNo = '<%=currentStepNo %>';
    var currentStoryType = '<%=StoryType %>';
    var selectedItemName = '<%= itemSampleProducts.LongName %>'
</script>
<script type="text/javascript"
    src="https://static.powerreviews.com/t/v1/tracker.js"></script>

<asp:HiddenField ID="hdnSampLostStatus" EnableViewState="true" ClientIDMode="Static" runat="server" />
<script type="text/javascript">
    var hdnSampLostStatusVal = document.getElementById('hdnSampLostStatus').value;
    if (hdnSampLostStatusVal == "1") {
        document.getElementById("ContentPlaceHolder1_divConfirmationSection").style.display = "block";
        document.getElementById("ContentPlaceHolder1_divSampleNotSaved").style.display = "none";
    }
    else {
        document.getElementById("ContentPlaceHolder1_divConfirmationSection").style.display = "none";
        document.getElementById("ContentPlaceHolder1_divSampleNotSaved").style.display = "block";
    }
</script>
<% if (IsActiveStep == true && itemSampleProducts != null)
    { %>
<% if ((StoryType.ToLower()) == "coupon" && (!string.IsNullOrEmpty(Convert.ToString(itemSampleProducts.KitId))))
    { %>
<% if (!string.IsNullOrEmpty(itemSampleProducts.RebateCode) && isQutaMeet == true)
    { %>
<div class="row vspace-sm" data-confirmation="yes" data-sampleorcoupon="yes">
    <h2><%= StepConfirmationText %><%=itemSampleProducts.RebateCode %></h2>
</div>
<% } %>
<div class="row vspace-sm">
    <div class="crate manual-padding small-padding crate-white">
        <div class="crate-content">
            <div class="row">
                <div class="col-12">
                    <div class="vspace-sm">
                        <!-- start component: event-carousel -->
                        <div class="caption-image image-container-fluid">
                            <img src="<%= ((!string.IsNullOrEmpty(itemSampleProducts.BigAssetUrl)) ? itemSampleProducts.BigAssetUrl : string.Empty) %>" alt="<%= itemSampleProducts.BigAssetUrlAltText %>">
                        </div>
                        <!-- end component: event-carousel -->
                    </div>
                </div>
                <div>
                    <div class="col-7 col-xs-12  vspace-sm-sm">
                        <div class="text-content"></div>
                    </div>
                    <% if (isQutaMeet == true && string.IsNullOrEmpty(itemSampleProducts.RebateCode))
                        { %>
                    <div class="col-4 col-push-1 col-xs-push-0 col-xs-12  vspace-sm-sm">
                        <div class="text-content">
                            <div class="align-text-right">
                                <a href="<%= ((!string.IsNullOrEmpty(itemSampleProducts.DownloadableAssetUrl)) ? itemSampleProducts.DownloadableAssetUrl : string.Empty) %>" class="btn btn-color btn-full vspace-xs btn-no-icon js-print-img"><%=CouponPrintBtnText%></a>
                                <a href="<%= ((!string.IsNullOrEmpty(itemSampleProducts.DownloadableAssetUrl)) ? itemSampleProducts.DownloadableAssetUrl : string.Empty) %>" class="btn btn-color btn-full btn-no-icon" target="_blank" download=""><%=CouponDownloadBtnText%></a>
                            </div>
                        </div>
                    </div>
                    <%} %>
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>
<% else
    { %>
<% if (isQutaMeet == true)
    { %>
<%if ((!string.IsNullOrEmpty(itemSampleProducts.RebateCode)) && (StoryType.ToLower() == "coupon"))
    { %>
<div class="row vspace-sm" data-confirmation="yes" data-sampleorcoupon="yes">
    <h2><%= StepConfirmationText %><%= itemSampleProducts.RebateCode %></h2>
</div>
<%}
    else if ((StoryType.ToLower() == "sample"))
    { %>
<div class="row vspace-sm" data-confirmation="yes" data-sampleorcoupon="yes">
    <h2><%= StepConfirmationText %><%= randSampleConfirmationNumber %></h2>
</div>
<%} %>
<div class="row vspace-sm">
    <div class="crate manual-padding small-padding crate-white">
        <div class="crate-content">
            <div class="row">
                <div class="col-12">
                    <!-- start component: product-box -->
                    <div class="component component-product-box">
                        <div class="product-box product-compact no-border vspace-none">
                            <div class="product-box-content align-height">
                                <div class="product-box-img">
                                    <img src="<%= itemSampleProducts.AssetUrl %>" alt="<%= itemSampleProducts.AssetUrlAltText %>">
                                </div>
                                <div class="product-box-text">
                                    <h3 class="title">
                                        <%= StepSelectedText %>
                                        <br />
                                        <br />
                                        <%= itemSampleProducts.LongName %></h3>
                                    <div class="text">
                                        <p><%= itemSampleProducts.Descripton %></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end component: product-box -->
                    <%=Helper.GeneratePowerReviewHtml("div" + itemSampleProducts.ArticleNumber + "sample", itemSampleProducts.ArticleNumber, ReviewSnippetType.CheckoutBeacon ,  ReviewData) %>
                </div>
            </div>
        </div>
    </div>
</div>
<% } %>
<%} %>
<%} %>
