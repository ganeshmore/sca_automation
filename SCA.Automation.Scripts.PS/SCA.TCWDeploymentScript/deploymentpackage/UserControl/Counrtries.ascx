﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Counrtries.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Counrtries" %>
<%@ OutputCache Duration="120" VaryByParam="none" %>
<%@ Import Namespace="System.Web.Services.Description" %>
<!-- Code testing Started -->
<div class="hybrid-selector">
    <div class="country-selector">
        <span class="title"><%= CountryName %></span>
        <div class="location has-submenu">
            <a href=""><%= ChangeText %><i class="icon icon-nav-arrow-down"></i></a>
            <div class="submenu submenu-bg-world-map sticky">
                <div class="container container-fluid">
                    <div class="row submenu-content">
                        <%-- <div class="column col-4">--%>
                        <% var data = itemvalue.Category.Keyword;
                           int indexcountpercol = (totalCount / 3);
                           int currentcount = 0;
                        %>

                        <%foreach (var dataValue in data)
                          {
                              if ((currentcount % indexcountpercol) == 0)
                              {%>
                        <!--first div started-->
                        <div class="column col-4">
                            <%} currentcount++; %>
                            <%if (currentcount != indexcountpercol) // added this condition 
                              { %>
                            <div class="submenu-title"><%= dataValue.Description.ToString()%></div>
                            <%} %>
                            <ul>
                                <% foreach (var dataValuechild in dataValue.ChildKeywords)
                                   {
                                       string languane = "";
                                       string link = ""; %>
                                <%  if (((currentcount % indexcountpercol) == 0) && (totalCount - currentcount) > 3)
                                    {%>
                                <!--ul closed-->
                            </ul>
                            <!--div closed-->
                        </div>
                        <%} %>
                        <%  if (((currentcount % indexcountpercol) == 0) && (totalCount - currentcount) > 3)
                            { %>
                        <!--div started-->
                        <div class="column col-4">
                            <!--ul Started-->
                            <ul>
                                <%} currentcount++; %>

                                <% foreach (var meta in dataValuechild.Meta)
                                   {
                                       if (dataValuechild.Meta.Count > 1)
                                       {
                                           languane = " (" + Convert.ToString(meta.language) + ")";
                                           link = Convert.ToString(meta.link);
                                       }
                                       else
                                       {
                                           languane = "";
                                           link = Convert.ToString(meta.link);
                                       } %>
                                <li><a href="<%=link%>"><%=dataValuechild.Description.ToString() + languane %> </a></li>
                                <%}%>
                                <%} %>
                            </ul>
                            <%  if ((currentcount % indexcountpercol) == 0)
                                {%>
                        </div>
                        <%} %>
                        <%} %>
                        <%-- </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    &nbsp;
    <% if (languageList.Count > 1)
       {%>
    <div class="language-selector">
        <span class="title"><%= LanguageText %></span>
        <div class="languages">
            <% Int32 count = 0;
                foreach (var language in languageList)
                {
                    count++;
                    if (language.IsActive == true)
                    {%>
            <a href="<%=language.url%>" class="active"><%=language.Language%></a>&nbsp;|
                    <%}
                     else if (count != languageList.Count)
                     {%>
            <a href="<%=language.url%>"><%=language.Language%></a>&nbsp;|
                    <%}
                     else
                     {%>
                        &nbsp;<a href="<%=language.url%>"><%=language.Language%></a>
            <%}
                 }%>
        </div>
    </div>
    <% } %>
</div>
<% if ((totalCount % 3) != 0)
   { %>
</div>
<%} %>

<!-- Code testing Ended -->



