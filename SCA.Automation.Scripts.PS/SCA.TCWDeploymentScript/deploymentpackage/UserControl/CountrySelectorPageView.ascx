﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountrySelectorPageView.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.CountrySelectorPageView" %>
<div class="com-background">
<div class="com-country-selector section">
    <div class="container">
        <div class="country-list">
            <!-- Proceed only if category is not null and it it has child keywords -->
            <% if (ItemValue != null && ItemValue.Category != null && ItemValue.Category.Keyword != null)
               {
                   foreach (var dataValue in ItemValue.Category.Keyword)
                   { %>
            <ul>
                <li><strong><%= dataValue.Description.ToString()%></strong></li>
                <% if (dataValue.ChildKeywords != null)
                   {
                       foreach (var dataValuechild in dataValue.ChildKeywords)
                       {
                           if (dataValuechild.Meta != null)
                           {
                               string countryName = Convert.ToString(dataValuechild.Description);
                               foreach (var meta in dataValuechild.Meta)
                               {
                                   string link = Convert.ToString(meta.link);
                                   string language = string.Empty;
                                   string countryLocalName = String.Empty;
                                   if (dataValuechild.Meta.Count > 1)
                                   {
                                       language = " (" + Convert.ToString(meta.language) + ")";
                                   }
                                   countryLocalName = Convert.ToString(meta.countryname);
                %>

                <li><a href="<%= link %>"><%= ((string.IsNullOrEmpty(countryLocalName)) || countryName.ToLower()==countryLocalName.ToLower())?countryName + " "  +  language:countryName +" | "+ countryLocalName +" "+ language %></a></li>
                <%  }
                           }
                       }
                   }
                %>
            </ul>


            <%  }
               }
            %>
        </div>
    </div>
</div>
</div>
