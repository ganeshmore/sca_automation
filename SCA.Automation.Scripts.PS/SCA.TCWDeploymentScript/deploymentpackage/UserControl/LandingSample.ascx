﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LandingSample.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.LandingSample" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<%@ Import Namespace="SCA.TCW.Men.Data" %>

<script type="text/javascript"
    src="https://static.powerreviews.com/t/v1/tracker.js"></script>

<asp:HiddenField runat="server" ID="hdnFormData" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdnTimeStamp" />

<div class="form-field align-text-right contact-inner-button">
    <button type="submit" runat="server" class="btn btn-no-icon field-submit" onclick="if (!ValidateSamplePage()) return false;" id="sampleSubmitBtn" OnServerClick="SampleSubmit_Click" data-sampleorcoupon="yes" >
	<span><%= SubmitButtonText %></span>
	</button>
</div>
<% if (CheckOutBeaconNeeded)
    { %>
<%=Helper.GeneratePowerReviewHtml("div" + SelectedArticleNumber + "sample", SelectedArticleNumber, ReviewSnippetType.CheckoutBeacon ,  ReviewData) %>
<%} %>