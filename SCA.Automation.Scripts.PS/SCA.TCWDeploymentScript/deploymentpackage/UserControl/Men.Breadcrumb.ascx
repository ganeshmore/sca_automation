﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Men.Breadcrumb.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Men_Breadcrumb" %>

<%@ Import Namespace="System.Web.Services.Description" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<!-- Code added as part of remaining target group. -->
<!-- This ifcondition is added, and it shows megamenu only if content author has not opted to hide it. -->

<% if (string.IsNullOrEmpty(IsMegaMenuRequired) || IsMegaMenuRequired.ToLower() != "no")
    { %>
<div class="header-menu">
    <div class="menu-wrapper">
        <div class="container">
            <div class="row">
                <div id="megamenu" runat="server" class="col-12">


                    <% string pageSubHeading = string.Empty; %>
                    <% List<SCA.TCW.Men.Data.ItemsItemItem> SecondLevel = objProducts.GetOrderListOfLevel1(itemvalue.item); %>
                    <%if (SecondLevel.Count > 0)
                        { %>
                    <ul class="menu align-left">
                        <% foreach (SCA.TCW.Men.Data.ItemsItemItem SecondLevelItems in SecondLevel)
                            {
                        %>
                        <% maxDtpercol = (SecondLevelItems.maxrowlimit > 0) ? SecondLevelItems.maxrowlimit : appSettingDtpercol; %>
                        <%string secondLevelUrlString = string.Empty; %>
                        <li class="menu-item has-submenu">
                            <%if (!string.IsNullOrEmpty(SecondLevelItems.LongURLUsingPageNameOverride))
                                { %>
                            <% secondLevelUrlString = SecondLevelItems.LongURLUsingPageNameOverride; %>
                            <%}
                                else
                                {%>
                            <% secondLevelUrlString = SecondLevelItems.url;
                                secondLevelUrlString = (String.Equals(SecondLevelItems.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? secondLevelUrlString + "/" : secondLevelUrlString; %>
                            <%}
                            %>
                            <a href="<%=secondLevelUrlString%>"><%= (!string.IsNullOrWhiteSpace(SecondLevelItems.overriddentitle))?SecondLevelItems.overriddentitle: SecondLevelItems.displaytitle %></a>
                            <div class="submenu">
                                <div class="container container-fluid">
                                    <div class="row submenu-content">
                                        <div class="col-12 submenu-header">
                                                <a href="<%= secondLevelUrlString%>" class="submenu-header-title"><%= (!string.IsNullOrWhiteSpace(SecondLevelItems.headingoverriddentitle))?SecondLevelItems.headingoverriddentitle: SecondLevelItems.displaytitle%></a>															</a>
                                        </div>
                                        <% pageSubHeading = GetPageSubHeadingLevel2(SecondLevelItems); %>
                                        <%if (!string.IsNullOrEmpty(SecondLevelItems.firstlevelsubheading) && !string.IsNullOrEmpty(pageSubHeading))
                                            {
                                                if (SecondLevelItems.firstlevelsubheading.Equals("yes", StringComparison.OrdinalIgnoreCase))
                                                {%>
                                        <div class="col-12 submenu-openText">
                                            <div><a href="<%= secondLevelUrlString%>"><%= pageSubHeading %></a></div>
                                        </div>
                                        <%} %>
                                        <%} %>

                                        <% var curCounter = 0;%>
                                        <%if (GetCountOfLevel2(SecondLevelItems) > 0 || string.Equals(SecondLevelItems.productdetailsreq, "yes", StringComparison.OrdinalIgnoreCase))
                                            {  %>
                                        <div class="column col-4 first">
                                            <div class="column-content">
                                                <% List<SCA.TCW.Men.Data.ItemsItemItemItem> ThirdLevelMenuItems=  objProducts.GetProductsMenuList(SecondLevelItems, productFamilies);%>
                                                <% foreach (SCA.TCW.Men.Data.ItemsItemItemItem thirldLevelItem in ThirdLevelMenuItems)
                                                    {
                                                        int totalcountPerItem = GetCountOfLevel3(thirldLevelItem) + 1;
                                                        int totaldata = totalcountPerItem + curCounter;
                                                %>
                                                <!--Con1-->
                                                <% if (totaldata <= maxDtpercol)
                                                    {
                                                        curCounter++;
                                                        string thirldLevelUrlString = string.Empty; %>
                                                <%if (!string.IsNullOrEmpty(thirldLevelItem.LongURLUsingPageNameOverride))
                                                    { %>
                                                <%  thirldLevelUrlString = thirldLevelItem.LongURLUsingPageNameOverride; %>
                                                <%}
                                                    else
                                                    {%>
                                                <%  thirldLevelUrlString = thirldLevelItem.url;
                                                    thirldLevelUrlString = (String.Equals(thirldLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? thirldLevelUrlString + "/" : thirldLevelUrlString; %>
                                                <%}
                                                %>
                                                <% if (!string.IsNullOrEmpty(thirldLevelUrlString))
                                                    { %>
                                                <div class="submenu-title">
                                                    <a href="<%= thirldLevelUrlString %>"><%= (!string.IsNullOrWhiteSpace(thirldLevelItem.overriddentitle))?thirldLevelItem.overriddentitle: thirldLevelItem.displaytitle %></a>
                                                </div>
                                                <% } %>
                                                <% pageSubHeading = GetPageSubHeadingLevel3(thirldLevelItem); %>
                                                <%if (!string.IsNullOrEmpty(SecondLevelItems.secondlevelsubheading) && !string.IsNullOrEmpty(pageSubHeading))
                                                    {
                                                        if (SecondLevelItems.secondlevelsubheading.Equals("yes", StringComparison.OrdinalIgnoreCase))
                                                        {%>
                                                <ul>
                                                    <li><a href="<%= thirldLevelUrlString%>"><%= pageSubHeading %></a></li>
                                                </ul>
                                                <%} %>
                                                <%}%>

                                                <% if (GetCountOfLevel3(thirldLevelItem) > 0)
                                                    { %>
                                                <ul>
                                                    <% foreach (SCA.TCW.Men.Data.ItemsItemItemItemItem fourthLevelItem in objProducts.GetOrderListOfLevel3(thirldLevelItem))
                                                        {
                                                            curCounter++;
                                                            string fourthLevelUrlString = string.Empty;  %>
                                                    <li>


                                                        <%if (!string.IsNullOrEmpty(fourthLevelItem.LongURLUsingPageNameOverride))
                                                            { %>
                                                        <%  fourthLevelUrlString = fourthLevelItem.LongURLUsingPageNameOverride; %>
                                                        <%}
                                                            else
                                                            {%>
                                                        <%  fourthLevelUrlString = fourthLevelItem.url;
                                                            fourthLevelUrlString = (String.Equals(fourthLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? fourthLevelUrlString + "/" : fourthLevelUrlString; %>
                                                        <%}
                                                        %>
                                                        <a href="<%= fourthLevelUrlString %>"><%= (!string.IsNullOrWhiteSpace(fourthLevelItem.overriddentitle))?fourthLevelItem.overriddentitle: fourthLevelItem.displaytitle %></a>
                                                    </li>

                                                    <% } %>
                                                </ul>
                                                <% } %>

                                                <% } %>
                                                <%else if (!string.IsNullOrEmpty(thirldLevelItem.url))
                                                    {
                                                        curCounter = 1;
                                                        string thirldLevelUrlString = string.Empty;%>

                                                <%if (!string.IsNullOrEmpty(thirldLevelItem.LongURLUsingPageNameOverride))
                                                    { %>
                                                <%  thirldLevelUrlString = thirldLevelItem.LongURLUsingPageNameOverride; %>
                                                <%}
                                                    else
                                                    {%>
                                                <%  thirldLevelUrlString = thirldLevelItem.url;
                                                    thirldLevelUrlString = (String.Equals(thirldLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? thirldLevelUrlString + "/" : thirldLevelUrlString; %>
                                                <%}
                                                %>
                                            </div>
                                        </div>
                                        <div class="column col-4">
                                            <div class="column-content">
                                                <% if (!string.IsNullOrEmpty(thirldLevelUrlString))
                                                    { %>
                                                <div class="submenu-title">
                                                    <a href="<%= thirldLevelUrlString%>"><%= (!string.IsNullOrWhiteSpace(thirldLevelItem.overriddentitle))?thirldLevelItem.overriddentitle: thirldLevelItem.displaytitle %></a>
                                                </div>
                                                <%} %>
                                                <% pageSubHeading = GetPageSubHeadingLevel3(thirldLevelItem); %>
                                                <%if (!string.IsNullOrEmpty(SecondLevelItems.secondlevelsubheading) && !string.IsNullOrEmpty(pageSubHeading))
                                                    {
                                                        if (SecondLevelItems.secondlevelsubheading.Equals("yes", StringComparison.OrdinalIgnoreCase))
                                                        {%>
                                                <ul>
                                                    <li><a href="<%= thirldLevelUrlString%>"><%= pageSubHeading %></a></li>
                                                </ul>
                                                <%} %>
                                                <%}%>

                                                <% if (GetCountOfLevel3(thirldLevelItem) > 0)
                                                    { %>
                                                <ul>
                                                    <% foreach (SCA.TCW.Men.Data.ItemsItemItemItemItem fourthLevelItem in objProducts.GetOrderListOfLevel3(thirldLevelItem))
                                                        {
                                                            curCounter++;
                                                            string fourthLevelUrlString = string.Empty; %>
                                                    <li>

                                                        <%if (!string.IsNullOrEmpty(fourthLevelItem.LongURLUsingPageNameOverride))
                                                            { %>
                                                        <%  fourthLevelUrlString = fourthLevelItem.LongURLUsingPageNameOverride; %>
                                                        <%}
                                                            else
                                                            {%>
                                                        <%  fourthLevelUrlString = fourthLevelItem.url;
                                                            fourthLevelUrlString = (String.Equals(fourthLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? fourthLevelUrlString + "/" : fourthLevelUrlString; %>
                                                        <%}

                                                        %>
                                                        <a href="<%= fourthLevelUrlString %>"><%= (!string.IsNullOrWhiteSpace(fourthLevelItem.overriddentitle))?fourthLevelItem.overriddentitle: fourthLevelItem.displaytitle %></a>
                                                    </li>
                                                    <%} %>
                                                </ul>
                                                <%} %>
                                                <% } %>
                                                <% } %>
                                            </div>
                                        </div>
                                        <% } %>


                                        <!--we will in future -->
                                        <% SCA.TCW.Men.Data.ItemsItemItemRight_component right_component = GetRightComponent(SecondLevelItems);
                                            if (right_component != null)
                                            {
                                                if ((!string.IsNullOrEmpty(right_component.Content.heading)) || (!string.IsNullOrEmpty(right_component.Content.subheading)) || (!string.IsNullOrEmpty(right_component.Content.description.RTF)))
                                                {
                                                    bool anchorRendered = false;
                                        %>
                                        <div class="column col-4">
                                            <!-- start component: pushbox -->
                                            <div class="component component-pushbox">
                                                <div class="pushbox  clickable-xs type-half-image">
                                                    <% if (right_component.Content.call_to_action1.link_info.internal_link != null && !string.IsNullOrEmpty(right_component.Content.call_to_action1.link_info.internal_link.href))
                                                        {
                                                            if (!string.IsNullOrEmpty(right_component.Content.call_to_action1.link_info.target) && (right_component.Content.call_to_action1.link_info.target.Equals("new tab", StringComparison.OrdinalIgnoreCase)))
                                                            {
                                                                anchorRendered = true;%>
                                                    <a target="_blank" title="<%=right_component.Content.call_to_action1.link_info.tool_tip%>" href="<%= SCA.TCW.Men.Business.BrokerBIZ.GetPageURLByTcmId(right_component.Content.call_to_action1.link_info.internal_link.href)%>">
                                                        <%}
                                                            else
                                                            {
                                                                anchorRendered = true; %>
                                                        <a title="<%=right_component.Content.call_to_action1.link_info.tool_tip%>" href="<%= SCA.TCW.Men.Business.BrokerBIZ.GetPageURLByTcmId(right_component.Content.call_to_action1.link_info.internal_link.href)%>">
                                                            <%} %>

                                                            <% }
                                                                else if (!string.IsNullOrEmpty(right_component.Content.call_to_action1.link_info.external_link))
                                                                {
                                                                    string linkUrl = (right_component.Content.call_to_action1.link_info.external_link.Trim().StartsWith("http://", StringComparison.OrdinalIgnoreCase) ||
                                                                                      right_component.Content.call_to_action1.link_info.external_link.Trim().StartsWith("https://", StringComparison.OrdinalIgnoreCase))
                                                                                      ? right_component.Content.call_to_action1.link_info.external_link : "http://" + right_component.Content.call_to_action1.link_info.external_link;
                                                            %>
                                                            <% if (right_component.Content.call_to_action1.link_info.target.Equals("new tab", StringComparison.OrdinalIgnoreCase))
                                                                {
                                                                    anchorRendered = true; %>
                                                            <a target="_blank" title="<%=right_component.Content.call_to_action1.link_info.tool_tip%>" href="<%=linkUrl%>">
                                                                <%}
                                                                    else
                                                                    {
                                                                        anchorRendered = true;%>
                                                                <a title="<%=right_component.Content.call_to_action1.link_info.tool_tip%>" href="<%=linkUrl%>">
                                                                    <%}

                                                                        }
                                                                        else if (right_component.Content.pdf != null)
                                                                        {
                                                                            anchorRendered = true;%>
                                                                    <a target="_blank" title="<%=right_component.Content.pdf.title%>" href="<%=GetRightComponentPdf(right_component)%>">
                                                                        <%}
                                                                        %>
                                                                        <div class="pushbox-content  transparent border not-fixed-height align-height has-button has-text">
                                                                            <div class="pushbox-content-inner">
                                                                                <div class="pushbox-text">
                                                                                    <div class="pushbox-text-content">
                                                                                        <h2 class="pretitle "><%= right_component.Content.heading %></h2>
                                                                                        <h3 class="title"><%=right_component.Content.subheading%></h3>
                                                                                        <div class="text">
                                                                                            <p><%=right_component.Content.description.RTF%></p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="pushbox-img js-align-bottom">
                                                                                    <img src="<%= GetRightCompontImage(right_component) %>" title="<%= right_component.Content.Media.ImageVideo.title %>" alt="<%= right_component.Content.Media.ImageVideo.title %>">
                                                                                </div>
                                                                                <%if (anchorRendered)
                                                                                    { %>
                                                                                <div class="pushbox-button ">
                                                                                    <div class="component component-btn">
                                                                                        <div class="btn">
                                                                                            <span><%=right_component.Content.call_to_action1.link_text%></span>
                                                                                            <i class="icon-button-right"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <%} %>
                                                                            </div>
                                                                        </div>

                                                                        <%if (anchorRendered)
                                                                            { %>
                                                                    </a>
                                                                    <%} %>
                                                </div>
                                            </div>


                                            <!-- end component: pushbox -->
                                        </div>
                                        <%} %>
                                        <%} %>
                                        <!--we will in future -->



                                    </div>
                                    <!-- Add bottom complonent-->
                                    <% SCA.TCW.Men.Data.ItemsItemItemBottom_component bottomComponent = GetBottomComponent(SecondLevelItems);
                                        if (bottomComponent != null)
                                        {
                                            if (!string.IsNullOrEmpty(bottomComponent.Content.heading))
                                            {%>
                                    <div class="row submenu-footer">
                                        <div class="column col-4">
                                            <a href="<%= SplitedSubDomain + string.Format(samplePagePath,bottomComponent.Content.product_id.ToString()) %>" class="title-item">
                                                <span class="title"><%=bottomComponent.Content.heading%></span>
                                                <% if (!string.IsNullOrEmpty(bottomComponent.Content.subheading))
                                                    { %>
                                                <span class="subtitle"><%=bottomComponent.Content.subheading%><i class="icon-nav-arrow-right"></i></span>
                                                <%} %>
                                            </a>
                                        </div>
                                        <% SCA.TCW.Men.Data.Product selectedProduct = GetProductDetail(bottomComponent.Content.product_id.ToString()); %>
                                        <%if (selectedProduct != null && !string.IsNullOrEmpty(selectedProduct.ArticleNumber))
                                            {
                                                string strPathprodname = string.Empty;
                                                SCA.TCW.Men.Data.FamilyAndProduct itemProductFamily = new SCA.TCW.Men.Business.Products().GetProductFamilyByProductName(selectedProduct.ArticleNumber, string.Empty, string.Empty, string.Empty, false);
                                                if (itemProductFamily != null)
                                                    strPathprodname = SplitedSubDomain + string.Format(SCA.TCW.Men.Utility.Helper.GetAppsettingKeyValue<string>("ProductDetailPage"), IsUserIncoEnabled ? Helper.GetUserIncoByFamily(UserIncoToFamilyMappings, itemProductFamily.UrlFriendlyName, SegmentId) + "/" + itemProductFamily.UrlFriendlyName : itemProductFamily.UrlFriendlyName, selectedProduct.URLFriendlyName);
                                        %>
                                        <div class="column col-4">
                                            <a href="<%= strPathprodname%>" class="product-item"><span class="image">
                                                <img src="<%=selectedProduct.AssetUrl%>" alt="">
                                            </span><span class="text"><span class="title"><%= selectedProduct.LongName %></span>
                                                <span class="description"><%= (selectedProduct.DescriptionHeadlines!=null && selectedProduct.DescriptionHeadlines.ContainsKey(SegmentId))?selectedProduct.DescriptionHeadlines[SegmentId]:string.Empty%></span>
                                                <span class="link"><%=ViewProductText%></span>
                                            </span>
                                            </a>
                                        </div>
                                        <%}%>
                                    </div>
                                    <%} %>
                                    <%}%>
                                    <!-- Add bottom complonent-->
                                </div>
                            </div>
                        </li>
                        <% } %>
                    </ul>
                    <% } %>
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>

<!-- Modified by Remaining Target Groups Team - This if condition is added, and it shows breadcrumb only if content author has not opted to hide it. -->
<% if (string.IsNullOrEmpty(IsBreadcrumbRequired) || IsBreadcrumbRequired.ToLower() != "no")
    { %>
<div class="header-breadcrumbs">
    <div id="RemoveBreadcrum" clientidmode="Static" runat="server">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="addthis-share align-right">
                            <div class="<%=(HideShareThisPageIconsHeader != "Yes") ? "addthis_sharing_toolbox" : "display-none;"%>"></div>
                        </div>
                        <ul class="breadcrumbs">
                            <asp:Literal ID="ltrBreadcrumb" runat="server" />
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>
<!-- End of condition -->
