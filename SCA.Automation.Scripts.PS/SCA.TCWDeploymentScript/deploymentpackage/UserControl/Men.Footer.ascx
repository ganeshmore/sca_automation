﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Men.Footer.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Men_Footer" %>

<div class="footer">
	<div id="footer-effect" class="effect"><canvas width="1366" height="190"></canvas>
		<div class="footer-share-link-container">
			<div class="container">
				<div class="row">
					<div class="col-4 col-sm-6 col-xs-12 footer-share-link-group">
						<a id="share-link-popup-link" href="#share-link-popup-panel">
							<img src="../img/share_this_page_icon.png" alt="">
							<p>Share this page</p>
						</a>
					</div>
					<!-- <div class="col-4 col-sm-6 col-xs-12"></div>
					<div class="col-4 col-sm-6 col-xs-12"></div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="footer-columns">
		<div class="container">
			<div class="row">
				<div class="footer-column col-4 col-md-4 col-sm-6 col-xs-12 vspace-lg">
											<h3>This is TENA Men</h3>
										<p class="vspace-md reduced-width-paragraph">Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ex ea commodo consequat. Duis aute irure in reprehenderit in voluptate velit esse.</p>
					
	<!-- start component: btn -->
	<div class="component component-btn">
		
		<a href="#" class="btn btn-hollow">
			
		<span>Read more about TENA </span>
		<i class="icon-button-right"></i>

		</a>
	
	</div>
	<!-- end component: btn -->

				</div>
				<div class="footer-column col-4 col-md-4 col-sm-6 col-xs-12 vspace-md">
											<h3>Follow TENA Men</h3>
										<div class="footer-social-media-link-container">
						<table>
							<tbody><tr>
								<td class="social-icon-container">
									<img src="../img/facebook_icon.png" alt="">
								</td>
								<td class="social-link-container">
									<a href="">
										<span class="social-text-container">Find us on Facebook <i class="icon-footer-link"></i></span>
									</a>
								</td>
							</tr>
						</tbody></table>
					</div>
					<div class="footer-social-media-link-container">
						<table>
							<tbody><tr>
								<td class="social-icon-container">
									<img src="../img/youtube_icon.png" alt="">
								</td>
								<td class="social-link-container">
									<a href="">
										<span class="social-text-container">Watch on Youtube <i class="icon-footer-link"></i></span>
									</a>
								</td>
							</tr>
						</tbody></table>
					</div>
					<div class="footer-social-media-link-container">
						<table>
							<tbody><tr>
								<td class="social-icon-container">
									<img src="../img/twitter_icon.png" alt="">
								</td>
								<td class="social-link-container">
									<a href="">
										<span class="social-text-container">Talk to us on Twitter <i class="icon-footer-link"></i></span>
									</a>
								</td>
							</tr>
						</tbody></table>
					</div>
				</div>
				<div class="footer-column col-4 col-md-4 col-sm-12 col-xs-12 vspace-md">
					<h3>Looking for other TENA sites?</h3>
					<p class="vspace-md">Dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut ex ea commodo consequat. Duis aute irure in reprehenderit in voluptate velit esse.</p>
					<a href="#" class=""><p class="footer-text-angle-links">TENA for Woman <i class="icon-footer-link"></i></p></a>
					<a href="#" class=""><p class="footer-text-angle-links">TENA for Men <i class="icon-footer-link"></i></p></a>
					<a href="#" class=""><p class="footer-text-angle-links">TENA for Professionals <i class="icon-footer-link"></i></p></a>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-toolbar clearfix">
		<div class="container">
			<div class="row">
				<div class="col-8 col-md-12">
					<ul>
						<li><i class="icon icon-color-circle"></i> <a href="#">About</a></li>
						<li><i class="icon icon-color-circle"></i> <a href="#">Terms of Use</a></li>
						<li><i class="icon icon-color-circle"></i> <a href="#">Privacy Policy</a></li>
						<li><i class="icon icon-color-circle"></i> <a href="#">Cookies</a></li>
						<li><i class="icon icon-color-circle"></i> <a href="glossary.html">Glossary</a></li>
						<li><i class="icon icon-color-circle"></i> <a href="contact-us.html">Contact</a></li>
					</ul>
				</div>
				<div class="copyright col-4 col-md-12">© 2014 Copyright SCA Hygiene Products AB </div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<img src="../dummy/sca.png" alt="">
				</div>
			</div>
		</div>
	</div>
</div>