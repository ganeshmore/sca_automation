﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Men.Header.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Men_Header" %>



<div class="component component-cookiebar">
		
	<div class="cookiebar" style="display: block;">
		<div class="container">
			<div class="row">
				<div class="col-12">
					SCA is using cookies lorem ipsum de silonter ·
					<a href="#" class="underlined toggle-tooltip js-toggle-tooltip" data-title="What are cookies?" data-description="Lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet.">What are cookies?</a>

					<a href="#" class="cookiebar-close align-right">
						<span>I accept</span>
						<i class="icon-validation-cross"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
	</div>
<div class="viewport">
        <div class="wrapper">
            <div class="header">
                <div class="header-utility">
                    <div class="utility-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="links">
                                        <li><a href=""><span>Press</span></a></li>
                                        <li>&middot;</li>
                                        <li><a href=""><span>Where to buy</span></a></li>
                                        <li>&middot;</li>
                                        <li><a href=""><span>Contact us</span></a></li>
                                        <li>&middot;</li>
                                        <li class="has-submenu">
                                            <a href="" class="other-sites-link">
                                                <span>Looking for other TENA sites?</span>
                                                <i class="icon-nav-arrow-down"></i>
                                            </a>
                                            <div class="submenu other-sites">
                                                <div class="container container-fluid">
                                                    <div class="row submenu-content">
                                                        <div class="column col-12">
                                                            <div class="column-content">
                                                                <ul class="other-sites-list">
                                                                    <li><a href="#">Women</a></li>
                                                                    <li><a href="#">Children</a></li>
                                                                    <li><a href="#">Looking after loved ones</a></li>
                                                                    <li><a href="../tpw/">Professionals</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="logo">
                                    <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/startpage.html">
                                        <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/logo_2x.png" alt="Tena" title="">
                                    </a>
                                </div>

                                <div class="header-details">
                                    <div class="site-name">
                                        Men
                                    </div>
                                    <%--Country Header part  --%>
                                 

                                    <div class="hybrid-selector">
                                        <div class="country-selector">
                                            <span class="title">United Kingdom</span>
                                            <div class="location has-submenu">
                                                <a href="">change <i class="icon icon-nav-arrow-down"></i></a>
                                                <div class="submenu submenu-bg-world-map">
                                                    <div class="container container-fluid">
                                                        <div class="row submenu-content">
                                                            <div class="column col-4">
                                                                <div class="submenu-title">North America</div>
                                                                <ul>
                                                                    <li><a href="">USA</a></li>
                                                                    <li><a href="">Mexico</a></li>
                                                                    <li><a href="">Canada (en)</a></li>
                                                                    <li><a href="">Canada (fr)</a></li>
                                                                </ul>
                                                                <div class="submenu-title">Central America</div>
                                                                <ul>
                                                                    <li><a href="">Costa Rica</a></li>
                                                                </ul>
                                                                <div class="submenu-title">South America</div>
                                                                <ul>
                                                                    <li><a href="">Brazil</a></li>
                                                                    <li><a href="">Chile</a></li>
                                                                    <li><a href="">Colombia</a></li>
                                                                    <li><a href="">Ecuador</a></li>
                                                                    <li><a href="">Peru</a></li>
                                                                    <li><a href="">Venezuela</a></li>
                                                                </ul>
                                                                <div class="submenu-title">Europe</div>
                                                                <ul>
                                                                    <li><a href="">Austria</a></li>
                                                                    <li><a href="">Belarus</a></li>
                                                                    <li><a href="">Belgium (nl)</a></li>
                                                                    <li><a href="">Belgium (fr)</a></li>
                                                                    <li><a href="">Bosnia and Herzegovina</a></li>
                                                                    <li><a href="">Croatia</a></li>
                                                                    <li><a href="">Czech Republic</a></li>
                                                                    <li><a href="">Denmark</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="column col-4">
                                                                <ul>
                                                                    <li><a href="">Estonia</a></li>
                                                                    <li><a href="">Finland</a></li>
                                                                    <li><a href="">France</a></li>
                                                                    <li><a href="">Germany</a></li>
                                                                    <li><a href="">Greece</a></li>
                                                                    <li><a href="">Hungary</a></li>
                                                                    <li><a href="">Ireland</a></li>
                                                                    <li><a href="">Italy</a></li>
                                                                    <li><a href="">Latvia</a></li>
                                                                    <li><a href="">Lithuania</a></li>
                                                                    <li><a href="">Malta</a></li>
                                                                    <li><a href="">Netherlands</a></li>
                                                                    <li><a href="">Norway</a></li>
                                                                    <li><a href="">Poland</a></li>
                                                                    <li><a href="">Portugal</a></li>
                                                                    <li><a href="">Romania</a></li>
                                                                    <li><a href="">Russia</a></li>
                                                                    <li><a href="">Serbia and Montenegro</a></li>
                                                                    <li><a href="">Slovakia</a></li>
                                                                    <li><a href="">Slovenia</a></li>
                                                                    <li><a href="">Spain</a></li>
                                                                    <li><a href="">Sweden</a></li>
                                                                    <li><a href="">Switzerland (de)</a></li>
                                                                    <li><a href="">Switzerland (fr)</a></li>
                                                                    <li><a href="">Switzerland (it)</a></li>
                                                                    <li><a href="">Turkey</a></li>
                                                                    <li><a href="">Ukraine</a></li>
                                                                    <li><a href="">United Kingdom</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="column col-4">


                                                                <div class="submenu-title">Africa</div>
                                                                <ul>
                                                                    <li><a href="">South Africa</a></li>
                                                                    <li><a href="">Tunisia</a></li>
                                                                </ul>

                                                                <div class="submenu-title">Asia</div>
                                                                <ul>
                                                                    <li><a href="">Hong Kong</a></li>
                                                                    <li><a href="">India</a></li>
                                                                    <li><a href="">Israel</a></li>
                                                                    <li><a href="">Japan</a></li>
                                                                    <li><a href="">Kazakhstan</a></li>
                                                                    <li><a href="">Lebanon</a></li>
                                                                    <li><a href="">Malaysia</a></li>
                                                                    <li><a href="">Philippines</a></li>
                                                                    <li><a href="">Republic of Korea</a></li>
                                                                    <li><a href="">Russia</a></li>
                                                                    <li><a href="">Singapore</a></li>
                                                                    <li><a href="">Taiwan</a></li>
                                                                    <li><a href="">Turkey</a></li>
                                                                </ul>

                                                                <div class="submenu-title">Asia Pacific</div>
                                                                <ul>
                                                                    <li><a href="">New Zealand</a></li>
                                                                    <li><a href="">Australia</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="language-selector">
                                            <span class="title">Language</span>
                                            <div class="languages">
                                                <a href="" class="active">EN</a> | <a href="">FR</a>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="header-items">
                                    <div class="item item-sample">
                                        <a href="">
                                            <span class="image">
                                                <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/dummy/men-sample-product.png" alt="">
                                            </span>
                                            <span class="text">
                                                <span class="title">Get free sample</span>
                                                <span class="subtitle">Order today</span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="item item-search">
                                        <%--<form action="search-result.html" method="post">--%>                                            
                                                <div class="form-field">
                                                    <input type="text" placeholder="Search" class="search-input field field-input">
                                                </div>
                                                <button type="submit" class="search-submit">
                                                    <i class="icon-search" title="Search"></i>
                                                </button>                                            
                                        <%--</form>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Menu -->
                <div class="header-menu">
                    <div class="menu-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="menu align-left">

                                        <li class="menu-item has-submenu">
                                            <a href="#">Find what's right for you</a>

                                            <div class="submenu ">
                                                <div class="container container-fluid">
                                                    <div class="row submenu-content">
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="findout.html">Find out</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum 1</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                    <li><a href="#">Lorem ipsum 4</a></li>
                                                                    <li><a href="#">Lorem ipsum 5</a></li>
                                                                    <li><a href="#">Lorem ipsum 6</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">Event calendar</div> <ul> <li> <div class="event-item"> <div class="date align-left"> <div class="month">Mar</div> <div class="day">17</div> </div> <div class="event-info"> <div class="event-title">Descritption copy</div> <div class="event-desccription">Lorem ipsum dolor amet, cons ectetur adipiscing elit. Aenean euismod bibendum laoreet.</div> </div> </div> </li> <li> <div class="event-item"> <div class="date align-left"> <div class="month">Apr</div> <div class="day">9</div> </div> <div class="event-info"> <div class="event-title">Descritption copy</div> <div class="event-desccription">Lorem ipsum dolor amet, cons ectetur adipiscing elit. Aenean euismod bibendum laoreet.</div> </div> </div> </li> </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>

                                        <li class="menu-item has-submenu">
                                            <a href="#">Products</a>

                                            <div class="submenu has-footer">
                                                <div class="container container-fluid">
                                                    <div class="row submenu-content">
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="products.html">Products</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="product-description.html">Product description</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum 1</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                    <li><a href="#">Lorem ipsum 4</a></li>
                                                                    <li><a href="#">Lorem ipsum 5</a></li>
                                                                    <li><a href="#">Lorem ipsum 6</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum 1</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum 2</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row submenu-footer">
                                                        <div class="column col-4"> <a href="#" class="title-item"> <span class="title">Try it out!</span> <span class="subtitle">Lorem ipsum dolores<i class="icon-nav-arrow-right"></i></span> </a> </div> <div class="column col-4"> <a href="" class="product-item"> <span class="image"> <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/dummy/menu-product.png" alt=""> </span> <span class="text"> <span class="title">Level 2</span> <span class="description">Designed for light to more than light urine leakage</span> <span class="link">View product detail</span> </span> </a> </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>

                                        <li class="menu-item has-submenu">
                                            <a href="#">How to get better</a>

                                            <div class="submenu ">
                                                <div class="container container-fluid">
                                                    <div class="row submenu-content">
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="article-list.html">Article list</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="article.html">Article</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="how-to-get-better.html">How to get better</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="types-and-causes.html">Types and causes</a></li>
                                                                    <li><a href="ask-the-doctor.html">Ask the doctor</a></li>
                                                                    <li><a href="#">Lorem ipsum 3</a></li>
                                                                    <li><a href="#">Lorem ipsum 4</a></li>
                                                                    <li><a href="#">Lorem ipsum 5</a></li>
                                                                    <li><a href="#">Lorem ipsum 6</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="#">Lorem ipsum</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Lorem ipsum 1</a></li>
                                                                    <li><a href="#">Lorem ipsum 2</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-4">

                                                            <!-- start component: pushbox -->
                                                            <div class="component component-pushbox">

                                                                <div class="pushbox  clickable-xs type-half-image">
                                                                    <div class="pushbox-content  transparent border not-fixed-height align-height has-button has-text">

                                                                        <a href="#" class="pushbox-clickable"></a>



                                                                        <div class="pushbox-text">
                                                                            <div class="pushbox-text-content">

                                                                                <h2 class="pretitle ">
                                                                                    Keep control
                                                                                </h2>

                                                                                <h3 class="title">
                                                                                    <a href="#">							Let this guy show how							</a>
                                                                                </h3>

                                                                                <div class="text">
                                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, architecto, corrupti.</p>
                                                                                </div>





                                                                            </div>

                                                                        </div>

                                                                        <div class="pushbox-img js-align-bottom">
                                                                            <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/dummy/men-keep-control.jpg" alt="">

                                                                        </div>


                                                                        <div class="pushbox-button ">

                                                                            <!-- start component: btn -->
                                                                            <div class="component component-btn">

                                                                                <a href="products.html" class="btn ">

                                                                                    <span>Go to products </span>
                                                                                    <i class="icon-button-right"></i>

                                                                                </a>

                                                                            </div>
                                                                            <!-- end component: btn -->

                                                                        </div>


                                                                    </div>
                                                                    <div class="pushbox-shadow"><img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/box-shadow.png" alt="" /></div>
                                                                </div>

                                                            </div>
                                                            <!-- end component: pushbox -->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>

                                        <li class="menu-item has-submenu">
                                            <a href="lifestories.html">Real life stories</a>

                                            <div class="submenu ">
                                                <div class="container container-fluid">
                                                    <div class="row submenu-content">
                                                        <div class="column col-6 col-md-6 col-sm-12">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="lifestories.html#martin">Martin's story</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="lifestories.html#martin">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a></li>
                                                                </ul>


                                                                <div class="submenu-title">
                                                                    <a href="lifestories.html#peter">Peter's story</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="lifestories.html#peter">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                        <div class="column col-6 col-md-6 col-sm-12">
                                                            <div class="column-content">

                                                                <div class="submenu-title">
                                                                    <a href="lifestories.html#richard">Richard's story</a>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="lifestories.html#richard">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a></li>
                                                                </ul>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="mobile-header">
                <div id="mobile-menu" class="mobile-menu mobile-menu-left active">
                    <div class="mobile-menu-top">
                        <a class="mobile-menu-trigger"><i class="icon icon-nav-menu"></i></a>

                        <div class="mobile-menu-top-content">
                            <div class="mobile-menu-search">
                                <%--<form action="search-result.html">--%>
                                    <button class="mobile-menu-search-submit icon icon-search"></button>
                                    <input type="text" placeholder="Search for products" class="mobile-menu-search-field">
                               <%-- </form>--%>
                            </div>
                        </div>
                    </div>

                    <div class="mobile-menu-content-wrapper">
                        <div class="mobile-menu-content-inner-wrapper">
                            <div class="mobile-main-menu mobile-menu-content">
                                <div class="mobile-menu-list vspace-sm">
                                    <ul class="mainmenu">
                                        <li class="has-submenu">
                                            <a href="#" class="submenu-trigger">Find what's right for you</a>

                                            <ul class="submenu">
                                                <li>
                                                    <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/findout.html">Find out</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum 1</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 6</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="has-submenu">
                                            <a href="#" class="submenu-trigger">Products</a>

                                            <ul class="submenu">
                                                <li>
                                                    <a href="products.html">Products</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="product-description.html">Product description</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum 1</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 6</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum 1</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum 2</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="has-submenu">
                                            <a href="#" class="submenu-trigger">How to get better</a>

                                            <ul class="submenu">
                                                <li>
                                                    <a href="article-list.html">Article list</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="article.html">Article</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="how-to-get-better.html">How to get better</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="types-and-causes.html">Types and causes</a>
                                                        </li>
                                                        <li>
                                                            <a href="ask-the-doctor.html">Ask the doctor</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 3</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 4</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 5</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 6</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Lorem ipsum</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="#">Lorem ipsum 1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Lorem ipsum 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="has-submenu">
                                            <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/lifestories.html" class="submenu-trigger">Real life stories</a>

                                            <ul class="submenu">
                                                <li>
                                                    <a href="lifestories.html#martin">Martin's story</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/lifestories.html#martin">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="lifestories.html#peter">Peter's story</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/lifestories.html#peter">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/lifestories.html#richard">Richard's story</a>

                                                    <ul class="grandchildmenu">
                                                        <li>
                                                            <a href="<%= SCA.TCW.Men.Data.DataConstant.Domain %>/lifestories.html#richard">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla aperiam quia reiciendis ducimus amet ullam.</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div class="country-selector vspace-xs">
                                   <%-- <form action="startpage.html">--%>
                                        <label class="title" for="location">Location</label>
                                        <select name="location" id="location">
                                            <optgroup label="North America">
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                            </optgroup>
                                            <optgroup label="Central America">
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                            </optgroup>
                                            <optgroup label="Europe">
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                                <option value="">Lorem</option>
                                                <option value="">Ipsum</option>
                                            </optgroup>
                                        </select>

                                    <%--</form>--%>
                                </div>
                                <div class="vspace-sm">
                                    <div class="language-selector">
                                        <span class="title">Language</span>
                                        <div class="languages">
                                            <a href="" class="active">EN</a> | <a href="">FR</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="item item-sample">
                                    <a href="">
                                        <span class="image">
                                            <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/dummy/tpw-sample-product.png" alt="">
                                        </span>
                                        <span class="text">
                                            <span class="title">Get free sample</span>
                                            <span class="subtitle">Order today</span>
                                        </span>
                                    </a>
                                </div>
                            </div>

                            <div class="mobile-menu-utility mobile-menu-content">
                                <ul>
                                    <li><a href="press-events.html">Utility link</a></li>
                                    <li><a href="#">Where to buy</a></li>
                                    <li><a href="#">Contact us</a></li>
                                    <li class="has-submenu">
                                        <span class="submenu-trigger">Looking for other TENA sites? <i class="icon-nav-arrow-down"></i></span>

                                        <ul class="submenu">
                                            <li><a href="#">Women</a></li>
                                            <li><a href="#">Children</a></li>
                                            <li><a href="#">Looking after loved ones</a></li>
                                            <li><a href="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/tpw/startpage.html">Professionals</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mobile-header-logo">
                    <a href="index.html">
                        <img class="mobile-header-logo-img" src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/logo_2x.png" alt="">
                    </a>
                    <h2 class="mobile-header-title">Men</h2>
                </div>

            </div>

            <div id="share-link-popup-panel" class="share-link-popup mfp-hide">
                <div class="popup-share-link-pair-container">
                    <a href="">
                        <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/facebook_icon.png" alt="">
                        <span class="text-link">Share on Facebook <i class="icon-footer-link"></i></span>
                    </a>
                </div>
                <div class="popup-share-link-pair-container">
                    <a href="">
                        <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/twitter_icon.png" alt="">
                        <span class="text-link">Share on Twitter <i class="icon-footer-link"></i></span>
                    </a>
                </div>
                <div class="popup-share-link-pair-container">
                    <a href="">
                        <img src="..<%= SCA.TCW.Men.Data.DataConstant.Domain %>/img/email-share.png" alt="">
                        <span class="text-link">Share by email <i class="icon-footer-link"></i></span>
                    </a>
                </div>
            </div>
        </div>
    </div>