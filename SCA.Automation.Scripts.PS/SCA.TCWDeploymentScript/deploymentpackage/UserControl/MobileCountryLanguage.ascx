﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileCountryLanguage.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.MobileCountryLanguage" %>
<%@ OutputCache Duration="120" VaryByParam="none" %>
<%@ Import Namespace="System.Web.Services.Description" %>
<div class="country-selector vspace-xs">
    <div class="form">
        <label class="title" for="location"><%= ChangeText %></label>
        <select name="location" id="location" data-links="true">
            <% var data = itemvalue.Category.Keyword; %>
            <%if (data.Count > 0)
              { %>
            <%foreach (var dataValue in data)
              { %>
                <optgroup label="<%= dataValue.KeywordValue.ToString() %>">
                <% foreach (var dataValuechild in dataValue.ChildKeywords)
                   {
                       string languane = "";
                       string link = ""; %>                       
                    <% foreach (var meta in dataValuechild.Meta)
                        {
                           if (dataValuechild.Meta.Count > 1)
                            {
                               languane = " (" + Convert.ToString(meta.language) + ")";
                               link = Convert.ToString(meta.link);
                            }
                           else
                           {
                               languane = "";
                               link = Convert.ToString(meta.link);
                           } %>                       
                        <% if (CountryName == dataValuechild.Description)
                        {%>
                            <option  selected="selected" value="<%=link.ToString()%>"><%= dataValuechild.Description.ToString()+languane%></option>
                       <%}
                       else
                       { %>
                            <option value="<%=link.ToString()%>"><%= dataValuechild.Description.ToString()+languane%></option>
                       <%}%>
                    <%} %>
                <%} %>
            </optgroup>
            <%} %>
            <%} %>
        </select>
        <input type="submit" class="hide" />
    </div>
</div>
<div class="vspace-sm"> 
    <% if (languageList.Count > 1)
       {%>
    <div class="language-selector">
        <span class="title"><%= LanguageText %></span>
        <div class="languages">
            <% Int32 count = 0;
                foreach (var language in languageList)
                {
                    count++;
                    if (language.IsActive == true)
                    {%>
                         <a href="<%=language.url%>" class="active"><%=language.Language%></a>&nbsp;|
                    <%}
                    else if (count != languageList.Count)
                    {%>
                         <a href="<%=language.url%>"><%=language.Language%></a>&nbsp;|
                    <%}
                    else
                    {%>
                        &nbsp;<a href="<%=language.url%>"><%=language.Language%></a>
                   <%}
                }%>    
        </div>
    </div>
    <%} %>
</div>


