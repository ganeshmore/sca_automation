﻿<%@ control language="C#" autoeventwireup="true" codebehind="MobileHeader.ascx.cs" inherits="SCA.TCW.Men.Web.UserControl.MobileHeader" %>
<div class="mobile-menu-list vspace-sm">
    <% if(itemvalue!=null && itemvalue.item.item!=null){
         List<SCA.TCW.Men.Data.ItemsItemItem> SecondLevel = objProducts.GetOrderListOfLevel1(itemvalue.item); %>
    <%if (SecondLevel!=null && SecondLevel.Count > 0)
      { %>
    <ul class="mainmenu">
        <% foreach (SCA.TCW.Men.Data.ItemsItemItem SecondLevelItems in SecondLevel)
           { %>
        <li class="has-submenu">
            <% string secondLevelUrlString = SecondLevelItems.url; %>
            <%if (!string.IsNullOrEmpty(SecondLevelItems.LongURLUsingPageNameOverride))
              { %>
            <% secondLevelUrlString = SecondLevelItems.LongURLUsingPageNameOverride; %>
            <%}%>
            <a href="<%=secondLevelUrlString%>" class="submenu-trigger"><%= (!string.IsNullOrWhiteSpace(SecondLevelItems.overriddentitle))?SecondLevelItems.overriddentitle: SecondLevelItems.displaytitle %></a>
            <ul class="submenu">
                <li class="submenu-header">
                    <a href="<%=secondLevelUrlString%>" class="submenu-header-title"><%= (!string.IsNullOrWhiteSpace(SecondLevelItems.overriddentitle))?SecondLevelItems.overriddentitle: SecondLevelItems.displaytitle %></a>
                </li>
                <%if (GetCountOfLevel2(SecondLevelItems) > 0 || string.Equals(SecondLevelItems.productdetailsreq, "yes", StringComparison.OrdinalIgnoreCase))                   
                  { 
                      List<SCA.TCW.Men.Data.ItemsItemItemItem> ThirdLevelMenuItems=  objProducts.GetProductsMenuList(SecondLevelItems, productFamilies);                   
                 foreach (SCA.TCW.Men.Data.ItemsItemItemItem thirldLevelItem in ThirdLevelMenuItems)
                   { %>
                <% string thirldLevelUrlString = thirldLevelItem.url; %>
                <%if (!string.IsNullOrEmpty(thirldLevelItem.LongURLUsingPageNameOverride))
                  { %>
                <%  thirldLevelUrlString = thirldLevelItem.LongURLUsingPageNameOverride; %>
                <%}%>
                <li><a href="<%=thirldLevelUrlString%>"><%= (!string.IsNullOrWhiteSpace(thirldLevelItem.overriddentitle))?thirldLevelItem.overriddentitle: thirldLevelItem.displaytitle %></a>
                    <% if (GetCountOfLevel3(thirldLevelItem) > 0)
                       { %>
                    <ul class="grandchildmenu">
                        <% foreach (SCA.TCW.Men.Data.ItemsItemItemItemItem fourthLevelItem in objProducts.GetOrderListOfLevel3(thirldLevelItem))
                           { %>
                        <li>
                            <% string fourthLevelUrlString = fourthLevelItem.url; %>
                            <%if (!string.IsNullOrEmpty(fourthLevelItem.LongURLUsingPageNameOverride))
                              { %>
                            <%  fourthLevelUrlString = fourthLevelItem.LongURLUsingPageNameOverride; %>
                            <%}%>
                            <a href="<%= fourthLevelUrlString %>"><%= (!string.IsNullOrWhiteSpace(fourthLevelItem.overriddentitle))?fourthLevelItem.overriddentitle: fourthLevelItem.displaytitle %></a>
                        </li>
                        <% } %>
                    </ul>
                    <% } %>                                
                </li>
                <% } %>
                <% } %>
            </ul>
        </li>
        <% } %>
    </ul>
    <% } 
        }
    %>
</div>
