﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetail.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.ProductDetail" %>
<%@ Import Namespace="SCA.TCW.Men.Data" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<%if (objProductDetail != null && !String.IsNullOrEmpty(objProductDetail.ArticleNumber))
    { %>
<div class='section'>

    <div class='component component-product-intro-right'>
        <%if (!string.IsNullOrEmpty(backgroundImage))
            { %>
        <div class='product-intro-item use_bg_image' style='background-image: url(<%= backgroundImage %>); background-color: <%= backgroundColor%>'>
            <%}
                else if (!string.IsNullOrEmpty(backgroundImage) && string.IsNullOrEmpty(backgroundColor))
                {%>
            <div class='product-intro-item use_bg_image' style='background-image: url(<%= backgroundImage %>)'>
                <%}
                    else if (string.IsNullOrEmpty(backgroundImage) && !string.IsNullOrEmpty(backgroundColor))
                    { %>
                <div class='product-intro-item use_bg_image' style='background-color: <%= backgroundColor%>'>
                    <%}
                        else
                        {%>
                    <div class='product-intro-item use_bg_image'>
                        <%}
                            if (!string.IsNullOrEmpty(textColor))
                            {%>
                        <div class='' style='color: <%= textColor%>'>
                            <%}
                                else
                                { %>
                            <div class='container'>
                                <%} %>
                                <!--Product Name-->
                                <div class='row'>
                                    <div class='first-column col-6 col-md-6 col-sm-12'>
                                        <h1 class='product-title top-heading main-heading uppercase_title'><%= objProductDetail.ProductLongName %></h1>
                                        <%=Helper.GeneratePowerReviewHtml("div" + objProductDetail.ArticleNumber + "header", objProductDetail.ArticleNumber, ReviewSnippetType.Review) %>

                                        <div class='text-content vspace-sm'>
                                            <!-- Modified by Remaining target group | Fetch the product detail description and benefits headline based on SegmentId -->
                                            <p><%= (objProductDetail.Description!=null && objProductDetail.Description.ContainsKey(SegmentId))?objProductDetail.Description[SegmentId]:string.Empty %></p>
                                            <%if (BenefitCount > 0)
                                                { %>
                                            <!--show only first 3 benefits-->
                                            <ul>
                                                <%foreach (SCA.TCW.Men.Data.ProductBenifits pb in ProductBenefits.Take(3))
                                                    { %>
                                                <li><%=pb.BenefitHeadline[SegmentId] %></li>
                                                <% } %>
                                            </ul>
                                            <%} %>
                                        </div>

                                        <!--try this & buy now-->
                                        <div class='align-right no-float-xs'>

                                            <!--Custom Code functionality-->
                                            <% if (IsBuyNowEnabled)
                                                { %>
                                            <a style="display: none;" id="buynowlink" href='#buyNow-wrapper' class='btn align-right horizontal-btns vspace-md-xs terms-popup' data-buynow="yes"><span><%=buyNow_LabelText %></span><i class='icon-button-right'></i></a>
                                            <% }
                                                else
                                                {  %>
                                            <%if (!string.IsNullOrEmpty(TenaCustomCode))
                                                {%>
                                            <%=HttpContext.Current.Server.HtmlDecode(TenaCustomCode)%>

                                            <!--Show buy now-->
                                            <% }
                                                else if (!string.IsNullOrEmpty(TenaMenWebsiteURL))
                                                {
                                            %>
                                            <a href='<%=TenaMenWebsiteURL %>' class='btn align-right horizontal-btns vspace-md-xs' target="_blank" data-buynow="yes" data-productname="<%= objProductDetail.ProductLongName %>"><span><%=buyNow_LabelText %></span><i class='icon-button-right'></i></a>
                                            <%} %>
                                            <%} %>
                                            <%
                                                if (IsAvailableForSample)
                                                { %>
                                            <!--Show Try this-->
                                            <a href='<%= orderSamplingPageURL %>' class='btn align-right horizontal-btns vspace-md-xs' target='<%= TryThisInNewTarget?"_blank":"_self" %>'><span><%=tryThis_LabelText %></span><i class='icon-button-right'></i></a>
                                            <%}
                                            %>
                                        </div>

                                    </div>
                                    <!--Image Carousel-->
                                    <%if (objProductDetail.Images != null && objProductDetail.Images.Count > 0)
                                        { %>
                                    <div class='col-6 col-md-6 col-sm-12 vspace-sm product-intro-carousel'>
                                        <div class='component component-image-carousel'>
                                            <div class='image-carousel' style='touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);'>
                                                <%if (objProductDetail.Images.Count > 1)
                                                    {%>
                                                <div class='arrow arrow-left'><i class='icon-carousel-left'></i></div>
                                                <div class='arrow arrow-right'><i class='icon-carousel-right'></i></div>
                                                <%} %>
                                                <div class='images gallery-popup vspace-sm'>
                                                    <!--packShotB2C, packShotB2B-->
                                                    <%if (PackShotImage != null)
                                                        { %><!--packShotB2C-->
                                                    <div class='image'>
                                                        <a href='<%=((!string.IsNullOrEmpty(PackShotImage.AssetUrlPngBigImage)) ? PackShotImage.AssetUrlPngBigImage : string.Empty)%>'>
                                                            <img src='<% =PackShotImage.AssetUrl%>' alt='<%= PackShotImage.AltText %>'
                                                                title='<%= PackShotImage.AltText %>'></a>
                                                    </div>
                                                    <%
                                                        }
                                                        foreach (SCA.TCW.Men.Data.ProductImages pi in ExcludePackShotImages(objProductDetail.Images))
                                                        {
                                                    %>
                                                    <div class='image'>
                                                        <a href='<%=((!string.IsNullOrEmpty(pi.AssetUrlPngBigImage)) ? pi.AssetUrlPngBigImage : string.Empty) %>'>
                                                            <img src='<% =pi.AssetUrl%>' alt='<%= pi.AltText %>'
                                                                title='<%= pi.AltText %>'></a>
                                                    </div>
                                                    <%
                                                        } %>
                                                </div>
                                                <ul class='image-pager'>
                                                    <%  if (PackShotImage != null)
                                                        { %>
                                                    <li class='active'>
                                                        <img src='<%=PackShotImage.AssetUrlPng %>' alt='<%= PackShotImage.AltText %>'
                                                            title='<%= PackShotImage.AltText %>'></li>
                                                    <%  }
                                                        foreach (SCA.TCW.Men.Data.ProductImages pi in ExcludePackShotImages(objProductDetail.Images))
                                                        {
                                                            if (PackShotImage == null)
                                                            {%>
                                                    <li class='active'>
                                                        <img src='<%=pi.AssetUrlPng  %>' alt='<%= pi.AltText %>'
                                                            title='<%= pi.AltText %>'></li>
                                                    <%}
                                                        else
                                                        {%>
                                                    <li>
                                                        <img src='<%=pi.AssetUrlPng  %>' alt='<%= pi.AltText %>'
                                                            title='<%= pi.AltText %>'></li>
                                                    <%} %>
                                                    <%
                                                        } %>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <%} %>
                                    <!-- End of Image Carousel-->
                                </div>
                                <!--End of Product Name-->


                                <%} %>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Product Desc & Facts-->
                <div class='section vspace-md'>
                    <div class='container'>
                        <div class='row'>
                            <!--Description-->
                            <%if (BenefitCount > 0)
                                {
                                    bool isBenefitimageExists = IsAllBenefitHasImage();
                            %>
                            <div class='col-8 col-sm-12 col-full vspace-sm-sm'>
                                <div class='component component-article-list'>
                                    <div class='article-list-container'>
                                        <h2 class='component-title article-list-title'><%=productDesc_LabelText %></h2>
                                        <div class='article-list show-more vspace-sm'>
                                            <%for (int i = 0; i < objProductDetail.Benefits.Count; i++)
                                                {
                                                    string benefitHeadline = (objProductDetail.Benefits[i].BenefitHeadline != null && objProductDetail.Benefits[i].BenefitHeadline.ContainsKey(SegmentId)) ? objProductDetail.Benefits[i].BenefitHeadline[SegmentId] : string.Empty;
                                                    string benefitDescription = (objProductDetail.Benefits[i].Description != null && objProductDetail.Benefits[i].Description.ContainsKey(SegmentId)) ? objProductDetail.Benefits[i].Description[SegmentId] : string.Empty;
                                                    string benefitAltText = objProductDetail.Benefits[i].BenefitAltText != null ? objProductDetail.Benefits[i].BenefitAltText : string.Empty;
                                            %>
                                            <!--Below code is Modified by the remaining target groups -->
                                            <% if (!string.IsNullOrEmpty(benefitHeadline))
                                                {  %>
                                            <div class='article-list-item'>
                                                <% if ((!string.IsNullOrEmpty(objProductDetail.Benefits[i].BenefitAssetURL)) && isBenefitimageExists)
                                                    {
                                                %>
                                                <div class="article-list-image">
                                                    <img src="<%=objProductDetail.Benefits[i].BenefitAssetURL %>" alt='<%=benefitAltText %>' title='<%=benefitAltText %>' class="">
                                                </div>
                                                <%     
                                                    }
                                                %>

                                                <h2><%=benefitHeadline %> </h2>
                                                <div class='text-content vspace-sm'><%=benefitDescription %></div>

                                            </div>
                                            <%} %>
                                            <!--End of code is modified by the remaining target groups -->
                                            <%} %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%} %>
                            <!--End of Description-->
                            <!-- Facts-->
                            <div class='col-4 col-sm-12'>
                                <div class='component component-product-details'>
                                    <h2 class='component-title'><%=productFacts_LabelText %></h2>

                                    <% if (objProductDetail.AbsorbencyValue != null && objProductDetail.AbsorbencyValue.ContainsKey(SegmentId) && objProductDetail.AbsorbencyValue[SegmentId] > 0)
                                        { %>
                                    <!--Absobancy & Color-->
                                    <div class='product-details vspace-sm col-full col-12 col-sm-6 col-xs-12'>
                                        <!--Absorbancy-->
                                        <div class='detail'>
                                            <h3><%=absorbancy_LabelText %></h3>

                                            <ul class='rating'>
                                                <%int lastCount = Convert.ToInt32(Math.Ceiling(objProductDetail.AbsorbencyValue[SegmentId]));
                                                    int restAbsorcount = MaxAbsorbancyToDisplay - lastCount;

                                                    for (int j = 1; j <= lastCount; j++)
                                                    {

                                                        if (j < lastCount)
                                                        { %>
                                                <li><i class='icon-drop-full'></i></li>
                                                <%}
                                                    else
                                                    {
                                                        if (j == lastCount && Convert.ToDouble(lastCount) - objProductDetail.AbsorbencyValue[SegmentId] > 0)
                                                        { %>
                                                <i class='icon-drop-half-full'></i>
                                                <%}
                                                    else
                                                    {%>
                                                <li><i class='icon-drop-full'></i></li>
                                                <%}
                                                    }
                                                %>
                                                <%} %>
                                                <% for (int j = lastCount + 1; j <= MaxAbsorbancyToDisplay; j++)
                                                    {%>
                                                <li><i class='icon-drop-empty'></i></li>
                                                <% } %>
                                            </ul>

                                        </div>
                                        <!--End of Absorbancy-->
                                    </div>

                                    <%} %>
                                    <!--Color-->
                                    <%if (DisplayVariantInfo)
                                        {
                                    %>
                                    <!--End of Color-->

                                    <!--Sizes-->
                                    <div class='tabs tabs-transparent col-full col-12 col-sm-6 col-xs-12'>
                                        <%if (objProductDetail.Varients != null && objProductDetail.Varients.ContainsKey(SegmentId) && objProductDetail.Varients[SegmentId].Count > 0)
                                            {
                                                string color = string.Empty;
                                                int counter = 0, counter1 = 0;
                                        %>

                                        <!--start by kamal-->
                                        <%int resultVal;

                                            bool flagIsShowTab = ((ProductVariants.Count == 1) && (int.TryParse(ProductVariants.FirstOrDefault().DisplayValue, out resultVal))) ? false : true;
                                            string tabCountClass = ProductVariants.Count > 5 ? "6" : "5";
                                        %>
                                        <ul class='tab-list tab-list-full tab-list-count-<%= tabCountClass %>'>
                                            <%
                                                foreach (SCA.TCW.Men.Data.ProductVarient pv in ProductVariants)
                                                {
                                                    if (flagIsShowTab == true)
                                                    {
                                                        if (counter == 0)
                                                        {%>
                                            <!--End by kamal-->
                                            <li class='tab active' style='height: 51px;'><a href='#<%=(!string.IsNullOrEmpty(pv.DisplayValue)) ? pv.DisplayValue.Replace("/", string.Empty).Replace(" ",string.Empty).ToLower() : string.Empty%>'><%=pv.DisplayValue + pv.Unit%></a></li>
                                            <%}
                                                else
                                                { %>
                                            <li class='tab' style='height: 51px;'><a href='#<%=(!string.IsNullOrEmpty(pv.DisplayValue)) ? pv.DisplayValue.Replace("/", string.Empty).Replace(" ",string.Empty).ToLower() : string.Empty%>'><%=pv.DisplayValue + pv.Unit%></a></li>
                                            <%}
                                                        counter++;
                                                    }
                                                }%>
                                        </ul>

                                        <%if (ProductVariants != null)
                                            {
                                                foreach (SCA.TCW.Men.Data.ProductVarient pv in ProductVariants)
                                                {
                                        %>

                                        <div id='<%=(!string.IsNullOrEmpty(pv.DisplayValue)) ? pv.DisplayValue.Replace("/", string.Empty).Replace(" ",string.Empty).ToLower() : string.Empty%>' class='tab-content with-border <%=counter1 == 0?" active":string.Empty %>'>
                                            <div class='crate crate-transparent no-border align-text-center'>
                                                <div class='crate-content'>

                                                    <!--Below code is added by the remaining target groups -->

                                                    <%if (objProductDetail.VarientColors != null && objProductDetail.VarientColors.ContainsKey(SegmentId) && objProductDetail.VarientColors[SegmentId].Count > 0)
                                                        { %>

                                                    <%SCA.TCW.Men.Data.VarientColor objLengthVarient = new SCA.TCW.Men.Data.VarientColor();
                                                        objLengthVarient = (from item in objProductDetail.VarientColors[SegmentId]
                                                                            where item.ProductObjectID == pv.ProductObjectID && item.ProductDetailsObjectID == pv.ProductDetailsObjectID
                                                                            select item).FirstOrDefault();
                                                        if (objLengthVarient != null && !string.IsNullOrEmpty(objLengthVarient.Thickness))
                                                        { %>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=AvailableInThicknessLabelText%></h4>
                                                        <ul class='packages-list'>
                                                            <li><%=ThicknessLabelText +" "+objLengthVarient.Thickness %></li>
                                                        </ul>
                                                    </div>
                                                    <%}

                                                        if (objLengthVarient != null && !string.IsNullOrEmpty(objLengthVarient.Length))
                                                        { %>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=AvailableInLengthsLabelText%></h4>
                                                        <ul class='packages-list'>
                                                            <li><%=LengthLabelText +" "+objLengthVarient.Length %></li>
                                                        </ul>
                                                    </div>
                                                    <%}
                                                        if (objLengthVarient != null && !string.IsNullOrEmpty(objLengthVarient.Width))
                                                        {%>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=AvailableInWidthsLabelText%></h4>
                                                        <ul class='packages-list'>
                                                            <li><%=WidthLabelText +" "+objLengthVarient.Width %></li>
                                                        </ul>
                                                    </div>
                                                    <%    }

                                                        } %>

                                                    <!--End of code is added by the remaining target groups -->

                                                    <%if (DisplayWaistAndHip)
                                                        { %>
                                                    <%if (IsHipSizeVariant)
                                                        { %>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=fitInSizeLabelText%></h4>
                                                        <%if (objProductDetail.Varients.ContainsKey(SegmentId))
                                                            {
                                                                SCA.TCW.Men.Data.ProductVarient objHipSizeMin = new SCA.TCW.Men.Data.ProductVarient(), objHipSizeMax = new SCA.TCW.Men.Data.ProductVarient();
                                                                objHipSizeMin = GetProductVarientByKey(objProductDetail.Varients[SegmentId], hipSizeMin, pv);
                                                                objHipSizeMax = GetProductVarientByKey(objProductDetail.Varients[SegmentId], hipSizeMax, pv);
                                                        %>
                                                        <%if (!string.IsNullOrEmpty(ShowOnlyMaxHipSize) && string.Equals(ShowOnlyMaxHipSize, "Yes", StringComparison.OrdinalIgnoreCase))
                                                            {%>
                                                        <p><%=( hipSizeToLabelText + " " + objHipSizeMax.DisplayValue + objHipSizeMax.Unit) %></p>
                                                        <%
                                                            }
                                                            else
                                                            {
                                                                if (objHipSizeMin != null && objHipSizeMax != null && !string.IsNullOrEmpty(objHipSizeMin.DisplayValue) && !string.IsNullOrEmpty(objHipSizeMax.DisplayValue))
                                                                {%>
                                                        <p><%=(objHipSizeMin.DisplayValue +objHipSizeMin.Unit+ " " + hipSizeToLabelText + " " + objHipSizeMax.DisplayValue+objHipSizeMax.Unit)%></p>
                                                        <%}
                                                                }
                                                            }%>
                                                    </div>
                                                    <% } %>

                                                    <%if (IsWeightVariant)
                                                        {%>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=FitInWeightLabelText%></h4>
                                                        <%
                                                            SCA.TCW.Men.Data.ProductVarient objWeightMin = new SCA.TCW.Men.Data.ProductVarient(), objWeightMax = new SCA.TCW.Men.Data.ProductVarient();
                                                            objWeightMin = GetProductVarientByKey(objProductDetail.Varients[SegmentId], WeightMinKeyText, pv);
                                                            objWeightMax = GetProductVarientByKey(objProductDetail.Varients[SegmentId], WeightMaxKeyText, pv);

                                                            if (objWeightMin != null && objWeightMax != null && !string.IsNullOrEmpty(objWeightMin.DisplayValue) && !string.IsNullOrEmpty(objWeightMax.DisplayValue))
                                                            {%>
                                                        <p><%=(objWeightMin.DisplayValue +objWeightMin.Unit+ " " + hipSizeToLabelText + " " + objWeightMax.DisplayValue+objWeightMax.Unit)%></p>
                                                        <%} %>
                                                    </div>
                                                    <%} %>
                                                    <%if (IsWaistSizeVariant)
                                                        {%>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=FitInWaistLabelText%></h4>
                                                        <%
                                                            SCA.TCW.Men.Data.ProductVarient objWaistSizeMin = new SCA.TCW.Men.Data.ProductVarient(), objWaistSizeMax = new SCA.TCW.Men.Data.ProductVarient();
                                                            objWaistSizeMin = GetProductVarientByKey(objProductDetail.Varients[SegmentId], WaistSizeMinKeyText, pv);
                                                            objWaistSizeMax = GetProductVarientByKey(objProductDetail.Varients[SegmentId], WaistSizeMaxKeyText, pv);
                                                            if (objWaistSizeMin != null && objWaistSizeMax != null && !string.IsNullOrEmpty(objWaistSizeMin.DisplayValue) && !string.IsNullOrEmpty(objWaistSizeMax.DisplayValue))
                                                            {%>
                                                        <p><%=(objWaistSizeMin.DisplayValue+ objWaistSizeMin.Unit+ " " + hipSizeToLabelText + " " + objWaistSizeMax.DisplayValue+objWaistSizeMax.Unit)%></p>
                                                        <%} %>
                                                    </div>
                                                    <%} %>
                                                    <%if (IsVolumeVariant)
                                                        { %>
                                                    <div>
                                                        <h4 class='vspace-xs'><%=VolumeLabelText%></h4>
                                                        <%
                                                            SCA.TCW.Men.Data.ProductVarient objVolumeVariant = new SCA.TCW.Men.Data.ProductVarient();
                                                            objVolumeVariant = GetProductVarientByKey(objProductDetail.Varients[SegmentId], VolumeKeyText, pv);

                                                            if (objVolumeVariant != null && !string.IsNullOrEmpty(objVolumeVariant.DisplayValue))
                                                            {%>
                                                        <p><%=objVolumeVariant.DisplayValue+ objVolumeVariant.Unit%></p>
                                                        <%} %>
                                                    </div>
                                                    <%} %>


                                                    <%}
                                                        if (DisplayPackInfo)
                                                        { %>

                                                    <%if (objProductDetail.VarientColors != null && objProductDetail.VarientColors.ContainsKey(SegmentId) && objProductDetail.VarientColors[SegmentId].Count > 0)
                                                        { %>
                                                    <div>
                                                        <%SCA.TCW.Men.Data.VarientColor objColor = new SCA.TCW.Men.Data.VarientColor();

                                                            IEnumerable<SCA.TCW.Men.Data.VarientColor> objMultiColor;

                                                            objMultiColor = from item in objProductDetail.VarientColors[SegmentId]
                                                                            where item.VariantAttributeValue == pv.DisplayValue
                                                                            select item;
                                                            //Code modified as only distinct pieces needs to be shown now in ascending order of their count     
                                                            List<Int32> pieces = objMultiColor.Where(eachItem => (!string.IsNullOrEmpty(eachItem.Pieces))).Select(eachItem => Convert.ToInt32(eachItem.Pieces)).Distinct().ToList<Int32>();
                                                            pieces = pieces.OrderBy(a => a).ToList<Int32>();

                                                            objColor = (from item in objProductDetail.VarientColors[SegmentId]
                                                                        where item.ProductObjectID == pv.ProductObjectID && item.ProductDetailsObjectID == pv.ProductDetailsObjectID
                                                                        select item).FirstOrDefault();
                                                            if (objColor != null && !string.IsNullOrEmpty(objColor.Pieces) && !string.IsNullOrEmpty(objColor.VariantAttributeValue))
                                                            {
                                                                color = objColor.DisplayValue != null && objColor.DisplayValue != string.Empty ? objColor.DisplayValue : string.Empty;%>
                                                        <h4 class='vspace-xs'><%=availablePackageLabelText%></h4>
                                                        <ul class='packages-list'>
                                                            <% foreach (var packCount in pieces)
                                                                { %>
                                                            <li><%=consumerPackLabelText + " " + packCount.ToString() + " " + packsLabelText%></li>
                                                            <% } %>
                                                        </ul>
                                                        <%}
                                                            else
                                                            {
                                                                objColor = (from item in objProductDetail.VarientColors[SegmentId]
                                                                            where Convert.ToString(item.VariantAttributeValue) == pv.DisplayValue && item.ProductObjectID == pv.ProductObjectID
                                                                            select item).FirstOrDefault();
                                                                if (objColor != null && !string.IsNullOrEmpty(objColor.Pieces) && !string.IsNullOrEmpty(objColor.VariantAttributeValue))
                                                                {
                                                                    color = objColor.DisplayValue != null && objColor.DisplayValue != string.Empty ? objColor.DisplayValue : string.Empty;%>
                                                        <h4 class='vspace-xs'><%=availablePackageLabelText%></h4>
                                                        <ul class='packages-list'>
                                                            <li><%=consumerPackLabelText + " " + objColor.Pieces + " " + packsLabelText%></li>
                                                        </ul>
                                                        <%} %>
                                                        <%} %>
                                                    </div>
                                                    <%} %>

                                                    <%} %>
                                                </div>
                                            </div>
                                            <span class='pDColorD'><%= (color != string.Empty ? color.ToLower() : objProductDetail.Color.ToLower())%></span>

                                        </div>



                                        <%  counter1++;
                                                    }
                                                }
                                            } %>

                                        <!-- If no sizes customer pack should display -->

                                        <%
                                            if (DisplayPackInfo || DisplayVariantInfo)
                                            {
                                                if (objProductDetail.VarientColors != null && objProductDetail.VarientColors.ContainsKey(SegmentId) && objProductDetail.VarientColors[SegmentId].Count > 0)
                                                { %>
                                        <%
                                            if (objProductDetail.VarientColors[SegmentId][0].VariantAttributeValue == null || objProductDetail.VarientColors[SegmentId][0].VariantAttributeValue == "")
                                            { %>

                                        <div class="component component-pushbox">
                                            <div class="pushbox no-shadow">
                                                <div class="pushbox-content normal-height border not-fixed-height align-height has-button">
                                                    <div class="crate-content crate-transparent">
                                                        <div class="pushbox-text-content">
                                                            <div class=" align-text-center prod-text-box">


                                                                <% bool flagAvailable = false; %>
                                                                <%    if (IsIncoVariant)
                                                                    {
                                                                        List<SCA.TCW.Men.Data.VarientColor> thicknessResult = objProductDetail.VarientColors[SegmentId].GroupBy(x => x.Thickness).Select(group => group.First()).ToList<SCA.TCW.Men.Data.VarientColor>();
                                                                        if (thicknessResult.Where(varientColor => !string.IsNullOrEmpty(varientColor.Thickness)).Count() > 0)
                                                                        {%>
                                                                <h4 class='vspace-xs'><%=AvailableInThicknessLabelText %></h4>
                                                                <ul class='packages-list'>
                                                                    <%
                                                                        foreach (SCA.TCW.Men.Data.VarientColor v in thicknessResult)
                                                                        {
                                                                            if (!string.IsNullOrEmpty(v.Thickness))
                                                                            {
                                                                    %>
                                                                    <li><%=ThicknessLabelText +" "+v.Thickness %></li>
                                                                    <%
                                                                            }
                                                                        }

                                                                    %>
                                                                </ul>

                                                                <%}

                                                                    List<SCA.TCW.Men.Data.VarientColor> lengthResult = objProductDetail.VarientColors[SegmentId].GroupBy(x => x.Length).Select(group => group.First()).ToList<SCA.TCW.Men.Data.VarientColor>();
                                                                    if (lengthResult.Where(varientColor => !string.IsNullOrEmpty(varientColor.Length)).Count() > 0)
                                                                    {  %>
                                                                <h4 class='vspace-xs'><%=AvailableInLengthsLabelText %></h4>
                                                                <ul class='packages-list'>
                                                                    <%
                                                                        foreach (SCA.TCW.Men.Data.VarientColor v in lengthResult)
                                                                        {
                                                                            if (!string.IsNullOrEmpty(v.Length))
                                                                            {
                                                                    %>
                                                                    <li><%=LengthLabelText +" "+v.Length %></li>
                                                                    <%
                                                                            }
                                                                        }

                                                                    %>
                                                                </ul>
                                                                <% }

                                                                    List<SCA.TCW.Men.Data.VarientColor> widthResult = objProductDetail.VarientColors[SegmentId].GroupBy(x => x.Width).Select(group => group.First()).ToList<SCA.TCW.Men.Data.VarientColor>();
                                                                    if (widthResult.Where(varientColor => !string.IsNullOrEmpty(varientColor.Width)).Count() > 0)
                                                                    { %>
                                                                <h4 class='vspace-xs'><%=AvailableInWidthsLabelText %></h4>
                                                                <ul class='packages-list'>

                                                                    <%
                                                                        foreach (SCA.TCW.Men.Data.VarientColor v in widthResult)
                                                                        {
                                                                            if (!string.IsNullOrEmpty(v.Width))
                                                                            {
                                                                    %>
                                                                    <li><%=WidthLabelText +" "+v.Width %></li>
                                                                    <%
                                                                            }
                                                                        }

                                                                    %>
                                                                </ul>


                                                                <%}
                                                                    if (DisplayPackInfo)
                                                                    { %>

                                                                <h4 class='vspace-xs'><%=availablePackageLabelText %></h4>

                                                                <ul class='packages-list'>

                                                                    <%
                                                                        //Ge Pack count Unique Value.
                                                                        //var result = objProductDetail.VarientColors.Distinct().ToList();
                                                                        //GetUniqueVarient(objProductDetail);
                                                                        List<SCA.TCW.Men.Data.VarientColor> result = objProductDetail.VarientColors[SegmentId].GroupBy(x => x.Pieces).Select(group => group.First()).ToList<SCA.TCW.Men.Data.VarientColor>();
                                                                        //Code modified as only distinct pack sizes needs to be shown now in ascending order of their count     
                                                                        List<Int32> PackSizes = result.Where(eachItem => (!string.IsNullOrEmpty(eachItem.Pieces))).Select(eachItem => Convert.ToInt32(eachItem.Pieces)).OrderBy(eachItem => eachItem).ToList<Int32>();

                                                                        foreach (Int32 packSize in PackSizes)
                                                                        {
                                                                    %>
                                                                    <li><%=consumerPackLabelText +" "+packSize.ToString() +" "+packsLabelText%></li>
                                                                    <% 
                                                                        } %>
                                                                    <% }%>
                                                                </ul>
                                                                <% }%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <%} %>
                                        <%}
                                            }%>

                                        <!-- End of If no sizes customer pack should display -->


                                    </div>

                                    <!--End of Sizes-->
                                    <% } %>

                                    <!--Start of Ingredients-->
                                    <%if (!string.IsNullOrWhiteSpace(objProductDetail.Ingredients))
                                        {%>
                                    <div class="component component-pushbox"></div>
                                    <div class="component component-pushbox">
                                        <div class="pushbox no-shadow">
                                            <div class="pushbox-content normal-height border not-fixed-height align-height has-button">
                                                <div class="crate-content crate-transparent">
                                                    <div class="pushbox-text-content">
                                                        <div class=" align-text-center prod-text-box">


                                                            <h4 class="vspace-xs"><%= IngredientsLabelText%></h4>

                                                            <%= objProductDetail.Ingredients%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%} %>
                                    <!--End of Ingredients-->

                                </div>
                            </div>

                            <!--End of Facts-->
                        </div>
                    </div>
                </div>
                <!--End of Product Desc & Facts-->

                <%=Helper.GeneratePowerReviewHtml("div" + objProductDetail.ArticleNumber + "productdetail" , objProductDetail.ArticleNumber,ReviewSnippetType.ReviewDisplay) %>

                <!--Coupon Sportlight Start-->
                <%if (IsAvailableForCoupon)
                    { %>
                <div class="section vspace-md">
                    <div class="container">
                        <div class="row">
                            <!-- start component: cta -->
                            <div class="component component-cta">
                                <div class="cta-one-background cta-gradient"></div>
                                <div class="cta-item col-12 col-xs-12 vspace-xs-xs">
                                    <div class="cta-content cta-btn-right">
                                        <div class="cta-content-text">
                                            <h2 class="title">
                                                <a href="<%=SubDomain+CouponLandingPageURL+objProductDetail.ArticleNumber%>" title="<%=CouponHeading%>" class="cta-link"><%=CouponHeading%></a>
                                            </h2>
                                            <p><%=CouponSubheading%></p>
                                        </div>
                                        <!-- start component: btn -->
                                        <div class="component component-btn">
                                            <span class="btn btn-light btn-hollow">
                                                <span><a href="<%=SubDomain+CouponLandingPageURL+objProductDetail.ArticleNumber%>" title="<%=CouponHeading%>" class="cta-link"><%=CouponCallToActionBtnText%></a></span>
                                                <i class="icon-button-right"></i>
                                            </span>
                                        </div>
                                        <!-- end component: btn -->
                                    </div>
                                </div>
                            </div>
                            <!-- end component: cta -->
                        </div>
                    </div>
                </div>
                <%} %>
                <!--Coupon Sportlight End-->
                <!--Similar products-->
                <%
                    if (objProductDetail.SimilarProducts != null && objProductDetail.SimilarProducts.ContainsKey(SegmentId) && objProductDetail.SimilarProducts[SegmentId].Count > 0)
                    { %>
                <div class='section vspace-md'>
                    <div class='container'>
                        <div class='row'>
                            <div class='col-12'>
                                <h2 class='section-title'><%=similerProducts_LabelText %></h2>
                            </div>
                        </div>
                        <div class='row'>
                            <!--Show only 3 products-->
                            <% int productCount = 1;
                                foreach (SCA.TCW.Men.Data.ProductSummary ps in objProductDetail.SimilarProducts[SegmentId])
                                {
                                    SCA.TCW.Men.Data.FamilyAndProduct itemProductFamily = new SCA.TCW.Men.Business.Products().GetProductFamilyByProductName(ps.ArticleNumber, string.Empty, string.Empty, string.Empty, false);
                                    if (productCount <= 3)
                                    {%>
                            <div class='col-4 col-sm-6 col-xs-6 col-xxs-12 vspace-md-sm'>
                                <div class='component component-product-box'>
                                    <div class='product-box product-compact'>
                                        <div class='product-box-content align-height' style='height: auto;'>
                                            <% if (itemProductFamily != null)
                                                { %>
                                            <div class='product-box-img'>
                                                <a href='<%= string.Format(productDetailPageURL,IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,itemProductFamily.UrlFriendlyName,SegmentId) +"/"+itemProductFamily.UrlFriendlyName: itemProductFamily.UrlFriendlyName, ps.ProductUrlFriendlyName) %>'>
                                                    <img class="lazyload" data-src='<%= (ps.AssetUrl != null ? ps.AssetUrl : string.Empty)%>' alt="">
                                                </a>
                                            </div>
                                            <div class='product-box-text'>
                                                <h3 class='title'><a href='<%= string.Format(productDetailPageURL, IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,itemProductFamily.UrlFriendlyName,SegmentId)+"/"+itemProductFamily.UrlFriendlyName: itemProductFamily.UrlFriendlyName, ps.ProductUrlFriendlyName) %>'><%=Convert.ToString(ps.LongName != null ? ps.LongName : string.Empty) %></a></h3>
                                                <div class='text'>
                                                    <p><%=ps.DescriptionHeading  %></p>
                                                </div>
                                                <%=Helper.GeneratePowerReviewHtml("div" + ps.ArticleNumber + productCount, ps.ArticleNumber, ReviewSnippetType.Category) %>
                                            </div>


                                            <div class='product-box-btn align-text-left'>
                                                <div class='component component-btn'>
                                                    <a href='<%= string.Format(productDetailPageURL, IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,itemProductFamily.UrlFriendlyName,SegmentId)+"/"+itemProductFamily.UrlFriendlyName: itemProductFamily.UrlFriendlyName, ps.ProductUrlFriendlyName) %>' class="btn">
                                                        <span><%= (productCallToActionLabelText != null ? productCallToActionLabelText : string.Empty)  %></span>
                                                        <i class='icon-button-right'></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <%} %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%}
                                productCount++;%>

                            <%} %>
                        </div>
                    </div>
                </div>
                <!--View all men Call to action -->
                <% string SimilarProductsCount = Helper.GetAppsettingKeyValue<string>("SimilarProductsCount");
                    if (objProductDetail.SimilarProducts[SegmentId].Count > Convert.ToInt32(SimilarProductsCount))
                    { %>
                <div class='section'>
                    <div class='container'>
                        <div class='row align-text-center vspace-md'>
                            <div class='col-12'>
                                <div class='component component-btn'>
                                    <a href='<%=productLandingPageURL %>' class='btn btn-hollow btn-dark'>
                                        <span><%=viewAllProducts_LabelText %></span>
                                        <i class='icon-button-right'></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
                <%} %>
                <!--End of Similar products-->


                <% if (IsBuyNowEnabled)
                    {
                %>
                <div id="buyNow-wrapper" class="popUp-container mfp-hide">
                    <div class="popUp-header">
                        <div class="row">
                            <div class="headProduct-img col-3 col-md-3 col-xs-12">
                                <%if (BuyNowImage != null)
                                    { %>
                                <img src='<%= PackShotImage.AssetUrl%>' alt='<%= PackShotImage.AltText %>' title='<%= PackShotImage.AltText %>'>
                                <%} %>
                            </div>
                            <div class="headProduct-details  col-8 col-md-8 col-xs-12">
                                <h3 class="popProduct-title"><%= objProductDetail.ProductLongName %></h3>

                                <% if (BuyNowVarientList != null && BuyNowVarientList.Count > 0)
                                    { %>
                                <div class="selectProduct-sizeWrap">
                                    <h5 class="selectProduct-size"><%= SelectOptionHeading %></h5>
                                    <div class="form">
                                    <div class="form-field  col-5  col-md-6 col-xs-12"> 
                                            <select id="ddlBuyNowSizes" class="field-select">
                                                <option value="0"><%= SelectOptionLabel %></option>
                                                <% foreach (KeyValuePair<string, string> currentKeyValue in BuyNowVarientList)
                                                    { %>
                                                <option value="<%= currentKeyValue.Value %>"><%=currentKeyValue.Key %></option>

                                                <%} %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <%} %>
                            </div>
                        </div>
                    </div>

                    <div class="messageInfo-wrap row hide">
                        <p class="messageInfo"></p>
                    </div>

                    <div class="productDetails-wrap" id="dealersConatiner"></div>


                </div>

                <script type="text/javascript">

                    var allVariantIds = '<%=BuyNowCompleteVarintIds  %>';
                    var allPieceCounts = '<%=GetPieceInfoForVariant(BuyNowCompleteVarintIds)  %>';
                    var dropdownData = '<%= new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(BuyNowVarientList) %>';
         
                </script>
                <%} %>
