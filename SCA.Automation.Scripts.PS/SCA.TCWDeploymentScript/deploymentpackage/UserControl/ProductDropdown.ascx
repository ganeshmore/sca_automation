﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDropdown.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.ProductDropdown" %>



<div class="row" id="divProductDropDown" runat="server">
    <div class="form">
        <div class="form-field  col-6 col-xs-12">

            <label class="field-label" for="productDetails"><%= DropdownLabelText %></label>
            <div class="validate validate-select">
                <asp:DropDownList ID="products" runat="server" class="field field-select" ClientIDMode="Static"></asp:DropDownList>
            </div>
            <div class="validate-message validate-message-required">
                <%= RequiredErrorMessage %>
            </div>
        </div>
    </div>
</div>


