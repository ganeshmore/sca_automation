﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFinder.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.ProductFinder" %>
<!-- Product finder - Component container - TPW and TCW -->
<!-- Product finder - Component container - TPW and TCW -->
<input type="hidden" id="hdnDomain" value="<%=domainString %>" />
<input type="hidden" id="hdnSegmentId" value="<%=segmentId %>" />
<input type="hidden" id="hdnProductDetailPageUrl" value="<%=hdnProductDetailPageUrl %>" />
<div class="section vspace-lg">
    <div>
        <div class="row">
            <div class="col-12 tabs-content-wrapper">

                <div id="product-finder" class="tab-content active product-finder with-border">
                    <div class="row">
                        <div class="col-10 col-md-12">
                            <h2 id="sectionHeading" class="section-title no-border margin-small"></h2>
                            <p id="sectionSubHeading"></p>
                        </div>
                        <div class="col-2 col-md-12 hide reset-btn-container" style="text-align: right;">
                            <button class="btn field-submit btn-no-icon btn-reset">
                                <span id="resetbtnText"></span>
                            </button>
                        </div>
                    </div>
                    <div class="form">
                        <!-- start component: question-box -->
                        <div class="component component-question-box">
                            <div class="question-box question-box-product-finder question-box-large-title" data-layout="no-steps">
                                <div class="crate crate-white manual-padding vspace-sm">
                                    <div class="crate-content">

                                        <div class="product-finder-questions">
                                            <!-- Questions -->
                                            <!--
				                          First question ('question-box-item') should have the class 'active'
				                          if not all questions are already answered on page load, i.e. user have
				                          answered the questions, submitted and page have reloaded, AND a results page exist.
				                          If no results page exist, then first item should always have the 'active' class
				                          -->

                                            <% if (listProductFinder != null && listProductFinder.Count > 0)
                                                { %>
                                            <% int index = 0; %>
                                            <%foreach (SCA.TCW.Men.Data.ProductFinderQuestionAnswer item in listProductFinder)
                                                { %>
                                            <!-- Start Question box item -->
                                            <% if (index == 0)
                                                { %>
                                            <div class="question-box-item form-field active" id="<%= item.QuestionCode %>" data-islast="<%= item.IsLastQuestion.ToString().ToLower() %>" data-previndex="">
                                                <%}
                                                    else if (index == (listProductFinder.Count - 1))
                                                    { %>
                                                <div class="question-box-item form-field last" id="<%= item.QuestionCode %>" data-islast="<%= item.IsLastQuestion.ToString().ToLower() %>" data-previndex="">
                                                    <%}
                                                        else
                                                        { %>
                                                    <div class="question-box-item form-field" id="<%= item.QuestionCode %>" data-islast="<%= item.IsLastQuestion.ToString().ToLower() %>" data-previndex="">
                                                        <%} %>
                                                        <h3 class="question-box-item-title"><%= item.QuestionString %></h3>
                                                        <% if (item.AnswerList != null && item.AnswerList.Count > 0)
                                                            { %>
                                                        <fieldset>
                                                            <% foreach (SCA.TCW.Men.Data.ProductFinderAnswer itemAnswer in item.AnswerList)
                                                                { %>
                                                            <div class="field field-radio">
                                                                <input type="radio" class="qa-<%=index + 1 %>" name="question-<%=index + 1 %>[]" id="option-<%=index + 1 %>-<%= itemAnswer.AnswerID %>" data-productids="<%= itemAnswer.ProductIds %>" data-islastans="<%= itemAnswer.IsLastAnswer.ToString().ToLower() %>" data-productcount="<%= itemAnswer.ProductsCount %>" value="<%= itemAnswer.NextQuestionCode %>">
                                                                <label for="option-<%=index + 1 %>-<%= itemAnswer.AnswerID %>">
                                                                    <i class="icon-radiobutton-outer_circle"></i><%=itemAnswer.AnswerString %>
                                                                </label>
                                                            </div>
                                                            <%} %>
                                                        </fieldset>
                                                        <%} %>
                                                    </div>
                                                    <% index++; %>
                                                    <!-- End Question box item -->
                                                    <%} %>
                                                    <%} %>
                                                    <!-- Results page -->
                                                    <!-- Should have the class 'active' if page is loaded and all questions are answered -->
                                                    <!-- Steps -->
                                                    <input type="hidden" name="questionsAnswered" value="true">
                                                    <a id="nextbtnText" class="btn btn-check-next btn-no-icon align-right btn-hide btn-inactive"></a>
                                                </div>
                                                <div class="product-finder-results"></div>
                                                <!-- //create-content -->
                                            </div>
                                        </div>
                                        <!-- Standard submit -->
                                    </div>
                                </div>
                                <!-- end component: question-box -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Product finder -->
