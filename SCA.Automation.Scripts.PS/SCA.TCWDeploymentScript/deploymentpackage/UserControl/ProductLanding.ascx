﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductLanding.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.ProductLanding" %>
<%@ Import Namespace="SCA.TCW.Men.Data" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<%         
    if (string.IsNullOrEmpty(CategoryName))
    { %>
<div id="AllFamilyHTML" runat="server">
    <% if (IsUserIncoEnabled)
        {
            if (UserIncoMappings != null && UserIncoMappings.Count > 0)
            {
                objProductFamilyListContainer = objProductFamilyList;
                foreach (UserIncoToFamilyMapping currentMapping in UserIncoMappings)
                {
                    if (currentMapping.SegmentToFamilyMapping != null && currentMapping.SegmentToFamilyMapping.ContainsKey(SegmentId) &&
                        currentMapping.SegmentToFamilyMapping[SegmentId] != null && currentMapping.SegmentToFamilyMapping[SegmentId].Count > 0)
                    {
    %>
    <div class='row'>
        <div class='col-12'>
            <div class='section-description'>
                <% if (UserIncoMappings.Count == 1)
                    {%>
                <br />
                <%}%>
                <h3 class='title'><a href='<%=string.Format(productFamilyPageURL, currentMapping.URLFriendlyName)%>'><%=currentMapping.UserInco%></a></h3>
            </div>
        </div>
    </div>
    <% 
        foreach (string familyName in currentMapping.SegmentToFamilyMapping[SegmentId])
        {
            objProductFamilyList = objProductFamilyListContainer.Where(productFamily => productFamily.UrlFriendlyName.ToLower() == familyName.ToLower()).ToList<FamilyAndProduct>();
            objProductFamilyList = (from currentProductFamily in objProductFamilyList
                                    join articleNumber in currentMapping.SegmentToArticleMapping[SegmentId]
                                        on currentProductFamily.ArticleNumber equals articleNumber
                                    select currentProductFamily).ToList<FamilyAndProduct>();

    %>
    <% 
        if (objProductFamilyList != null && objProductFamilyList.Count > 0)
        {
            var groupedProductFamily = objProductFamilyList.GroupBy(item => item.CategoryObjectID);

            foreach (var item in groupedProductFamily)
            {
    %>
    <div class="vspace-md">
        <!--Family benefit start-->
        <div class='row'>
            <div class='col-12'>
                <div class='section-description'>
                    <%int benifitCount = 1; if (objProductFamilyBenefitList != null && objProductFamilyBenefitList.Count > 0)
                        { %>
                    <%--<h3 class='title'><a href='<%=productFamilyPageURL %>?ID=<%=item.Key.ToString() %>'><%=item.First().CategoryName %></a></h3>--%>

                    <h3 class='title'><a href='<%=string.Format(productFamilyPageURL, currentMapping.URLFriendlyName+"/"+ item.First().UrlFriendlyName.ToString())%>'><%=item.First().CategoryName%></a></h3>
                    <ul>
                        <%foreach (SCA.TCW.Men.Data.FamilyBenifit fb in objProductFamilyBenefitList)
                            {
                                if (fb.CategoryObjectID == item.Key)
                                { %>

                        <%if (!String.IsNullOrEmpty(Convert.ToString(fb.familyBenifit)))
                            {%>
                        <li>
                            <%=fb.familyBenifit.ToString()%>
                        </li>
                        <% benifitCount++;
                                    }
                                    if (benifitCount >= 4)
                                        break;

                                }
                            }%>
                    </ul>
                    <%} %>
                </div>
            </div>
        </div>
        <!--Family benefit end-->

        <!-- Products Div start-->
        <div class='row flex-row vspace-sm'>
            <%int productCount = 1; foreach (SCA.TCW.Men.Data.FamilyAndProduct product in objProductFamilyList)
                {
                    if (product.CategoryObjectID == item.Key)
                    {

            %>
            <div class='col-4 col-sm-6 col-xxs-12'>
                <div class='component component-product-box'>
                    <div class='product-box product-compact'>
                        <div class='product-box-content'>

                            <!-- Image div-->
                            <div class='product-box-img'>
                                <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><img src='<%=product.AssetUrl %>' alt=''></a>--%>
                                <a href='<%=string.Format(productDetailPageURL, currentMapping.URLFriendlyName + "/"+product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'>
                                    <img class="lazyload" data-src='<%=product.AssetUrl%>' alt='<%= product.ProductImageAltText %>'
                                        title='<%= product.ProductImageAltText %>'></a>
                            </div>

                            <!-- Text div-->
                            <div class='product-box-text'>
                                <%--<h3 class='title'><a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><%=product.LongName %></a></h3>--%>
                                <h3 class='title'><a href='<%=string.Format(productDetailPageURL, currentMapping.URLFriendlyName + "/"+product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'><%=product.LongName%></a></h3>
                                <%=Helper.GeneratePowerReviewHtml("div" + product.ArticleNumber + productCount + currentMapping.URLFriendlyName + product.UrlFriendlyName, product.ArticleNumber, ReviewSnippetType.Category) %>
                                <div class='text'>
                                    <p><%=(product.DescriptionHeading != null && product.DescriptionHeading.ContainsKey(SegmentId)) ? product.DescriptionHeading[SegmentId] : string.Empty%></p>
                                </div>
                            </div>




                            <!-- Call to action div -->
                            <div class='product-box-btn align-text-left'>
                                <div class='component component-btn'>
                                    <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>' class='btn'>--%>
                                    <a href='<%=string.Format(productDetailPageURL, currentMapping.URLFriendlyName + "/"+product.UrlFriendlyName, product.ProductUrlFriendlyName)%>' class='btn'>
                                        <span><%=productCallToActionLabelText%> </span>
                                        <i class='icon-button-right'></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <%if (productCount == 3)
                            break;
                        productCount++;
                    }
                } %>
        </div>
        <!-- Products Div end-->

        <!-- Call to action to family page start-->
        <%int productCountForFamily = objProductFamilyList.Count(product => product.CategoryObjectID == item.Key);

            if (productCountForFamily > familyProductsCount)
            { %>
        <div class='row vspace-sm'>
            <div class='col-12 align-text-center'>
                <div class='component component-btn'>
                    <%--<a href='<%=productFamilyPageURL %>?ID=<%=item.Key.ToString() %>' class='btn btn-dark btn-hollow align-center'>--%>
                    <a href='<%=string.Format(productFamilyPageURL, currentMapping.URLFriendlyName + "/"+ item.First().UrlFriendlyName.ToString())%>' class='btn btn-dark btn-hollow align-center'>
                        <span><%=familyCallToActionLabelText%></span>
                        <i class='icon-button-right'></i>
                    </a>
                </div>
            </div>
        </div>
        <%} %>
        <!-- Call to action to family page end-->
    </div>
    <%} %>
    <%} %>


    <%}
                    }
                }
            }
        }
        else
        { %>
    <% 
        if (objProductFamilyList != null && objProductFamilyList.Count > 0)
        {
            var groupedProductFamily = objProductFamilyList.GroupBy(item => item.CategoryObjectID);
            foreach (var item in groupedProductFamily)
            {
    %>
    <div class="vspace-md">
        <!--Family benefit start-->
        <div class='row'>
            <div class='col-12'>
                <div class='section-description'>
                    <%int benifitCount = 1; if (objProductFamilyBenefitList != null && objProductFamilyBenefitList.Count > 0)
                        { %>
                    <%--<h3 class='title'><a href='<%=productFamilyPageURL %>?ID=<%=item.Key.ToString() %>'><%=item.First().CategoryName %></a></h3>--%>

                    <h3 class='title'><a href='<%=string.Format(productFamilyPageURL, item.First().UrlFriendlyName.ToString())%>'><%=item.First().CategoryName%></a></h3>
                    <ul>
                        <%foreach (SCA.TCW.Men.Data.FamilyBenifit fb in objProductFamilyBenefitList)
                            {
                                if (fb.CategoryObjectID == item.Key)
                                { %>

                        <%if (!String.IsNullOrEmpty(Convert.ToString(fb.familyBenifit)))
                            {%>
                        <li>
                            <%=fb.familyBenifit.ToString()%>
                        </li>
                        <% benifitCount++;
                                    }
                                    if (benifitCount >= 4)
                                        break;

                                }
                            }%>
                    </ul>
                    <%} %>
                </div>
            </div>
        </div>
        <!--Family benefit end-->

        <!-- Products Div start-->
        <div class='row flex-row vspace-sm'>
            <%int productCount = 1; foreach (SCA.TCW.Men.Data.FamilyAndProduct product in objProductFamilyList)
                {
                    if (product.CategoryObjectID == item.Key)
                    {
            %>

            <div class='col-4 col-sm-6 col-xxs-12'>
                <div class='component component-product-box'>
                    <div class='product-box product-compact'>
                        <div class='product-box-content'>

                            <!-- Image div-->
                            <div class='product-box-img'>
                                <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><img src='<%=product.AssetUrl %>' alt=''></a>--%>
                                <a href='<%=string.Format(productDetailPageURL, product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'>
                                    <img class="lazyload" data-src='<%=product.AssetUrl%>' alt='<%= product.ProductImageAltText %>'
                                        title='<%= product.ProductImageAltText %>'></a>
                            </div>

                            <!-- Text div-->
                            <div class='product-box-text'>
                                <%--<h3 class='title'><a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><%=product.LongName %></a></h3>--%>
                                <h3 class='title'><a href='<%=string.Format(productDetailPageURL, product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'><%=product.LongName%></a></h3>
                                <%=Helper.GeneratePowerReviewHtml("div" + product.ArticleNumber + productCount + product.UrlFriendlyName, product.ArticleNumber, ReviewSnippetType.Category) %>

                                <div class='text'>
                                    <p><%=(product.DescriptionHeading != null && product.DescriptionHeading.ContainsKey(SegmentId)) ? product.DescriptionHeading[SegmentId] : string.Empty%></p>
                                </div>
                            </div>



                            <!-- Call to action div -->
                            <div class='product-box-btn align-text-left'>
                                <div class='component component-btn'>
                                    <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>' class='btn'>--%>
                                    <a href='<%=string.Format(productDetailPageURL, product.UrlFriendlyName, product.ProductUrlFriendlyName)%>' class='btn'>
                                        <span><%=productCallToActionLabelText%> </span>
                                        <i class='icon-button-right'></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <%if (productCount == 3 && (groupedProductFamily.Count() > 1))
                            break;
                        productCount++;
                    }
                } %>
        </div>
        <!-- Products Div end-->

        <!-- Call to action to family page start-->
        <%int productCountForFamily = objProductFamilyList.Count(product => product.CategoryObjectID == item.Key);

            if (productCountForFamily > familyProductsCount && (groupedProductFamily.Count() > 1))
            { %>
        <div class='row vspace-sm'>
            <div class='col-12 align-text-center'>
                <div class='component component-btn'>
                    <%--<a href='<%=productFamilyPageURL %>?ID=<%=item.Key.ToString() %>' class='btn btn-dark btn-hollow align-center'>--%>
                    <a href='<%=string.Format(productFamilyPageURL, item.First().UrlFriendlyName.ToString())%>' class='btn btn-dark btn-hollow align-center'>
                        <span><%=familyCallToActionLabelText%></span>
                        <i class='icon-button-right'></i>
                    </a>
                </div>
            </div>
        </div>
        <%} %>
        <!-- Call to action to family page end-->
    </div>
    <%} %>
    <%} %>
    <%} %>
</div>
<%}
    else
    {%>
<div id="SingleFamilyHTML" runat="server">
    <% var groupedProductFamily = objProductFamilyList.GroupBy(item => item.CategoryObjectID);

        if (IsUserIncoEnabled)
        {
            objProductFamilyListContainer = objProductFamilyList;
            objProductFamilyList = objProductFamilyListContainer.Where(productFamily => productFamily.UrlFriendlyName.ToLower() == CategoryName.ToLower()).ToList<FamilyAndProduct>();
            objProductFamilyList = (from currentProductFamily in objProductFamilyList
                                    join articleNumber in UserIncoMappings[0].SegmentToArticleMapping[SegmentId]
                                        on currentProductFamily.ArticleNumber equals articleNumber
                                    select currentProductFamily).ToList<FamilyAndProduct>();

            groupedProductFamily = objProductFamilyList.GroupBy(item => item.CategoryObjectID);
    %>

    <div class="row">

        <div class="col-12">
            <div class="component component-intro">
                <div class="intro-content  has-preamble">
                    <div class="intro-text-content text-content col-12 col-md-10 col-sm-11 col-xs-12">
                        <h1 class="intro-title section-title"><a href='<%=string.Format(productFamilyPageURL, UserIncoMappings[0].URLFriendlyName)%>'><%=UserIncoMappings[0].UserInco%></a></h1>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <% 
        }
        else
        { %>

    <% 
            if (objProductFamilyList != null && objProductFamilyList.Count > 0)
            {
                groupedProductFamily = objProductFamilyList.GroupBy(item => item.CategoryObjectID);
            }
        }
    %>
    <!-- Top section start-->
    <div class='section vspace-sm'>
        <div class='container'>
            <div class='row'>
                <div class='col-12'>
                    <div class='component component-intro'>
                        <div class='intro-content  has-preamble'>
                            <div class='intro-text-content text-content col-8 col-md-10 col-sm-11 col-xs-12'>
                                <h1 class='intro-title'>
                                    <%=(objProductFamilyList != null && objProductFamilyList.Count > 0 ) ? objProductFamilyList[0].CategoryName : string.Empty%></h1>
                                <!-- Modified by Remaining target group | Fetch the product Family description based on SegmentId -->
                                <p><%=(objProductFamilyList != null && objProductFamilyList.Count > 0 && objProductFamilyList[0].CategoryDesc != null && objProductFamilyList[0].CategoryDesc.ContainsKey(SegmentId)) ? objProductFamilyList[0].CategoryDesc[SegmentId] : string.Empty%></p>
                                <!-- End - Modified by Remaining target group -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top section end-->

    <!-- Benefits and drops start-->
    <div class='section vspace-sm'>
        <div class='container'>
            <div class='row'>
                <!-- Benefits-->
                <%if (objProductFamilyBenefitList != null && objProductFamilyBenefitList.Count > 0 && objProductFamilyBenefitList.Any(x => !string.IsNullOrEmpty(x.familyBenifit)))
                    {   %>

                <div class='col-7 col-xs-7 col-xxs-12 vspace-xs-sm'>
                    <div class='text-content'>
                        <div class='listing-title'><b><%=benifitLabelText%></b></div>
                        <ul>
                            <%foreach (SCA.TCW.Men.Data.FamilyBenifit fb in objProductFamilyBenefitList)
                                { %>

                            <%if (!String.IsNullOrEmpty(Convert.ToString(fb.familyBenifit)))
                                {%>
                            <li>
                                <%=fb.familyBenifit.ToString()%>
                            </li>
                            <% } %>

                            <%} %>
                        </ul>
                    </div>
                </div>

                <%} %>

                <!-- Drops-->

                <%
                    List<double> absorbancy = new List<double>();
                    double minAbsorbancy = 0, maxAbsorbancy = 0;

                    for (int i = 0; i < objProductFamilyList.Count; i++)
                    {

                        if (objProductFamilyList[i].AbsorbencyValue != null && objProductFamilyList[i].AbsorbencyValue.ContainsKey(SegmentId) && objProductFamilyList[i].AbsorbencyValue[SegmentId] > 0)
                        {
                            absorbancy.Add(Math.Ceiling(objProductFamilyList[i].AbsorbencyValue[SegmentId]));
                        }
                        if (i == 0)
                        {
                            int tempAbsorbencyContainer;
                            MaxAbsorbancyToDisplay = intMaxAbsorbancyDroplets;
                            if (objProductFamilyList[i].Absorbencycode != null && objProductFamilyList[i].Absorbencycode.ContainsKey(SegmentId)
                                && (!string.IsNullOrEmpty(objProductFamilyList[i].Absorbencycode[SegmentId])) && objProductFamilyList[i].Absorbencycode[SegmentId].ToLower().Contains("dot")
                                && (!string.IsNullOrEmpty(MaxAbsorbancyDots)) && int.TryParse(MaxAbsorbancyDots, out tempAbsorbencyContainer))
                            {
                                MaxAbsorbancyToDisplay = tempAbsorbencyContainer;
                            }
                        }
                    }
                    if (absorbancy.Count > 0)
                    {
                        absorbancy.Sort();
                        minAbsorbancy = absorbancy[0];
                        maxAbsorbancy = absorbancy[absorbancy.Count - 1];
                    }
                    else if (absorbancy.Count == 1)
                    {
                        minAbsorbancy = absorbancy[0];
                        maxAbsorbancy = absorbancy[0];
                    }

                    if (minAbsorbancy > 0)
                    {
                %>
                <div class='col-3 col-push-2 col-md-4 col-md-push-1 col-xs-push-0 col-xs-5 col-xxs-12'>
                    <div class='crate top-padding'>
                        <div class='crate-content align-text-center'>
                            <h2 class='crate-title-xsmall-upper'><%=absorbancyRangeLabelText%></h2>
                            <div class='rating-range'>
                                <ul class='rating rating-lg'>
                                    <%int lastCount = Convert.ToInt32(Math.Ceiling(minAbsorbancy));
                                        int restAbsorcount = MaxAbsorbancyToDisplay - lastCount;
                                       
                                        for (int j = 1; j <= lastCount; j++)
                                        {
                                            if (j < lastCount)
                                            {%>
                                    <li><i class='icon-drop-full'></i></li>
                                    <%}
                                        else
                                        {
                                            if (j == lastCount && Convert.ToDouble(lastCount) - minAbsorbancy > 0)
                                            { %>
                                    <i class='icon-drop-half-full'></i>
                                    <%}
                                        else
                                        { %>
                                    <li><i class='icon-drop-full'></i></li>
                                    <%} %>

                                    <%} %>
                                 
                                    <%} %>
                                    <%for (int j = lastCount + 1; j <= MaxAbsorbancyToDisplay; j++)
                                        {%>
                                    <li><i class='icon-drop-empty'></i></li>
                                   
                                    <% } %>
                                </ul>

                                <p><%=absorbancyRange_ToLabelText%></p>
                                <%} %>
                                <%if (maxAbsorbancy > 0)
                                    {
                                        int lastCount = Convert.ToInt32(Math.Ceiling(maxAbsorbancy));
                                        int restAbsorcount = MaxAbsorbancyToDisplay - lastCount;
                                 %>

                                <ul class='rating rating-lg'>
                                    <%for (int j = 1; j <= lastCount; j++)
                                        {
                                            if (j < lastCount)
                                            { %>
                                    <li><i class='icon-drop-full'></i></li>
                                    <%}
                                        else
                                        {
                                            if (j == lastCount && Convert.ToDouble(lastCount) - maxAbsorbancy > 0)
                                            {%>
                                    <i class='icon-drop-half-full'></i>
                                    <%}
                                        else
                                        {%>
                                    <li><i class='icon-drop-full'></i></li>
                                    <%} %>
                                    <%} %>
                                    <%} %>
                                    <%for (int j = lastCount + 1; j <= MaxAbsorbancyToDisplay; j++)
                                        {%>
                                        <li><i class='icon-drop-empty'></i></li>
                                    <% } %>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>
    <!-- Benefits and drops end-->

    <!-- Products start-->
    <div class='section vspace-md'>
        <div class='container'>
            <div class='row flex-row vspace-sm'>
                <%int productCount = 0; foreach (SCA.TCW.Men.Data.FamilyAndProduct product in objProductFamilyList)
                    {
                        productCount = productCount + 1;
                %>

                <div class='col-4 col-sm-6 col-xxs-12'>
                    <div class='component component-product-box'>
                        <div class='product-box product-compact'>
                            <div class='product-box-content'>
                                <!--Image-->
                                <div class='product-box-img'>
                                    <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><img src='<%=product.AssetUrl %>' alt=''></a>--%>
                                    <a href='<%=string.Format(productDetailPageURL, IsUserIncoEnabled?UserInco+"/"+ product.UrlFriendlyName:product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'>
                                        <img class="lazyload" data-src='<%=product.AssetUrl%>' alt='<%= product.ProductImageAltText %>'
                                            title='<%= product.ProductImageAltText %>'></a>
                                </div>

                                <!--Text-->
                                <div class='product-box-text'>
                                    <%--<h3 class='title'><a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>'><%=product.LongName %></a></h3>--%>
                                    <h3 class='title'><a href='<%=string.Format(productDetailPageURL,IsUserIncoEnabled?UserInco+"/"+ product.UrlFriendlyName:product.UrlFriendlyName, product.ProductUrlFriendlyName)%>'><%=product.LongName%></a></h3>
                                    <%=Helper.GeneratePowerReviewHtml("div" + product.ArticleNumber + productCount + product.UrlFriendlyName, product.ArticleNumber, ReviewSnippetType.Category) %>


                                    <div class='text'>
                                        <!-- Modified by Remaining target group | Fetch the product description based on SegmentId -->
                                        <p><%=(product.DescriptionHeading != null && product.DescriptionHeading.ContainsKey(SegmentId)) ? product.DescriptionHeading[SegmentId] : string.Empty%></p>
                                        <!-- End - Modified by Remaining target group -->
                                    </div>
                                </div>




                                <!-- Call to action-->
                                <div class='product-box-btn align-text-left'>
                                    <div class='component component-btn'>
                                        <%--<a href='<%=productDetailPageURL %>?ID=<%=product.ArticleNumber %>' class='btn'>--%>
                                        <a href='<%= string.Format(productDetailPageURL, IsUserIncoEnabled?UserInco+"/"+ product.UrlFriendlyName:product.UrlFriendlyName, product.ProductUrlFriendlyName)%>' class='btn'>
                                            <span><%=productCallToActionLabelText%></span>
                                            <i class='icon-button-right'></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <%} %>
            </div>
        </div>
    </div>
    <!-- Products end-->

</div>
<%     }%>
