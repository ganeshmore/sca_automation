﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductList.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.ProductList" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<%@ Import Namespace="SCA.TCW.Men.Data" %>
<div id="productListHTML" runat="server">
    <div class="flex-row">
    <asp:Repeater ID="productList" runat="server">
        <ItemTemplate>
            <%if (totalCount == 2)
              { %>
            <div class='col-6 col-sm-6 col-xs-12 col-xxs-12'>
                <%}
              else if (totalCount == 1)
              { %>
                <div class='col-12 col-sm-6 col-xs-12 col-xxs-12'>
                    <%}
              else
              { %>
                    <div class='col-4 col-sm-6 col-xs-12 col-xxs-12'>
                        <%} %>
                        <div class='component component-product-promo'>
                            <div class='product-promo'>
                                <div class='product-promo-content'>

                                    <div class='product-promo-img-wrapper'>
                                        <div class='product-promo-img'>
                                            <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, (IsUserIncoEnabled? Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString( Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"): Eval("FamilyURLFriendlyName")), Eval("URLFriendlyName"))%>'>
                                            
                                                    <img class="lazyload" data-src=' <%# Eval("AssetUrl") %>' alt=''>
                                            </a>
                                        </div>
                                    </div>

                                    <div class='product-promo-text'>
                                        <div class='text'>
                                            <p><a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL,  (IsUserIncoEnabled? Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString( Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"): Eval("FamilyURLFriendlyName")), Eval("URLFriendlyName")) %>' class='link'><%# Eval("LongName") %></a></p>
                                             <%# Helper.GeneratePowerReviewHtml("div"  + Convert.ToString(Eval("ArticleNumber") ) + Container.ItemIndex  ,Convert.ToString(Eval("ArticleNumber") ),ReviewSnippetType.Category ) %>
                                            <!-- Modified by Remaining Target Groups Team -  Get the product descriptions based on segment-->
                                            <p><%# SCA.TCW.Men.Utility.Helper.GetSegmentWideCopy(Eval("DescriptionHeadlines") as Dictionary<int,string>,SegmentId) %></p>
                                            <!-- End - Modified by Remaining Target Groups Team -  -->
                                        </div>
                                    </div>

                                   
                                    
                                    <div class='product-promo-btn'>
                                        <div class='component component-btn'>
                                            <a href='<%=  Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL,(  IsUserIncoEnabled? Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString( Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"): Eval("FamilyURLFriendlyName")), Eval("URLFriendlyName")) %>' class='btn btn-no-icon' target='_self'>
                                                <span><%#productLink_LabelText %> </span>
                                                <i class='icon-button-right'></i>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
        </ItemTemplate>

    </asp:Repeater>
        </div>
</div>


