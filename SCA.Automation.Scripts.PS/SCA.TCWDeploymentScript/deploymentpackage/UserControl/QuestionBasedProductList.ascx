﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionBasedProductList.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.QuestionBasedProductList" %>
<%@ Import Namespace="SCA.TCW.Men.Utility" %>
<%@ Import Namespace="SCA.TCW.Men.Data" %>

<asp:Repeater ID="questionBasedProductList" runat="server">
    <ItemTemplate>
        <%if (totalProdCount > 0)
          { %>

        <% if (ColCount % 2 == 0)
           { %>
        <div class="accordion-content">
            <div class='accordion-content-inner flex-row'>
                <%} %>
                <div class='col-6 col-xs-12'>
                    <div class='component component-product-box'>
                        <div class='product-box product-compact'>
                            <div class='product-box-content'>
                                <div class='product-box-img'>
                                    <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL,IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString(Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"):Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>'>
                                        <img class="lazyload" data-src='<%# Eval("AssetUrl") %>' alt=''>
                                    </a>
                                </div>
                                <div class='product-box-text'>
                                    <h3 class='title'>
                                        <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString(Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"):Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>'><%# Eval("LongName") %></a>
                                    </h3>
                                   <%# Helper.GeneratePowerReviewHtml("div"  + Convert.ToString(Eval("ArticleNumber") ) + Container.ItemIndex + this.controlID  ,Convert.ToString(Eval("ArticleNumber") ),ReviewSnippetType.Category ) %>

                                     <div class='text'>
                                          <!-- Modified by Remaining Target Groups Team -  Get the product descriptions based on segment-->
                                        <p><%# SCA.TCW.Men.Utility.Helper.GetSegmentWideCopy(Eval("Descripton") as Dictionary<int,string>,SegmentId) %></p>
                                         <!-- End - Modified by Remaining Target Groups Team -  -->
                                    </div>
                                </div>
                                                                       
                                <div class='product-box-btn align-text-left'>
                                    <div class='component component-btn'>
                                        <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, IsUserIncoEnabled?Helper.GetUserIncoByFamily(UserIncoToFamilyMappings,Convert.ToString(Eval("FamilyURLFriendlyName")),SegmentId)+"/"+ Eval("FamilyURLFriendlyName"):Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>' target='_self' class='btn'>
                                            <span><%#productLink_LabelText %></span>
                                            <i class='icon-button-right'></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <% ColCount++; if (ColCount % 2 == 0 || ColCount == totalProdCount)
                   { %>
            </div>
        </div>
        <%} %>

        <%}
          else
          {
              ContentPlaceHolder myPlaceHolder = (ContentPlaceHolder)Page.Master.FindControl("ContentPlaceHolder1");
              HtmlGenericControl myControl = (HtmlGenericControl)myPlaceHolder.FindControl(controlID);
              myControl.Style.Add("display", "none");
          }%>
    </ItemTemplate>
</asp:Repeater>

