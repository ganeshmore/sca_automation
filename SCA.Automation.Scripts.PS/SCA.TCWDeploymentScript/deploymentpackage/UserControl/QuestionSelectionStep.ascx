﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuestionSelectionStep.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.QuestionSelectionStep" %>

<script type="text/javascript">
    var currentStepNo = '<%=currentStepNo %>';
    var currentStoryType = '<%=StoryType %>'; 
    //Modified by Remaining target group | Added variable to check the second last step
    var isSecondLastStep = '<%=IsSecondLastStep %>';
</script>
<asp:HiddenField ID="hdnSelectedQuAnsString" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hdnTextAreaString" runat="server" ClientIDMode="Static" />
<!-- Modified by Remaining target group - Added hidden fields to store the selected size value -->
<asp:HiddenField ID="hdnSelectedSizeValue" EnableViewState="true" ClientIDMode="Static" runat="server" />
<% string btnClass=""; %>
<% if (IsActiveStep == true && objSampleQuestions != null)
   { %>
<input type="hidden" id="currenet-step-no" value="<%=currentStepNo %>/<%= totalStepCount %>" />
<!-- start component: question-box -->
<div class="component component-question-box">
    <%if(IsQuestionsMandatory){ btnClass="btn-inactive"; %>
    <div class="question-box question-box-v2" >
    <%}else{ btnClass=""; %>
     <div class="question-box question-box-v2" data-optional="1">
    <%} %>
    
        <div class="crate manual-padding small-padding vspace-sm">
            <div class="crate-content">
                <h2 class="crate-title no-top-padding"><%=(objSampleQuestions.SelectionText != null) ? ((objSampleQuestions.SelectionText.RTF != null) ? objSampleQuestions.SelectionText.RTF : string.Empty) : string.Empty %></h2>
                <!-- Questions -->
                <!--
		First question ('question-box-item') should have the class 'active'
		if not all questions are already answered on page load, i.e. user have
		answered the questions, submitted and page have reloaded, AND a results page exist.
		If no results page exist, then first item should always have the 'active' class
	-->
                <% if (objSampleQuestions.QuestionAndAnswer != null && objSampleQuestions.QuestionAndAnswer.Count > 0)
                   { %>
                <% bool active = true; Int32 count = 0; %>
                <% List<SCA.TCW.Men.Data.QuestionAndAnswer> listItems = objSampleQuestions.QuestionAndAnswer; %>
                <% foreach (SCA.TCW.Men.Data.QuestionAndAnswer itemQuestionAnswer in listItems)
                   {
                       string questionClass = "question-box-item form-field";
                       count = count + 1; %>

                <% if (active == true)
                   {
                       questionClass = questionClass + " active";
                       active = false;
                   }
                   else if (listItems.Count == count)
                   {
                       questionClass = questionClass + " last";
                   }                                           
                %>
                <% string answerOptionClass=""; %>
                <% answerOptionClass=IsMultipleSelectionAllowed?"no-continue optional":"optional"; %>

                <div class="<%= questionClass %>">
                    <h3 class="question-box-item-title" data-questionid="<%= itemQuestionAnswer.QuestionId.ToString() %>"><%= (itemQuestionAnswer.QuestionId+1).ToString()+"." %> <%= itemQuestionAnswer.Question %></h3>
                    <fieldset>
                        <% foreach (SCA.TCW.Men.Data.Answer itemAns in itemQuestionAnswer.Answers)
                           { %>
                        <div class="field field-radio">
                            <% string itemChecked = (itemAns.Selected) ? "checked=\"checked\"" : string.Empty; %>
                            <!--Modified by Remaining target group | Added condition for multi selection of questions-->
                                <%if (itemQuestionAnswer.IsMultiSelection == "multi")
                              { %>
                            <input type="checkbox" <%= itemChecked %> name="question-<%= itemQuestionAnswer.QuestionId.ToString() %>[]" class="<%= answerOptionClass %> qa-<%= itemQuestionAnswer.QuestionId  + 1 %> checkbox-button-align" id="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>" value="<%= itemAns.AnswerId.ToString() %>">
                                <label for="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>"><i class="icon-radiobutton-outer_circle"></i><%= itemAns.AnswerString %></label>
                            <%}
                                  else if (itemQuestionAnswer.IsMultiSelection == "text")
                                  { %>
                                <textarea class="txtArea <%= answerOptionClass %> qa-<%= itemQuestionAnswer.QuestionId + 1 %>" name="question-<%= itemQuestionAnswer.QuestionId.ToString() %>[]" class="field field-radio" id="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>" value="<%= itemAns.AnswerId.ToString() %>" cols="65"></textarea>
                                <%} %>
                                <%
                              else
                              { %>
                            <input type="radio" class="<%= answerOptionClass %> qa-<%= itemQuestionAnswer.QuestionId + 1 %> radio-button-align" name="question-<%= itemQuestionAnswer.QuestionId.ToString() %>[]" class="field field-radio" id="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>" value="<%= itemAns.AnswerId.ToString() %>">
                                <label for="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>"><i class="icon-radiobutton-outer_circle"></i><%= itemAns.AnswerString %></label>
                            <%} %>
                                <!--<p class="para_align_text">
                                    <label for="option-<%= itemQuestionAnswer.QuestionId.ToString() %>-<%= itemAns.AnswerId.ToString() %>"><i class="icon-radiobutton-outer_circle"></i><%= itemAns.AnswerString %></label>
                                </p>-->
                            <!--End - Modified by Remaining target group -->
                        </div>
                        <%} %>
                    </fieldset>
                </div>
                <%} %>
                <%} %>
                <!-- Results page -->
                <!-- Should have the class 'active' if page is loaded and all questions are answered -->

                <!-- Steps -->
                <div class="vspace-lg">
                    <% if (objSampleQuestions.QuestionAndAnswer != null && objSampleQuestions.QuestionAndAnswer.Count > 0)
                       { %>
                    <ol class="question-box-steps steps-<%=objSampleQuestions.QuestionAndAnswer.Count.ToString() %> horizontal-list">
                        <% bool active = true; Int32 count = 0; %>
                        <% List<SCA.TCW.Men.Data.QuestionAndAnswer> listItems = objSampleQuestions.QuestionAndAnswer; %>
                        <% foreach (SCA.TCW.Men.Data.QuestionAndAnswer itemQuestionAnswer in listItems)
                           { %>
                        <% string doneString = (IsAnswerSelected(itemQuestionAnswer.QuestionId) == true || (!IsQuestionsMandatory)) ? "done" : string.Empty; %>
                        <% if (active == true)
                           { %>
                        <li class="question-box-step active <%= doneString %>">
                            <% active = false;
                           }
                           else
                           { %><li class="question-box-step <%= doneString %>">
                               <%} %>
                               <span class="question-box-step-indicator" data-question="<%= count %>"><%= count + 1 %></span>
                               <% count = count + 1;  %>
                           </li>
                        <%} %>
                    </ol>
                    <%} %>
                </div>
                <input type="hidden" name="questionsAnswered" value="true">
                <%if (CheckMultiSelectionExist() == true)
                  { %>
                
                <a class="btn btn-check-next btn-no-icon align-right btn-hide <%= btnClass%>"><%= NextBtnText %></a>
                <%} %>
            </div>
        </div>
    
 <%if(IsQuestionsMandatory){ %>
    </div>
    <%}else{ %>
     </div>
    <%} %>

    <!-- Standard submit -->
    <div class="question-box-submit align-text-right">
        <% if (isFirst)
           { %>
        <!-- start component: btn -->
        <div class="component component-btn">
            <a id="btnPrevious" href="<%=Request.Url.ToString()%>" class="btn btn-transparent btn-no-icon align-left vspace-xs-xs" runat="server" onserverclick="btnPrevious_ServerClick">
                <span><%=SamplePreviousBtnText%></span>
                <i class="icon-button-right"></i>
            </a>
        </div>
        <!-- end component: btn -->
        <%} %>
      
        <button type="submit" runat="server" name="qsubmit" onclick="if ((typeof SampleSizeValidation ==='function') && (SampleSizeValidation()>0)) return false;" onserverclick="btnSubmit_Click"  id="question_submit" data-sampleorcoupon="yes" >
            
            <span><%=SampleQuestionBtnText%></span>
            <i class="icon-long-right-arrow"></i>
        </button>
      </div>
</div>
        
<!-- end component: question-box -->
<%} %>
