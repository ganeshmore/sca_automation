﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SCAsearch.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.SCAsearch" %>

<div style="display:none">

    <asp:Button ID="btnAll" runat="server" Text="All" OnClick="btnAll_Click" />
    
    <asp:Button ID="btnProduct" runat="server" Text="Product" OnClick="btnProduct_Click" />
    
    <asp:Button ID="btnArticle" runat="server" Text="Article" OnClick="btnArticle_Click" />
</div>
<div class="container">
    <div class="row"></div>
    <div class="row vspace-md">
        <div class="component component-search-result">

            <div class="search-results">
                <asp:Repeater ID="ProductRepeater" runat="server" OnItemDataBound="ProductRepeater_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-12">
                            <div class="search-result odd">
                                
                                <div class="col-9 col-xxs-12 search-result-content">
                                    <div class="content-tags" style="display:none"><a href="#"><i class="icon icon-tag"></i><strong><%#Eval("CategoryName") %></strong></a></div>
                                    <h3 class="search-result-title"><a href='<%#Eval("Url") %>'><%#Eval("ProductName") %></a></h3>
                                    <p class="search-result-text"><%#Eval("Description") %></p>
                                </div>

                                <div class="search-result-right"></div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>

        </div>
    </div>
</div>

<div>
    <asp:Label ID="lblresultmsg" runat="server" Visible="false"></asp:Label>
</div>
<div  class="component component-pagination">
    <div class="pagination">
    <asp:LinkButton ID="lnkPrev_page" runat="server" OnClick="lnkPrev_page_OnClick" Text="<" Font-Bold="true"></asp:LinkButton>&nbsp;&nbsp;
        <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand" OnItemDataBound="rptPaging_ItemDataBound">
            <ItemTemplate>
                <div class="num">
                    <asp:LinkButton ID="btnPage"  CommandName="Page" CommandArgument="<%# Container.DataItem %>" runat="server"><%# Container.DataItem %></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    &nbsp;&nbsp;
    <asp:LinkButton ID="lnkNext_Page" runat="server" OnClick="lnkNext_Page_OnClick" Text=">" Font-Bold="true"></asp:LinkButton>
</div>
    </div>
