﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SampleSelectedItemDetail.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.SampleSelectedItemDetail" %>
<script type="text/javascript">
   
    var selectedItemName = '<%= itemSampleProducts.LongName %>'
</script>
<% if (itemSampleProducts != null && !string.IsNullOrEmpty(itemSampleProducts.ArticleNumber))
   { 
    string toolTip=string.IsNullOrEmpty(itemSampleProducts.LongName)?string.Empty:itemSampleProducts.LongName.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
    %>



<div class="col-12 col-sm-6 col-xs-12 vspace-sm">
    <h2 class="section-title"><%= SelectedProductText %></h2>
    <!-- start component: product-box -->
    <div class="component component-product-box">
        <div class="product-box product-compact no-border">
            <div class="product-box-content align-height">
                <div class="product-box-img">                    
                    <img src="<%= itemSampleProducts.AssetUrl %>" alt="<%= toolTip   %>">
                </div>
                <div class="product-box-text">
                    <h3 class="title">
                        <%= itemSampleProducts.LongName %>
                    </h3>
                    <div class="text">
                        <p><%= itemSampleProducts.Descripton %></p>
                    </div>
                    <!-- Code added to render the available sizes drop down -->  
                  <%if (itemSampleProducts.AvailableSizes != null && itemSampleProducts.AvailableSizes.Count > 0)
                      { %>
                                <div class="form-field  col-12 col-xs-12">
                                    <label class="field-label" for="AvailableSizesOfSelectedItem"><%= AddSizesCaption %></label>
                                    <div class="validate validate-select">
                                        <select name="AvailableSizesOfSelectedItem" id="AvailableSizesOfSelectedItem" class="SampleBox-sizeSelect">
                                            <option value=""><%= AvailableSizesCaption %></option>
                                            <%foreach (KeyValuePair<string, string> currentKVP in itemSampleProducts.AvailableSizes)
                                              { %>
                                            <option value="<%=currentKVP.Value %>" <%=currentKVP.Value.ToLower()==SelectedSizeValue.ToLower()?"selected":string.Empty  %>><%=currentKVP.Key %></option>
                                            <%} %>
                                        </select>
                                    </div>

                                    <div class="validate-message validate-message-mandatory" style="display: none;color:red;">
                                        <%= SizeMandatoryMessage %>
                                    </div>
                                </div>

                                <%} %>
                    <!-- End of Code added to render the available sizes drop down -->
                </div>
            </div>
        </div>
    </div>
    <!-- end component: product-box -->
</div>
<%} %>