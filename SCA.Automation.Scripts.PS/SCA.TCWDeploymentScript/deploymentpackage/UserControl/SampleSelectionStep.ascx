﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SampleSelectionStep.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.SampleSelectionStep" %>

<script type="text/javascript">
    var currentStepNo = '<%=currentStepNo %>';
    var currentStoryType = '<%=StoryType %>';   
    //Modified by Remaining target group | Added variable to check the second last step
    var isSecondLastStep = '<%=IsSecondLastStep %>';
</script>
<asp:HiddenField ID="hdnSampleId" EnableViewState="true" ClientIDMode="Static" runat="server" />
<!-- Modified by Remaining target group |Added new hidden field to hold valueof selected size -->
<asp:HiddenField ID="hdnSelectedSizeValue" EnableViewState="true" ClientIDMode="Static" runat="server" />
<% if (IsActiveStep == true)
   { %>

<input type="hidden" id="currenet-step-no" value="<%=currentStepNo %>/<%= totalStepCount %>" />
<div class="crate manual-padding small-padding vspace-sm">
    <div class="crate-content">
        <h3 class="crate-title no-top-padding"><%= (objSampleOrder.SelectionText != null) ? ((objSampleOrder.SelectionText.RTF != null) ? objSampleOrder.SelectionText.RTF : string.Empty) : string.Empty %></h3>
        <div class="row">
            <% Int32 count = 0; %>
            <% bool flagChecked = false; %>
            <% string toolTip = string.Empty; %>
            <% foreach (SCA.TCW.Men.Data.SampleProducts item in objSampleProductList)
               {
                   count = count + 1;
                   toolTip=string.IsNullOrEmpty(item.LongName)?string.Empty:item.LongName.Replace("</br>", string.Empty).Replace("<br>", string.Empty).Replace("<br/>", string.Empty);
                    string currentId = (!string.IsNullOrEmpty(item.KitId)) ? item.KitId : item.ArticleNumber;
                 %>
            <div class="col-12 col-md-6 col-xxs-12 col-full">
                <!-- start component: product-box -->
                <div class="component component-product-box">
                    <div class="product-box product-compact no-border">
                        <div class="product-box-content align-height js-trigger-selection-hook u-cursorPointer">
                            <div class="product-box-img">
                                <img src="<%= item.AssetUrl %>" alt="<%= toolTip %>" title="<%= toolTip %>">
                            </div>
                            <div class="product-box-text">
                                <h3 class="title">
                                    <%= item.LongName %>
                                </h3>
                                <div class="text">
                                    <p><%= item.Descripton %></p>
                                </div>

                                <!-- Modified by Remaining target group | Code added to render the Available sizes drop down-->
                                <%if(item.AvailableSizes!=null && item.AvailableSizes.Count>0){ %>
                                <div class="row js-ignore-selection-hook u-cursorDefault">
                                <div class="form-field  col-6 col-sm-7 col-xs-12 <%= IsSecondLastStep?"validate-mandatory":string.Empty %>">
                                    <label class="field-label" for="AvailableSizesOfSelectedItem"><%= AddSizesCaption %></label>
                                    <div class="validate validate-select">
                                        <select name="AvailableSizesOfSelectedItem" id="AvailableSizesOfSelectedItem" class="SampleBox-sizeSelect" >
                                            <option value=""><%= AvailableSizesCaption %></option>
                                            <%foreach( KeyValuePair<string,string> currentKVP in item.AvailableSizes){ %>
                                            <option value="<%=currentKVP.Value %>"  <%= (currentId==objSampleOrder.SelectedSampleId && currentKVP.Value.ToLower()==SelectedSizeValue.ToLower()) ?"selected":string.Empty  %> ><%=currentKVP.Key %></option>
                                            <%} %>
                                        </select>
                                    </div>

                                    <div class="validate-message validate-message-mandatory" style="display: none;">
                                        <%= SizeMandatoryMessage %>
                                    </div>
                                </div>
                                 </div>
                                <%} %>
                                <!--End - Modified by Remaining target group -->

                            </div>
                            
                            <% string checkedItem = (currentId == objSampleOrder.SelectedSampleId) ? "checked=\"checked\"" : string.Empty; %>
                            <% flagChecked = ((flagChecked == true) ? flagChecked : ((currentId == objSampleOrder.SelectedSampleId) ? true : false)); %>
                            <div class="sample sample-box-btn product-box-select-btn field-hidden-input align-text-right">
                                <%= "<input type=\"radio\" "+checkedItem+" name=\"products\" value=\""+ currentId +"\" id=\"product"+count.ToString()+"\" data-form-activate-on-change=\"#"+Button1.ClientID+"\">" %>
                                <%= "<label for=\"product"+count.ToString()+"\" class=\"btn btn-no-icon btn-dark\">"%>
                                <span class="not-checked"><%=SampleSelectBtnText%></span>
                                <span class="checked"><%=SampleSelectBtnText%></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end component: product-box -->
            </div>
            <%} %>
        </div>
    </div>
</div>
<div class="form-field" style="display: none">
    <% if (isFirst)
       { %>
    <!-- start component: btn -->
    <div class="component component-btn">
        <a id="btnPrevious" href="<%= Request.Url.ToString() %>" class="btn btn-transparent btn-no-icon align-left vspace-xs-xs" runat="server" onserverclick="btnPrevious_ServerClick">
            <span><%=SamplePreviousBtnText%></span>
            <i class="icon-button-right"></i>
        </a>
    </div>
    <%} %>
    <!-- end component: btn -->
    <% if (flagChecked == false)
       {%>
    <input id="Button1" class="btn btn-no-icon field-submit col-xs-12" style="float: right;" value="" type="button" runat="server" onserverclick="btnSubmit_Click" />
    <%}
       else
       {%>
    <input id="Button2" class="btn btn-no-icon field-submit col-xs-12" style="float: right;" value="" type="button" runat="server" onserverclick="btnSubmit_Click" data-sampleorcoupon="yes" />
    <%}%>
</div>
<%} %>