﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchContainer.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.SearchContainer" %>
<%if (IsSeachReq == true)
  { %>
<div class="item item-search">
    <div class="form-field">
        <input type="text" id="txtSearch" placeholder="Search" class="search-input field field-input" runat="server">
    </div>
    <button type="submit" class="search-submit" runat="server" onserverclick="btnSearch_Click">
        <i class="icon-search" title="Search"></i>
    </button>
</div>
<%} %>

