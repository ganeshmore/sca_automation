﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleProductList.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.SimpleProductList" %>
<asp:Repeater ID="simpleProductList" runat="server">
    <ItemTemplate>
        <div class='col-4 col-md-6 col-sm-6 col-xs-12'>
            <div class='component component-product-box'>
                <div class='product-box product-compact'>
                    <div class='product-box-content align-height'>
                        <div class='product-box-img'>
                            <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>'>
                                <img src=' <%# Eval("AssetUrl") %>' alt=''/>
                            </a>
                        </div>

                        <div class='product-box-text'>
                            <h3 class='title'>
                                <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>'>
                                    <%# Eval("LongName") %>
                                </a>
                            </h3>
                            <div class='text'>
                                <p><%# Eval("DescriptionHeadlines") %></p>
                            </div>
                        </div>

                        <div class='product-box-btn align-text-left'>
                            <div class='component component-btn'>
                                <a href='<%= Helper.GetAppsettingKeyValue<string>("Domain") %><%#string.Format(productDetailPageURL, Eval("FamilyURLFriendlyName"), Eval("URLFriendlyName")) %>' class='btn' target='_self'>
                                    <span><%#productLink_LabelText %> </span>
                                    <i class='icon-button-right'></i>
                                </a>
                            </div>                        
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>