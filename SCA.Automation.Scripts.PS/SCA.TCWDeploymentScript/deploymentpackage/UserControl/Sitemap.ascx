﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Sitemap.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Sitemap" %>

<!-- start component: sitemap -->
<div class="component component-sitemap">
    <%--Level start--%>
    <% List<SCA.TCW.Men.Data.ItemsItemItem> SecondLevel = objProducts.GetOrderListOfLevel1(itemvalue.item);%>
    <%if (SecondLevel.Count > 0)
        { %>
    <% foreach (SCA.TCW.Men.Data.ItemsItemItem SecondLevelItems in SecondLevel)
        {
    %>
    <div class="col-3 col-md-4 col-sm-4 col-xs-6 col-xxs-12 vspace-xs align-height" style="height: 268px;">
        <ul class="sitemap-list">
            <li class="sitemap-list-item">
                <h2 class="sitemap-list-title">
                    <% string secondLevelUrlString = SecondLevelItems.url; %>
                    <%if (!string.IsNullOrEmpty(SecondLevelItems.LongURLUsingPageNameOverride))
                        {
                            secondLevelUrlString = SecondLevelItems.LongURLUsingPageNameOverride;
                        }
                        else
                        {
                            secondLevelUrlString = (String.Equals(SecondLevelItems.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? secondLevelUrlString + "/" : secondLevelUrlString;
                        }
                    %>
                    <a href="<%=subDomainValue+ secondLevelUrlString%>" class=""><%= (!string.IsNullOrWhiteSpace(SecondLevelItems.overriddentitle))?SecondLevelItems.overriddentitle: SecondLevelItems.displaytitle %></a>
                </h2>
                <% List<SCA.TCW.Men.Data.ItemsItemItemItem> ListOfLevel3 =objProducts.GetProductsMenuList(SecondLevelItems, productFamilies);%>               

                <% if (ListOfLevel3 != null && ListOfLevel3.Count > 0)
                    { %>
                <% foreach (SCA.TCW.Men.Data.ItemsItemItemItem thirldLevelItem in ListOfLevel3)
                    { %>
                <ul class="sitemap-sub-list">
                    <li class="sitemap-list-item">
                        <% string thirldLevelUrlString = thirldLevelItem.url; %>
                        <%if (!string.IsNullOrEmpty(thirldLevelItem.LongURLUsingPageNameOverride))
                            {
                                thirldLevelUrlString = thirldLevelItem.LongURLUsingPageNameOverride;
                            }
                            else
                            {
                                thirldLevelUrlString = (String.Equals(thirldLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? thirldLevelUrlString + "/" : thirldLevelUrlString;
                            }%>
                        <a href="<%=subDomainValue+thirldLevelUrlString%>" class=""><%= (!string.IsNullOrWhiteSpace(thirldLevelItem.overriddentitle))?thirldLevelItem.overriddentitle: thirldLevelItem.displaytitle %></a>
                        <% List<SCA.TCW.Men.Data.ItemsItemItemItemItem> ListOfLevel4 = objProducts.GetOrderListOfLevel3(thirldLevelItem); %>
                        <% if (ListOfLevel4 != null && ListOfLevel4.Count > 0)
                            { %>
                        <% foreach (SCA.TCW.Men.Data.ItemsItemItemItemItem fourthLevelItem in ListOfLevel4)
                            { %>
                        <ul class="sitemap-sub-list">
                            <li class="sitemap-list-item">
                                <% string fourthLevelUrlString = fourthLevelItem.url; %>
                                <%if (!string.IsNullOrEmpty(fourthLevelItem.LongURLUsingPageNameOverride))
                                    {
                                        fourthLevelUrlString = fourthLevelItem.LongURLUsingPageNameOverride;
                                    }
                                    else
                                    {
                                        fourthLevelUrlString = (String.Equals(fourthLevelItem.type, "StructureGroup", StringComparison.OrdinalIgnoreCase)) ? fourthLevelUrlString + "/" : fourthLevelUrlString;
                                    }
                                %>
                                <a href="<%=subDomainValue+fourthLevelUrlString%>" class=""><%= (!string.IsNullOrWhiteSpace(fourthLevelItem.overriddentitle))?fourthLevelItem.overriddentitle: fourthLevelItem.displaytitle %></a>
                            </li>
                        </ul>
                        <% } %>
                        <% } %>
                    </li>
                </ul>
                <% } %>
                <%} %>                 
            </li>
        </ul>
    </div>
    <% } %>
    <% } %>
</div>

