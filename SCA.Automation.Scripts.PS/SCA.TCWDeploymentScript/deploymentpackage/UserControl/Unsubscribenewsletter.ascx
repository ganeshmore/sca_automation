﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Unsubscribenewsletter.ascx.cs" Inherits="SCA.TCW.Men.Web.UserControl.Unsubscribenewsletter" %>
<div class="form-field validate-required validate-email form-field-input">
    <div class="row sectionTitleSpacingTop">
        <label class="field-label col-12" for="form-email">Enter your email address*</label>
    </div>
    <div class="row">
        <div class="col-8 col-xxs-12 vspace-xs-xs">
            <asp:TextBox CssClass="field field-input" ID="txtemail" runat="server"> </asp:TextBox>
            <div class="validate-message validate-message-required">This field can not be empty</div>
            <div class="validate-message validate-message-email">This is not a valid email address</div>
        </div>
        <div class="col-4 col-xxs-12">
            <asp:Button CssClass="btn btn-inline-field btn-no-icon" ID="btnUnsubscribe" runat="server" Text="Unsubscribe" OnClick="btnUnsubscribe_Click" />
        </div>
    </div>
</div>
