﻿##############################################################################
##
## TORK CONNECT Automation Deployment
## Created by Gursewak Singh 
## Date : 08 Jan 2018
## Version : 5.0
## Email: gursewaksingh189@gmail.com 
##############################################################################

clear

Import-Module WebAdministration

# ===== Deploy the Package  ================== #
function DeployPackage($physicalpath)
{
    $subdomains = @()
    $checkpath = @()
    #Variable
    # ------------- Variable ------------------ #
    $packagepath = "E:\TorkConnectDeploymentScript\deploymentpackage"
    # --------- END Variable ------------------ #

    $Deployfolder = Get-ChildItem $packagepath
    foreach($Dfolder in $Deployfolder.Name){
				try{

                    #Check the FolderName and then deploy
                    $message += "Current Folder " +$Dfolder + "`r`n"
                    #Write-Host "Current Folder : " $Dfolder
                    $checkpath = $physicalpath + "\"+$Dfolder
                    $message += "START DEPLOYMENT - $Dfolder `r`n"
                    Write-Host "START DEPLOYMENT - $Dfolder `r`n"
                    $childItems = "$packagepath\$Dfolder"
                    <#$childItems = Get-ChildItem "$packagepath\$Dfolder"
                    $childItems | ForEach-Object {
                        Copy-Item -Path $_.FullName -Destination $checkpath -Recurse -Force
                        $message += "Deployment Package : " + $_.FullName + "`r`n" 
                        Write-Host "Deployment Package : " $checkpath
                    }#>
                    Copy-Item -Path $childItems -Destination $physicalpath -Recurse -Force
                    $message += "END DEPLOYMENT - $Dfolder" + "`r`n"
                }
				catch{
				Write-Host $_.Exception
                $message += "ERROR - " + $_.Exception
				}
        }
    $message += "END DEPLOYMENT of " + $Uri + "`r`n`r`n"
    $message >> $LogFile
    $message = ""
}
# ===== END Method to Deploy the Package  ============== #

# ======= Deploy the Web.config  ==================== #
function DeployWebConfig($phypathconfig,$websiteName)
{
    # ------------- Variable ------------------ #
    $packagepath = "E:\TorkConnectDeploymentScript\Webconfigpackage"
    # --------- END Variable ------------------ #
    Write-Host $websiteName -foreground Yellow
    
    $pathcheck = "$packagepath\$websiteName"

    $message += "Web Config Path check : $pathcheck" + "`r`n"

    if(Test-Path $pathcheck)
    {
        $Deployfolder = Get-ChildItem $pathcheck
        foreach($Dfolder in $Deployfolder.Name){
		    try
            {
                $childItems = "$pathcheck\$Dfolder"
                Copy-Item -Path $childItems -Destination $phypathconfig -Recurse -Force
                #Write-Host "Deployment Package to : " $phypathconfig  -ForegroundColor Red
                #Write-Host "Deployment Package : " $childItems "`r`n" -ForegroundColor Yellow
                $message += "Deploy Package to : $phypathconfig `r`n"
                $message += "Deployed Package  : $childItems `r`n"
            }
            catch{
                Write-Host $_.Exception
                $message += "ERROR - " + $_.Exception
			}
        }
    }
    $message >> $LogFile
    $message = ""
    #Write-Host "Message : " $message
}
# ===== END Method to Deploy the Web.config  ================== #

# ========== Taking Backup  ================= #
function TakeBackup($BackupsiteURL,$subdomainName)
{
    # ------------- Variable ------------------ #
    $BackupPath = "D:\Backup\TDP\"
    # --------- END Variable ------------------ #

    $message = ""
    
    ##  START ---- Take Backup 
    $message += "START Backup : $BackupsiteURL `r`n"
    $BackupSitePath = Get-ChildItem -Path IIS:\Sites | Where-Object {$_.Name -contains $BackupsiteURL}
    $BackupSitePhyPath = ''
      $BackupLocation = "$BackupPath"+$a+"\"+$Uri
      New-Item -ItemType directory -Path $BackupLocation
      $BackupSitePhyPath = $BackupSitePath.physicalpath
      if (-not ([string]::IsNullOrEmpty($subdomainName)))
      {
        $BackupSitePhyPath = $BackupSitePhyPath + "\$subdomainName"
        $BackupLocation = $BackupLocation + "\$subdomainName"
      }
      $BackupSitePhyPath = $BackupSitePhyPath + "\connect"
      $BackupLocation = $BackupLocation + "\connect"
      New-Item -ItemType directory -Path $BackupLocation
      $message += "Backup Site Physical Path : "+$BackupSitePhyPath + "`r`n"
      $Backupfolder = Get-ChildItem $BackupSitePhyPath

        foreach($BKfolder in $Backupfolder.Name){
            try{
                $temppath = $BackupSitePhyPath+"\"+$BKfolder
                Copy-Item -Path $temppath -Destination $BackupLocation -Recurse -Force
            }
    `           catch{
            Write-Host $_.Exception.Message
            $message += $_.Exception.Message
            }
        } 
	$message += "Backup Complete `r`n" 
    $message >> $LogFile
    $message = ""
	##  END ---- Take Backup of  Site
}
# ======== END Taking Backup  =============== #

# =========== Site Check Before and After Deployment ============== #
function sitecheck($sitename,$value)
{
    $sitename = $sitename.replace("\","/")
    $message = ""
    $time = try{
	    $request = $null 
   ## Request the URI, and measure how long the response took. 
   add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
  $result1 = Measure-Command { $request = Invoke-WebRequest -Uri $sitename } 
  $result1.TotalMilliseconds 
 
	} 
	catch
	{
	   $message = "Exception while checking status"
	   <# If the request generated an exception (i.e.: 500 server
	   error or 404 not found), we can pull the status code from the
	   Exception.Response property #>
	   $request = $_.Exception.Response
	   $time = -1
	   $message += "ERROR in Checking Site - " + $_.Exception.Response
       Write-Host "ERROR in Checking Site $sitename - "  $_.Exception.Response -ForegroundColor Red
	   #$message12 >> $LogFile1
	}  
    if($value -eq "1")
    {
        Write-Host $request.StatusCode
        if($request.StatusCode -ne "200")
        {
            Write-Host "Recycle App pool" $sitename
            RecycleAppPool -site $sitename
        }
    }
	$result += [PSCustomObject] @{
	Time = Get-Date;
	Urires = $sitename;
	StatusCode = [int] $request.StatusCode;
	StatusDescription = $request.StatusDescription;
	ResponseLength = $request.RawContentLength;
	TimeTaken =  $time;
  }
  $message >> $LogFile
  $message = ""
  return $result
}
# ========= END Site Check Before and After Deployment ============ #

# =========== Site Check Result to HTML format ================ #
function SiteResultToHTML($siteresult)
{
    if($siteresult -ne $null)
    {
    $msg = @()
    $msg = "Write on HTML"
    $Outputreport = "<HTML><TITLE>Website Availability Report</TITLE><BODY background-color:peachpuff><font color =""#99000"" face=""Microsoft Tai le""><H2> Website Availability Report </H2></font><Table border=1 cellpadding=0 cellspacing=0><TR bgcolor=gray align=center><TD><B>Website</B></TD><TD><B>Before Deployment</B></TD><TD><B>Site Status</B></TD><TD><B>After Deployment</B></TD><TD><B>Site Status</B></TD</TR>"
    Foreach($Entry in $siteresult)
    {
        $msg = $null
        $firstURL = $($Entry.Urires)
        #Write-Host "First URL : " $firstURL
        #Write-Host "Second URL : " $secondURL
        if($firstURL -eq $secondURL)
        {
            if($Entry.StatusCode -ne "200")
            {
                $Outputreport += "<TR bgcolor=red>"
            }
            else
            {
                $Outputreport += "<TR>"
            }
            $Outputreport += "<TD>$($Entry.Urires)</TD><TD align=center>$statusCode</TD><TD align=center>$StatusDescription</TD><TD align=center>$($Entry.StatusCode)</TD><TD align=center>$($Entry.StatusDescription)</TD></TR>"
        }
        $secondURL = $firstURL
        

        #Write-Host "After Output First URL : " $firstURL
        #Write-Host "After Output Second URL : " $secondURL
        $statusCode = $($Entry.StatusCode)
        $StatusDescription = $($Entry.StatusDescription)
        
        $msg >> $LogFile
        $msg = "URL: " + $($Entry.Urires) + " |  statusCode: " + $($Entry.StatusCode) + " |  Description: "+ $($Entry.StatusDescription)
        $msg >> $LogFile
    }
    $Outputreport += "</Table></BODY></HTML>"
}

$Outputreport | out-file "E:\TorkConnectDeploymentScript\Result_$a.htm"
Invoke-Expression "E:\TorkConnectDeploymentScript\Result_$a.htm"
}
#========= END Site Check Result to HTML format =============== #

# ==========  Recycle App Pool  ==================== #
function RecycleAppPool($site)
{
    # Get pool name by the site name:
    $pool = (Get-Item "IIS:\Sites\$site"| Select-Object applicationPool).applicationPool
    # Recycle the application pool:
    Restart-WebAppPool $pool
}
# ========== END Recycle App Pool  ==================== #





clear

####################   MAIN     #################################
$a = "{0:yyyy-MM-dd}" -f (get-date)
# ------------- Variable ------------------ #
$URLListFile = "E:\TorkConnectDeploymentScript\URLList.txt" 
$LogFile = 'E:\TorkConnectDeploymentScript\DeploymentLogs.txt'
# --------- END Variable ------------------ #

#-----------------  User Interaction  -----------------#
$BoolBackup = Read-Host -Prompt 'Do you want to take backup (Y/N)';
$BoolWebConfig = Read-Host -Prompt 'Do you want to Deploy Web.config (Y/N)';
$BoolPackage = Read-Host -Prompt 'Do you want to Deploy Package (Y/N)';
#--------------- END User Interaction  ----------------#


$URLList = Get-Content $URLListFile -ErrorAction SilentlyContinue

$subdomain = @()
$message = @()
$result = @()
$rsr = @()
$message = ""

$message += "======================================="+ $a + "===================================================== `r`n `r`n"
$message >> $LogFile
$message = ""

Foreach($Uri in $URLList) {
    $siteName = Get-ChildItem -Path IIS:\Sites | Where-Object {$_.Name -contains $Uri}
    
    
    
    
    # >>>>>>>>>>>>>  Check Multiligual Site  <<<<<<<<<<<<<<<<<<<<< #
    If (Get-ChildItem $siteName.physicalpath -dir | Where-Object {$_.Name -eq "fr" -or $_.Name -eq "ru" -or $_.Name -eq "nl" -or $_.Name -eq "uk" -or $_.Name -eq "de" -or $_.Name -eq "it" -or $_.Name -eq "en"})
    {
        $forphysicalpath = $siteName.physicalpath
        $subdomain = Get-ChildItem $forphysicalpath -dir | Where-Object {$_.Name -eq "fr" -or $_.Name -eq "ru" -or $_.Name -eq "nl" -or $_.Name -eq "uk" -or $_.Name -eq "de" -or $_.Name -eq "it" -or $_.Name -eq "en"} 
        foreach($sbdmn in $subdomain)
        {
            $phypath = $forphysicalpath+"\"+$sbdmn.Name+"\connect"
            $message += "Deployment for $Uri\" + $sbdmn.Name +"\connect"+"`r`n"
            Write-Host "Deployment for $Uri\"  $sbdmn.Name "\connect" "`r`n"
            $message >> $LogFile
            $message = ""
            #Write-Host "Physical Path Before : " $phypath -foreground Yellow
            $name = $Uri +"\" + $sbdmn.Name +"\connect" 

            ##  START ---- Take Backup 
            if($BoolBackup.ToString().ToLower() -eq "y" -or $BoolBackup.ToString().ToLower() -eq "yes")
            {
                TakeBackup -BackupsiteURL $Uri -subdomainName $sbdmn.Name;
            }
            else
            {
                 Write-Host "Not taking the backup" -foreground Yellow;
            }
            ##  END ---- Take Backup of  Site

            # ------ Check Site Before Deployment --------- #
            Write-Host "Check Site Response before Deployment : "$name
            $rsr += sitecheck -sitename $name -value "0"
            # ------ Recycle App Pool --------- #
            RecycleAppPool -site $name
            Start-sleep -Seconds 30
            # ------ Deploy Package ------------ #
            if($BoolPackage.ToString().ToLower() -eq "y" -or $BoolPackage.ToString().ToLower() -eq "yes")
            {
                DeployPackage -physicalpath $phypath;
            }
            # ------ Deploy Webconfig ---------- #
            if($BoolWebConfig.ToString().ToLower() -eq "y" -or $BoolWebConfig.ToString().ToLower() -eq "yes")
            {
                DeployWebConfig -phypathconfig $phypath -websiteName $name;
            }
            
            # ------ Check Site After Deployment --------- #
            Write-Host "Check Site Response after Deployment : "$name
            $rsr += sitecheck -sitename $name -value "1"
        }
    }
    else
    {
        $singlename = $Uri +"\connect" 
        $forphysicalpath = $siteName.physicalpath+"\connect"
        $message += "Deployment for $singlename `r`n"
        Write-Host "Deployment for $singlename `r`n"
        $message >> $LogFile
        $message = ""
        ##  START ---- Take Backup 
        if($BoolBackup.ToString().ToLower() -eq "y" -or $BoolBackup.ToString().ToLower() -eq "yes")
        {
            TakeBackup -BackupsiteURL $Uri -subdomainName '';
        }
        else
        {
             Write-Host "Not taking the backup" -foreground Yellow;
        }
        ##  END ---- Take Backup of  Site
        # ------ Check Site Before Deployment --------- #
        Write-Host "Check Site Response before Deployment : "$singlename
        $rsr += sitecheck -sitename $singlename -value "0"
        # ------ Recycle App Pool --------- #
        RecycleAppPool -site $singlename
        Start-sleep -Seconds 30
        # ------ Deploy Package ------------ #
        if($BoolPackage.ToString().ToLower() -eq "y" -or $BoolPackage.ToString().ToLower() -eq "yes")
        {
            DeployPackage -physicalpath $forphysicalpath;
        }
        # ------ Deploy Webconfig ---------- #
        if($BoolWebConfig.ToString().ToLower() -eq "y" -or $BoolWebConfig.ToString().ToLower() -eq "yes")
        {
            DeployWebConfig -phypathconfig $forphysicalpath -websiteName $singlename;
        }
        
        # ------ Check Site After Deployment --------- #
        Write-Host "Check Site Response after Deployment : "$singlename
        $rsr += sitecheck -sitename $singlename -value "1"
    }
    
}
SiteResultToHTML -siteresult $rsr
Write-Host "Deployment Complete"