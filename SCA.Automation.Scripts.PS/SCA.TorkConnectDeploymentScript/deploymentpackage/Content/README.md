**Please note that all files and folders in "/TorkExtranet/Content" origins from "/resources" and must *NOT* be changed.** 

**To make changes in these files please follow the below procedures**

1. Install node https://docs.npmjs.com/getting-started/installing-node

2. Install gulp (from an Administrative command prompt or using sudo)" `npm install -g gulp`

3. Change your working directory to the root of the branch folder and run these commands to clean up the "/TorkExtranet/Content" folder and build the files from "/resources"

```
npm install
gulp clean
gulp build
```