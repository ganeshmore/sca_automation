/**
 * Global class for site-wide JavaScript functionality
 */

window.TDPWeb = {};

TDPWeb.Utils = (function () {
    function findJqueryFormElementsToSerialize($elements) {
        // if we have a form or inputs, return those
        if ($elements.is('form,:input')) {
            return $elements.filter('form,:input');
        }

        // otherwise, search for inputs (e.g. parent is a container of elements)
        return $elements.find(':input');
    }

    // jQuery Helper to provide JSON serialization
    $.fn.serializeObject = function () {
        // If we are given a form OR given a list of inputs, use this. Otherwise if it's not a form or a single
        // element, search for child :input elements.
        var $toSerialize = findJqueryFormElementsToSerialize(this),
            serializedArray = $toSerialize.serializeArray(),
            serializedObject = {};

        $.each(serializedArray, function () {
            if (serializedObject[this.name] !== undefined) {
                if (!serializedObject[this.name].push) {
                    serializedObject[this.name] = [serializedObject[this.name]];
                }
                if (typeof this.value !== 'undefined') {
                    serializedObject[this.name].push(this.value);
                }
            } else {
                if (typeof this.value !== 'undefined') {
                    serializedObject[this.name] = this.value;
                }
            }
        });

        return serializedObject;
    };

    // jQuery helper to serialize form and non-form objects' inputs
    $.fn.serializeQueryString = function () {
        var $toSerialize = findJqueryFormElementsToSerialize(this);
        return $toSerialize.serialize();
    }

    function getKeyCode(ev) {
        return ev.which || ev.keyCode;
    }

    return {
        isEnter: function (ev) {
            return getKeyCode(ev) === 13;
        },

        isEsc: function (ev) {
            return getKeyCode(ev) === 27;
        }
    }
})();

TDPWeb.ExtraNet = (function () {
    var Application = {
        selectedIcon: null,
        headerElem: $("#header"),
        wizardValidatorFunction: null,
        workingFormID: null,
        unchangedForm: null,
        currentForm: null,
        disableFormConfirm: false,
        productListController: null,
        useClickEvent: false,
        navWidth: 0,
        addPriceController: null,

        Init: function () {
            // All application initialization goes here

            $.ajaxSetup({ cache: false });
            this.bindHandlers();

            this.setEqualHeight();

            $subjectSelect = $("#contact-subject");
            $subjectSelect.select2({
                minimumResultsForSearch: -1,
                placeholder: $subjectSelect.data("placeholder"),
                theme: 'bootstrap'
            });

            Application.setInitialNavWidth();

            if (Application.enoughNavigationSpace() === true) {
                Application.removeCollapsibleNav();
            }
            else {
                Application.useCollapsibleNav();
            }

            $.datepicker.setDefaults({ dateFormat: window.shortDateFormat });

            this.showHideLanguageSelector();
        },

        bindHandlers: function () {
            var $body = $("body "),
                hash = window.location.hash,
                default_message = "You have unsaved changes, are you sure you want to leave?";

            $(window).on("beforeunload", function (e) {
                if ($(".use-collapsible-nav").length > 0) {
                    Application.hideExtranetNav();
                }

                if (Application.workingFormID != null) {
                    Application.currentForm = $(Application.workingFormID).serializeQueryString();

                    if (Application.currentForm != Application.unchangedForm && !Application.disableFormConfirm) {
                        var confirmMsg = $("[data-warning='" + Application.workingFormID + "']").text();

                        if (confirmMsg.length < 1) {
                            confirmMsg = default_message;
                        }

                        return confirmMsg;
                    }
                }

                if (Application.productListController != null) {
                    if (Application.productListController.unsavedChanges
                        && Application.productListController.numberOfItems() > 0
                        && !Application.disableFormConfirm) {
                        var confirmMsg = Application.productListController.leaveWarning();

                        if (confirmMsg.length < 1) {
                            confirmMsg = default_message;
                        }

                        return confirmMsg;
                    }
                }

                Application.disableFormConfirm = false;
            });

            if (Application.enoughNavigationSpace() === true) {
                Application.removeCollapsibleNav();
            }
            else {
                Application.useCollapsibleNav();
            }

            $(window).bind("resize", function () {
                Application.setPanelHeight();
                Application.setEqualHeight();

                if (Application.enoughNavigationSpace() === true) {
                    Application.removeCollapsibleNav();
                }
                else {
                    Application.useCollapsibleNav();
                }
            });

            $(document).on('click', '.WhatIsThis-indicator', this.toggleWhatIsThis);

            this.initializeModalTabs();
            this.initializeTabs();

            this.headerElem.on('click', '.tab-item > a', this.showMenuTab);

            this.headerElem.on('click', '.toolbar-list-item > a', this.toolbarClick);

            // Do not propagate click (i.e. do not close tab if clicking on the content)
            this.headerElem.on('click', ".tab-pane, #profile-action-tab, .LBD_ReloadLink, .LBD_SoundLink, .LBD_ReloadIcon, .LBD_SoundIcon", function (evt) {
                evt.stopPropagation();
            });

            // Close open toolbar item on left click anywhere alt. esc
            $(document).click(function (e) {
                switch (e.which) {
                    case 1: {
                        if (!$(e.target).hasClass("prevent-close-all")) {
                            Application.closeAllOpenItems(e.target);
                        }
                    }
                }
            });
            $(document).keyup(function (e) {
                if (TDPWeb.Utils.isEsc(e)) {
                    Application.closeAllOpenItems(e.target);
                }
            });


            $(".global-nav-item").on("click", this.showGlobalMenu);
            $(".global-nav-item").on("mouseleave", this.hideGlobalMenu);

            $(".mainMenuThreedots").dotdotdot({
                ellipsis: "...",
                watch: "true"
            });

            $(".no-touch #extranet-header .dropdownToggler").on("mouseenter", function (e) {
                if ($(".use-collapsible-nav").length === 0) {
                    Application.toggleExtranetNav(e.target);
                }
            });
            $(".no-touch #extranet-header .dropdownToggler").on("mouseleave", function (e) {
                if ($(".use-collapsible-nav").length === 0) {
                    Application.hideExtranetNav();
                }
            });

            $(".no-touch #extranet-header .dropdownToggler").on("click", function (e) {
                if ($(".use-collapsible-nav").length > 0) {
                    Application.toggleExtranetNav(e.target);
                }
            });

            $(".touch #extranet-header .dropdownToggler").on("click", function (e) {
                Application.toggleExtranetNav(e.target);
            });

            $(".touch .main-content-container, .touch footer[role='contentinfo']").on("click", Application.closeAllOpenItems);

            $(".touch body").on("click", function (evt) {
                var toggler = $(".dropdownToggler");
                if (!toggler.is(evt.target) && toggler.has(evt.target).length === 0) {
                    Application.hideExtranetNav();
                }
            });

            // fix for bootstrap input size not updating correctly (causing the text to disappear to the left)
            $(".bootstrap-tagsinput input").on("paste keyup", function (e) {
                var $input = $(this);

                setTimeout(function () {
                    var text = $input.val();
                    $input.attr("size", text.length);
                }, 50);
            });

            $("#contact-form .cancel-form-tab").click(function (evt) {
                Application.closeAllOpenItems();
            });

            $("#contact-form input[type=submit]").click(function (evt) {
                evt.preventDefault();
                $form = $(this).closest("form");
                if (!TDPWeb.ExtraNet.validateInput($form)) {
                    TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
                    setTimeout(Application.setPanelHeight, 400);
                } else {
                    TDPWeb.ExtraNet.resetFormValidations($form);

                    if ($form.find("#ContactFormCaptchaInput").length != 0) {
                        Application.checkCaptcha("#ContactFormCaptchaInput", $form, function () {
                            Application.sendContactForm($form);
                        });
                    } else {
                        Application.sendContactForm($form);
                    }
                }
            });

            $body.delegate("[download]", "click", function (e) {
                e.stopPropagation();
            });

            $body.delegate(".panel-content", "show.bs.collapse", function () {
                var $toggler = $(this).siblings(".panel-toggler"),
                    expandIcon = "icon-plus",
                    collapseIcon = "icon-minus";

                if ($toggler.hasClass("vertical-arrow-toggle")) {
                    expandIcon = "icon-arrow_down";
                    collapseIcon = "icon-arrow_up";
                }

                if ($toggler.hasClass("horizontal-arrow-toggle")) {
                    expandIcon = "icon-arrow_right";
                    collapseIcon = "icon-arrow_left";
                }

                if ($toggler.hasClass("right-down-arrow-toggle")) {
                    expandIcon = "icon-arrow_right";
                    collapseIcon = "icon-arrow_down";
                }

                $toggler.find(".toggle-icon")
                    .removeClass(expandIcon)
                    .addClass(collapseIcon);
            });

            $body.delegate(".panel-content", "hide.bs.collapse", function () {
                var $toggler = $(this).siblings(".panel-toggler"),
                    expandIcon = "icon-plus",
                    collapseIcon = "icon-minus";

                if ($toggler.hasClass("vertical-arrow-toggle")) {
                    expandIcon = "icon-arrow_down";
                    collapseIcon = "icon-arrow_up";
                }

                if ($toggler.hasClass("horizontal-arrow-toggle")) {
                    expandIcon = "icon-arrow_right";
                    collapseIcon = "icon-arrow_left";
                }

                if ($toggler.hasClass("right-down-arrow-toggle")) {
                    expandIcon = "icon-arrow_right";
                    collapseIcon = "icon-arrow_down";
                }

                $toggler.find(".toggle-icon")
                    .removeClass(collapseIcon)
                    .addClass(expandIcon);
            });

            $body.delegate('[data-table-sort]', 'click', function () {
                var $clicked = $(this);
                var $sortCol = $clicked.closest('.dropdown').nextAll('table').find('thead .' + $clicked.data('tableSort').split(' ').join('.'));
                Application.sortTableByColumn($sortCol);
            });

            $body.delegate('[data-table-sort-order]', 'click', function () {
                var $clicked = $(this);
                var $sortCol = $clicked.closest('.dropdown').nextAll('table').find('thead .is-active').closest('th');
                Application.sortTableByColumn($sortCol);
            })

            $body.delegate(".Sort .Sort-header", "click", function () {
                Application.sortTableByColumn($(this));
            });

            $body.delegate("select", "select2-opening", function () {
                $(this).closest("form").find("input").blur(); // fix for focus/keyboard issue on iPad
            });

            setInterval(function () {
                $.each($(".modal form:not(.exclude-from-confirm), .control-module form:not(.exclude-from-confirm), .working-form:not(.exclude-from-confirm)"), function () {
                    if ($(this).is(":visible") && (!Application.workingFormID || Application.workingFormID != "#" + $(this).attr("id"))) {
                        Application.workingFormID = "#" + $(this).attr("id");
                        Application.unchangedForm = $(Application.workingFormID).serializeQueryString();
                    }
                });
            }, 500);

            $body.delegate(".modal .cancel-form-modal, .modal .close", "click", function () {

                if (!$(this).data("dismiss")) {
                    if (!$(this).hasClass("disable-form-confirm")) {

                        if (TDPWeb.ExtraNet.discardFormChanges() === false) {
                            return;
                        }
                    }
                    TDPWeb.ExtraNet.cancelFormModal(TDPWeb.ExtraNet.getModalInstance(this));
                }
            });

            $body.delegate(".disable-form-confirm", "click", function () {
                Application.disableFormConfirm = true;
            });

            $body.delegate(".modal .delete-btn", "click", function () {
                TDPWeb.ExtraNet.confirmDelete($(this), $(this).siblings(".confirm-delete-btn"));
            });

            $body.delegate("#delete-users-btn", "click", function () {
                TDPWeb.ExtraNet.confirmDelete($(this), $(".confirm-delete-users-btn"));
            });

            $body.delegate(".select-all", "click", function () {
                Application.clearOrSelectAll($(this), true);
            });

            $body.delegate(".clear-all", "click", function () {
                Application.clearOrSelectAll($(this), false);
            });

            $body.delegate("#unsubscribeSuccess", "click", function () {
                $("#unsubscribeSuccess").fadeOut("slow");
            });

            $body.delegate("#unsubscribeFailure", "click", function () {
                $("#unsubscribeFailure").fadeOut("slow");
            });

            // Show Hide the Term and Condition on New Registration Page.
            $body.delegate("#open-terms-link", "click", function () {
                if ($("#new-user-registration").length > 0 && $("#new-user-registration").hasClass("in")) {

                    // Hide Register Button
                    $(".register-btn").hide();
                    $(".cancel-form-modal").hide();

                    // Hide the Registration Cross Button
                    $("#new-register-close").hide().removeClass("modal-header");
                    $("#close-term-condition").css("float", "right");

                    // Hide the new Registration Div
                    $("#newRegistration").hide();

                    // Hide the Error message if any.
                    $("#footer-registration-error").hide();

                    // Show the Term and Condition Dive
                    $("#registration-term-condition").show();

                    // Get the Content from Footer Link
                    $("#registration-term-condition p").html($("#term-content-div").html());
                }
                else {
                    $modal = $("#terms-modal");
                    $modal.modal();
                }
            });

            // Show hide the Term Link  section on New Registration Page.
            $body.delegate("#close-term-condition", "click", function (e) {

                // Show Register Button
                $(".register-btn").show();
                $(".cancel-form-modal").show();

                // Show the Close Button
                $("#new-register-close").show().addClass("modal-header");

                // Show the new Registration Div
                $("#newRegistration").show();

                // Show the Error message if any.
                $("#footer-registration-error").show();

                // Hide the Term and Condition Dive
                $("#registration-term-condition").hide();
            });

            $body.delegate("select.select2-offscreen", "select2-opening", function () {
                Application.fixSelect2Placeholders($(this));
            });

            $body.delegate(".trigger[data-toggle='tooltip']", "click", function (evt) {
                evt.preventDefault();
                evt.stopPropagation();

                $tooltip = $(this).next("[data-target='tooltip']");
                var isVisible = $tooltip.hasClass("visible");
                Application.closeAllOpenItems();

                if (!isVisible) {
                    Application.openTooltip($tooltip);
                }
            });

            $body.delegate(".datepicker-container .datepicker-icon", "click", function () {
                $(this).siblings(".datepicker").focus();
            });

            $body.delegate("[data-popover-target]", "mouseleave", function () {
                $(this).popover('hide');
            });

            $('input#search-field, input#search-input').on('input', function (e) {
                var $searchFieldClearer = $(this).parent().find(".clear-search-field");

                if ($(this).val().match(/^ *$/) !== null) {
                    $searchFieldClearer.hide();
                }
                else {
                    $searchFieldClearer.show();
                }
            });

            $body.delegate(".clear-search-field", "click", function () {
                $(this).hide();
            });

            $body.delegate("[data-required-group]", "change", function () {
                TDPWeb.ExtraNet.updateRequiredGroup($(this).data("required-group"));
            });

            $body.delegate(".wizard-content", "wizard-page-changed", function () {
                TDPWeb.ExtraNet.setEqualHeight();
            });

            $body.on('hide.bs.collapse show.bs.collapse', '#mobile-nav', function () {
                $('.extranet-menu-toggler').children().toggleClass('hidden')
            });

            $('#empty-modal').on('show.bs.modal', function (event) {
                var $modal = $(this);
                var target = $(event.relatedTarget).attr('href');
                var title = $(event.relatedTarget).find('[data-title]').text();
                var $content = $(target).children();
                $modal.find('.modal-body').append($content);
                $modal.find('.modal-title').text(title);
                $("#mobile-nav").collapse('hide');
                $modal.data('target', target);
            });

            $('#empty-modal').on('hidden.bs.modal', function (event) {
                var $modal = $(this);
                var $content = $modal.find('.modal-body').children();
                $($modal.data('target')).append($content);
                $modal.find('.modal-title').text('');
            });

            // Set up menu
            $('#mobile-nav').find('.VerticalNavigation').navigation({
                linkSelector: '.VerticalNavigation-link',
                selectedClass: 'is-active',
                parentLink: function ($node) {
                    // Parent link is the prev sibling of the parent
                    return $node.parent('.VerticalNavigation-subnav').prev('.VerticalNavigation-link');
                },
                onSelect: function ($to, $from, event) {
                    $from.not($to).find('.icon-arrow_down').removeClass('icon-arrow_down').addClass('icon-arrow_right');
                    $to.find('.icon-arrow_right').removeClass('icon-arrow_right').addClass('icon-arrow_down');
                    if (event.currentTarget.getAttribute('href').substr(0, 1) === '#') {
                        event.preventDefault();
                    }
                }
            });
        },

        setInitialNavWidth: function () {
            var $navItems = $(".header-item").not("#collapsed-nav-toggler"),
                navWidth = 0;

            $.each($navItems, function () {
                navWidth += $(this).outerWidth();
            });

            Application.navWidth = navWidth;
        },

        enoughNavigationSpace: function () {
            var availableSpace = $("#extranet-header").outerWidth() - $(".logOut").outerWidth();
            return (availableSpace - Application.navWidth > 0);
        },

        useCollapsibleNav: function () {
            var $header = $("#extranet-header"),
                $navContainer = $("#collapsed-nav-container")

            $header.addClass("use-collapsible-nav");
            $navContainer.hide();

            $("#collapsed-nav-toggler").on("click", function (e) {
                e.preventDefault();

                if (e.handled !== true) {
                    if ($navContainer.is(":visible")) {
                        Application.hideCollapsibleNav();
                    }
                    else {
                        $navContainer.show();
                    }

                    e.handled = true;
                }
            });
        },

        removeCollapsibleNav: function () {
            var $header = $("#extranet-header"),
                $navContainer = $("#collapsed-nav-container")

            $header.removeClass("use-collapsible-nav");
            $header.find("#collapsed-nav-container").show();
        },

        hideCollapsibleNav: function () {
            var $navContainer = $("#collapsed-nav-container");

            $navContainer.hide();
            $navContainer.find(".dropdownToggler").removeClass("open");
        },

        hideGlobalMenu: function () {
            var links = $('.global-nav-item > a');
            $(links).removeClass('faded active');
            $(".global-nav-item .headerNavDropdown").hide();
            $('#menuOverlay').hide();
        },

        showGlobalMenu: function () {
            var link = $(this).find('> a');
            $(this).find(".headerNavDropdown").show();
            $('.global-nav-item > a').not(link).addClass('faded');
            $(link).addClass('active');
            $('#menuOverlay').show();
        },

        removeDoubleSlashes: function (url) {
            return url.replace(/\/+/g, "/");
        },

        fixSelect2Placeholders: function ($select) {
            var text = $select.find("option:selected").text(),
                placeholder = $select.data("placeholder");

            if (placeholder !== undefined) {
                $option = $select.find("option").first();
                if (text === placeholder || text === "") {
                    $option.text("");
                    $option.val("");
                }
                else {
                    $option.text(placeholder);
                    $option.val("");
                }
            }
        },

        sortTableByColumn: function ($th, force_descending, noSpinner) {
            var $spinnerLocation = $th.closest("table");

            if (!noSpinner) {
                TDPWeb.ExtraNet.spinnerOn($spinnerLocation);
            }
            //need this timeout to get the spinner started immediately
            setTimeout(function () {

                if (force_descending === undefined) {
                    force_descending = false;
                }

                $sortIcon = $th.find(".Sort-icon");
                var wasAscending = $sortIcon.hasClass("icon-arrow_up"),
                    column_index = $th.prevAll().length,
                    sort_order,
                    $allThs;

                $allThs = $th.closest("thead").find("th .Sort-icon");
                $allThs.removeClass("icon-arrow_up").removeClass("icon-arrow_down").removeClass("is-active");
                $allThs.addClass("icon-arrow_down");
                $sortIcon.removeClass("icon-arrow_down");

                if (wasAscending || force_descending) {
                    $sortIcon.addClass("icon-arrow_down");
                    var sort_order = -1;
                }
                else {
                    $sortIcon.addClass("icon-arrow_up");
                    var sort_order = 1;
                }

                $sortIcon.addClass("is-active");

                // Set text content for mobile sort dropdown
                var $dropdown = $th.closest('table').prevAll('.dropdown');

                $dropdown.find('[data-toggle="dropdown"]').text($th.find('.Sort-label').text().trim());
                var $icon = $dropdown.find('[data-table-sort-order]').children('span');
                $icon.toggleClass('icon-arrow_up_borderless', sort_order === 1);
                $icon.toggleClass('icon-arrow_down_borderless', sort_order === -1);

                $table_body = $th.closest("table").find("tbody");
                var rows = $table_body.find("tr:not(.non-item)").get();
                rows.sort(function (a, b) {
                    var A = $(a).children('td').eq(column_index).data("sorting-value");
                    if (A === undefined || A.length === 0) {
                        A = $(a).children('td').eq(column_index).text();
                    }

                    var B = $(b).children('td').eq(column_index).data("sorting-value");
                    if (B === undefined || B.length === 0) {
                        B = $(b).children('td').eq(column_index).text();
                    }

                    if (!isNaN(A) && !isNaN(B)) {
                        A = parseFloat(A);
                        B = parseFloat(B);
                    }
                    else {
                        A = ("" + A).toUpperCase();
                        B = ("" + B).toUpperCase();
                    }

                    if (A < B) {
                        return -1 * sort_order;
                    }
                    if (A > B) {
                        return 1 * sort_order;
                    }
                    return 0;
                });

                var non_items_lookup = {}
                $.each($table_body.find("tr.non-item"), function (idx, item) {
                    non_items_lookup[$(item).data("linked-item")] = item;
                });

                $pagination_list = $table_body.closest(".pagination-list-container");
                $pagination_list.addClass("no-pagination-change");
                $.each(rows, function (index, row) {
                    $table_body.append(row);

                    var item_id = $(row).data("item-id"),
                        linked_item = non_items_lookup[item_id];
                    if (linked_item !== undefined) {
                        $table_body.append(linked_item);
                    }
                });
                $pagination_list.removeClass("no-pagination-change");
                $pagination_list.addClass("resorted");

                if (!noSpinner) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                }
            }, 50);
        },

        toggleWhatIsThis: function () {
            $('.WhatIsThis-indicator i').toggleClass('icon-plus icon-minus');
            $('.WhatIsThis-description').stop().slideToggle('fast');
        },

        sendContactForm: function ($form) {
            $button = $form.find("input[type=submit]");
            TDPWeb.ExtraNet.spinnerOn($button);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Contact/Send'),
                type: 'POST',
                data: $form.serializeQueryString()
            }).done(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);

                var elems = $form.find("input, select, textarea"),
                    events = "click focus";
                function successDone() {
                    $form.find(".alert-success").slideUp(200);
                    elems.unbind(events, successDone);
                }

                TDPWeb.ExtraNet.displayMessage({
                    element: $form.find(".formSuccess"),
                    onComplete: function () {
                        $form.find(".alert:not(.alert-success)").slideUp(200);
                        elems.on(events, successDone);
                        TDPWeb.ExtraNet.cleanForm($form);
                        TDPWeb.ExtraNet.resetForm($form);
                    }
                });
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);

                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                }
            });
        },

        setEqualHeight: function () {
            var windowWidth = $(window).width();
            if (Application.previousWidth == 0) {
                Application.previousWidth = windowWidth;
            }

            $(".equalHeightItem").css("height", "auto");

            if (windowWidth < 480) {
                return;
            }

            $(".equalHeightContainer").each(function () {
                var highestBox = 0,
                    trueHeight = 0,
                    equalHeightMaster = $(this).find(".equalHeightMaster");

                if (equalHeightMaster.length > 0) {
                    $.each(equalHeightMaster.children(), function (idx, elem) {
                        trueHeight += $(elem).outerHeight(true);
                    });
                }

                $(this).find('.equalHeightItem').each(function () {
                    trueHeight = $(this).outerHeight();

                    if (trueHeight > highestBox) {
                        highestBox = trueHeight;
                    }

                });
                $(this).find('.equalHeightItem').css('height', highestBox);
                Application.previousWidth = windowWidth;
            });
        },

        btnState: function ($btn, state) {
            $btn.prop('disabled', state === 'disable');
        },

        // Spinner

        spinnerOn: function ($element, miniSpinner, float, params) {
            var defaultParams = { color: "#575757", zIndex: 15 },
                appliedParams = params === undefined ? defaultParams : params;

            if (float == undefined) {
                float = "right";
            }

            // Table Spinner
            // note: uses a CSS fix for border issue: _tables.scss selector ".spinner + thead"
            if ($element.is("table") || $element.hasClass("spinLikeTable")) {
                var $visible_modal = $(".modal:visible");
                // Only spin if there are no modals visible or the table to spin in is inside the visible modal
                if ($visible_modal.length === 0 || $.contains($visible_modal[0], $element[0])) {
                    $element.spin(appliedParams);
                }
                return;
            }

            // Other Spinners
            var spinnerContainer,
                miniSpinnerParams = { lines: 9, radius: 5, length: 5, width: 3 },
                spinnerClass = ".spinner-container",
                $spinner;

            if (miniSpinner) {
                spinnerContainer = "<div class='mini-spinner-container'></div>";
                spinnerClass = ".mini-spinner-container";
                for (var attrName in miniSpinnerParams) { appliedParams[attrName] = miniSpinnerParams[attrName]; }
            } else
                switch (float) {
                    case "right":
                        spinnerContainer = "<div class='spinner-container pull-right'></div>";
                        break;
                    case "left":
                        spinnerContainer = "<div class='spinner-container pull-left'></div>";
                        break;
                    default:
                        spinnerContainer = "<div class='spinner-container'></div>";
                        break;
                }

            $(spinnerContainer).insertAfter($element);
            $spinner = $element.next(spinnerClass);

            if (miniSpinner) {
                switch (float) {
                    case "right":
                        var right = $spinner.outerWidth() / 2 + 5;
                        $spinner.css({ "right": right });
                        break;
                    case "left":
                        var left = $element.outerWidth() + $spinner.outerWidth();
                        $spinner.css({ "left": left });
                    default:
                        break;
                }
            }

            if ($element.is("input[type=button], input[type=submit], button")) {
                $element.attr("disabled", true);
            }

            $spinner.spin(appliedParams);
        },

        spinnerOff: function ($element) {

            // Table Spinner
            if ($element.is("table") || $element.hasClass("spinLikeTable")) {
                $element.spin(false);
                return;
            }

            // Other Spinners
            var $spinner = $element.next(".spinner-container, .mini-spinner-container");

            if ($element.is("input[type=button], input[type=submit], button")) {
                $element.attr("disabled", false);
            }

            $spinner.spin(false);
            $spinner.remove();

            // used for canceled modals with ongoing spinners
            if ($element.hasClass("spinner-container") || $element.hasClass("mini-spinner-container")) {
                $element.spin(false);
                $element.remove();
            }
        },

        emptySpinnerContainer: "<div id='empty-spinner-container' class='spinLikeTable' style='height:125px;'></div>",


        // Header/global navigation

        closeAllOpenItems: function (target) {
            Application.closeOpenToolbarItems();
            Application.closeOpenMenuTabItems();
            Application.closeTooltip();

            if (!$(target).closest("#collapsed-nav-toggler").siblings("#collapsed-nav-container").length > 0) {
                if (!$(target).is("#collapsed-nav-container *") && $("#collapsed-nav-toggler").is(":visible")) {
                    Application.hideCollapsibleNav();
                }
            }
        },

        openToolbarItem: function (toolbarItem) {
            Application.hideExtranetNav();
            Application.closeAllOpenItems();

            var toolbarItemContent = toolbarItem.find('.tab-pane'),
                toolbarItemLink = toolbarItem.find('a');

            toolbarItemLink.addClass('active');

            toolbarItemContent.stop().fadeTo(0, 0, function () {
                toolbarItemContent.show().fadeTo(200, 1, Application.setPanelHeight);
                Application.setHeightOfToolbarContentHeaders(toolbarItem);
            });
        },

        closeToolbarItem: function (toolbarItem, callback) {
            var toolbarItemContent = toolbarItem.find('.tab-pane'),
                toolbarItemLink = toolbarItem.find('a');

            toolbarItemContent.stop().fadeOut(200, function () {
                toolbarItemContent.hide();
                Application.setPanelHeight();
            });

            toolbarItemLink.delay(100).queue(function (nxt) {
                $(this).removeClass('active');
                nxt();
            });
            if (callback) {
                callback();
            }
        },

        checkCaptcha: function (captchaInputId, form, callback, button, backEndErrors) {
            $button = button || form.find("input[type=submit]");
            $backEndErrors = backEndErrors || form.find(".backEndErrors");
            TDPWeb.ExtraNet.spinnerOn($button);

            var captchaObj = $(captchaInputId).get(0).Captcha;

            var params = {
                captchaId: captchaObj.Id,
                captchaInputId: captchaInputId.replace("#", ""),
                instanceId: captchaObj.InstanceId,
                userInput: $(captchaInputId).val()
            };

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Captcha/Verify'),
                type: 'POST',
                data: params
            }).done(function () {
                TDPWeb.ExtraNet.spinnerOff($button);
                callback();
                captchaObj.ReloadImage();
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);
                captchaObj.ReloadImage();
                TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, form, $backEndErrors);
            });
        },

        getOpenToolbarItems: function () {
            var openToolbarContent = $('#toolbarList').find('.tab-pane').filter(':visible');

            if (openToolbarContent.length) {
                return openToolbarContent;
            }
            else {
                return false;
            }
        },

        closeOpenToolbarItems: function () {
            var openToolbarContent = Application.getOpenToolbarItems();

            if (openToolbarContent && openToolbarContent.length) {
                $.each(openToolbarContent, function (idx, item) {
                    Application.closeToolbarItem($(item).closest('.toolbar-list-item'));
                });
            }
        },

        toolbarClick: function (evt) {
            evt.stopPropagation();
            var toolbarTarget = $(evt.currentTarget),
                toolbarTargetItem = toolbarTarget.closest('.toolbar-list-item'),
                toolbarTargetContent = toolbarTargetItem.find('.tab-pane').first(),
                openToolbarContent = $('#toolbarList').find('.tab-pane').not(toolbarTargetContent).filter(':visible');

            if (!toolbarTargetContent.length) { // toolbar link has no content
                //Close open content if any
                if (openToolbarContent.length) {
                    Application.closeToolbarItem(openToolbarContent.first().closest('.toolbar-list-item'));
                }
            }
            else { //toolbar has content, prevent default
                evt.preventDefault();

                if (toolbarTargetContent.is(':visible')) {
                    Application.closeToolbarItem(toolbarTargetItem);
                }
                else if (openToolbarContent.length) {
                    Application.closeToolbarItem(openToolbarContent.first().closest('.toolbar-list-item'), function () {
                        Application.openToolbarItem(toolbarTargetItem);
                    });
                }
                else {
                    Application.openToolbarItem(toolbarTargetItem);
                }
            }

            if ($(this).attr("href") != undefined) {
                if ($(this).attr("href").indexOf("searchTab") > -1) {
                    $("#product-search-input").focus();
                }
            }
        },

        toggleExtranetNav: function (target) {
            var $items = $("#extranet-header .dropdownToggler"),
                $target = $(target)

            if (!$target.hasClass(".dropdownToggler")) {
                $target = $target.closest(".dropdownToggler");
            }

            $dropdown = $target.find(".dropdownContent");

            $items.not($target).removeClass("open");

            if ($target.hasClass("open")) {
                $target.removeClass("open");
            }
            else {
                $target.addClass("open");
            }
        },

        hideExtranetNav: function () {
            $("#extranet-header .dropdownToggler").removeClass("open");
        },

        showMenuTab: function (evt) {
            evt.stopPropagation();

            Application.closeOpenToolbarItems();

            var menuTabTarget = $(evt.currentTarget),
                menuTabTargetItem = menuTabTarget.closest('.tab-item'),
                menuTabTargetContent = menuTabTargetItem.find('.tab-pane').first(),
                openMenuTabContent = $('#header-nav').find('.tab-pane').not(menuTabTargetContent).filter(':visible');

            if (!menuTabTargetContent.length) { // toolbar link has no content
                //Close open content if any
                if (openMenuTabContent.length) {
                    Application.closeToolbarItem(menuTabTargetContent.first().closest('.toolbar-list-item'));
                }
            }
            else { //toolbar has content, prevent default
                evt.preventDefault();

                if (menuTabTargetContent.is(':visible')) {
                    Application.closeMenuTabItem(menuTabTargetItem);
                }
                else if (openMenuTabContent.length) {
                    Application.closeMenuTabItem(openMenuTabContent.first().closest('.nav-item'), function () {
                        Application.openMenuTabItem(menuTabTargetItem);
                    });
                }
                else {
                    Application.openMenuTabItem(menuTabTargetItem);
                }
            }
        },

        openMenuTabItem: function (menuTabItem) {
            var menuTabItemContent = $('.tab-pane', menuTabItem),
                menuTabItemLink = $(menuTabItem).children("a");

            menuTabItemLink.addClass('active-tab-link').closest('#header-nav').addClass("header-nav-tab-active");
            menuTabItemContent.stop().fadeTo(0, 0, function () {
                menuTabItemContent.show().fadeTo(200, 1);
                menuTabItemContent.find("input[type=text], input[type=email], input[type=password]").first().focus();
            });
        },

        closeMenuTabItem: function (menuTabItem, callback) {
            var menuTabItemContent = $('.tab-pane', menuTabItem),
                menuTabItemLink = $(menuTabItem).children("a");

            menuTabItemContent.stop().fadeOut(200, function () {
                menuTabItemContent.hide();
            });

            menuTabItemLink.delay(100).queue(function (nxt) {
                $(this).removeClass('active-tab-link').blur().closest('#header-nav').removeClass("header-nav-tab-active");
                nxt();
            });

            if (callback) {
                callback();
            }
        },

        closeOpenMenuTabItems: function () {
            var openMenuTabContent = $('#header-nav').find('.tab-pane').filter(':visible');

            if (openMenuTabContent && openMenuTabContent.length) {
                $.each(openMenuTabContent, function (idx, item) {
                    Application.closeMenuTabItem($(item).closest('.nav-item'));
                });
            }
        },

        initializeTabs: function () {
            $('body').on('click', '[data-tabs]', this.tabSelector);
        },

        initializeModalTabs: function () {
            $(document).on("click", ".modal ul.modal-tabs a", function (evt) {
                evt.preventDefault();
                var $modalInstance = Application.getModalInstance(this);
                Application.modalTabSelector(this, $modalInstance);
            });
        },

        tabSelector: function (evt) {
            evt.preventDefault();
            var $tab = $(evt.target);
            var $container = $(this);
            var $target = $($tab.attr('href'));

            $container.find('.btn-primary').removeClass('btn-primary').addClass('btn-default');

            $target.siblings('.tab-content').removeClass("active-tab");

            $tab.addClass('btn-primary').removeClass('btn-default');

            $target.addClass("active-tab");

        },

        modalTabSelector: function (sender, $modalInstance) {
            var target = $(sender).attr('href');
            $modalInstance
                .find("ul.modal-tabs li a")
                .not(sender)
                .removeClass("active");

            $modalInstance
                .find(".tab-content")
                .not(target)
                .removeClass("active-tab");

            $(sender)
                .addClass("active")
                .trigger({ type: "tabvisible" });

            $modalInstance
                .find(target)
                .addClass("active-tab");

        },

        getModalInstance: function (sender) {
            return $(sender).closest(".modal");
        },

        setPanelHeight: function () {
            if (Application.headerElem) {
                var panel = Application.headerElem.find(".tab-pane").filter(':visible');

                if (panel.length > 0) {
                    var panelOffset = panel.offset().top,
                        windowHeight = $(window).height() + $(window).scrollTop(),
                        maxHeight = windowHeight - panelOffset;
                    panel.css({ "max-height": (maxHeight) + "px" });
                    if (maxHeight <= (panel.outerHeight())) {
                        panel.css({ 'overflow-y': 'scroll' });

                    } else {
                        panel.css({ 'overflow-y': 'hidden' });

                    }

                }
            }
        },

        setHeightOfToolbarContentHeaders: function (toolbarItem) {
            var toolbarItemContent = toolbarItem.find('.tab-pane'),
                headers = toolbarItemContent.find('h4');

            if (headers.length > 1) {
                var maxHeaderHeight = 0;

                headers.each(function () {
                    var headerHeight = $(this).height();
                    if (headerHeight > maxHeaderHeight) {
                        maxHeaderHeight = headerHeight;
                    }
                });

                headers.height(maxHeaderHeight);
            }
        },

        // Forms and validations, messages

        displayMessage: function (params) {
            /**
             * Displays a warning or confirmation message.
             *
             * Options (object):
             * @element {string} Element to display
             * @duration {integer} Display duration
             * @placeholders {object} Contains placeholders and replacement text
             * @type {string} Type of message (danger|success)
             * @onComplete {function} Function to execute after message has been displayed
             */

            var options = $.extend({ should_hide: false }, params);

            $messageElement = $(options.element);
            $messageElement.siblings(".alert-danger, .alert-success").slideUp(200);

            $messageElement.addClass("alert-" + options.type);

            if ($(options.placeholders).length) {

                var messageContent, messageContentOriginal;

                messageContentOriginal = messageContent = $messageElement.html();

                $.each(options.placeholders, function (index, value) {
                    messageContent = messageContent.replace(index, value);
                });

                $messageElement.html(messageContent);
            }

            $messageElement
                .stop()
                .slideDown(200, function () {
                    if (!options.should_hide) {
                        if ($.isFunction(options.onComplete)) {
                            options.onComplete();
                        }
                    }
                });

            if (options.should_hide) {
                $messageElement
                    .delay(options.duration)
                    .slideUp(200, function () {
                        $messageElement.html(messageContentOriginal);

                        if ($.isFunction(options.onComplete)) {
                            options.onComplete();
                        }
                    });
            }

        },

        restoreDeleteButton: function ($deleteButton, $confirmDeleteButton) {
            $deleteButton.show();
            $confirmDeleteButton.hide();
        },

        confirmDelete: function ($deleteButton, $confirmDeleteButton) {
            $deleteButton.hide();
            $confirmDeleteButton.show();
            setTimeout(function () {
                if (!$confirmDeleteButton.prop("disabled")) {
                    Application.restoreDeleteButton($deleteButton, $confirmDeleteButton);
                }
            }, 3000);
        },

        discardFormChanges: function () {
            Application.currentForm = $(Application.workingFormID).serializeQueryString();
            var confirmMsg = $("[data-warning='" + Application.workingFormID + "']").text(),
                default_message = "You have unsaved changes, are you sure you want to leave?";

            if (confirmMsg.length < 1) {
                confirmMsg = default_message;
            }

            if (Application.unchangedForm) {
                if (Application.currentForm != Application.unchangedForm && !Application.disableFormConfirm) {
                    Application.disableFormConfirm = false;
                    if (confirm(confirmMsg)) {
                        Application.workingFormID = null;
                        Application.currentForm = null;
                        Application.unchangedForm = null;
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }

            if (Application.productListController != null) {
                if (Application.productListController.unsavedChanges
                    && Application.productListController.numberOfItems() > 0
                    && !Application.disableFormConfirm) {
                    Application.disableFormConfirm = false;
                    var confirmMsg = Application.productListController.leaveWarning();

                    if (confirmMsg.length < 1) {
                        confirmMsg = default_message;
                    }

                    if (confirm(confirmMsg)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }

            Application.disableFormConfirm = false;

            return true;
        },

        cleanForm: function ($form) {
            if (!$form.hasClass("exclude-from-confirm")) {
                Application.workingFormID = "#" + $form.attr("id");
                Application.unchangedForm = $(Application.workingFormID).serializeQueryString();
            }
        },

        resetForm: function ($form) {
            var $elems_not_to_reset = $form.find("input.no-reset, select.no-reset, textarea.no-reset"),
                key = "temp-reset-value",
                $multipleInput = $form.find(".emailList, .ccemailList");

            $.each($elems_not_to_reset, function () {
                $(this).data(key, $(this).val());
            });

            $form.trigger("reset");

            if ($multipleInput.length > 0) {
                $multipleInput.tagsinput("removeAll");
            }

            $.each($form.find("select.select2-offscreen:not([disabled],[readonly])"), function () {
                var value = $(this).data("placeholder");

                if (value === undefined) {
                    value = $(this).find("option").first().text();
                }

                $(this).select2("val", value);
            });

            $.each($elems_not_to_reset, function () {
                var value = $(this).data(key);
                $(this).data(key, "");

                if ($(this).prop("tagName") === "SELECT" && $(this).hasClass("select2-offscreen")) {
                    $(this).select2("val", value);
                }
                else {
                    $(this).val(value);
                }
            });
        },

        resetFormValidations: function ($elem) {
            $elem.find(".alert-danger, .alert-success").slideUp(200);
            $elem.find(".error").removeClass("error");
        },

        validateInput: function ($elem) {
            var inputOk = true;

            function isValidEmailAddress(emailAddress) {
                var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
                return pattern.test(emailAddress);
            }

            function isValidateDecimal(price) {
                var pattern = new RegExp(/^[0-9]+([,.][0-9]+)?$/g);
                return pattern.test(price);
            }

            $elem
                .find("input")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);

                    if (!$this.parents().hasClass("rebate-item")) {
                        $this.removeClass("error");
                        if ($this.is("[required]")
                            && $this.val().trim().length == 0) {
                            $this.addClass("error");
                            inputOk = false;
                        }
                    }
                });

            $elem
                .find("textarea")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);
                    $this.removeClass("error");
                    if ($this.is("[required]")
                        && $this.val().trim().length == 0) {
                        $this.addClass("error");
                        inputOk = false;
                    }
                });

            $elem
                .find("input[type='checkbox']")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);
                    $this.removeClass("error");
                    if ($this.is("[required]")
                        && !$this.prop('checked')) {
                        $this.addClass("error");
                        inputOk = false;
                    }
                });

            $elem
                .find("select")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this),
                        $select2 = $this.next(".select2-container");
                    $select2.removeClass("error");
                    if ($this.is("[required]")
                        && !$this.val()) {
                        $select2.addClass("error");
                        inputOk = false;
                    }
                });

            $elem
                .find(".rebate-item")
                .each(function () {
                    var $this = $(this);
                    if (!Application.validateRebateItem($this)) {
                        inputOk = false;
                    }
                });

            $elem
                .find("input[type='email']")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);
                    $this.removeClass("error");
                    if ($this.is("[required]")
                        && !isValidEmailAddress($this.val())) {
                        $this.addClass("error");
                        inputOk = false;
                    }
                });

            $elem
                .find(".emailList, .ccemailList")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this),
                        multipleInput = $this.siblings(".bootstrap-tagsinput"),
                        emailArray = $this.tagsinput("items");

                    multipleInput.removeClass("error");

                    for (i = 0; i < emailArray.length; i++) {
                        var $tag = $(multipleInput.children(".tag")[i]);
                        if (!isValidEmailAddress(emailArray[i])) {
                            multipleInput.addClass("error");
                            $tag.removeClass("validInput");
                            $tag.addClass("invalidInput");
                            inputOk = false;
                        }
                    }

                    if ($this.is("[required]") && $this.val().length == 0) {
                        multipleInput.addClass("error");
                        inputOk = false;
                    }
                });

            $elem
                .find(".currency")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);
                    $this.removeClass("error");
                    if ($this.val() !== '')
                        if (!isValidateDecimal($this.val())) {
                            $this.addClass("error");
                            inputOk = false;
                        }
                });

            $tabs = $elem.closest(".modal").find(".modal-tabs");
            $tabs.find("a").removeClass("error")

            if (!inputOk) {
                this.highlightErrorTabs($tabs);
            }

            return inputOk;
        },

        validateRebateItem: function ($elem) {
            var allHaveValue = true,
                noneHasValue = true;

            $elem
                .find("input")
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);
                    $this.removeClass("error");

                    if ($this.val()) {
                        noneHasValue = false;
                    } else {
                        if ($this.is("[required]")) {
                            allHaveValue = false;
                            $this.addClass("error");
                        }
                    }
                });

            if (noneHasValue) {
                $elem
                    .find("input")
                    .not('[disabled]')
                    .each(function () {
                        var $this = $(this);
                        if ($this.is("[required]")) {
                            $this.removeClass("error");
                        }
                    });
            }

            return allHaveValue || noneHasValue;
        },

        highlightErrorTabs: function ($tabs) {
            $tabs
                .find("a")
                .each(function () {
                    var $tabItem = $(this),
                        $tabContent = $($tabItem.attr("href"));

                    $tabItem.removeClass("error");

                    if ($tabContent.find(".error").length > 0) {
                        $tabItem.addClass("error");
                    }
                })
        },

        displayBackendErrors: function (errors, $form, $errorDiv) {
            $form.find(".error").removeClass("error");
            var messagesHtml = "<span class='align-icon-left icon-exclamation'></span>";

            for (var i in errors) {
                var error = errors[i];
                if (error.FieldVisible) {
                    $.each(error.FieldName.split(" "), function () {
                        var field_name = this;
                        $form.find("[name='" + field_name + "']").addClass("error");
                        if (field_name == "year") {
                            $form.find("[name='month']").addClass("error");
                            $form.find("[name='day']").addClass("error");
                        }
                    });
                }

                messagesHtml += "<span>" + error.Message + "</span><br />";
            }

            $errorDiv.html(messagesHtml);
            this.displayMessage({
                element: $errorDiv,
                should_hide: false
            });

            $tabs = $form.parents(".modal").find(".modal-tabs");
            if ($tabs.is(":visible")) {
                this.highlightErrorTabs($tabs);
            }
        },

        cancelFormModal: function ($modal) {
            var $spinnerContainer = $modal.find(".spinner").parent();
            Application.spinnerOff($spinnerContainer);

            // Delay in order to fix issue when Confirm dialog disappears
            setTimeout(function () {
                $modal.one('hidden.bs.modal', function () {
                    // Do this after the modal is hidden
                    Application.resetForm($modal.find("form"));
                    Application.resetFormValidations($modal);
                    Application.cleanForm($modal.find("form"));

                    if ($modal.find(".confirm-delete-btn")) {
                        $deleteBtn = $modal.find(".delete-btn")
                        $confirmDelBtn = $modal.find(".confirm-delete-btn")

                        Application.restoreDeleteButton($deleteBtn, $confirmDelBtn);
                    }
                }).modal('hide');

                $(".modal-backdrop").fadeOut("slow");
            }, 300);
        },

        successMessageModal: function ($modal) {
            var click_elems = $modal.find(".modal-tabs li a:not(.active), input, select, .select2-container, .select2-focusser"),
                focus_elems = $modal.find(".modal-tabs li a:not(.active), select, .select2-container, .select2-focusser");
            function successDone() {
                $modal.find(".alert-success").slideUp(200);
                click_elems.unbind("click.successMessageModal", successDone);
                focus_elems.unbind("focus.successMessageModal", successDone);
            }

            Application.displayMessage({
                element: $modal.find(".formSuccess"),
                onComplete: function () {
                    $modal.find(".alert:not(.alert-success)").slideUp(200);
                    click_elems.on("click.successMessageModal", successDone);
                    focus_elems.on("focus.successMessageModal", successDone);
                    TDPWeb.ExtraNet.cleanForm($($modal).find("form"));
                }
            });
        },

        successMessageForm: function ($form) {
            var click_elems = $form.find("input, select, .select2-container, .select2-focusser, textarea"),
                focus_elems = $form.find("select, .select2-container, .select2-focusser, textarea");

            function successDone() {
                $form.find(".alert-success").slideUp(200);
                click_elems.unbind("click.successMessageForm", successDone);
                focus_elems.unbind("focus.successMessageForm", successDone);
            }

            Application.displayMessage({
                element: $form.find(".formSuccess"),
                onComplete: function () {
                    $form.find(".alert:not(.alert-success)").slideUp(200);
                    click_elems.on("click.successMessageForm", successDone);
                    focus_elems.on("focus.successMessageForm", successDone);
                    TDPWeb.ExtraNet.cleanForm($form);
                    TDPWeb.ExtraNet.resetForm($form);
                }
            });
        },

        clearOrSelectAll: function ($elem, select_all) {
            var parent = $elem.data("target-parent");

            if (parent !== undefined) {
                $elem.closest(parent).find("input[type=checkbox]:not([disabled],[readonly])").prop('checked', select_all);
            }
        },

        closeTooltip: function () {
            $("[data-target='tooltip']").removeClass("visible");
        },

        openTooltip: function ($tooltip) {
            Application.closeTooltip();
            $tooltip.addClass("visible");
        },

        resolveURL: function (url) {
            if (url.toLowerCase().indexOf("http") === 0) {
                return url;
            }

            return Application.removeDoubleSlashes("/" + window.applicationRootURL + "/" + url);
        },

        registerProductListController: function (controller) {
            Application.productListController = controller;
        },

        getProductListController: function () {
            return Application.productListController;
        },

        registerAddPriceController: function (controller) {
            Application.addPriceController = controller;
        },

        getAddPriceController: function () {
            return Application.addPriceController;
        },

        isOnIE: function () {
            return "ActiveXObject" in window;
        },

        setUpPopovers: function (params) {
            var options = $.extend(params),
                placement = "top";

            if (options.placement) {
                switch (options.placement) {
                    case "bottom":
                        placement = "bottom";
                        break;
                    case "right":
                        placement = "right";
                        break;
                    case "left":
                        placement = "left";
                        break;
                    default:
                        break;
                }
            }

            $("[data-popover-target]").popover({
                html: true,
                trigger: "click",
                placement: placement,
                animation: false,
                content: function () {
                    return $(this).siblings($(this).data("popover-target")).html();
                }
            });
        },

        updateDatepicker: function (datepickerId, minOrMax, selectedDate) {
            setTimeout(function () {
                $(datepickerId).datepicker("option", minOrMax, selectedDate);
            }, 200);
        },

        setUpDatepickerPair: function (idBase, max_years_apart) {
            var fromId = idBase + "-from",
                toId = idBase + "-to";

            $(fromId).datepicker({
                onSelect: function (selectedDate) {
                    Application.updateDatepicker(toId, "minDate", selectedDate);

                    if (max_years_apart !== undefined && max_years_apart > 0) {
                        var date = $(this).datepicker("getDate");
                        date.setYear(date.getFullYear() + max_years_apart);
                        Application.updateDatepicker(toId, "maxDate", date);
                        Application.updateDatepicker(toId, "maxDate", undefined);
                    }
                }
            });
            $(fromId).change(function () {
                var selectedDate = $(this).val();
                Application.updateDatepicker(toId, "minDate", selectedDate);

                if (max_years_apart !== undefined && max_years_apart > 0) {
                    var date = $(this).datepicker("getDate");
                    date.setYear(date.getFullYear() + max_years_apart);
                    Application.updateDatepicker(toId, "maxDate", date);
                    Application.updateDatepicker(toId, "maxDate", undefined);
                }
            });

            $(toId).datepicker({
                onSelect: function (selectedDate) {
                    Application.updateDatepicker(fromId, "maxDate", selectedDate);

                    if (max_years_apart !== undefined && max_years_apart > 0) {
                        var date = $(this).datepicker("getDate");
                        date.setYear(date.getFullYear() - max_years_apart);
                        Application.updateDatepicker(fromId, "minDate", date);
                        Application.updateDatepicker(fromId, "minDate", undefined);
                    }
                }
            });
            $(toId).change(function () {
                var selectedDate = $(this).val();
                Application.updateDatepicker(fromId, "maxDate", selectedDate);

                if (max_years_apart !== undefined && max_years_apart > 0) {
                    var date = $(this).datepicker("getDate");
                    date.setYear(date.getFullYear() - max_years_apart);
                    Application.updateDatepicker(fromId, "minDate", date);
                    Application.updateDatepicker(fromId, "minDate", undefined);
                }
            });
        },

        reportEmailSentEvent: function () {
            dataLayer.push({
                'event': 'emailSentEvent',
                'sentFromUrl': window.location.href
            });
        },

        scrollUpToTarget: function ($targetElement, params) {
            var $targetOffset = $targetOffset = $targetElement.offset().top,
                options = $.extend(params),
                $headerOffset;

            if (!options.topMargin) {
                options.topMargin = 15;
            }

            if ($(".in.modal").length > 0) {
                return;
            }
            else {
                $headerOffset = $("#header-toolbar").offset().top;

                if ($targetOffset < $headerOffset) {
                    $("html, body").animate({ scrollTop: $targetOffset - ($("#header-toolbar").height() + options.topMargin) }, 200);
                }
            }
        },

        select2TooltipFormat: function (data, container) {
            container.attr('title', data.text).text(data.text);
        },

        ie9BrowserCheck: function () {
            var browserVersion = (function () {
                var ua = navigator.userAgent, tem,
                    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                if (/trident/i.test(M[1])) {
                    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                    return 'IE ' + (tem[1] || '');
                }
                if (M[1] === 'Chrome') {
                    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                    if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
                }
                M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
                if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
                return M.join(' ');
            })();

            return browserVersion == "MSIE 9";
        },

        updateRequiredGroup: function (id) {
            $inputs = $("[data-required-group='" + id + "']");
            any_non_empty = false;
            $.each($inputs, function () {
                if ($(this).val().trim().length > 0) {
                    any_non_empty = true;
                    return false;
                }
            });

            $.each($inputs, function () {
                $(this).prop("required", !any_non_empty);
                $(this).siblings("label").find(".mandatory").text(any_non_empty ? "" : "*");
            });
        },


        // Language Selector for Ca, Ch, Be
        showHideLanguageSelector: function () {

            var locationURL = window.location.href;

            if (locationURL.length > 0) {

                //Generate CountryParam
                var countryParam = [];

                // Add for Tork.Ca
                countryParam.push({ CountryName: 'tork.ca', FirstLanguage: 'en', SecondLanguage: 'fr', FirstLanguageText: 'EN', SecondLanguageText: 'FR' });

                // Add for Tork.ch
                countryParam.push({ CountryName: 'tork.ch', FirstLanguage: 'de', SecondLanguage: 'fr', FirstLanguageText: 'DE', SecondLanguageText: 'FR' });

                // Add for Tork.be : Future Scope.
                countryParam.push({ CountryName: 'tork.be', FirstLanguage: 'nl', SecondLanguage: 'fr', FirstLanguageText: 'NL', SecondLanguageText: 'FR' });

                $.each(countryParam, function (index, value) {

                    if (locationURL.indexOf(value.CountryName) > 0) {

                        $("#divLanguageSelector").show();

                        var locationPathName = window.location.pathname;
                        if (locationPathName.indexOf(value.FirstLanguage) >= 0) {

                            $("#spanLanguageText").html(value.SecondLanguageText);
                            $("#languageRedirectLink").attr('href', locationURL.replace(value.FirstLanguage, value.SecondLanguage));
                            return false;
                        }

                        else if (locationPathName.indexOf(value.SecondLanguage) >= 0) {

                            $("#spanLanguageText").html(value.FirstLanguageText);
                            $("#languageRedirectLink").attr('href', locationURL.replace(value.SecondLanguage, value.FirstLanguage));
                            return false;
                        }
                    }
                    else {
                        // Hide the Language Selector
                        $("#divLanguageSelector").hide();
                    }
                });

                $("#languageRedirectLink").click(function () {
                    if ($(this).attr('href').length > 0) {
                        window.location.href = $(this).attr('href');
                    }
                });
            }
            else {
                // Hide the Language Selector
                $("#divLanguageSelector").hide();
            }
        },

        // Returns true if url is not undefined and url ends with a file extension
        isFileUrl: function (url) {
            return url && url.split('/').pop().indexOf('.') != -1;
        },

        loadDataSrcImageInContainer: function ($container) {
            $.each($container.find("img[data-src]:not([src])"), function () {
                $(this).attr("src", $(this).data("src"));
                $(this).attr("data-src", undefined);
            });
        },

        
    }

    $(document).ready(function () {
            Application.Init();
        });

    return {
        bindHandlers: Application.bindHandlers,

        closeAllOpenItems: Application.closeAllOpenItems,

        toggleExtranetNav: Application.toggleExtranetNav,
        hideExtranetNav: Application.hideExtranetNav,
        openToolbarItem: Application.openToolbarItem,
        closeToolbarItem: Application.closeToolbarItem,
        closeOpenMenuTabItems: Application.closeOpenMenuTabItems,
        getOpenToolbarItems: Application.getOpenToolbarItems,

        btnState: Application.btnState,
        spinnerOn: Application.spinnerOn,
        spinnerOff: Application.spinnerOff,
        emptySpinnerContainer: Application.emptySpinnerContainer,

        getModalInstance: Application.getModalInstance,

        setPanelHeight: Application.setPanelHeight,
        setHeightOfToolbarContentHeaders: Application.setHeightOfToolbarContentHeaders,
        setEqualHeight: Application.setEqualHeight,

        displayMessage: Application.displayMessage,
        restoreDeleteButton: Application.restoreDeleteButton,
        confirmDelete: Application.confirmDelete,
        discardFormChanges: Application.discardFormChanges,
        cleanForm: Application.cleanForm,
        resetForm: Application.resetForm,
        resetFormValidations: Application.resetFormValidations,
        validateInput: Application.validateInput,
        highlightErrorTabs: Application.highlightErrorTabs,
        displayBackendErrors: Application.displayBackendErrors,
        cancelFormModal: Application.cancelFormModal,
        successMessageModal: Application.successMessageModal,
        successMessageForm: Application.successMessageForm,

        closeTooltip: Application.closeTooltip,
        openTooltip: Application.openTooltip,
        sortTableByColumn: Application.sortTableByColumn,

        registerProductListController: Application.registerProductListController,
        getProductListController: Application.getProductListController,
        isOnIE: Application.isOnIE,
        setUpPopovers: Application.setUpPopovers,
        setUpDatepickerPair: Application.setUpDatepickerPair,
        reportEmailSentEvent: Application.reportEmailSentEvent,

        scrollUpToTarget: Application.scrollUpToTarget,
        checkCaptcha: Application.checkCaptcha,
        select2TooltipFormat: Application.select2TooltipFormat,

        resolveURL: Application.resolveURL,
        ie9BrowserCheck: Application.ie9BrowserCheck,

        updateRequiredGroup: Application.updateRequiredGroup,
        showHideLanguageSelector: Application.showHideLanguageSelector,

        isFileUrl: Application.isFileUrl,

        loadDataSrcImageInContainer: Application.loadDataSrcImageInContainer,

        dateFormat: window.shortDateFormat,
        registerAddPriceController: Application.registerAddPriceController,
        getAddPriceController: Application.getAddPriceController,
    }
})();

Pagination = function () {
    var currentPage = 1,
        currentListLength = 0,
        $pagination,
        $list,
        $initialiList,
        item_class,
        items_per_page,
        usingFilteredList = false;

    function initialize($p, $l, i_class, i_per_page, prev_text, next_text) {
        prev_text = prev_text == undefined ? "" : prev_text
        next_text = next_text == undefined ? "" : next_text

        $pagination = $p;
        $list = $l;
        $initialiList = $l;
        $list.addClass("pagination-list-container");
        item_class = i_class;
        items_per_page = i_per_page;

        $pagination.pagination({
            items: listLength(),
            itemsOnPage: items_per_page,
            edges: 1,
            prevText: "<span class='icon-arrow_left'></span>" + prev_text,
            nextText: next_text + "<span class='icon-arrow_right'></span>",
            onInit: function () {
                if (noPagination()) {
                    $pagination.hide();
                }
            },
            onPageClick: function (pageNumber) {
                setCurrentPage(pageNumber);
                TDPWeb.ExtraNet.scrollUpToTarget($list, { topMargin: 45 });
            }
        });

        var classes = $list.attr("class");
        $list.observe('childlist subtree', function (record) {
            // Observe changes in the subtree
            updatePagination();
        })
            .observe({ attributes: true }, function (record) {
                // Observe changes in attribute class on $list
                var new_classes = $list.attr("class");
                if (new_classes !== classes) {
                    classes = new_classes;
                    updatePagination();
                }
            });
    }

    function listLength() {
        return $list.find(item_class).not(".ui-sortable-placeholder, .ui-sortable-helper").length;
    }

    function lastPage() {
        return Math.ceil(currentListLength / items_per_page);
    }

    function setCurrentPage(pageNumber) {
        currentPage = pageNumber;
        $pagination.pagination('drawPage', pageNumber);
        showItemsInPage(pageNumber);
    }

    function refreshPage() {
        setCurrentPage(currentPage);
    }

    function resetPagination() {
        setCurrentPage(1);
    }

    var filterList = function (filterClass) {
        $list = $list.find("." + filterClass);
        usingFilteredList = true;
        updatePagination();
        setCurrentPage(1);
    };

    var useUnfilteredList = function () {
        $list = $initialiList;
        usingFilteredList = false;
        updatePagination();
        setCurrentPage(1);
    };

    var updatePagination = function () {
        var oldListLength = currentListLength;
        currentListLength = usingFilteredList ? $list.length : listLength();

        var ignoreChanges = $list.hasClass("no-pagination-change"),
            resorted = $list.hasClass("resorted");
        if (!ignoreChanges && (oldListLength !== currentListLength || resorted)) {
            $list.removeClass("resorted");
            $pagination.pagination('updateItems', currentListLength);

            if (currentListLength > oldListLength) {
                // item was added
                setCurrentPage(1);
            }
            else {
                // item was removed
                var last = lastPage();
                if (currentPage > last) {
                    // goto last page
                    setCurrentPage(last);
                }
                else {
                    setCurrentPage(currentPage);
                }
            }

            if (noPagination()) {
                $pagination.hide();
            }
            else {
                $pagination.show();
            }
        }
    }

    function noPagination() {
        return currentListLength <= items_per_page;
    }

    function lastItemInLastPage() {
        var rem,
            isLastPage;
        rem = currentListLength % items_per_page;

        isLastPage = lastPage() === currentPage;

        return rem == 1 && isLastPage;
    }

    function showItemsInPage(pageNumber) {
        var $items = usingFilteredList ? $list : $list.find(item_class);

        //hide all items
        for (var i = 0; i < $items.length; i++) {
            $($items[i]).hide();
        }

        //show items only in this pageNumber
        var start_item = items_per_page * (pageNumber - 1),
            next_page_first_item = items_per_page * pageNumber;
        for (var i = start_item; i < next_page_first_item && i < $items.length; i++) {
            var $item = $($items[i]);
            $item.show();
            TDPWeb.ExtraNet.loadDataSrcImageInContainer($item);
        }
    }

    return {
        initialize: initialize,
        setCurrentPage: setCurrentPage,
        resetPagination: resetPagination,
        refreshPage: refreshPage,
        filterList: filterList,
        useUnfilteredList: useUnfilteredList
    }
};
Pagination = function () {
    var currentPage = 1,
        currentListLength = 0,
        $pagination,
        $list,
        $initialiList,
        item_class,
        items_per_page,
        usingFilteredList = false,
        $slim_pagination,
        $nextButton,
        $prevButton;

    function initialize($p, $l, i_class, i_per_page, prev_text, next_text, $slim_p) {
        prev_text = prev_text == undefined ? "" : prev_text
        next_text = next_text == undefined ? "" : next_text

        $pagination = $p;
        $list = $l;
        $initialiList = $l;
        $list.addClass("pagination-list-container");
        item_class = i_class;
        items_per_page = i_per_page;
        $slim_pagination = $slim_p || $();
        $nextButton = $slim_pagination.find('[data-navigate="nextPage"]');
        $prevButton = $slim_pagination.find('[data-navigate="prevPage"]');

        $pagination.pagination({
            items: listLength(),
            itemsOnPage: items_per_page,
            edges: 1,
            prevText: "<span class='icon-arrow_left'></span>" + prev_text,
            nextText: next_text + "<span class='icon-arrow_right'></span>",
            onInit: function () {
                if (noPagination()) {
                    $pagination.hide();
                }
                if (currentListLength === 0) {
                    $slim_pagination.hide();
                }
            },
            onPageClick: function (pageNumber) {
                setCurrentPage(pageNumber);
                TDPWeb.ExtraNet.scrollUpToTarget($list, { topMargin: 45 });
            }
        });

        var classes = $list.attr("class");
        $list.observe('childlist subtree', function (record) {
            // Observe changes in the subtree
            updatePagination();
        })
            .observe({ attributes: true }, function (record) {
                // Observe changes in attribute class on $list
                var new_classes = $list.attr("class");
                if (new_classes !== classes) {
                    classes = new_classes;
                    updatePagination();
                }
            });
    }

    function listLength() {
        return $list.find(item_class).not(".ui-sortable-placeholder, .ui-sortable-helper").length;
    }

    function lastPage() {
        return Math.ceil(currentListLength / items_per_page);
    }

    function setCurrentPage(pageNumber) {
        currentPage = pageNumber;
        $pagination.pagination('drawPage', pageNumber);
        showItemsInPage(pageNumber);
        if ($prevButton.length) {
            var count = $pagination.pagination('getPagesCount');
            var current = $pagination.pagination('getCurrentPage');

            $prevButton.prop('disabled', current === 1);
            $nextButton.prop('disabled', current === count);
        }
    }

    function refreshPage() {
        setCurrentPage(currentPage);
    }

    function resetPagination() {
        setCurrentPage(1);
    }

    var filterList = function (filterClass) {
        $list = $list.find("." + filterClass);
        usingFilteredList = true;
        updatePagination();
        setCurrentPage(1);
    };

    var useUnfilteredList = function () {
        $list = $initialiList;
        usingFilteredList = false;
        updatePagination();
        setCurrentPage(1);
    };

    var updatePagination = function () {
        var oldListLength = currentListLength;
        currentListLength = usingFilteredList ? $list.length : listLength();

        var ignoreChanges = $list.hasClass("no-pagination-change"),
            resorted = $list.hasClass("resorted");
        if (!ignoreChanges && (oldListLength !== currentListLength || resorted)) {
            $list.removeClass("resorted");
            $pagination.pagination('updateItems', currentListLength);

            if (currentListLength > oldListLength) {
                // item was added
                setCurrentPage(1);
            }
            else {
                // item was removed
                var last = lastPage();
                if (currentPage > last) {
                    // goto last page
                    setCurrentPage(last);
                }
                else {
                    setCurrentPage(currentPage);
                }
            }

            if (noPagination()) {
                $pagination.hide();
            }
            else {
                $pagination.show();
            }

            if (currentListLength === 0) {
                $slim_pagination.hide();
            } else {
                $slim_pagination.show();
            }
        }
    }

    function noPagination() {
        return currentListLength <= items_per_page;
    }

    function lastItemInLastPage() {
        var rem,
            isLastPage;
        rem = currentListLength % items_per_page;

        isLastPage = lastPage() === currentPage;

        return rem == 1 && isLastPage;
    }

    function showItemsInPage(pageNumber) {
        var $items = usingFilteredList ? $list : $list.find(item_class);

        //hide all items
        for (var i = 0; i < $items.length; i++) {
            $($items[i]).hide();
        }

        //show items only in this pageNumber
        var start_item = items_per_page * (pageNumber - 1),
            next_page_first_item = items_per_page * pageNumber;
        for (var i = start_item; i < next_page_first_item && i < $items.length; i++) {
            var $item = $($items[i]);
            $item.show();
            TDPWeb.ExtraNet.loadDataSrcImageInContainer($item);
        }
    }

    return {
        initialize: initialize,
        setCurrentPage: setCurrentPage,
        resetPagination: resetPagination,
        refreshPage: refreshPage,
        filterList: filterList,
        useUnfilteredList: useUnfilteredList,
        updatePagination: updatePagination
    }
};