(function ($) {

    var accountController = (function () {

        var sortOrder = "name",
            currentPage = 1,
            hasSearched = false,
            selectedCountryId = -1,
            itemsToPaginate,
            divToPaginate;

        function searchAccounts(usingSearchInput, query, clearSearch) {
            var $searchField = $("#search-field"),
                $searchResults = $(".search-result"),
                $spinnerLocation = $("#accounts-table"),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage;

            if (selectedCountryId > 0) {
                postParams += "&extraParams={selectedCountryId:" + selectedCountryId + "}";
            }

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/Accounts/Search"),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);

            }).fail(function (error) {

                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
            return false;
        }

        function PaginateDetails(itemsToPaginate, divToPaginate) {
            if (itemsToPaginate.length > 1) {
                var numItems = itemsToPaginate.length;
                var perPage = 10;
                itemsToPaginate.slice(perPage).hide();
                divToPaginate.pagination({
                    items: numItems,
                    itemsOnPage: perPage,
                    prevText: "<span class='icon-arrow_left Testone'></span>" + $("#account-contracts-list-pagination").data("previous-text"),
                    nextText: $("#account-contracts-list-pagination").data("next-text") + "<span class='icon-arrow_right testone'></span>",
                    onPageClick: function (pageNumber) {
                        // someone changed page, lets hide/show trs appropriately
                        var showFrom = perPage * (pageNumber - 1);
                        var showTo = showFrom + perPage;

                        itemsToPaginate.hide() // first hide everything, then show for the new page
                             .slice(showFrom, showTo).show();
                    }
                });
            }
            else {
                divToPaginate.remove();
            }
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchAccounts();
        }

        function sortByName() {
            sortByProperty("name", "name_desc");
        }

        function sortByCountry() {
            sortByProperty("country", "country_desc");
        }

        function sortByDate() {
            sortByProperty("date", "date_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchAccounts(false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function hierarchyFormat(state) {
            if (!state.id) { return state.text; }
            var marginLeft = state.element[0].getAttribute('data-depth') * 20,
                fontWeight = state.element[0].getAttribute('data-depth') == 0 ? "bold" : "normal",
                $state = $('<span style="margin-left:' + marginLeft + 'px;' + 'font-weight:' + fontWeight + '"> ' + state.text + '</span>');
            return $state;
        }

        function showAccountHierarchyModal(body_html, animated) {
            var $modal = $("#account-hierarchy");

            $modal.find(".alert").hide();

            $modal.find(".modal-body").html(body_html);
            $tree = $modal.find(".tree");


            if ($tree.length > 0) {
                $tree.treegrid({ 'initialState': 'collapsed' });
                var rootId = $tree.treegrid('getRootNodes').treegrid('getNodeId');
                $current_node = $tree.find(".current-node");
                traverseUpExpand($current_node, rootId);

                $tree.find("tr").click(function () {
                    $(this).treegrid("toggle");
                });

                //the .treegrid-expander class has a built in toggle, which combined with the tr's toggle gives a double toggle (looks like nothing happens)
                $tree.find(".treegrid-expander").click(function (e) {
                    e.stopPropagation();
                });
            }


            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        function prepareNewAccount() {
            var $modal = $("#new-account");

            TDPWeb.ExtraNet.resetForm($modal.find("form"));
            TDPWeb.ExtraNet.resetFormValidations($modal);

            $modal.find(".modal-body").html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal().addClass("fade");

            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Accounts/AddModalBody'),
                type: 'GET'
            }).done(function (html) {

                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $modal.find(".modal-body").html(html)

                var $parentAccountSelect = $modal.find("select.select-account"),
                    $countrySelect = $modal.find("select.select-country"),
                $currencySelect = $modal.find("select.select-currency");

                $parentAccountSelect.select2({
                    placeholder: $parentAccountSelect.data("placeholder"),
                    formatResult: hierarchyFormat,
                    theme: 'bootstrap'
                });
                $countrySelect.select2({
                    placeholder: $countrySelect.data("placeholder"),
                    theme: 'bootstrap'
                });
                $currencySelect.select2({
                    placeholder: $countrySelect.data("placeholder"),
                    theme: 'bootstrap'
                });

                $modal.modal();
            }).fail(function (error) {

                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });
        }

        function updateLogo(file) {
            if (file == undefined) {
                logo = undefined;
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    logo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                }
                reader.readAsDataURL(file);
            }
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        };

        function deSelectSubTools(element) {
            if (!$(element).is(":checked")) {
                var $subTools = $(element).closest(".acountTool").find(".sub-tool");
                $subTools.prop("checked", false);
            }
        }

        function selectParentTool(element) {
            if ($(element).is(":checked")) {
                var $parentTool = $(element).closest(".acountTool").find(".parent-tool");
                $parentTool.prop("checked", true);
            }
        }


        function showEditAccountModal(body_html, animated) {
            $modal = $("#cb2");
            $modal.find(".alert").hide();
            $modal.find(".modal-body").html(body_html);
            TDPWeb.ExtraNet.cleanForm($modal.find("form"));
            $modal.find("ul.modal-tabs a").first().click();

            var $parentAccountSelect = $modal.find("select.select-account"),
                $countrySelect = $modal.find("select[name=selectedcountry]"),
           $currencySelect = $modal.find("select[name=selectedcurrency]");

            $parentAccountSelect.select2({
                placeholder: $parentAccountSelect.data("placeholder"),
                formatResult: hierarchyFormat,
                theme: 'bootstrap'
            });
            $countrySelect.select2({
                placeholder: $countrySelect.data("placeholder"),
                theme: 'bootstrap'
            });

            $currencySelect.select2({
                placeholder: $countrySelect.data("placeholder"),
                theme: 'bootstrap'
            });

            var $logoInput = $("#logo-file"),
                    $spinnerLocation = $(".prev-wrapper_logo-file");

            $logoInput.change(function () {
                var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                switch (ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true);
                        var file = this.files[0];
                        updateLogo(file);
                        displaySelectedFileName($logoInput);
                        break;
                    default:
                        this.value = '';
                }
            });

            var defaultLogo = $("#defaultLogo");
            if (defaultLogo.length) {
                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true);
                $logoInput.preimage({
                    logoId: defaultLogo.data("logo-id"),
                    name: defaultLogo.data("name"),
                    file: defaultLogo.data("file")
                });
                $("#selectedFileNamePlaceHolder .fileName").html(defaultLogo.data("name"));
            } else {
                $logoInput.preimage();
            }

            $("#preview-image").bind("imageLoaded", function () {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });

            if (!animated) {
                $modal.removeClass("fade");
            }

            $(".remove-logo-btn").on("click", function () {
                $("#logo-file").val('');
                $("#preview-image").hide();
                updateLogo(undefined);
                displaySelectedFileName($logoInput);
            })

            $modal.modal().addClass("fade");
        }

        function addAccount($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#add-account"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".add-account-btn");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Accounts/Create'),
                    type: 'POST',
                    data: $modal.find("form").serialize()
                }).done(function (data) {
                    if (data.id !== undefined) {
                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/Accounts/EditModalBody'),
                            type: 'GET',
                            data: { id: data.id }
                        }).done(function (html) {

                            $modal.removeClass("fade").modal("hide").addClass("fade");
                            TDPWeb.ExtraNet.resetFormValidations($modal);
                            TDPWeb.ExtraNet.spinnerOff($button);

                            showEditAccountModal(html, false);
                            searchAccounts();
                        });
                    }

                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function saveAccount($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#manage-account"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".save-account-btn");
                $form = $modal.find("form");
                var formData = new FormData($form[0]);

                if ($form.find("input[name='logo']")[0].files.length > 0) {
                    var fileName = $("#logo-file").val().split('\\').pop(),
                    fileData = $form.find("input[name='logo']")[0].files[0];
                    formData.append("sameFile", false);
                    formData.append("logoFile", fileData, fileName);
                } else if ($("#defaultLogo").length) {
                    formData.append("sameFile", $("#preview-image").is(":visible"));
                }

                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Accounts/Update'),
                    type: 'PUT',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {

                    searchAccounts();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageModal($modal);

                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function traverseUpExpand($node, rootId) {

            if ($node.treegrid('getNodeId') == rootId) {
            } else {
                var $parent = $node.treegrid('getParentNode');
                $parent.treegrid('expand');
                traverseUpExpand($parent, rootId);
            }
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.text();

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function resetFileInput() {
            $("#choose-file-btn-id").val("");

            // Empty file input for IE
            if (TDPWeb.ExtraNet.isOnIE()) {
                $("#choose-file-btn-id").replaceWith($("#choose-file-btn-id").clone(true));
            }
        }

        function importFile(importId) {

            var $resultsDiv = $("#results"),
                $spinnerLocation = $("#selectedFileNamePlaceHolder");
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
            // Delay in order to show spinner first
            setTimeout(function () {
                var formData = new FormData($("#upload-form")[0]);
                if (importId) {
                    formData.append('id', importId);
                }

                // Add the selected Tools for the Accounts
                var selectedTools = $("#import-accounts-modal input[name='selectedtools']").filter(function (index) { return $(this).is(":checked"); }).map(function (index) { return $(this).val(); }).get().join(',');
                formData.append("selectedTools", selectedTools);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL("/Import/ImportAccounts"),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($("#import-accounts-module"));
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html(html);
                    resetFileInput();
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html("");
                    TDPWeb.ExtraNet.displayMessage({ element: $(".formError") });
                });
            }, 100);
        }

        function displayImportModal() {
            $modal = $("#import-accounts-modal");
            $importAccountsModule = $("#import-accounts-module");
            $importAccountsModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();

            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Import/ImportAccountsModalBody'),
                type: 'GET'
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $importAccountsModule.html(html);

                // This onchange work around is required because the normal .change() make IE 11 fire submit twice.
                $importAccountsModule.find('#choose-file-btn-id').click(function () {
                    $(this).one(
                         'change',
                         function () {
                             displaySelectedFileName($(this));
                             $("#upload-form").submit();
                         }
                    )
                });

                $importAccountsModule.find("#upload-form").submit(function (ev) {
                    ev.preventDefault();
                    importFile();
                });

                $importAccountsModule.delegate("#import-accounts-button", "click", function () {
                    resetFileInput();
                    var importId = $(this).data('import-id');
                    importFile(importId);
                });

            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });;
        }

        function displayCurrencySymbol() {
            var selectedCountry = $(".select-country").val();
            var url = TDPWeb.ExtraNet.resolveURL("/Accounts/GetCurrencyByCountryId");
            var currencyDropdownOption = $('.currency-symbol .select-currency');
            var currencyDropdown = $('.currency-symbol');
            TDPWeb.ExtraNet.spinnerOn(currencyDropdown);
            $(currencyDropdown).hide();
            if (selectedCountry > 0) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        country_id: selectedCountry,
                    }
                }).done(function (data) {

                    if (data.length > 0) {
                        $('.currency-symbol option[value !="0"]').remove();
                        $(currencyDropdown).show();
                        $.each(data, function (i, p) {
                            currencyDropdownOption.append($('<option></option>').val(p.id).html(p.currency_symbol));
                        });
                    }
                    TDPWeb.ExtraNet.spinnerOff(currencyDropdown);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff(currencyDropdown);
                });
            }
        }
        return {
            focusOnSearchField: function () {
                $("#search-field").focus();
            },
            attachDomEvents: function () {
                var $body = $("body "),
                    $searchResult = $(".search-result");

                $searchResult.show();

                $("#account-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    $(e.target).parents("#search-input") ? searchAccounts(true) : searchAccounts();
                });

                $(".clear-search-field").on("click", function () {
                    var $searchField = $("#search-field");
                    if ($searchField.val().length != 0) {
                        $searchField.val("");
                        if (hasSearched) {
                            currentPage = 1;
                            $searchResult.has("table").length != 0 ? searchAccounts(false, "", true) : searchAccounts(true, "", true);
                        }
                    }
                });

                $searchResult.delegate(".show-hierarchy-btn", "click", function (event) {
                    event.stopPropagation();
                    showAccountHierarchyModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Accounts/HierarchyModalBody'),
                        type: 'GET',
                        data: { id: $(this).closest("tr").data("account-id") }
                    }).done(function (html) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showAccountHierarchyModal(html, true);

                    }).fail(function (error) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $searchResult.delegate("tr.account, .edit-managed-product-btn", "click", function (event) {
                    event.stopPropagation();
                    showEditAccountModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Accounts/EditModalBody'),
                        type: 'GET',
                        data: { id: $(this).data("account-id") }
                    }).done(function (html) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showEditAccountModal(html, true);

                        // Paginate the User Details
                        PaginateDetails($("#users_table tbody tr"), $("#account-contracts-list-pagination"));

                    }).fail(function (error) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $body.delegate(".table-head .account", "click", function () {
                    sortByName();
                });

                $body.delegate(".table-head .country", "click", function () {
                    sortByCountry();
                });

                $body.delegate(".table-head .last-updated", "click", function () {
                    sortByDate();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $body.delegate(".modal .save-account-btn", "click", function () {
                    saveAccount(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $("#add-account-btn").on("click", function () {
                    prepareNewAccount();
                });

                $body.delegate(".modal .add-account-btn", "click", function () {
                    addAccount(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $.each($body.find("select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $("#country-filter").on("change", function () {
                    selectedCountryId = $(this).find(":selected").val();
                    currentPage = 1;
                    searchAccounts();
                });

                $("#import-accounts-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }
                    displayImportModal();
                });

                $body.delegate(".modal .parent-tool", "change", function () {
                    deSelectSubTools(this);
                });

                $body.delegate(".modal .sub-tool", "change", function () {
                    selectParentTool(this);
                });

                $body.delegate(".modal .select-country", "change", function () {
                    displayCurrencySymbol();
                });
            },

            showEditAccountModal: showEditAccountModal
        }
    })();

    $(function () {
        accountController.focusOnSearchField();
        accountController.attachDomEvents();

        var modal_body_html = $("#cb2 .modal-body").html();
        if ($.trim(modal_body_html).length > 0) {
            accountController.showEditAccountModal(modal_body_html, true);
        }
    });

})(jQuery);
