(function ($) {
    var AddPriceController = (function () {

        function calculateRebate() {
            var rebatePercent = $("#rebate-percent-input").val().replace(',', '.').trim();
            if (ValidRebatePer(rebatePercent)) {
                $("#product-rebate-alert-msg").hide();
                var grossPrice, discountedPrice, decimalSeparator = null, decimalPoint = 0;
                if (rebatePercent >= 0 && rebatePercent <= 100) {
                    $("#addtoprice-products-list .productPriceListItem").each(function () {
                        grossPrice = $(this).find(".product-gross-price").html().replace(',', '.').trim();
                        if (grossPrice != "" && grossPrice > 0) {
                            if (decimalSeparator == null) {
                                decimalSeparator = getDecimalSeparator($(this).find(".product-gross-price").html());
                            }
                            if (decimalPoint == 0) {
                                decimalPoint = getDecimalPoint(grossPrice)
                            }
                            
                            discountedPrice = (grossPrice - (grossPrice * rebatePercent / 100)).toFixed(decimalPoint)
                            discountedPrice = setDecimalSeparator(discountedPrice, decimalSeparator)
                            var priceInput =  $(this).find(".application-product-price-list-input")
                            if(!$(priceInput).is(':disabled'))
                            {
                                $(this).find(".product-discount").html(setDecimalSeparator(rebatePercent, decimalSeparator));
                                $(this).find(".application-product-price-list-input").val(discountedPrice);
                            }
                        }
                    });
                }
            }
            else {
                TDPWeb.ExtraNet.displayMessage({ element: "#product-rebate-alert-msg" });
            }
        };

        function getDecimalSeparator(val) {
            var separator = "."
            if (val.indexOf(',') !== -1) {
                separator = ","
            }
            return separator;
        };

        function getDecimalPoint(val) {
            var array = val.split('.');
            if (array.length > 1) {
                return array[1].length;
            }
        }

        function setDecimalSeparator(val, separator) {
            val = val.toString()
            if (val.indexOf(separator) === -1) {
                if (separator == '.') {
                    val = val.replace(',', '.')
                }
                else if (separator == ',') {
                    val = val.replace('.', ',')
                }
            }
            return val;
        }

        function recalculateRebate() {
            var discountedPrice = $(this).val().replace(',', '.').trim();
            var grossPrice = $(this).closest("tr").find(".product-gross-price").html().replace(',', '.');
            if (validateNumber(discountedPrice)) {
                $(this).removeClass('error')
                if (grossPrice != "" && grossPrice > 0) {
                    var decimalSeparator = getDecimalSeparator($(this).closest("tr").find(".product-gross-price").html());
                    var percentageDiscount = (1 - discountedPrice / grossPrice) * 100;
                    percentageDiscount = setDecimalSeparator(percentageDiscount.toFixed(2), decimalSeparator)
                    $(this).closest("tr").find(".product-discount").html(percentageDiscount);
                }
            } else {
                $(this).addClass('error')
            }
        };

        function populateProductRebateDetail() {

            //Get Product translation based on account Id
            var url = TDPWeb.ExtraNet.resolveURL("/ProductData/ProductTranslatorsForAccount");
                       
            if ($("#product-translation-accountid").val() > 0) {
                var spinnedDiv = $("#addtoprice-products-list");
                TDPWeb.ExtraNet.spinnerOn(spinnedDiv, false);
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        id: $("#product-translation-accountid").val(),
                        productPrices: getProductPriceList()
                    }
                }).done(function (data) {
                    resetAddPriceControls();
                    if (data.length > 0) {
                        $(".product-discount").html("");
                        var formattedPrice;
                        $("#addtoprice-products-list .productPriceListItem").each(function () {
                            var articleNum = $(this).find(".article-number").html();
                            var pricedetail = data.filter(function (a) {
                                return a.ProductArticleNumber.trim() === articleNum.trim();
                            });
                            formattedPrice = ""
                            if (pricedetail.length > 0) {
                                formattedPrice = pricedetail[0].ProductPriceFormatted;
                            }

                            $(this).find(".product-gross-price").html(formattedPrice);
                            $(this).find(".application-product-price-list-input").val(formattedPrice);
                        });
                        $(".rebate-control").show();
                    }
                    else {
                        $(".rebate-control").hide();
                    }
                    TDPWeb.ExtraNet.spinnerOff(spinnedDiv, false);

                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff(spinnedDiv, false);
                });
            }
            else {
                resetAddPriceControls();
                $(".rebate-control").hide();
            }
        };

        function resetAddPriceControls() {
            $(".application-product-price-list-input").val("");
            $(".tdapcheckbox").prop('checked', false);
            $(".application-product-price-list-input").attr("disabled", false);
        };

        function populateProductTranslatorAccount() {
            if ($(".add-price-wizard").length > 0) {
                var url = TDPWeb.ExtraNet.resolveURL("/ProductData/TranslatorAccountsByArticleNumber");
                $('#product_translation').hide();
                $(".rebate-control").hide();
                var addPriceTable = $("#addtoprice-products-list");
                TDPWeb.ExtraNet.spinnerOn(addPriceTable, false);

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        articleNumbers: getProductArticleNumber()
                    }
                }).done(function (data) {
                    TDPWeb.ExtraNet.spinnerOff(addPriceTable);
                    if (data.length > 0) {
                        $('#product_translation').show();
                        $('#product-translation-accountid').empty();
                        $.each(data, function (i, p) {
                            $('#product-translation-accountid').append($('<option></option>').val(p.Value).html(p.Text));
                        });
                        populateProductRebateDetail();
                    }
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff(addPriceTable)
                });
            }
        };

        function getProductPriceList() {
            // Get the Product Price Deails
            var productPrices = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productPrices.push({ ArticleNumber: $(this).find(".article-number").html(), Price: $(this).find("input").val() });
            });

            return productPrices;
        };

        function getProductArticleNumber() {
            // Get the Product Price Deails
            var productArticleNumber = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productArticleNumber.push($(this).find(".article-number").html());
            });

            return productArticleNumber;
        };

        function ValidRebatePer(val) {

            var isvalid = true;

            if (!validateNumber(val) || val < 0 || val > 100) {
                $("#rebate-percent-input").addClass('error')
                isvalid = false;
            } else {
                $("#rebate-percent-input").removeClass('error');
            }

            return isvalid;
        }

        function validateNumber(val) {
            return val.match(/^[0-9]+([,.][0-9]+)?$/g) != null;
        }

        function validPrice() {
            var invalidCount = 0;

            $("#addtoprice-products-list .productPriceListItem [type='text']").each(function () {
                try {
                    if ($(this).val().length > 0) {
                        var val = $(this).val();
                        if (val.match(/^[0-9]+([,.][0-9]+)?$/g) == null) {
                            $(this).addClass('error');
                            invalidCount = invalidCount + 1;
                        }
                        else {
                            $(this).removeClass('error');
                            
                        }
                    }
                    else {
                        $(this).removeClass('error');
                    }

                } catch (e) {
                    $(this).addClass('error');
                    invalidCount = invalidCount + 1;
                }
            });
            if (invalidCount == 0) {
                $("#rebate-percent-input").removeClass('error');
                $("#rebate-percent-input").val("");
            }

            return invalidCount == 0;
        };

        function clearDisabledPrice() {
            var priceElement = $(this).closest("tr").find(".application-product-price-list-input");
            if (this.checked) {
                priceElement.attr("disabled", true);
                priceElement.val("");
                priceElement.removeClass("error");
                $(this).closest("tr").find(".product-discount").html("");
            }
            else {
                priceElement.attr("disabled", false);
            }
        };

        return {
            attachDomEvents: function () {
                var $body = $("body");
                $('#product_translation').hide();
                $('#rebate-control').hide();
                $body.delegate("#calculate-rebate-btn", "click", calculateRebate);
                $body.delegate(".application-product-price-list-input", "change", recalculateRebate);
                $body.delegate("#product-translation-accountid", "change", populateProductRebateDetail);
                $body.delegate(".tdapcheckbox", "change", clearDisabledPrice);
            },
            populateProductTranslatorAccount: populateProductTranslatorAccount,
            validPrice: validPrice,
        }
    })();

    TDPWeb.ExtraNet.registerAddPriceController(AddPriceController);

    $(function () {
        AddPriceController.attachDomEvents();
    });
})(jQuery);