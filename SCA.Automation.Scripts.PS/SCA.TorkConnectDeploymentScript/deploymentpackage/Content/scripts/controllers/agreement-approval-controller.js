(function ($) {
    'use strict';

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        $('#agreement-approval-pdf-file').hide();
        $('#agreement-approval-pdf-file-ie9-dummy').show();
    } else {
        $('#agreement-approval-pdf-file').show();
        $('#agreement-approval-pdf-file-ie9-dummy').hide();
    }

    //Display the IE9 error Message
    $('#spanIE9Dummy').click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $('#IE9BrowserVersion-modal').modal();
        return;
    });

    var agreementApprovalModalId = '#agreement-approval-modal',
        getAgreementAjax;

    var TKADController = (function () {
        var sortOrder = 'sold_to_name_number',
            currentPage = 1;

        function searchAgreements() {
            var $searchResults = $('.search-result'),
                query = $('#search-field').val(),
                $spinnerLocation = $('#agreements-table'),
                soldToNameNumberFilter = $('#sca-sold-to-name-number-filter').val(),
                countryFilter = $('#country-filter').val(),
                useMiniSpinner = false;

            if (query === undefined) {
                query = '';
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var data = {
                searchTerm: query,
                sortOrder: sortOrder,
                currentPage: currentPage,
                countryFilter: countryFilter,
                soldToNameNumberFilter: soldToNameNumberFilter
            };
            $.ajax({
                type: 'POST',
                url: TDPWeb.ExtraNet.resolveURL('/AgreementApproval/Search'),
                data: data
            }).done(function (result) {
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });

            return false;
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder !== ascending) {
                sortOrder = ascending;
            } else {
                sortOrder = descending;
            }

            searchAgreements();
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) {
                return;
            }

            currentPage = n;
            searchAgreements(query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function showAgreementApprovalModal(content_html, animated) {
            var $modal = $(agreementApprovalModalId);

            $modal.find('.modal-body').html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find('form'));

            if (!animated) {
                $modal.removeClass('fade');
            }

            $modal.modal().addClass('fade');
        }

        function prepareShowingAgreementApprovalModal(agreement_id, is_mobile) {
            showAgreementApprovalModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
            var $spinner_position = $('#empty-spinner-container');

            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            var data = {
                id: agreement_id,
                isOnIE: TDPWeb.ExtraNet.isOnIE(),
                hasAcrobat: getAcrobatInfo().acrobat !== false,
                isMobile: is_mobile
            };
            getAgreementAjax = $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/AgreementApproval/AgreementApprovalModalBody'),
                type: 'POST',
                data: data
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                showAgreementApprovalModal(html, true);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });
        }

        function validateAgreementForm() {
            var $form = $('#agreement-form');
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find('.formError') });
                return false;
            }

            return true;
        }

        function toggleButtons($button, $confirmButton) {
            $button.toggleClass('hidden');
            $confirmButton.toggleClass('hidden');
        }

        function updateAgreement($button) {
            var $form = $('#agreement-form');
            TDPWeb.ExtraNet.spinnerOn($button, true);
            $('#agreement-form :input').prop('disabled', true);

            $.ajax({
                method: 'POST',
                dataType: 'json',
                data: {
                    id: $('#agreement-id').val(),
                    approved: ($button).data('agreement-approved'),
                    contractNumber: $('#agreement-form input[id=pricing-contract-number]').val(),
                    customerNumber: $('#agreement-form input[id=end-customer-number]').val(),
                    crmNumber: $('#agreement-form input[id=CRM-contract-number]').val(),
                },
                url: TDPWeb.ExtraNet.resolveURL('/AgreementApproval/ApproveRejectAgreement')
            }).done(function () {
                TDPWeb.ExtraNet.spinnerOff($button);
                $button.prop('disabled', true);
                TDPWeb.ExtraNet.displayMessage({ element: $form.find('.formSuccess') });
                searchAgreements();
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);
                $('#agreement-form :input').prop('disabled', false);
                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty('errors')) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find('.backEndErrors'));
                }
            });
        }

        function showUploadAgreementPdfModal(e) {
            e.preventDefault();

            var $modal = $('#upload-pdf-modal');
            $modal.modal();
        }

        function resetUploadAgreementPdfForm() {
            $('#agreement-approval-upload-pdf-form input, #agreement-approval-upload-pdf-form select').not(':input[type=button], :input[type=submit], :input[type=reset]').val(null).trigger('change');
            $('#agreement-approval-pdf-file').trigger('change');
            $('#upload-pdf-modal').modal('hide');
            $('#agreement-approval-upload-pdf-success').hide();
            $('#agreement-approval-upload-pdf-error').hide();
        }

        function uploadAgreementPdf() {
            // Validate first

            var $spinnerLocation = $('#agreement-approval-pdf-file-spinner-placeholder'),
                $uploadFormSuccessMessage = $('#agreement-approval-upload-pdf-success'),
                $uploadFormErrorMessage = $('#agreement-approval-upload-pdf-error'),
                $uploadForm = $('#agreement-approval-upload-pdf-form'),
                $uploadFormInputs = $uploadForm.find(':input'),
                uploadFormData = new FormData($uploadForm[0]);

            $uploadFormSuccessMessage.hide();
            $uploadFormErrorMessage.hide();
            $uploadFormInputs.prop('disabled', true);
            $uploadFormInputs.addClass('disabled');
            TDPWeb.ExtraNet.resetFormValidations($uploadForm);
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, 'right');

            setTimeout(function () {
                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/AgreementApproval/UploadAgreementPdf'),
                    type: 'POST',
                    data: uploadFormData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    // Reset form values
                    $uploadFormSuccessMessage.show();
                    setTimeout(function () {
                        resetUploadAgreementPdfForm();
                        $uploadFormInputs.prop('disabled', false);
                        $uploadFormInputs.removeClass('disabled');
                    }, 5000);
                }).fail(function (data) {
                    var $uploadFormErrors = $uploadForm.find('.form-errors');

                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $uploadForm, $uploadFormErrors);
                    $uploadFormInputs.prop('disabled', false);
                    $uploadFormInputs.removeClass('disabled');
                    $uploadFormErrorMessage.show();
                    setTimeout(function () {
                        $uploadFormErrorMessage.hide();
                    }, 5000);
                }).always(function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });
            }, 100);
        }

        return {
            attachDomEvents: function () {
                var $body = $('body');

                $body.delegate('#agreement-approval-button', 'click', function (e) {
                    e.stopPropagation();

                    prepareShowingAgreementApprovalModal($(this).data('agreement-id'), $('html.touch').length > 0);
                });

                $.each($body.find('select'), function () {
                    $(this).select2({
                        placeholder: $(this).data('placeholder'),
                        theme: 'bootstrap',
                        allowClear: true
                    });
                });

                $('#agreement-search-form').on('submit', function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    searchAgreements();
                });

                $(agreementApprovalModalId).on('hide.bs.modal', function () {
                    if (getAgreementAjax != undefined) {
                        getAgreementAjax.abort();
                    }
                });

                $(agreementApprovalModalId + ' .panel-content').on('show.bs.collapse hide.bs.collapse', function () {
                    $(agreementApprovalModalId + ' .panel-toggler').find('.show-when-collapsed, .hide-when-collapsed').toggleClass('hidden');
                });

                $body.delegate('.table-head .Sort-header', 'click', function () {
                    sortByProperty($(this).data('sort-property'), $(this).data('sort-property-desc'));
                });

                $body.delegate('.tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next', 'click', function () {
                    goToPage($(this).data('page-number'), $(this).closest('.tork-pagination').data('query'));
                });

                $body.delegate('#approve-agreement', 'click', function () {
                    TDPWeb.ExtraNet.resetFormValidations($('#agreement-form'));
                    if (validateAgreementForm()) {
                        toggleButtons($('#approve-agreement'), $('#approve-agreement-confirm'));
                    }
                });

                $body.delegate('#reject-agreement', 'click', function () {
                    TDPWeb.ExtraNet.resetFormValidations($('#agreement-form'));                   
                    toggleButtons($('#reject-agreement'), $('#reject-agreement-confirm'));
                    
                });

                $body.delegate('#approve-agreement-confirm', 'click', function () {
                    TDPWeb.ExtraNet.resetFormValidations($('#agreement-form'));
                    if (validateAgreementForm()) {
                        var $approveAgreement = $('#approve-agreement');
                        toggleButtons($approveAgreement, $('#approve-agreement-confirm'));
                        updateAgreement($approveAgreement);
                    }
                });

                $body.delegate('#reject-agreement-confirm', 'click', function () {
                    TDPWeb.ExtraNet.resetFormValidations($('#agreement-form'));

                    var $rejectAgreement = $('#reject-agreement');
                    toggleButtons($rejectAgreement, $('#reject-agreement-confirm'));
                    updateAgreement($rejectAgreement);

                });

                $body.delegate('#upload-agreement-btn', 'click', showUploadAgreementPdfModal);

                $body.delegate('#agreement-approval-upload-pdf-form', 'submit', function (e) {
                    e.preventDefault();
                    uploadAgreementPdf();
                });

                $body.delegate('#agreement-approval-cancel-upload-pdf', 'click', resetUploadAgreementPdfForm);

                $('#agreement-approval-pdf-file').on('change', function () {
                    var regExMatch = (this.value || '').match(/\.([^\.]+)$/),
                        ext = (regExMatch && regExMatch.length > 0) ? regExMatch[1].toLowerCase() : '',
                        fileName = $(this).val().split('\\').pop(),
                        $placeHolder = $('#agreement-approval-pdf-file-name-placeholder .selected-file-name'),
                        noFileSelected = $placeHolder.data('default-text');

                    switch (ext) {
                        case 'pdf':
                            if (fileName && fileName.length) {
                                $placeHolder.html(fileName);
                            } else {
                                $placeHolder.html(noFileSelected);
                            }

                            break;
                        default:
                            $placeHolder.html(noFileSelected);
                            this.value = '';
                    }
                });
            }
        };
    })();

    TKADController.attachDomEvents();
    TDPWeb.ExtraNet.setUpPopovers();
})(jQuery);