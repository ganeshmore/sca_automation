(function ($) {

    var CompetitorReferenceGuideController = (function () {

        var manufacturerList = null,  // A list of Manufacturer objects
            categoriesList = null, // A list of Category objects
            productListPagination,
            fromVirtualClick = false,
            sidebarDisabled = false,
            fromSearch = false,
            initialLoad = true,
            hasSearched = false;

        $("#PrintompetitorRefGuideDetails").click(function () {
            $("#CompetitorRefGuidedetailsContent").print({});
            return false;
        });

        function Category(namePrefix, jsonObject) {
            this.namePrefix = namePrefix;
            this.jsonObject = jsonObject;
            this.nItems = 0;
            this.children = [];

            this.addChild = function (child) {
                this.children.push(child);
            };

            this.getName = function () {
                return this.namePrefix + this.jsonObject.name;
            };

            this.updateItemCount = function (manufacturerName, searchString) {
                this.nItems = 0;

                // Count the items on this node:
                var items = jsonObject.items;
                if (items != null) {
                    for (var i = 0; i < items.length; i++) {
                        if ((manufacturerName == null || items[i].manufacturer == manufacturerName) && items[i].artNo.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
                            this.nItems++;
                        }
                    }
                }

                // Count the items on the children:
                for (var i = 0; i < this.children.length; i++) {
                    this.nItems += this.children[i].nItems;
                }
            };

            this.getItemCountForManufacturer = function (manufacturerName, searchString) {
                var count = 0,
                    items;

                // Count the items on this node:
                items = jsonObject.items;
                if (items != null) {
                    for (var i = 0; i < items.length; i++) {
                        if ((manufacturerName == null || items[i].manufacturer == manufacturerName) && items[i].artNo.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
                            count++;
                        }
                    }
                }

                // Count the items on the children:
                for (var i = 0; i < this.children.length; i++) {
                    count += this.children[i].getItemCountForManufacturer(manufacturerName, searchString);
                }

                return count;
            };

            this.getItemsToShow = function (manufacturerName, searchString) {
                var itemsToShow = [],
                    items;

                // Count the items on this node:
                items = jsonObject.items;
                if (items != null) {
                    for (var i = 0; i < items.length; i++) {
                        if ((manufacturerName == null || items[i].manufacturer == manufacturerName) && items[i].artNo.toLowerCase().indexOf(searchString.toLowerCase()) != -1) {
                            itemsToShow.push(new ItemToShow(items[i], this.jsonObject.code));
                        }
                    }
                }

                // Count the items on the children:
                for (var i = 0; i < this.children.length; i++) {
                    var childItems = this.children[i].getItemsToShow(manufacturerName, searchString);
                    for (var j = 0; j < childItems.length; j++) {
                        itemsToShow.push(childItems[j]);
                    }
                }
                return itemsToShow;
            }
        };

        function Manufacturer(name) {
            this.name = name;
            this.nItems = 0;
        };

        function ItemToShow(item, categoryCode) {
            this.item = item;
            this.categoryCode = categoryCode;
        };

        // Read the json document:
        function handleData(data) {
            manufacturerList = [];
            categoriesList = [];

            for (var i = 0; i < data.length; i++) {
                populateManufacturersAndCategories("", data[i], null);
            }

            // Sort the manufacturer list:
            manufacturerList.sort(sortByName);

            // populate the manufacturers list:
            var manufacturersListUl = $("#manufacturers-list");

            for (var i = 0; i < manufacturerList.length; i++) {
                var liHtml = "<li class='comp-manufacturer";
                if (i === 0) {
                    liHtml += " selected";
                }
                liHtml += "' data-competitor-id='" + i + "'><div class='Arrange Arrange--withGutter Arrange--middle'><div class='Arrange-sizeFill' data-content='manufacturer'>" + manufacturerList[i].name + "</div><div class='Arrange-sizeFit'><span class='icon'></span></div></div></li>";
                var $liHtmlCompetitor = $(liHtml);

                // populate the categories list:
                var $categoriesListUl = $("<ul data-toggle='false' class='categories-list collapse" + (i === 0 ? " in" : "") + "'></ul>");
                for (var j = 0; j < categoriesList.length; j++) {
                    var liHtmlProductType = "<li class='comp-category' data-category-id='" + j + "'>" + categoriesList[j].getName() + "</li>";
                    $categoriesListUl.append(liHtmlProductType);
                }
                $liHtmlCompetitor.append($categoriesListUl);

                manufacturersListUl.append($liHtmlCompetitor);
            }

            update();
        };

        // Populates the manufacturerList and categoriesList after a json specification is received.
        // @param jsonCategory

        function populateManufacturersAndCategories(categoryNamePrefix, jsonCategory, parent) {
            jsonCategory.nItems = 10;
            var category = new Category(categoryNamePrefix, jsonCategory);
            categoriesList.push(category);
            if (parent != null) {
                parent.addChild(category)
            }

            // Loop through the items and look for manufacturers:
            var items = jsonCategory.items;
            if (items != null) {
                for (var i = 0; i < items.length; i++) {
                    var itemManufacturer = items[i].manufacturer;

                    // Is the manufacturer already in the list?
                    var addToList = true;
                    for (var j = 0; j < manufacturerList.length; j++) {
                        if (manufacturerList[j].name == itemManufacturer) {
                            addToList = false;
                            break;
                        }
                    }
                    if (addToList) {
                        var manufacturer = new Manufacturer(itemManufacturer);
                        manufacturerList.push(manufacturer);
                        manufacturer.nItems = i;
                    }
                }
            }

            // Recursive calls down through the structure:
            var categories = jsonCategory.categories;
            if (categories != null) {
                var nextCategoryNamePrefix = categoryNamePrefix;
                for (var i = 0; i < categories.length; i++) {
                    populateManufacturersAndCategories(nextCategoryNamePrefix, categories[i], category);
                }
            }
        };

        function showItem(data) {
            var compArtNo = data.attr("data-id"),
                compCatCode = data.attr("data-category"),
                torkArtNos = data.attr("data-tork-art-nos"),
                $modal = $("#reference-guide-modal"),
                $modalReferenceTable = $modal.find(".comparison-container");


            TDPWeb.ExtraNet.spinnerOn($modal.find(".modal-body"));
            $modal.modal();

            $.ajax({
                type: "GET",
                url: TDPWeb.ExtraNet.resolveURL("/ProductData/GetReferencedTorkItem"),
                data: {
                    torkArtNos: torkArtNos,
                    compArtNo: compArtNo,
                    compCatCode: compCatCode
                },
            }).done(function (result) {
                $modalReferenceTable.html(result);
                $modalReferenceTable.find("select").select2({
                    minimumResultsForSearch: -1,
                    placeholder: $(this).data("placeholder"),
                    theme: 'bootstrap'
                });
                TDPWeb.ExtraNet.spinnerOff($modal.find(".modal-body"));
                $modalReferenceTable.show();
                setTimeout(TDPWeb.ExtraNet.setEqualHeight(), 200);

                !$modalReferenceTable.find(".tork-product").length ? $modal.removeClass("wide") : $modal.addClass("wide");
            }).fail(function (error) {
                $("#reference-product-error-message").slideDown();
                TDPWeb.ExtraNet.spinnerOff($modal.find(".modal-body"));
            });
        };

        function sortByName(a, b) {
            var aName = a.name.toLowerCase(),
                bName = b.name.toLowerCase();
            return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
        };

        function update(clearSearch) {
            var $selectedManufacturer = $("#manufacturers-list").find(".comp-manufacturer.selected").first(),
                selectedManufacturerId = $selectedManufacturer.data('competitor-id'),
                selectedCategoryId = $selectedManufacturer.find(".comp-category.selected").first().data('category-id'),
                searchString = $("#search-input").val();
            
            var selectedCategory = categoriesList[selectedCategoryId];
            
            var $filterTitleManufacturer = $('[data-filter-title="manufacturer"]'),
            $filterTitleCategory = $('[data-filter-title="category"]');

            $filterTitleManufacturer.text(selectedManufacturerId !== undefined && !!manufacturerList[selectedManufacturerId].nItems ? manufacturerList[selectedManufacturerId].name : '');
            $filterTitleCategory.text(selectedCategory !== undefined ? selectedCategory.getName() : '');
            // When label text is empty the element should be hidden - but it isn't when the class `u-inlineBlock` is applied.
            $filterTitleManufacturer.toggleClass('u-inlineBlock', selectedManufacturerId !== undefined && !!manufacturerList[selectedManufacturerId].nItems);
            $filterTitleCategory.toggleClass('u-inlineBlock', selectedCategory !== undefined);

            // Loop through the manufacturers and count the items for the selected category:
            for (var i = 0; i < manufacturerList.length; i++) {
                manufacturerList[i].nItems = 0;

                $.each(categoriesList, function (idx, cat) {
                    manufacturerList[i].nItems += cat.getItemCountForManufacturer(manufacturerList[i].name, searchString);
                });
            }

            var totalMatches = 0;
            for (var a = 0; a < manufacturerList.length; a++) {
                totalMatches += manufacturerList[a].nItems;
            }
            if (totalMatches == 0) {
                $("ul.categories-list").parent().removeClass("selected");
                $("ul.categories-list").collapse("hide");
                updateHitList([], clearSearch);
            }

            if (selectedManufacturerId == undefined || manufacturerList[selectedManufacturerId].nItems == 0) {
                // No items to show for this combination of category, manufacturer and search string.
                // Find the first manufacturer with any items and select that one instead:
                for (var i = 0; i < manufacturerList.length; i++) {
                    if (manufacturerList[i].nItems > 0) {
                        // Select this manufacturer instead:
                        $competitorLi = $("#manufacturers-list").find("[data-competitor-id=" + i + "]");
                        $competitorLi.removeClass("disabled");
                        fromVirtualClick = true;
                        $competitorLi.click();
                        return;
                    }
                }
            }

            if (selectedManufacturerId != undefined) {
                // Loop through the category list and update the item count based on selected manufacturer:
                var manufacturerName = selectedManufacturerId == -1 ? null : manufacturerList[selectedManufacturerId].name;
                for (var i = categoriesList.length - 1; i >= 0; i--) {
                    categoriesList[i].updateItemCount(manufacturerName, searchString);
                }
            }

            var items = [];

            if (selectedCategory === undefined) {
                $.each(categoriesList, function (idx, cat) {
                    items = items.concat(cat.getItemsToShow(manufacturerName, searchString));
                });
            }
            else {
                items = selectedCategory.getItemsToShow(manufacturerName, searchString);
            }

            updateManufacturerList();
            updateCategoriesList(clearSearch);
            if (!fromSearch) {
                updateHitList(items.map(function (item) {
                    return { categoryCode: item.categoryCode, artNo: item.item.artNo };
                }), clearSearch);
            }
        };

        // Updates the html list of manufacturers
        function updateManufacturerList() {
            var manufacturersListUl = $("#manufacturers-list");
            for (var i = 0; i < manufacturerList.length; i++) {
                var manufacturer = manufacturerList[i],
                    $ul = manufacturersListUl.find("[data-competitor-id=" + i + "]"),
                    $span = $ul.find('[data-content="manufacturer"]');
                if (manufacturer.nItems == 0) {
                    $span.html(manufacturer.name);
                    $ul.addClass("disabled");
                    $ul.removeClass("u-cursorPointer");
                } else {
                    $span.html(manufacturer.name + " (" + manufacturer.nItems + ")");
                    $ul.removeClass("disabled");
                    $ul.addClass("u-cursorPointer");
                }
            }
        };

        // Updates the html list of categpries
        function updateCategoriesList(clearSearch) {
            var $selectedManufacturer = $("#manufacturers-list").find(".comp-manufacturer.selected").first(),
                selectedManufacturerId = $selectedManufacturer.data('competitor-id'),
                searchString = $("#search-input").val(),
                isSelected;

            if (fromSearch) {
                $li.parent().find(".selected").removeClass("selected");
                isSelected = false;
            }
            $.each($selectedManufacturer.find(".comp-category"), function (idx, catLi) {
                $li = $(catLi);
                var catId = $li.data("category-id"),
                    cat = categoriesList[catId];

                if (cat.nItems == 0) {
                    $li.html(cat.getName());
                    $li.hide();
                } else {
                    $li.html(cat.getName() + " (" + cat.nItems + ")");
                    $li.removeClass("disabled");
                    $li.show();
                    $li.addClass("u-cursorPointer");

                    if ((fromSearch || fromVirtualClick) && !isSelected) {
                        fromVirtualClick = false;
                        isSelected = true;

                        var $filterTitleCategory = $('[data-filter-title="category"]');
                        $filterTitleCategory.text(cat.getName());
                        $filterTitleCategory.addClass('u-inlineBlock');

                        //$li.click();
                        $li.addClass("selected");
                        var items = cat.getItemsToShow(manufacturerList[selectedManufacturerId].name, searchString);
                        updateHitList(items.map(function (item) {
                            return { categoryCode: item.categoryCode, artNo: item.item.artNo };
                        }), clearSearch);
                    }
                }
            });
        };

        function updateHitList(items, clearSearch) {
            disableSidebar();
            var $hitListTable = $("#hit-list-table"),
                $spinnerLocation = fromSearch || initialLoad ? $("#search-input") : $hitListTable.closest("table");

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, fromSearch || initialLoad);
            initialLoad = false;

            if ($("#update-list-error-message").is(":visible")) {
                $("#update-list-error-message").slideUp();
            }
            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/ProductData/GetFiltered"),
                dataType: "text",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ itemsToShow: items }),
            }).done(function (result) {
                hasSearched = !clearSearch;

                $hitListTable.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                $.each($(".product-module").find("select"), function () {
                    $(this).select2({
                        minimumResultsForSearch: -1,
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });
                CompetitorReferenceGuideController.productListPagination.resetPagination();
                enableSidebar();
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.displayMessage({ element: $("#update-list-error-message") });
                enableSidebar();
            });

            return false;
        };

        function addItemToList(torkArtNo, listId) {
            var buttonSelector = '*[data-tork-art-no="' + torkArtNo + '"] .add-to-list-button';
            var $addButton = $(buttonSelector);
            TDPWeb.ExtraNet.spinnerOn($addButton);

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/ProductData/AddItemToProductList"),
                data: { torkArtNo: torkArtNo, listId: listId },
            }).done(function (result) {
                TDPWeb.ExtraNet.spinnerOff($addButton);

                var $select = $addButton.parents('.add-to-list').find('select');
                $select.find('option:selected').remove();
                $select.select2('val', '');

                $addButton.parents(".add-to-list").find(".add-to-list-button").hide();
                $addButton.parents('.add-to-list').find("#alert-info").slideDown().delay(1000).slideUp();
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($addButton);
            });

            return false;
        }

        function disableSidebar() {
            sidebarDisabled = true;
            $('.competitor-list').addClass('sidebar-disabled');
            $('.competitor-list li:not(.disabled)').css('cursor', 'wait');
        }

        function enableSidebar() {
            $('.competitor-list').removeClass('sidebar-disabled');
            $('.competitor-list li.disabled').css('cursor', 'not-allowed');
            $('.competitor-list li:not(.disabled)').css('cursor', 'pointer');
            sidebarDisabled = false;
        }

        return {
            productListPagination: productListPagination,

            init: function () {
                var $loadingMsg = $("#loading-msg"),
                    $competitorRefGuide = $("#competitor-reference-guide-content"),
                    $spinnerLocation = $("#loading-msg-spinner");

                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ProductData/CompetitorReferenceGuideJson'),
                    type: 'GET'
                }).done(function (data) {
                    $loadingMsg.slideUp();
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);

                    handleData(data);
                    $competitorRefGuide.show();

                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $loadingMsg.find(".loading-msg-error").show()
                    $loadingMsg.find(".loading-msg-pending").hide()
                });

                var $pagination = $("#product-list-pagination"),
                    $resultsContainer = $("#hit-list-table"),
                    $slim_pagination = $('[data-slim-pagination]');
                CompetitorReferenceGuideController.productListPagination = new Pagination();
                CompetitorReferenceGuideController.productListPagination.initialize($pagination, $resultsContainer, ".expandable-header, .expandable", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"), $slim_pagination);

                CompetitorReferenceGuideController.attachDomEvents();
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    $competitorReferenceGuideElem = $("#competitor-reference-guide-module");

                // Add callbacks for the list items
                $competitorReferenceGuideElem.on("click", ".comp-category", function (e) {
                    e.stopPropagation();
                    if (!$(this).hasClass("disabled") && !sidebarDisabled) {
                        $(this).parent().find(".selected").removeClass("selected");
                        $(this).addClass("selected");
                        fromSearch = fromVirtualClick;
                        update();
                        if ($('#competitor-filter-toggle:visible').length) {
                            toggleFilter();
                        }
                        
                    }
                });

                var $pagination = $("#product-list-pagination");
                $body.delegate('[data-slim-pagination] [data-navigate]', 'click', function () {
                    var $clicked = $(this);
                    $pagination.pagination($clicked.data('navigate'));
                });

                $competitorReferenceGuideElem.on("click", ".comp-manufacturer", function (e) {
                    if (!$(this).hasClass("disabled") && !sidebarDisabled) {
                        $(this).parent().find(".selected").removeClass("selected");
                        $(this).addClass("selected");

                        $ulToOpen = $(this).find("ul.categories-list");
                        $(this).parent().find("ul.categories-list.in, ul.categories-list.collapsing").not($ulToOpen).collapse('hide');
                        $ulToOpen.collapse('show');
                        fromSearch = fromVirtualClick;
                        update();
                    }
                });

                $competitorReferenceGuideElem.on("click", ".expandable-header", function () {
                    showItem($(this));
                });

                $body.delegate(".toggleReadMore", "click", function () {
                    $link = $(this);
                    $content = $link.closest('.readMoreContainer').find(".readMoreContent");

                    $link.find('.icon').toggleClass('icon-plus icon-minus');
                    $link.find('.iconlabel').toggle();

                    $content.slideToggle('fast');
                });

                $body.delegate(".add-to-list-button", "click", function () {
                    var $wrapperDiv = $(this).parents(".add-to-list"),
                        torkArtNo = $wrapperDiv.attr("data-tork-art-no"),
                        listId = $wrapperDiv.find("select").val();
                    addItemToList(torkArtNo, listId);
                });

                $body.delegate(".add-to-list-select", "change", function () {
                    if ($(this).val()) {
                        $(this).parents(".add-to-list").find(".add-to-list-button").show();
                    }
                    else {
                        $(this).parents(".add-to-list").find(".add-to-list-button").hide();
                    }
                });

                $body.delegate("#competitor-filter-toggle", "click", toggleFilter);

                $("#reference-guide-modal").on("hide.bs.modal", function () {
                    $(this).find(".alert, table").slideUp();
                });

                // Add callback for the search input
                $("#search-input").keyup(function (e) {
                    if (e.keyCode == 13) {
                        fromSearch = true;
                        update();
                        PushGADetails();
                    }
                });

                $body.delegate("#search-submit", "click", function () {
                    fromSearch = true;
                    update();
                    PushGADetails();
                });

                function toggleFilter() {
                    var $toggler = $('#competitor-filter-toggle'),
                        $filterContent = $('#competitor-filter'),
                        $togglerIndicator = $toggler.find('[data-indicator]'),
                        indicatorStateUp = 'icon-arrow_right_borderless',
                        indicatorStateDown = 'icon-arrow_down_borderless';


                    if ($filterContent.is(":visible")) {
                        $filterContent.collapse('hide');

                        $togglerIndicator
                            .removeClass(indicatorStateDown)
                            .addClass(indicatorStateUp);
                    } else {
                        $filterContent
                            .collapse('show');

                        $togglerIndicator
                            .addClass(indicatorStateDown)
                            .removeClass(indicatorStateUp);
                    }

                    $(this).blur();
                }

                // Push the Search Term to Google Analytics
                function PushGADetails()
                {
                    dataLayer.push({
                        'event': 'gaEvent',
                        'eventCategory': 'Competitor reference guide',
                        'eventAction': 'Search',
                        'eventLabel': $("#search-input").val()
                    });
                }

                $(".clear-search-field").on("click", function () {
                    var $searchInput = $("#search-input");
                    if ($searchInput.val().length != 0) {
                        $searchInput.val("");

                        if (hasSearched) {
                            fromSearch = true;
                            update(true);
                        }
                    }
                });

                $body.on("click", "[data-toggle-col]", function(e) {
                    var $tab = $(e.currentTarget);
                    $tab.siblings().removeClass('is-active');
                    $tab.addClass('is-active');
                    $('[data-show-col]').attr('data-show-col', $tab.data('toggleCol'));
                });
            }
        }
    })();

    (function () {
        CompetitorReferenceGuideController.init();
    })();

})(jQuery);