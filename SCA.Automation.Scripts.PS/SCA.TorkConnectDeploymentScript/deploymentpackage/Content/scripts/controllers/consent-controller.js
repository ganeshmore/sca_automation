(function ($) {
    var Consent = {
        init: function () {
            this.attachDomEvents();
        },
        attachDomEvents: function () {
            $('#consentForm').on('click','button[type="submit"]', this.formSubmit);
        },
        formSubmit: function (e) {            
            form = $('#consentForm');
            
            valid = Consent.validateFields(form);

            if (!valid) {
                $('#consentErrorMessage').show(300);
            } else {
                $('#consentErrorMessage').hide();
            }

            return valid;
        },
        validateFields: function (form) {
            valid = true;
            form.find('input[required]').each(function () {
                element = $(this);
                element.removeClass('error');

                if (!$(this).prop('checked')) {
                    element.addClass('error');
                    valid =  false;
                }
            })
            return valid;
        }
    }

    Consent.init();
})(jQuery);