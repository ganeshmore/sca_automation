(function ($) {

    var ContractsController = (function () {
        var pagination,
            ajaxCall,
            _contractNumber,
            _contractDate,
            _contractEndDate,
            _soldToNumber;

        $("#PrintContactDetails").click(function () {
            $("#contractDetailsContent").print({});
            return false;
        });

        // Download the Contract Details
        $("#contract-download-btn").click(function (e) {
            e.stopPropagation();
            turnButtonsOff();
            getContractExcel(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.downloadurl) + "&" + "contractNumber=" + _contractNumber;
                turnButtonsOn();
            }, function (error) {
                onExcelGenerationFailure();
            });
        });

        // Generate Contract Excel
        function getContractExcel(done, fail) {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Excel/GenerateContractDetails"),
                type: "POST",
                data: { contractNumber: _contractNumber, contractDate: _contractDate, contractEndDate: _contractEndDate, soldToNumber: _soldToNumber }
            }).done(done).fail(fail);
        }

        function turnButtonsOff() {
            var $downloadButton = $("#contract-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'disable');
            TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getFileWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });
                }

            }, 15000);
        };

        function turnButtonsOn() {
            var $downloadButton = $("#contract-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'enable');
            clearTimeout(timeOut);

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }
        };

        function onExcelGenerationFailure() {
            turnButtonsOn();
            TDPWeb.ExtraNet.displayMessage({ element: $("#getExcelError-msg") });
        };

        function searchContracts($form) {
            var $button = $form.find("[type='submit']"),
                $resultsContainer = $(".search-result");

            var spinnerDisplay = $("#spinnerDiv").is(":visible");

            if (spinnerDisplay) {
                TDPWeb.ExtraNet.spinnerOn($button);
            }

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            }
            else {
                if (ajaxCall) {
                    ajaxCall.abort(); // if getLatestContracts is still pending
                }

                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.btnState($button, 'disable');
                $("#contracts-results").hide();

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Contracts/SearchContracts'),
                    type: 'POST',
                    data: $form.serialize()
                }).done(function (html) {

                    if (spinnerDisplay) {
                        TDPWeb.ExtraNet.spinnerOff($button);
                    }
                    TDPWeb.ExtraNet.resetFormValidations($form);
                    TDPWeb.ExtraNet.btnState($button, 'enable');
                    $("#contracts-results").show();

                    $resultsContainer.html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th").first(), false, true);
                }).fail(function (error) {
                    if (spinnerDisplay) {
                        TDPWeb.ExtraNet.spinnerOff($button);
                    }
                    TDPWeb.ExtraNet.btnState($button, 'enable');
                });
            }
        }

        function showContractDetailsModal(content_html, animated) {
            $modal = $("#contract-details");

            $modal.find(".modal-body").html(content_html);

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        return {
            getLatestContracts: function () {
                var $resultsContainer = $(".search-result");
                TDPWeb.ExtraNet.spinnerOn($resultsContainer);

                ajaxCall = $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Contracts/FetchLatestContracts'),
                    type: 'POST',
                    data: {}
                }).done(function (html) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                    $resultsContainer.html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th.validity-from-date").first(), true, true);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                });
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    getContractDetails,
                    $resultsContainer = $(".search-result");

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $("#contract-date").datepicker();

                $body.delegate("#contracts-form", "submit", function (evt) {
                    evt.preventDefault();
                    searchContracts($(this));
                });

                $resultsContainer.delegate("tr.contract", "click", function (evt) {
                    evt.stopPropagation();
                    showContractDetailsModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var contractNumber = $(this).data("contract-number"),
                        contractDate = $(this).data("contract-date"),
                        soldToNumber = $(this).data("soldto-number"),
                        $spinner_position = $("#empty-spinner-container"),
                        contractEndDate = $(this).data("contract-todate");
                        _contractNumber = contractNumber;
                        _contractDate = contractDate;
                        _soldToNumber = soldToNumber;
                        _contractEndDate = contractEndDate;

                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    getContractDetails = $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Contracts/ContractDetails'),
                        type: 'POST',
                        data: { contractNumber: contractNumber, contractDate: contractDate, contractEndDate: contractEndDate, soldToNumber: soldToNumber }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showContractDetailsModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $(".modal").on("hide.bs.modal", function () {
                    if (getContractDetails != undefined) {
                        getContractDetails.abort();
                    }
                });

                var $pagination = $("#search-contracts-pagination");
                var $slim_pagination = $('[data-slim-pagination]');

                ContractsController.pagination = new Pagination();
                ContractsController.pagination.initialize($pagination, $resultsContainer, "tbody tr.item", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"), $slim_pagination);

                $body.delegate('[data-slim-pagination] [data-navigate]', 'click', function () {
                    var $clicked = $(this);
                    $pagination.pagination($clicked.data('navigate'));
                });
            }
        }
    })();

    $(function () {
        ContractsController.attachDomEvents();
        ContractsController.getLatestContracts();
    });
})(jQuery);