(function ($) {

    var countryController = (function () {

        var sortOrder = "name",
            currentPage = 1,
            hasSearched = false;

        function searchCountries(usingSearchInput, query, clearSearch) {
            var $searchField = $("#search-field"),
                $searchResults = $(".search-result"),
                $spinnerLocation = $("#countries-table"),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage;

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/Countries/Search"),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
            return false;
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchCountries();
        }

        function sortByName() {
            sortByProperty("name", "name_desc");
        }

        function sortByDate() {
            sortByProperty("date", "date_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchCountries(false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function saveCountry($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#manage-country"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".save-country-btn");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Countries/Update'),
                    type: 'PUT',
                    data: $modal.find("form").serialize()
                }).done(function (data) {

                    searchCountries();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageModal($modal);

                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function addCountry($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#add-country"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                var country_name = $modal.find("#add-country .select-country option:selected").text(),
                    email_recipients = $modal.find(".emailList").val(),
                    $button = $modal.find(".add-country-btn"),
                    emailArray = $modal.find(".emailList").tagsinput("items"),
                    code = $modal.find("input[name=code]").val();

                TDPWeb.ExtraNet.resetFormValidations($modal);
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Countries/Create'),
                    type: 'POST',
                    data: {
                        name: country_name,
                        code: code,
                        emailrecipients: email_recipients
                    }
                }).done(function (data) {
                    if (data.id !== undefined) {
                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/Countries/EditModalBody'),
                            type: 'GET',
                            data: { id: data.id }
                        }).done(function (html) {
                            $modal.removeClass("fade").modal("hide").addClass("fade");

                            TDPWeb.ExtraNet.spinnerOff($button);
                            showEditCountryModal(country_name, html, false);
                            searchCountries();
                        });
                    }
                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function prepareNewCountry() {
            var $modal = $("#new-country"),
                $select = $modal.find("select.select-country");

            TDPWeb.ExtraNet.resetForm($modal.find("form"));
            TDPWeb.ExtraNet.resetFormValidations($modal);

            $select.select2({
                placeholder: $select.data("placeholder"),
                theme: 'bootstrap'
            });
            $modal.modal();
        }

        function showEditCountryModal(country_name, body_html, animated) {
            $modal = $("#cb1");
            $modal.find(".alert").hide();
            $modal.find(".modal-body").html(body_html);

            TDPWeb.ExtraNet.cleanForm($modal.find('form'));

            $("#country-name").text(country_name);

            $modal.find("ul.modal-tabs a").first().click();

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.find("input[data-role=tagsinput]").tagsinput();
            $modal.modal().addClass("fade");
        }

        function showModal(sender) {
            var $productRow = $(sender),
                modalTarget = $productRow.find("a").attr("data-target"),
                $modal = $(modalTarget),
                $randomizedTools = $(".tool-trigger");

            $modal.find("form").trigger("reset");

            var returnedRandomTools = $randomizedTools.get(),
                sizeAllTools = $randomizedTools.length;

            for (var i = 0; i < sizeAllTools - 1; i++) {
                var swap = Math.floor(Math.random() * (sizeAllTools - i)) + i;
                returnedRandomTools[swap] = returnedRandomTools.splice(i, 1, returnedRandomTools[swap])[0];
            }

            returnedRandomTools = returnedRandomTools.slice(0, 9);
            $randomizedTools.not(returnedRandomTools).trigger("click");
            $modal.modal();
        }

        function showToolOptions(element) {
            var $mandatoryCheckbox = $(element)
               .closest(".row")
               .find(".mandatory-checkbox"),

                $accountCheckbox = $(element)
               .closest(".row")
               .find(".account-checkbox");

            if ($(element).is(":checked")) {
                $mandatoryCheckbox.prop("disabled", false);
                $accountCheckbox.prop("disabled", $mandatoryCheckbox.is(":checked"));

            } else {
                $mandatoryCheckbox.prop("disabled", true);
                $accountCheckbox.prop("disabled", true);
            }
        }

        function toggleAccountMandatory(element) {

            if ($(element).hasClass("account-checkbox")) {
                var $mandatoryCheckbox = $(element)
                   .closest(".row")
                   .find(".mandatory-checkbox");

                if ($(element).is(":checked")) {
                    $mandatoryCheckbox.prop("checked", false);
                }
            } else if ($(element).hasClass("mandatory-checkbox")) {
                var $accountCheckbox = $(element)
                   .closest(".row")
                   .find(".account-checkbox");

                if ($(element).is(":checked")) {
                    $accountCheckbox.prop("checked", false);
                }
            }
        }

        function deSelectSubTools(element) {
            if (!$(element).is(":checked")) {
                var $subTools = $(element).closest(".country-tool").find(".sub-tool");
                $subTools.prop("checked", false);
                showToolOptions($subTools);
            }
        }

        function selectParentTool(element) {
            if ($(element).is(":checked")) {
                var $parentTool = $(element).closest(".country-tool").find(".parent-tool");
                $parentTool.prop("checked", true);
                showToolOptions($parentTool);
            }
            
        }

        return {
            focusOnSearchField: function () {
                $("#search-field").focus();
            },

            attachDomEvents: function () {

                var $body = $("body"),
                    hash = window.location.hash,
                    $searchResult = $(".search-result");

                $searchResult.show();

                if (hash && $(hash).hasClass("modal")) {
                    $(hash).modal();
                }

                $("#country-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    $(e.target).parents("#search-input") ? searchCountries(true) : searchCountries();
                });

                $(".clear-search-field").on("click", function () {
                    var $searchField = $("#search-field");
                    if ($searchField.val().length != 0) {
                        $searchField.val("");
                        if (hasSearched) {
                            currentPage = 1;
                            $searchResult.has("table").length != 0 ? searchCountries(false, "", true) : searchCountries(true, "", true);
                        }
                    }
                });

                $body.delegate(".modal .save-country-btn", "click", function () {
                    saveCountry(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $("#add-country-btn").on("click", function () {
                    prepareNewCountry();
                });

                $body.delegate(".modal .add-country-btn", "click", function () {
                    addCountry(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal .tool-trigger", "change", function () {
                    showToolOptions(this);
                });

                $body.delegate(".modal .mandatory-checkbox, .modal .account-checkbox", "change", function () {
                    toggleAccountMandatory(this);
                });

                $searchResult.delegate("tr.country", "click", function () {
                    var country_name = $(this).find(".country").text();

                    showEditCountryModal(country_name, TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Countries/EditModalBody'),
                        type: 'GET',
                        data: { id: $(this).data("country-id") }
                    }).done(function (html) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showEditCountryModal(country_name, html, true);

                    }).fail(function (error) {

                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $body.delegate(".table-head .country", "click", function () {
                    sortByName();
                });

                $body.delegate(".table-head .last-updated", "click", function () {
                    sortByDate();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $body.delegate(".modal .parent-tool", "change", function () {
                    deSelectSubTools(this);
                });

                $body.delegate(".modal .sub-tool", "change", function () {
                    selectParentTool(this);
                });

            }
        }
    })();

    $(function () {
        countryController.focusOnSearchField();
        countryController.attachDomEvents();
    });

})(jQuery);
