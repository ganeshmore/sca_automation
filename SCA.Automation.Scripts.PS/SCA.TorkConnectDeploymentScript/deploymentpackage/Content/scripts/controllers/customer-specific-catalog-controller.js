(function ($) {

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        //Hide the Choose File & Show the IE9
        $("#company-logo-file").hide();
        $("#spanIE9Dummy").show();
    }
    else
    {
        $("#company-logo-file").show();
        $("#spanIE9Dummy").hide();
    }

    //Display the IE9 error Message
    $("#spanIE9Dummy").click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $("#IE9BrowserVersion-modal").modal();
        return;
    });

    var CustomerSpecificCatalogController = (function () {
        var companyLogo = undefined,
            companyLogoEmail = undefined,
            currentStep,
            timeOut;
        var $companyLogo = $("#company-logo-file"),
           $spinnerLocation = $companyLogo.next("[data-spinner]"),
           $defaultLogo = $("#defaultLogo"),
           $companyLogoEmail = $("#company-logo-file-email"),
           $spinnerLocationEmail = $companyLogoEmail.next("[data-spinner]"),
           $defaultLogoEmail = $("#defaultLogoEmail"),
           $invalidFormatLocation = $("#invalid-image-format"),
           $removebuttonPDFlogo = $(".remove-logo-btn"),
           $removebuttonEmaillogo = $(".remove-logo-btn-email");

        function validateWizard(step) {

            switch (step) {
                case 1:
                    if (!$("#product-list-added-products .productListItem").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-list-alert-msg" });
                        return false;
                    }
                    break;

                case 2:
                    if (false || !$("#template-selector .template-selected").length) {
                        TDPWeb.ExtraNet.displayMessage({
                            element: "#design-template-alert-msg", onComplete: function () {
                                $("#templateRequiredIcon").show();
                            }
                        });
                        return false;
                    }
                    break;
                case 3:
                    TDPWeb.ExtraNet.getAddPriceController().populateProductTranslatorAccount();
                    break;
                case 4:
                    if (!TDPWeb.ExtraNet.getAddPriceController().validPrice()) {
                        // Show Eror Messages.
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-price-list-alert-msg" });
                        return false;
                    }
                    break;
            }
            return true;
        };

        function selected_products() {
            return $.map($("#step1 .productListItem .article-number"), function (val, i) { return $(val).text(); });
        }

        function update_company_logo(file, isEmail) {
            if (file == undefined) {
                if (isEmail) {
                    companyLogoEmail = undefined;
                }
                else {
                    companyLogo = undefined;
                }
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    if (isEmail) {
                        companyLogoEmail = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                    }
                    else {
                        companyLogo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                    }
                }
                reader.readAsDataURL(file);
            }
        }

        function fetchCurrentUrlHost() {
            var website = window.location.href.match(/(.*\/{2}.[^\/]*\/?)|(.[^\/]*\/?)/)[0].replace(/\/$/, '');

            if (website.split('//').length > 1) {
                return website.split('//')[1];
            } else {
                return website
            }
        }

        function catalog_params() {
            return {
                customer_company: $("#customerCompanyTextArea").val(),
                headline: $("#catalogueHeadlineTextArea").val(),
                customer_name: $("#customerNameTextArea").val(),
                template: $("#template-selector .template-selected").first().parent(".template-container").data("tcm-id"),
                first_name: $("#step3 input[name='contactFirstName']").val(),
                last_name: $("#step3 input[name='contactLastName']").val(),
                contact_business_phone: $("#step3 input[name='contactBusinessPhone']").val(),
                contact_mobile_phone: $("#step3 input[name='contactMobilePhone']").val(),
                contact_email: $("#step3 input[name='contactEmail']").val(),
                contact_website: $("#step3 input[name='contactWebsite']").val(),
                current_website: fetchCurrentUrlHost,
                contact_company: $("#step3 input[name='contactCompany']").val(),
                include_date: $('#includeDate').prop('checked')
            }
        }

        function getPDF(done, fail) {

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Pdf/GenerateCustomerSpecificCatalog"),
                type: "POST",
                data: {
                    catalog_params: catalog_params(),
                    products: selected_products(),
                    displayLogo: $("#enableLogo").length == 0 || $("#enableLogo").is(":checked"),
                    logo_data: companyLogo,
                    ptAccountId:$("#product-translation-accountid").val() || 0,
                    sku_number: $("#step5 input[name='skuNumber']").is(":checked"),
                    selected_account_id: $("#step5 #account-select").val(),
                    productPrices: getProductPriceList()
                }
            }).done(done).fail(fail);
        }

        function getProductPriceList()
        {
            var productPrices = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productPrices.push(
                    {
                        ArticleNumber: $(this).find(".article-number").html(),
                        Price: $(this).find("input").val(),
                        IsDispenserContract: $(this).find(".tdapcheckbox").prop("checked")
                    });
            });

            return productPrices;
        }

        function sendCatalog(evt) {

            evt.preventDefault();
            var $form = $(this).closest("form"),
                $button = $form.find("[type=submit]"),
                $spinner = $button.find('[data-spinner]');

            TDPWeb.ExtraNet.spinnerOff($spinner);

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($spinner, true, 'right');

                var url = TDPWeb.ExtraNet.resolveURL("/Pdf/SendCustomerSpecificCatalog"),
                    emailArray = $form.find(".emailList").tagsinput("items");
                    ccemailArray = $form.find(".ccemailList").tagsinput("items");

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        catalog_params: catalog_params(),
                        products: selected_products(),
                        displayLogo: $("#enableLogo").length == 0 || $("#enableLogo").is(":checked"),
                        displayEmailLogo: $("#EmailenableLogo").length == 0 || $("#EmailenableLogo").is(":checked"),
                        logo_data: companyLogo,
                        email_logo_data: companyLogoEmail,
                        ptAccountId: $("#product-translation-accountid").val() || 0,
                        logo_file_name: $companyLogo.val() ? $companyLogo.val().split('\\').pop() : "",
                        email_logo_file_name: $companyLogoEmail.val() ? $companyLogoEmail.val().split('\\').pop() : "",
                        emailList: emailArray,
                        ccemailList: ccemailArray,
                        subject: $form.find("input[name='subject']").val(),
                        message: $form.find("textarea[name='message']").val(),
                        sku_number: $("#step5 input[name='skuNumber']").is(":checked"),
                        selected_account_id: $("#step5 #account-select").val(),
                        productPrices: getProductPriceList()
                    }
                }).done(function (data) {
                    TDPWeb.ExtraNet.reportEmailSentEvent();
                    TDPWeb.ExtraNet.spinnerOff($spinner);
                    TDPWeb.ExtraNet.successMessageForm($form);

                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinner);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }
                });
            }
        };

        function turnButtonsOff() {
            var $downloadButton = $("#catalog-download-btn"),
                $previewButton = $("#catalog-preview-btn");

            $previewButton.prop("disabled", true);
            $downloadButton.prop("disabled", true);
            TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getPDFWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWaitMore-msg") });
                }

            }, 15000);
        };

        function turnButtonsOn() {
            var $downloadButton = $("#catalog-download-btn"),
                $previewButton = $("#catalog-preview-btn");

            $previewButton.prop("disabled", false);
            $downloadButton.prop("disabled", false);

            TDPWeb.ExtraNet.spinnerOff($previewButton);
            TDPWeb.ExtraNet.spinnerOff($downloadButton);

            clearTimeout(timeOut);

            if ($("#getPDFWait-msg").is(":visible")) {
                $("#getPDFWait-msg").slideUp();
            }

            if ($("#getPDFWaitMore-msg").is(":visible")) {
                $("#getPDFWaitMore-msg").slideUp();
            }
        };

        function onPdfGenerationFailure() {
            turnButtonsOn();
            TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFError-msg") });
        };

        function downloadCatalog(e) {
            var $downloadButton = $("#catalog-download-btn");
            if (e.target.id == "modal-catalog-download-btn") {
                // Delay in order to let disableFormConfirm be set to true
                setTimeout(function () {
                    window.location = TDPWeb.ExtraNet.resolveURL($("#modal-catalog-download-btn").data("download-url"));
                }, 10);
            }
            else {
                turnButtonsOff();
                TDPWeb.ExtraNet.spinnerOn($downloadButton, true);

                getPDF(function (data) {
                    window.location = TDPWeb.ExtraNet.resolveURL(data.downloadurl);
                    turnButtonsOn();
                }, function (error) {
                    onPdfGenerationFailure();
                });
            }
        };

        function displaySelectedFileName($fileButton, isEmail) {
            var $placeHolder = isEmail ? $("#selectedFileNamePlaceHolderEmail .fileName") : $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if ($fileButton == undefined) {
                $placeHolder.html(noFileSelected);
            }
            else {
                var fileName = '';
                if ($.type($fileButton) == "object") {
                    fileName = $fileButton.val().split('\\').pop();
                }
                else {
                    fileName = $fileButton;
                }

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
            }
        };
        function showPreviewModal() {
            var $modal = $("#catalog-preview-modal"),
                $object = $modal.find("object"),
                $previewButton = $("#catalog-preview-btn"),
                attribute = "data";

            // IE need to use iframe instead of object
            if (TDPWeb.ExtraNet.isOnIE()) {
                $object = $modal.find("iframe");
                attribute = "src";
            }

            turnButtonsOff();
            TDPWeb.ExtraNet.spinnerOn($previewButton, true);

            getPDF(function (data) {
                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL(data.url),
                    type: "GET"
                }).done(function (result) {
                    $("#modal-catalog-download-btn").data("download-url", data.downloadurl);
                    turnButtonsOn();

                    if (!TDPWeb.ExtraNet.isOnIE() || getAcrobatInfo().acrobat) {
                        $object.prop(attribute, TDPWeb.ExtraNet.resolveURL(data.url));
                        $modal.find(".modal-body").html($object.clone());

                        $modal.modal();
                    }
                    else {
                        window.open(TDPWeb.ExtraNet.resolveURL(data.url));
                    }
                }).fail(function () {
                    onPdfGenerationFailure();
                });
            }, function (error) {
                onPdfGenerationFailure();
            });
        };

        function reportCurrentStep(stepNumber) {
            dataLayer.push({
                'event': 'currentStepEvent',
                'stepIndex': stepNumber,
                'stepUrl': window.location.href
            });
        };

        function toggleAccountSelector() {
            $("#acccount-selector").toggle();
        };

        // Remove the selected logo
        function removeSelectedLogo(isEmail) {
            var companyLogoControl = isEmail ? $companyLogoEmail : $companyLogo,
                removeButtonConteol = isEmail ? $removebuttonEmaillogo : $removebuttonPDFlogo;
            companyLogoControl.val("");
            removeButtonConteol.closest('[data-container="logo"]').hide();
            update_company_logo(undefined, isEmail);
            displaySelectedFileName(companyLogoControl, isEmail);
        };

        // Bind the selected files from input
        function bindFileInput(isEmail) {
            var spinnerControl = '';
            var companylogoControl = '';

            if (isEmail) {
                spinnerControl = $spinnerLocationEmail;
                companylogoControl = $companyLogoEmail;
            }
            else {
                spinnerControl = $spinnerLocation;
                companylogoControl = $companyLogo;
            }

            companylogoControl.change(function () {
                $invalidFormatLocation.hide();
                var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                switch (ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        TDPWeb.ExtraNet.spinnerOn(spinnerControl, true, "right");
                        var file = this.files[0];
                        update_company_logo(file, isEmail);
                        displaySelectedFileName(companylogoControl, isEmail);
                        break;
                    default:
                        this.value = '';
                        displaySelectedFileName(undefined, isEmail);
                        TDPWeb.ExtraNet.displayMessage({ element: "#invalid-image-format" });
                        return false;
                }
            });
        };

        // Added method to preview the account logo on selection
        function previewAccountLogo() {
            var selectedAccountId = $("#divAccountLogo select[name=accountid]").val();
            if (selectedAccountId !== undefined && selectedAccountId.length > 0) {
                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "right");
                $companyLogo.val("");
                companyLogo = undefined;

                var url = TDPWeb.ExtraNet.resolveURL("/Marketing/GetAccountLogoDataByAccountId");

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        id: selectedAccountId
                    }
                }).done(function (data) {
                    // Load the PreImage for CompanyLogo Object
                    if (data.LogoData.length > 0) {

                        // Unbind the change event
                        $companyLogo.unbind('change');

                        $companyLogo.preimage({
                            logoId: 'company-logo-file',
                            name: data.FileName,
                            file: data.LogoData
                        });

                        // Bind the change event
                        bindFileInput(false);
                        displaySelectedFileName(data.FileName, false);
                        companyLogo = data.LogoData;
                    }
                    else {
                        removeSelectedLogo(false);
                        TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                        return false;
                    }
                }).fail(function (data) {
                    // Fail to Get the data
                });
            }

        };

        function validPrice() {
            var invalidCount = 0;
            var pat = new RegExp(/^(\d*([.,](?=\d{2}))?\d+)+((?!\2)[.,]\d\d)?$/);
            $("#addtoprice-products-list .productPriceListItem input").each(function () {
                if ($(this).val().length > 0 && !pat.test($(this).val())) {
                    $(this).addClass('error');
                    invalidCount = invalidCount + 1;
                }
                else {
                    $(this).removeClass('error');
                }
            });

            return invalidCount == 0;
        };

        return {
            attachDomEvents: function () {
                $invalidFormatLocation.hide();

                var $body = $("body");

                Wizard.initiateWizard(validateWizard);
                currentStep = Wizard.getCurrentWizardTab();

                $body.delegate(".wizard-item, #nextButton-top, #nextButton-bottom, #previousButton-top, #previousButton-bottom", "click", function () {
                    if (currentStep != Wizard.getCurrentWizardTab()) {
                        currentStep = Wizard.getCurrentWizardTab();
                        reportCurrentStep(currentStep);
                    }
                });

                $body.delegate(".template", "click", function () {
                    $(".template").removeClass("template-selected");
                    $(this).addClass("template-selected");
                });

                $body.delegate("[id^=catalogue-help-]", "mouseleave", function () {
                    $(this).popover('hide');
                });

                // Bind the PDF logo change Event
                bindFileInput(false);

                // Bind Default PDF Logo
                    if ($defaultLogo.length) {
                        $companyLogo.preimage({
                            logoId: $defaultLogo.data("logo-id"),
                            name: $defaultLogo.data("name"),
                            file: $defaultLogo.data("file")
                        });
                    displaySelectedFileName($defaultLogo.data("name"), false);
                    } else {
                        $companyLogo.preimage();
                    }

                // PFD logo Image Loaded event
                $removebuttonPDFlogo.closest('[data-container="logo"]').bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });

                // Remove selected Logo click event
                $removebuttonPDFlogo.on("click", function () {
                    removeSelectedLogo(false);
                });

                // Rest button click .. Not Found
                $("#reset-logo-btn").on("click", function () {
                    TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "right");
                    $companyLogo.unbind('change');
                    bindFileInput();
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });
                    data.FileName($defaultLogo.data("name"), false);
                });

                // Bind the Email logo change event
                bindFileInput(true);

                // Bind default Eamil Logo
                if ($defaultLogoEmail.length) {
                    $companyLogoEmail.preimage({
                        logoId: $defaultLogoEmail.data("logo-id"),
                        name: $defaultLogoEmail.data("name"),
                        file: $defaultLogoEmail.data("file")
                    });
                    displaySelectedFileName($defaultLogo.data("name"), true);
                } else {
                    $companyLogoEmail.preimage();
                }

                // Email logo Image Loaded event
                $removebuttonEmaillogo.closest('[data-container="logo"]').bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocationEmail);
                });

                // Remove selected Logo click event
                $removebuttonEmaillogo.on("click", function () {
                    removeSelectedLogo(true);
                });

                $body.delegate("#catalog-email-btn", "click", sendCatalog);
                $body.delegate("#catalog-download-btn, #catalog-download-link, #modal-catalog-download-btn", "click", downloadCatalog);

                $(".no-touch #catalog-preview-btn").on("click", showPreviewModal);
                $(".touch #catalog-preview-btn").on("click", downloadCatalog);

                $body.delegate("#step5 input[name='skuNumber']", "change", toggleAccountSelector);

                // Add the logic on Account drop down change evernt for Logo display
                $body.delegate("#account-select-Logo", "change", previewAccountLogo);

                //Load logo if one account exists (Added for  Cosmos Id - 1773876)
                previewAccountLogo();

                // Show Hide Wizard
                if ($(".wizard").find(".wizard-item").length == 4) {
                    // No Add Price wizard
                    $(".wizard").find(".wizard-item").addClass("width25");
                    $(".wizard").find(".wizard-item.last").attr("data-index", "4");
            }
                else {
                    // Add Price available
                    $(".wizard").find(".wizard-item").addClass("width20");
        }

            }
        }
    })();

    (function () {
        CustomerSpecificCatalogController.attachDomEvents();
        TDPWeb.ExtraNet.setUpPopovers();
    })();

})(jQuery);
