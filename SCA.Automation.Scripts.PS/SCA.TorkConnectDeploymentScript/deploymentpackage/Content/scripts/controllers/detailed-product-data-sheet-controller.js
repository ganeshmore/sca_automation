(function ($) {

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        //Hide the Choose File & Show the IE9
        $("#company-logo-file").hide();
        $("#spanIE9Dummy").show();
    }
    else {
        $("#company-logo-file").show();
        $("#spanIE9Dummy").hide();
    }

    //Display the IE9 error Message
    $("#spanIE9Dummy").click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $("#IE9BrowserVersion-modal").modal();
        return;
    });

    var DetailedProductDataSheetController = (function () {
        var companyLogo = undefined,
            companyLogoEmail = undefined,
            timeOut;
        var $companyLogo = $("#company-logo-file"),
            $spinnerLocation = $("#company-logo-file"),
            $defaultLogo = $("#defaultLogo"),
            $companyLogoEmail = $("#company-logo-file-email"),
            $spinnerLocationEmail = $("#company-logo-file-email"),
            $defaultLogoEmail = $("#defaultLogoEmail"),
            $invalidFormatLocation =  $("#invalid-image-format"),
            $removebuttonPDFlogo = $(".remove-logo-btn"),
            $removebuttonEmaillogo = $(".remove-logo-btn-email");


        function validateWizard(step) {
            switch (step) {
                case 1:
                    if (!$("#product-list-added-products .productListItem").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-list-alert-msg" });
                        return false;
                    }
                    TDPWeb.ExtraNet.getAddPriceController().populateProductTranslatorAccount();
                    break;
                case 2:

                    if (!TDPWeb.ExtraNet.getAddPriceController().validPrice()) {
                        // Show Eror Messages.
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-price-list-alert-msg" });
                        return false;
                    }
                    break;
            }
            return true;
        };

        function selected_product_ids() {
            return $.map($("#step1 .productListItem .article-number"), function (val, i) { return $(val).text(); });
        };

        function getProductPriceList() {
            // Get the Product Price Deails 1. Article Number. 2. Price, 3. DispenserContract
            var productPrices = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productPrices.push(
                    {
                        ArticleNumber: $(this).find(".article-number").html(),
                        Price: $(this).find("input").val(),
                        IsDispenserContract: $(this).find(".tdapcheckbox").prop("checked")
                    });
            });

            return productPrices;
        };

        function getProductArticleNumber() {
            // Get the Product Price Deails
            var productArticleNumber = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productArticleNumber.push($(this).find(".article-number").html());
            });

            return productArticleNumber;
        };

        function selected_product_names() {
            return $.map($("#step1 .productListItem .article-name"), function (val, i) { return $(val).text(); });
        };

        function turnButtonOff() {
            var $downloadButton = $("#detailed-product-data-sheet-download-btn");

            TDPWeb.ExtraNet.spinnerOn($downloadButton, true);
            TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getPDFWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWaitMore-msg") });
                }

            }, 15000);
        };

        function turnButtonOn() {
            var $downloadButton = $("#detailed-product-data-sheet-download-btn");
            TDPWeb.ExtraNet.spinnerOff($downloadButton);

            clearTimeout(timeOut);

            if ($("#getPDFWait-msg").is(":visible")) {
                $("#getPDFWait-msg").slideUp();
            }

            if ($("#getPDFWaitMore-msg").is(":visible")) {
                $("#getPDFWaitMore-msg").slideUp();
            }
        };

        function getZIP(done, fail) {

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Pdf/GenerateDetailedProductDataSheet"),
                type: "POST",
                data: {
                    product_names: selected_product_names(),
                    product_ids: selected_product_ids(),
                    displayLogo: $("#enableLogo").length == 0 || $("#enableLogo").is(":checked"),
                    logo_data: companyLogo,
                    user_choice: $("#user_Choice").val(),
                    ptAccountId: $("#product-translation-accountid").val() || 0,
                    sku_number: $("#step3 input[name='skuNumber']").is(":checked"),
                    selected_account_id: $("#step3 #account-select").val(),
                    productPrices: getProductPriceList(),
                    
                }
            }).done(done).fail(fail);
        };

        function downloadZip(e) {
            e.preventDefault();
            turnButtonOff();

            getZIP(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.url);
                turnButtonOn();

            }, function (error) {
                turnButtonOn();
                TDPWeb.ExtraNet.displayMessage({ element: "#getPDFError-msg" });
            });
        };

        function sendZip(evt) {
            evt.preventDefault();

            var $form = $(this).closest("form"),
                $button = $form.find("input[type=submit]");

            TDPWeb.ExtraNet.spinnerOff($button);

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($button);
                $('#downloadError').slideUp(200);

                var url = TDPWeb.ExtraNet.resolveURL("/Pdf/SendDetailedProductDataSheet"),
                    emailArray = $form.find(".emailList").tagsinput("items");
                    ccemailArray = $form.find(".ccemailList").tagsinput("items");

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        product_ids: selected_product_ids(),
                        product_names: selected_product_names(),
                        displayLogo: $("#enableLogo").length == 0 || $("#enableLogo").is(":checked"),
                        displayEmailLogo: $("#EmailenableLogo").length == 0 || $("#EmailenableLogo").is(":checked"),
                        logo_data: companyLogo,
                        email_logo_data: companyLogoEmail,
                        user_choice: $("#user_Choice").val(),
                        ptAccountId: $("#product-translation-accountid").val() || 0,
                        logo_file_name: $companyLogo.val() ? $companyLogo.val().split('\\').pop() : "",
                        email_logo_file_name: $companyLogoEmail.val() ? $companyLogoEmail.val().split('\\').pop() : "",
                        emailList: emailArray,
                        ccemailList:ccemailArray,
                        subject: $form.find("input[name='subject']").val(),
                        message: $form.find("textarea[name='message']").val(),
                        sku_number: $("#step3 input[name='skuNumber']").is(":checked"),
                        selected_account_id: $("#step3 #account-select").val(),
                        productPrices: getProductPriceList()
                    }
                }).done(function (data) {
                    TDPWeb.ExtraNet.reportEmailSentEvent();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageForm($form);

                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }
                });
            }
        };

        function toggleAccountSelector() {
            $("#acccount-selector").toggle();
        };

        function update_company_logo(file, isEmail) {
            if (file == undefined) {
                if (isEmail) {
                    companyLogoEmail = undefined;
                }
                else {
                    companyLogo = undefined;
                }
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    if (isEmail) {
                         companyLogoEmail = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                    }
                    else {
                        companyLogo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                    }

                }
                reader.readAsDataURL(file);
            }
        };

        function displaySelectedFileName($fileButton, isEmail) {
            var $placeHolder = isEmail ? $("#selectedFileNamePlaceHolderEmail .fileName") : $("#selectedFileNamePlaceHolder .fileName"),
             noFileSelected = $placeHolder.data("placeholder");

            if ($fileButton == undefined) {
                $placeHolder.html(noFileSelected);
            }
            else {
                var fileName = '';
                if ($.type($fileButton) == "object") {
                    fileName = $fileButton.val().split('\\').pop();
                }
                else {
                    fileName = $fileButton;
                }

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
            }
        };

        // Bind the selected files from input
        function bindFileInput(isEmail) {
            var spinnerControl = '';
            var companylogoControl = '';

            if (isEmail) {
                spinnerControl = $spinnerLocationEmail;
                companylogoControl = $companyLogoEmail;
            }
            else
            {
                spinnerControl = $spinnerLocation;
                companylogoControl = $companyLogo;
            }

            companylogoControl.change(function () {
                $invalidFormatLocation.hide();
                var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                switch (ext) {
                    case 'jpg':
                    case 'jpeg':
                    case 'png':
                    case 'gif':
                        TDPWeb.ExtraNet.spinnerOn(spinnerControl, true, "left");
                        var file = this.files[0];
                        update_company_logo(file, isEmail);
                        displaySelectedFileName(companylogoControl, isEmail);
                        break;
                    default:
                        this.value = '';
                        displaySelectedFileName(undefined, isEmail);
                        TDPWeb.ExtraNet.displayMessage({ element: "#invalid-image-format" });
                        return false;
                }
            });
        };

        // Remove the selected logo
        function removeSelectedLogo(isEmail) {
            var companyLogoControl = isEmail ? $companyLogoEmail : $companyLogo,
                removeButtonConteol = isEmail ? $removebuttonEmaillogo : $removebuttonPDFlogo;
            companyLogoControl.val("");
            removeButtonConteol.closest('[data-container="logo"]').hide();
            update_company_logo(undefined, isEmail);
            displaySelectedFileName(companyLogoControl, isEmail);
        };

        // Added method to preview the account logo on selection
        function previewAccountLogo() {
            var selectedAccountId = $("#divAccountLogo select[name=accountid]").val();
                if (selectedAccountId !== undefined && selectedAccountId.length > 0) {
                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                $companyLogo.val("");
                companyLogo = undefined;

                var url = TDPWeb.ExtraNet.resolveURL("/Marketing/GetAccountLogoDataByAccountId");

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        id: selectedAccountId
                    }
                }).done(function (data) {
                    // Load the PreImage for CompanyLogo Object
                    if (data.LogoData.length > 0) {

                        // Unbind the change event
                        $companyLogo.unbind('change');

                        $companyLogo.preimage({
                            logoId: 'company-logo-file',
                            name: data.FileName,
                            file: data.LogoData
                        });

                        // Bind the change event
                        bindFileInput(false);
                        displaySelectedFileName(data.FileName, false);
                        companyLogo = data.LogoData;
                    }
                    else {
                        removeSelectedLogo(false);
                        TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                        return false;
                    }
                }).fail(function (data) {
                    // Fail to Get the data
                });
            }

        };

        function validPrice() {
            var invalidCount = 0;
            var pat = new RegExp(/^(\d*([.,](?=\d{2}))?\d+)+((?!\2)[.,]\d\d)?$/);
            $("#addtoprice-products-list .productPriceListItem input").each(function () {
                try {
                    if ($(this).val().length > 0 && !pat.test($(this).val())) {
                        $(this).addClass('error');
                        invalidCount = invalidCount + 1;
                    }
                    else {
                        $(this).removeClass('error');
                    }

                } catch (e) {
                    $(this).addClass('error');
                    invalidCount = invalidCount + 1;
                }

            });

            return invalidCount == 0;
        };

        return {
            attachDomEvents: function () {
                $invalidFormatLocation.hide();
                var $body = $("body");

                Wizard.initiateWizard(validateWizard);

               // Bind the PDF logo change Event
               bindFileInput(false);

                // Bind Default PDF Logo
                if ($defaultLogo.length) {
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });
                    displaySelectedFileName($defaultLogo.data("name"), false);
                } else {
                    $companyLogo.preimage();
                }

                // PFD logo Image Loaded event
                $removebuttonPDFlogo.closest('[data-container="logo"]').bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });

                // Remove selected Logo click event
               $removebuttonPDFlogo.on("click", function () {
                    removeSelectedLogo(false);
                });

                // Bind the Email logo change event
                bindFileInput(true);

                // Bind default Eamil Logo
                if ($defaultLogoEmail.length) {
                    $companyLogoEmail.preimage({
                        logoId: $defaultLogoEmail.data("logo-id"),
                        name: $defaultLogoEmail.data("name"),
                        file: $defaultLogoEmail.data("file")
                    });
                    displaySelectedFileName($defaultLogo.data("name"), true);
                } else {
                    $companyLogoEmail.preimage();
                }

                // Email logo Image Loaded event
                $removebuttonEmaillogo.closest('[data-container="logo"]').bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocationEmail);
                });

                // Remove selected Logo click event
                $removebuttonEmaillogo.on("click", function () {
                    removeSelectedLogo(true);
                });

                $body.delegate("#detailed-product-sheet-email-btn", "click", sendZip);
                $body.delegate("#detailed-product-data-sheet-download-btn", "click", downloadZip);

                $body.delegate("#step3 input[name='skuNumber']", "change", toggleAccountSelector);

                // Add the logic on Account drop down change evernt for Logo display
                $body.delegate("#account-select-Logo", "change", previewAccountLogo);
                //Load logo if one account exists (Added for  Cosmos Id - 1773876)
                previewAccountLogo();

                // Show Hide Wizard
                if ($(".wizard").find(".wizard-item").length == 2)
                {
                    // No Add Price wizard
                    $(".wizard").find(".wizard-item").addClass("width50");
                    $(".wizard").find(".wizard-item.last").attr("data-index", "2");
            }
                else {
                    // Add Price available
                    $(".wizard").find(".wizard-item").addClass("width33");
        }
            }
        }
    })();

    (function () {
        DetailedProductDataSheetController.attachDomEvents();
        TDPWeb.ExtraNet.setUpPopovers();

    })();

})(jQuery);
