(function ($) {

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        //Hide the Choose File & Show the IE9
        $("#company-logo-file").hide();
        $("#spanIE9Dummy").show();
    }
    else {
        $("#company-logo-file").show();
        $("#spanIE9Dummy").hide();
    }

    //Display the IE9 error Message
    $("#spanIE9Dummy").click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $("#IE9BrowserVersion-modal").modal();
        return;
    });

    var ExportProductListController = (function () {
        var companyLogo = undefined,
            timeOut;

        function validateWizard(step) {
            switch (step) {
                case 1:
                    if (!$("#product-list-added-products .productListItem").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-list-alert-msg" });
                        return false;
                    }
                    break;
                case 2:
                    TDPWeb.ExtraNet.getAddPriceController().populateProductTranslatorAccount();
                    break;
                case 3:

                    if (!TDPWeb.ExtraNet.getAddPriceController().validPrice()) {
                        // Show Eror Messages.
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-price-list-alert-msg" });
                        return false;
                    }
                    break;
            }
            return true;
        };

        function turnButtonOff() {
            var $downloadButton = $("#product-list-download-btn");

            TDPWeb.ExtraNet.spinnerOn($downloadButton);
            TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getFileWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWaitMore-msg") });
                }

            }, 15000);
        };

        function turnButtonOn() {
            var $downloadButton = $("#product-list-download-btn");
            TDPWeb.ExtraNet.spinnerOff($downloadButton);

            clearTimeout(timeOut);

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }

            if ($("#getFileWaitMore-msg").is(":visible")) {
                $("#getFileWaitMore-msg").slideUp();
            }
        };

        function getProductPriceList() {
            var productPrices = [];
            $("#addtoprice-products-list .productPriceListItem").each(function () {
                productPrices.push(
                    {
                        ArticleNumber: $(this).find(".article-number").html(),
                        Price: $(this).find("input").val(),
                        IsDispenserContract: $(this).find(".tdapcheckbox").prop("checked")
                    });
            });

            return productPrices;
        };

        function downloadProductList() {
            var params = {},
                url = TDPWeb.ExtraNet.resolveURL("/Excel/ExportProductList");

            params.fields = $.map($("#step2 input:checked"), function (val, i) { return $(val).attr("name"); });
            params.products = $.map($("#step1 .productListItem .article-number"), function (val, i) { return $(val).text(); });
            params.sku_number = $("#step2 input[data-tag='BasicDataProductSkuNumber']").is(":checked");
            params.selected_account_id = $("#step4 select[name=accountid]").val();
            params.ptAccountId = $("#product-translation-accountid").val() || 0;
            params.productPrices = getProductPriceList();

            turnButtonOff();

            $.ajax({
                url: url,
                type: 'POST',
                data: params
            }).done(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.url);
                turnButtonOn();

            }).fail(function (error) {
                turnButtonOn();
                TDPWeb.ExtraNet.displayMessage({ element: "#getFileError-msg" });
            });
        };

        function sendProductList(evt) {
            evt.preventDefault();
            var $form = $(this).closest("form");
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                var $button = $form.find("input[type=submit]"),
                    url = TDPWeb.ExtraNet.resolveURL("/Excel/SendProductList"),
                    params = {};

                TDPWeb.ExtraNet.spinnerOn($button);

                $.each($form.serializeArray(), function (idx, elem) {
                    params[elem.name] = elem.value;
                });

                params.fields = $.map($("#step2 input:checked"), function (val, i) { return $(val).attr("name"); });
                params.products = $.map($("#step1 .productListItem .article-number"), function (val, i) { return $(val).text(); });
                params.emailList = $form.find(".emailList").tagsinput("items");
                params.ccemailList = $form.find(".ccemailList").tagsinput("items");
                params.sku_number = $("#step2 input[data-tag='BasicDataProductSkuNumber']").is(":checked");
                params.selected_account_id = $("#step4 select[name=accountid]").val();
                params.displayLogo = $("#enableLogo").length == 0 || $("#enableLogo").is(":checked");
                params.logo_data = companyLogo;
                params.logo_file_name = $("#company-logo-file").val() ? $("#company-logo-file").val().split('\\').pop() : "";
                params.ptAccountId = $("#product-translation-accountid").val() || 0;
                params.productPrices = getProductPriceList();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: params
                }).done(function (data) {
                    TDPWeb.ExtraNet.reportEmailSentEvent();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageForm($form);

                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }
                });;
            }
        };

        function update_company_logo(file) {
            if (file == undefined) {
                companyLogo = undefined;
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    companyLogo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                }
                reader.readAsDataURL(file);
            }
        };

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        };


        return {
            attachDomEvents: function () {
                $("#invalid-image-format").hide();
                var $body = $("body"),
                    $companyLogo = $("#company-logo-file"),
                    $spinnerLocation = $("#company-logo-file"),
                    $defaultLogo = $("#defaultLogo");

                Wizard.initiateWizard(validateWizard);

                function bindFileInput() {
                    $companyLogo.change(function () {
                        $("#invalid-image-format").hide();
                        var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                        switch (ext) {
                            case 'jpg':
                            case 'jpeg':
                            case 'png':
                            case 'gif':
                                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                                var file = this.files[0];
                                update_company_logo(file);
                                displaySelectedFileName($companyLogo);
                                break;
                            default:
                                this.value = '';
                                $("#selectedFileNamePlaceHolder .fileName").html('');
                                TDPWeb.ExtraNet.displayMessage({ element: "#invalid-image-format" });
                                return false;
                        }
                    });
                };

                bindFileInput();

                if ($defaultLogo.length) {
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });
                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                } else {
                    $companyLogo.preimage();
                }

                $("#preview-image").bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });

                $(".remove-logo-btn").on("click", function () {
                    $companyLogo.val("");
                    $("#preview-image").hide();
                    update_company_logo(undefined);
                    displaySelectedFileName($companyLogo);
                });

                $("#reset-logo-btn").on("click", function () {
                    TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                    $companyLogo.unbind('change');
                    bindFileInput();
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });

                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                });

                $body.delegate("#product-list-download-btn", "click", downloadProductList);
                $body.delegate("#product-list-email-btn", "click", sendProductList);
                if ($(".wizard").find(".wizard-item").length == 4) {
                    $(".wizard").find(".wizard-item").addClass("width25");
                    $(".wizard").find(".wizard-item.last").attr("data-index", "4");
                }
                else {
                    $(".wizard").find(".wizard-item.last").attr("data-index", "3");
                }
            }
        }
    })();

    (function () {
        ExportProductListController.attachDomEvents();
        TDPWeb.ExtraNet.setUpPopovers();
    })();

})(jQuery);