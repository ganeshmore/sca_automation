(function ($) {
    var userController = (function () {
        var sortOrder = "last_name",
            currentPage = 1,
            exportLink = "/Excel/ActiveUsers",
            status = "",
            numberOfPending,
            hasSearched = false,
            selectedCountryId = -1;

        function decrementNumberOfPending() {
            numberOfPending--;
            $("#numberOfPending").html(numberOfPending);
        }

        function searchUsers(usingSearchInput, switchTabs, query, clearSearch) {
            var $searchField = $("#search-field"),
                $searchResults = $(".search-result"),
                $spinnerLocation = $("#users-table"),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            if (switchTabs) {
                $spinnerLocation = $("#userTabs");
                //turn off remaining spinner if any
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                $searchResults.fadeOut(100);
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage;

            if (selectedCountryId > 0) {
                postParams += "&extraParams={selectedCountryId:" + selectedCountryId + "}";
            }

            var userIdToPass = $("#selectedUserLabel").val().split(",");
            userIdToPass = userIdToPass.filter(Boolean);

            if (userIdToPass.length > 0) {
                postParams += "&extraParams={selectedUserIds:'" + $("#selectedUserLabel").val() + "'}";
            }

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/ExternalUsers/Search/" + status),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                $searchResults.fadeIn(100);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            }).fail(function (error) {
                $searchResults.fadeIn(100)
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });

            // show hide delete button
            $("#delete-users-btn").hide();
            if (status == "inactive" || status == "pending") {
                // Display the delete button
                $("#delete-users-btn").show();
            }

            return false;
        }

        function selectActiveUsersTab() {
            TDPWeb.ExtraNet.resetFormValidations($('#validationMessages'));
            $('#delete-users-btn').hide();
            status = "";
            window.history.pushState('Active', '', TDPWeb.ExtraNet.resolveURL("/ExternalUsers/"));
            exportLink = "/Excel/ActiveUsers";
            searchUsers(false, true);
        }

        function selectInactiveUsersTab() {
            TDPWeb.ExtraNet.resetFormValidations($('#validationMessages'));
            $('#delete-users-btn').show();
            status = "inactive";
            window.history.pushState('Active', '', TDPWeb.ExtraNet.resolveURL("/ExternalUsers/" + status));
            exportLink = "/Excel/InactiveUsers";
            searchUsers(false, true);
        }

        function selectPendingUsersTab() {
            TDPWeb.ExtraNet.resetFormValidations($('#validationMessages'));
            $('#delete-users-btn').show();
            status = "pending";
            window.history.pushState('Active', '', TDPWeb.ExtraNet.resolveURL("/ExternalUsers/" + status));
            currentPage = 1;
            exportLink = "/Excel/PendingUsers";
            searchUsers(false, true);
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchUsers();
        }

        function sortByFirstName() {
            sortByProperty("first_name", "first_name_desc");
        }

        function sortByLastName() {
            sortByProperty("last_name", "last_name_desc");
        }

        function sortByAccount() {
            sortByProperty("account", "account_desc");
        }

        function sortByRole() {
            sortByProperty("role", "role_desc");
        }

        function sortByCountry() {
            sortByProperty("country", "country_desc");
        }

        function sortByEnabled() {
            sortByProperty("enabled", "enabled_desc");
        }

        function sortByInactiveDays() {
            sortByProperty("inactiveDays", "inactiveDays_desc");
        }

        function sortByCreatedDate() {
            sortByProperty("createddate", "createddate_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchUsers(false, false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function addUser($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#new-user"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".add-user-btn");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/Create'),
                    type: 'POST',
                    data: $modal.find("form").serialize()
                }).done(function (data) {
                    if (data.id !== undefined) {
                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/EditModal'),
                            type: 'GET',
                            data: { id: data.id }
                        }).done(function (html) {
                            $modal.removeClass("fade").modal("hide").addClass("fade");
                            TDPWeb.ExtraNet.resetFormValidations($modal);
                            TDPWeb.ExtraNet.spinnerOff($button);

                            showEditUserModal(html, false);
                            searchUsers();
                        });
                    }
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function deleteUser($modal) {
            var $button = $modal.find(".confirm-delete-user-btn"),
                $saveButton = $modal.find(".save-user-btn"),
                $deleteButton = $modal.find(".delete-user-btn");

            TDPWeb.ExtraNet.spinnerOn($button);
            $saveButton.prop("disabled", true);
            $deleteButton.prop("disabled", true);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/DeleteUsers'),
                type: 'DELETE',
                data: {
                    userIds: $modal.find("form input[name=id]").val(),
                    status: status
                }
            }).done(function (data) {
                searchUsers();

                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);
                TDPWeb.ExtraNet.spinnerOff($button);

                if (status == "pending") {
                    decrementNumberOfPending();
                }
                $modal.modal("hide");
                $("#selectedUserLabel").val('');
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);
                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);
                $deleteButton.show();
                $button.hide();

                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                }
                $("#selectedUserLabel").val('');
            });
        }

        function updateToolsList(userId, roleId, accountId) {
            $spinnerLocation = $("#roles-dropdown-spinner");
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, false, "left");
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/ToolsList'),
                type: 'GET',
                data: { id: userId, role_id: roleId, account_id: accountId }
            }).done(function (html) {
                $(".toolsList").html(html);
                if ($("#hide-details-btn").is(":visible")) {
                    showDetails(0);
                }
                TDPWeb.ExtraNet.setUpPopovers();
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
        }

        function updateAccountNameAndCountryName(accountId) {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/GetAccountNameAndCountryName'),
                type: 'POST',
                data: { account_id: accountId }
            }).done(function (data) {
                $("#country").val(data.countryName);
                $(".mandatory-access").text(data.accountName);
            }).fail(function (error) {
                // should only be error from serious problem like providing wrong account id
            });
        }

        function showEditUserModal(content_html, animated) {
            $modal = $("#edit-user-modal");

            $modal.find(".alert").hide();

            $modal.find(".modal-content").html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));

            $modal.find("ul.modal-tabs a").first().click();

            $.each($modal.find("select"), function () {
                var $this = $(this);

                $this.select2({
                    // data name changed from "placeholder" to "placeholder-text"
                    // select2 v3.5 is doing its own stuff with using empty option to show placeholder text,
                    // which causes the first option to be the placeholder and selectable

                    containerCssClass: null,
                    minimumResultsForSearch: null,
                    placeholder: $this.data("placeholder-text"),
                    formatSelection: TDPWeb.ExtraNet.select2TooltipFormat,
                    theme: 'bootstrap'
                }).unbind("change.editUserModal").on("change.editUserModal", function () {
                    if ($this.attr("name") === "selectedaccount" || $this.attr("name") === "selectedrole") {
                        $form = $this.closest("form");
                        var userId = $form.find("input[name='id']").val(),
                            roleId = $form.find("select[name='selectedrole']").val(),
                            accountId = $form.find("select[name='selectedaccount']").val();

                        updateToolsList(userId, roleId, accountId);

                        if ($this.attr("name") === "selectedaccount") {
                            updateAccountNameAndCountryName(accountId)
                        }
                    }
                });
            });

            TDPWeb.ExtraNet.setUpPopovers();

            if (!animated) {
                $modal.removeClass("fade");
            }

            // this is to fix the multiple select placeholder text being cut off
            $('a[href="#extended-accesses"]').on("click", function () {
                setTimeout(function () {
                    $(".select2-search-field input").blur();
                }, 100)
            });

            TDPWeb.ExtraNet.updateRequiredGroup("edit-external-user-phones");

            $modal.modal().addClass("fade");
        }

        function saveUser($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#manage-user"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".save-user-btn");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/Update'),
                    type: 'PUT',
                    data: $modal.find("form").serialize()
                }).done(function (data) {
                    searchUsers();

                    if ($("#isPendingUser").length > 0) {
                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/EditModal'),
                            type: 'GET',
                            data: { id: $modal.find("#userId").val() }
                        }).done(function (html) {
                            showEditUserModal(html, true);
                            decrementNumberOfPending();
                        }).fail(function (error) {
                            TDPWeb.ExtraNet.spinnerOff($button);
                        });
                    }
                    else {
                        TDPWeb.ExtraNet.spinnerOff($button);
                    }

                    TDPWeb.ExtraNet.successMessageModal($modal);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function prepareNewUser() {
            var $modal = $("#new-user-modal");
            //reset the new external user form
            TDPWeb.ExtraNet.resetForm($modal.find($('.exclude-from-confirm')));
            TDPWeb.ExtraNet.resetFormValidations($modal);

            $.each($modal.find("select"), function () {
                var $this = $(this);

                $this.select2({
                    placeholder: $this.data("placeholder"),
                    theme: 'bootstrap'
                });
            });

            TDPWeb.ExtraNet.updateRequiredGroup("new-user-phones");

            $modal.modal();
        }

        function showDetails(duration) {
            if (duration == null) {
                duration = 400;
            }
            $(".emptyItem").show();
            $(".subtool-container").slideDown(duration);
            $("#show-details-btn").hide();
            $("#hide-details-btn").show();
        }

        function hideDetails() {
            $(".emptyItem").hide();
            $(".subtool-container").slideUp();
            $("#hide-details-btn").hide();
            $("#show-details-btn").show();
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.text();

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function resetFileInput() {
            $("#choose-file-btn-id").val("");

            // Empty file input for IE
            if (TDPWeb.ExtraNet.isOnIE()) {
                $("#choose-file-btn-id").replaceWith($("#choose-file-btn-id").clone(true));
            }
        }

        function importFile(importId) {
            var $resultsDiv = $("#results"),
                $spinnerLocation = $("#selectedFileNamePlaceHolder");
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");

            // Delay in order to show spinner first
            setTimeout(function () {
                var formData = new FormData($("#upload-form")[0]);
                if (importId) {
                    formData.append('id', importId);
                }

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL("/Import/ImportExternalUsers"),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($("#import-users-module"));
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html(html);
                    resetFileInput();
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html("");
                    TDPWeb.ExtraNet.displayMessage({ element: $(".formError") });
                });
            }, 100);
        }

        function updateFileUpload() {
            var isDisabled = !$("#accountsDropdown").val() || !$("#rolesDropdown").val();

            $(".Upload-button.btn.btn-primary").prop("disabled", isDisabled);

            if (isDisabled) {
                $(".Upload-button.btn.btn-primary").addClass("disabled");
                // Add the disabled property from the input Type
                $(".Upload-button.btn.btn-primary input").prop("disabled", true);
            } else {
                $(".Upload-button.btn.btn-primary").removeClass("disabled");

                // Remove the disabled property from the input Type
                $(".Upload-button.btn.btn-primary input").prop("disabled", false);
            }
        }

        function displayImportModal() {
            $modal = $("#import-users-modal");
            $importUsersModule = $("#import-users-module");
            $importUsersModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();

            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Import/ImportExternalUsersModalBody'),
                type: 'GET'
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $importUsersModule.html(html);

                $.each($importUsersModule.find("select"), function () {
                    var $this = $(this);

                    $this.select2({
                        placeholder: $this.data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $importUsersModule.find("#accountsDropdown, #rolesDropdown").change(function () {
                    updateFileUpload();
                });

                // This onchange work around is required because the normal .change() make IE 11 fire submit twice.
                $importUsersModule.find('#choose-file-btn-id').click(function () {
                    var $this = $(this);

                    $this.one(
                        'change',
                        function () {
                            displaySelectedFileName($this);
                            $("#upload-form").submit();
                        }
                    )
                });

                $importUsersModule.find("#upload-form").submit(function (ev) {
                    ev.preventDefault();
                    importFile();
                });

                $importUsersModule.delegate("#import-user-button", "click", function () {
                    resetFileInput();
                    var importId = $(this).data('import-id');
                    importFile(importId);
                });
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });;
        }

        function displayExportUsersModal() {
            $modal = $("#export-users-modal");
            $exportUsersModule = $("#export-users-module");
            $exportUsersModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();
        }

        function removeUsers() {
            var userIds = [];
            $button = $(".confirm-delete-users-btn");
            $deleteButton = $("#delete-users-btn");

            userIds = $("#selectedUserLabel").val().split(",");
            userIds = userIds.filter(Boolean);

            if (userIds.length == 0) {
                TDPWeb.ExtraNet.displayMessage({ element: $(".formError") });
            }
            else {
                $deleteButton.prop("disabled", true);
                TDPWeb.ExtraNet.spinnerOn($button, false, "left");
                $('.alert-danger').slideUp("slow", function () { });

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('ExternalUsers/DeleteUsers'),
                    type: 'DELETE',
                    data: {
                        userIds: userIds,
                        status: status
                    }
                }).done(function (data) {
                    $deleteButton.prop("disabled", false);
                    $button.hide();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    $deleteButton.show();
                    $('.alert-success').slideDown("slow", function () { }).delay(1500).slideUp("slow", function () { });
                    if (status == "pending") {
                        for (var i = 0; i < userIds.length; i++) {
                            decrementNumberOfPending();
                        }
                    }
                    $("#selectedUserLabel").val('');

                    searchUsers();
                }).fail(function (data) {
                    $deleteButton.prop("disabled", false);
                    TDPWeb.ExtraNet.spinnerOff($button);
                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }

                    $("#selectedUserLabel").val('');
                });
            }
        };

        function showHideDeleteButton() {
            var currentTab = $(".tab-content.active-tab").attr('id');
            if (currentTab == "userTab") {
                // Display the delete button
                $("#delete-users-btn").hide();
            }
            else {
                $("#delete-users-btn").show();
            }
        };

        return {
            focusOnSearchField: function () {
                $("#search-field").focus();
            },
            attachDomEvents: function () {
                var $body = $("body"),
                    $searchResult = $(".search-result");

                numberOfPending = $("#numberOfPending").text();

                $searchResult.show();

                $("#external-users-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    $(e.target).parents("#search-input") ? searchUsers(true) : searchUsers();
                });

                $(".clear-search-field").on("click", function () {
                    var $searchField = $("#search-field");
                    if ($searchField.val().length != 0) {
                        $searchField.val("");
                        if (hasSearched) {
                            currentPage = 1;
                            $searchResult.has("table").length != 0 ? searchUsers(false, false, "", true) : searchUsers(true, false, "", true);
                        }
                    }
                });

                $.each($body.find("select"), function () {
                    var $this = $(this);

                    $this.select2({
                        placeholder: $this.data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $searchResult.delegate("tr.user td:not('.checkbox')", "click", function () {
                    showEditUserModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/ExternalUsers/EditModal'),
                        type: 'GET',
                        data: {
                            id: $(this).closest('tr').data('user-id'), status: status
                        }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showEditUserModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $body.delegate(".table-head .first-name", "click", function () {
                    sortByFirstName();
                });

                $body.delegate(".table-head .last-name", "click", function () {
                    sortByLastName();
                });

                $body.delegate(".table-head .account", "click", function () {
                    sortByAccount();
                });

                $body.delegate(".table-head .role", "click", function () {
                    sortByRole();
                });

                $body.delegate(".table-head .country", "click", function () {
                    sortByCountry();
                });

                $body.delegate(".table-head .enabled", "click", function () {
                    sortByEnabled();
                });

                $body.delegate(".table-head .inactiveDays", "click", function () {
                    sortByInactiveDays();
                });

                $body.delegate(".table-head .createddate", "click", function () {
                    sortByCreatedDate();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    var $this = $(this);

                    goToPage($this.data("page-number"), $this.closest(".tork-pagination").data("query"));
                });

                $body.delegate("#userTabLink", "click", function () {
                    selectActiveUsersTab();
                });

                $body.delegate("#inactiveUserTabLink", "click", function () {
                    selectInactiveUsersTab();
                });

                $body.delegate("#pendingUserTabLink", "click", function () {
                    selectPendingUsersTab();
                });

                $body.delegate(".modal .add-user-btn", "click", function () {
                    addUser(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal .save-user-btn", "click", function () {
                    saveUser(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal .confirm-delete-user-btn", "click", function () {
                    deleteUser(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal #show-details-btn", "click", function (e) {
                    e.preventDefault();
                    showDetails();
                });

                $body.delegate(".modal #hide-details-btn", "click", function (e) {
                    e.preventDefault();
                    hideDetails();
                });

                $("#add-user-btn").on("click", function () {
                    prepareNewUser();
                });

                $("#import-users-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }

                    displayImportModal();
                });

                if ($("#export-user-btn").length) {
                    $("#export-user-btn").on("click", function () {
                        window.location = TDPWeb.ExtraNet.resolveURL(exportLink);
                    });
                }

                $("#disclaimer-export-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }

                    displayExportUsersModal();
                });

                $(".confirm-delete-users-btn").on("click", function () {
                    removeUsers();
                });

                $body.on('change', '#users-table .checkbox input[type=checkbox]', function () {
                    if ($(this).prop("checked")) {
                        var selectedIds = $("#selectedUserLabel").val();
                        $("#selectedUserLabel").val(selectedIds + "," + $(this).val());
                    }
                    else {
                        // Update the preselected label
                        var selectedIds = $("#selectedUserLabel").val();
                        $("#selectedUserLabel").val(selectedIds.replace($(this).val(), ""));
                    }
                });

                $("#userTabs").removeClass("active-tab");
                var url = window.location.href;
                status = url.substr(url.lastIndexOf('/') + 1);

                if (status == "pending") {
                    $("#pendingUserTab").addClass("active-tab");
                }
                else if (status == "inactive") {
                    $("#inactiveUserTab").addClass("active-tab");
                }
                else {
                    $("#userTab").addClass("active-tab");
                }

                $("#country-filter").on("change", function () {
                    selectedCountryId = $(this).find(":selected").val();
                    currentPage = 1;
                    searchUsers();
                });

                // Show hide delete option on page refresh or load
                showHideDeleteButton();
            }
        }
    })();

    $(function () {
        userController.focusOnSearchField();
        userController.attachDomEvents();
    });
})(jQuery);