(function ($) {

    var HowToController = (function () {

        function showPrevDescription($carousel) {
            var $lastDescription = $carousel.find(".slide-description").last();
            var $currentDescription = $carousel.find(".slide-description:visible");
            var $prevDescription = $currentDescription.prev(".slide-description").length > 0 ? $currentDescription.prev() : $lastDescription;

            $currentDescription.hide();
            $prevDescription.show();
        }

        function showNextDescription($carousel) {
            var $firstDescription = $carousel.find(".slide-description").first();
            var $currentDescription = $carousel.find(".slide-description:visible");
            var $nextDescription = $currentDescription.next(".slide-description").length > 0 ? $currentDescription.next() : $firstDescription;

            $currentDescription.hide();
            $nextDescription.show();
        }

        return {
            attachDomEvents: function () {

                $(".carousel").on("slide.bs.carousel", function (e) {
                    // direction is for the way the carousel content slides
                    if (e.direction == "left") {
                        showNextDescription($(this));
                    } else if (e.direction == "right") {
                        showPrevDescription($(this));
                    }
                });

                $(".panel-content").on("show.bs.collapse hide.bs.collapse", function () {
                    var $togglerMore = $(this).siblings(".panel-toggler").find("a #read-more"),
                        $togglerLess = $(this).siblings(".panel-toggler").find("a #read-less")

                    $togglerMore.toggle();
                    $togglerLess.toggle();
                });
            }
        }
    })();

    $(function () {
        HowToController.attachDomEvents();
    });
})(jQuery);