(function ($) {
    var userController = (function () {
        var sortOrder = "last_name",
            currentPage = 1,
            exportLink = "/Excel/InternalUsers",
            hasSearched = false;

        function searchUsers(usingSearchInput, query, clearSearch) {
            var $searchField = $("#search-field"),
                $searchResults = $(".search-result"),
                $spinnerLocation = $("#users-table"),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage;

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/InternalUsers/Search"),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
            return false;
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchUsers();
        }

        function sortByFirstName() {
            sortByProperty("first_name", "first_name_desc");
        }

        function sortByLastName() {
            sortByProperty("last_name", "last_name_desc");
        }

        function sortByRole() {
            sortByProperty("role", "role_desc");
        }

        function sortByCountry() {
            sortByProperty("country", "country_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchUsers(false, query);
        }

        function totalPages() {
            return parseInt($(".tork-pagination ul li:last-child")[0].textContent);
        }

        function showEditUserModal(content_html, animated) {
            $modal = $("#editUserModal");

            $modal.find(".alert").hide();

            $modal.find(".modal-content").html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));

            $modal.find("ul.modal-tabs a").first().click();

            $.each($modal.find("select"), function () {
                $(this).select2({
                    minimumResultsForSearch: -1,
                    // data name changed from "placeholder" to "placeholder-text"
                    // select2 v3.5 is doing its own stuff with using empty option to show placeholder text,
                    // which causes the first option to be the placeholder and selectable
                    placeholder: $(this).data("placeholder-text"),
                    formatSelection: TDPWeb.ExtraNet.select2TooltipFormat,
                    theme: 'bootstrap'
                }).unbind("change.editUserModal").on("change.editUserModal", function () {
                    if ($(this).attr("name") === "selectedrole") {
                        var $spinnerLocation = $("#roles-dropdown-spinner");
                        TDPWeb.ExtraNet.spinnerOn($spinnerLocation, false, "left");

                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/InternalUsers/ToolsList'),
                            type: 'GET',
                            data: { id: $(this).closest("form").find("input[name='id']").val(), role_id: $(this).val() }
                        }).done(function (html) {
                            $(".toolsList").html(html);
                            if ($("#hide-details-btn").is(":visible")) {
                                showDetails(0);
                            }
                            TDPWeb.ExtraNet.setUpPopovers();
                            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                        }).fail(function (error) {
                            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                        });
                    }
                });
            });

            TDPWeb.ExtraNet.setUpPopovers();

            if (!animated) {
                $modal.removeClass("fade");
            }

            // this is to fix the multiple select placeholder text being cut off
            $('a[href="#extended-accesses"]').on("click", function () {
                setTimeout(function () {
                    $(".select2-search-field input").blur();
                }, 100)
            });

            $modal.modal().addClass("fade");
        }

        function saveUser($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("#manage-user"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".save-user-btn, #save-my-profile"),
                    onMyProfile = $button.attr("id") == "save-my-profile";
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/InternalUsers/Update'),
                    type: 'PUT',
                    data: $modal.find("form").serialize()
                }).done(function (data) {
                    if (!onMyProfile) {
                        searchUsers();
                    }

                    TDPWeb.ExtraNet.spinnerOff($button);

                    TDPWeb.ExtraNet.successMessageModal($modal);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function showDetails(duration) {
            if (duration == null) {
                duration = 400;
            }
            $(".emptyItem").show();
            $(".subtool-container").slideDown(duration);
            $("#show-details-btn").hide();
            $("#hide-details-btn").show();
        }

        function hideDetails() {
            $(".emptyItem").hide();
            $(".subtool-container").slideUp();
            $("#hide-details-btn").hide();
            $("#show-details-btn").show();
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.text();

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function resetFileInput() {
            $("#choose-file-btn-id").val("");

            // Empty file input for IE
            if (TDPWeb.ExtraNet.isOnIE()) {
                $("#choose-file-btn-id").wrap("<form>").closest("form").get(0).reset();
                $("#choose-file-btn-id").unwrap();
            }
        }

        function importFile(importId) {
            var $resultsDiv = $("#results"),
                $spinnerLocation = $("#selectedFileNamePlaceHolder");
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");

            // Delay in order to show spinner first
            setTimeout(function () {
                var formData = new FormData($("#upload-form")[0]);
                if (importId) {
                    formData.append('id', importId);
                }

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL("/Import/ImportInternalUsers"),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($("#import-users-module"));
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html(html);
                    resetFileInput();
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html("");
                    TDPWeb.ExtraNet.displayMessage({ element: $(".formError") });
                });
            }, 100);
        }

        function updateFileUpload() {
            var isDisabled = $("#countriesDropdown").val().length == 0 || $("#rolesDropdown").val().length == 0;

            $(".Upload-button.btn.btn-primary").prop("disabled", isDisabled);

            if (isDisabled) {
                $(".Upload-button.btn.btn-primary").addClass("disabled");
            } else {
                $(".Upload-button.btn.btn-primary").removeClass("disabled");
            }
        }

        function displayImportModal() {
            $modal = $("#import-users-modal");
            $importUsersModule = $("#import-users-module");
            $importUsersModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();

            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Import/ImportInternalUsersModalBody'),
                type: 'GET'
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $importUsersModule.html(html);

                $.each($importUsersModule.find("select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $importUsersModule.find("#accountsDropdown, #rolesDropdown").change(function () {
                    updateFileUpload();
                });

                // This onchange work around is required because the normal .change() make IE 11 fire submit twice.
                $importUsersModule.find("#choose-file-btn-id").click(function () {
                    $(this).one(
                        'change',
                        function () {
                            displaySelectedFileName($(this));
                            $("#upload-form").submit();
                        }
                    )
                });

                $importUsersModule.find("#upload-form").submit(function (ev) {
                    ev.preventDefault();
                    importFile();
                });

                $importUsersModule.delegate("#import-user-button", "click", function () {
                    resetFileInput();
                    var importId = $(this).data('import-id');
                    importFile(importId);
                });
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });;
        }

        function displayExportUsersModal() {
            $modal = $("#export-users-modal");
            $exportUsersModule = $("#export-users-module");
            $exportUsersModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();
        }

        return {
            focusOnSearchField: function () {
                $("#search-field").focus();
            },
            attachDomEvents: function () {
                var $body = $("body"),
                    $searchResult = $(".search-result");

                $searchResult.show();

                $("#internal-users-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    $(e.target).parents("#search-input") ? searchUsers(true) : searchUsers();
                });

                $(".clear-search-field").on("click", function () {
                    var $searchField = $("#search-field");
                    if ($searchField.val().length != 0) {
                        $searchField.val("");
                        if (hasSearched) {
                            currentPage = 1;
                            $searchResult.has("table").length != 0 ? searchUsers(false, "", true) : searchUsers(true, "", true);
                        }
                    }
                });

                $searchResult.delegate("tr.user", "click", function () {
                    showEditUserModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/InternalUsers/EditModal'),
                        type: 'GET',
                        data: { id: $(this).data("user-id") }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showEditUserModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $body.delegate(".table-head .first-name", "click", function () {
                    sortByFirstName();
                });

                $body.delegate(".table-head .last-name", "click", function () {
                    sortByLastName();
                });

                $body.delegate(".table-head .role", "click", function () {
                    sortByRole();
                });

                $body.delegate(".table-head .country", "click", function () {
                    sortByCountry();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $body.delegate(".modal .save-user-btn", "click", function () {
                    saveUser(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal #show-details-btn", "click", function (e) {
                    e.preventDefault();
                    showDetails();
                });

                $body.delegate(".modal #hide-details-btn", "click", function (e) {
                    e.preventDefault();
                    hideDetails();
                });

                $("#disclaimer-export-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }

                    displayExportUsersModal();
                });

                $("#export-user-btn").on("click", function () {
                    window.location = TDPWeb.ExtraNet.resolveURL(exportLink);
                });

                $("#import-users-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }

                    displayImportModal();
                });
            }
        }
    })();

    $(function () {
        userController.focusOnSearchField();
        userController.attachDomEvents();
    });
})(jQuery);