(function ($) {

    var InvoicesController = (function () {
        var pagination,
            ajaxCall,
            _orderNumber,
            _invoiceNumber,
            timeOut;

        $("#PrintInvoiceDetails").click(function () {
            $(this).closest(".modal-content").print({});
            return false;
        });

        // Download Invoice line Item Details in PDF
        $("#invoice-download-btn").on("click", function (e) {
            e.stopPropagation();
            turnButtonsOff();
            getPDF(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.downloadurl);
                turnButtonsOn();
            }, function (error) {
                onPdfGenerationFailure();
            });
        });

		// Generate PDF
        function getPDF(done, fail) {

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Pdf/ExportInvoiceDetails"),
                type: "POST",
                data: {
                    orderNumber: _orderNumber,
                    invoiceNumber: _invoiceNumber
                }
            }).done(done).fail(fail);
        }

        function turnButtonsOff() {
            var $downloadButton = $("#invoice-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'disable');
            TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getPDFWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFWaitMore-msg") });
                }

            }, 15000);
        };

        function turnButtonsOn() {
            var $downloadButton = $("#invoice-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'enable');
            clearTimeout(timeOut);

            if ($("#getPDFWait-msg").is(":visible")) {
                $("#getPDFWait-msg").slideUp();
            }

            if ($("#getPDFWaitMore-msg").is(":visible")) {
                $("#getPDFWaitMore-msg").slideUp();
            }
        };

        function onPdfGenerationFailure() {
            turnButtonsOn();
            TDPWeb.ExtraNet.displayMessage({ element: $("#getPDFError-msg") });
        };

        function searchInvoices($form) {
            var $button = $form.find("[type='submit']");

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            }
            else {
                if (ajaxCall) {
                    ajaxCall.abort(); // if getLatestInvoices is still pending
                }

                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.btnState($button, 'disable');

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Invoices/SearchInvoices'),
                    type: 'POST',
                    data: $form.serialize()
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($form);
                    TDPWeb.ExtraNet.btnState($button, 'enable');

                    $('.search-result').html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th").first(), false, true);
                    $(".search-result").find("[data-alert-type=search]").slideDown(200);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.btnState($button, 'enable');
                });
            }
        }

        function showInvoiceDetailsModal(content_html, animated) {
            $modal = $("#invoice-details");

            $modal.find(".modal-body").html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        return {
            getLatestInvoices: function () {
                var $resultsContainer = $(".search-result"),
                    getInvoiceDetails;
                TDPWeb.ExtraNet.spinnerOn($resultsContainer);

                ajaxCall = $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Invoices/FetchLatestInvoices'),
                    type: 'POST',
                    data: {}
                }).done(function (html) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);

                    $resultsContainer.html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th.invoice-date").first(), true, true);
                    $(".search-result").find("[data-alert-type=search]").slideDown(200);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                });
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    $resultsContainer = $(".search-result"),
                    getInvoiceDetails;

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                TDPWeb.ExtraNet.setUpDatepickerPair("#po-date");
                TDPWeb.ExtraNet.setUpDatepickerPair("#invoice-date", 1);

                $body.delegate("#invoices-form", "submit", function (evt) {
                    evt.preventDefault();
                    searchInvoices($(this));
                });

                $resultsContainer.delegate("tr.invoice", "click", function (evt) {
                    evt.stopPropagation();
                    showInvoiceDetailsModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container"),
                        orderNumber = $(this).data("sca-order-number");
                        _orderNumber = orderNumber;
                    var invoiceNumber = $(this).find(".invoice-number").data("sorting-value");
                        _invoiceNumber = invoiceNumber;
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    getInvoiceDetails = $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Invoices/GetInvoiceDetails'),
                        type: 'POST',
                        data: { orderNumber: orderNumber, invoiceNumber: invoiceNumber }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showInvoiceDetailsModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $(".modal").on("hide.bs.modal", function () {
                    if (getInvoiceDetails != undefined) {
                        getInvoiceDetails.abort();
                    }
                });

                var $pagination = $("#search-invoices-pagination");
                var $slim_pagination = $('[data-slim-pagination]');

                InvoicesController.pagination = new Pagination();
                InvoicesController.pagination.initialize($pagination, $resultsContainer, "tbody tr", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"), $slim_pagination);

                $body.delegate('[data-slim-pagination] [data-navigate]', 'click', function () {
                    var $clicked = $(this);
                    $pagination.pagination($clicked.data('navigate'));
                });
            }
        }
    })();

    $(function () {
        InvoicesController.attachDomEvents();
        InvoicesController.getLatestInvoices();
    });
})(jQuery);