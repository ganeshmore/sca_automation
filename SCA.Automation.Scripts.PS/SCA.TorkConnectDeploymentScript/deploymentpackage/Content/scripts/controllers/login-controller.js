(function ($) {

    // Check for the Forgot Password Option
    //var referrer = document.referrer;
    //if (referrer != undefined && referrer.length > 0) {

    //    if (referrer.indexOf("dsts.sca.com/adfs") >=0) {

    //        // Forgot Password option is True. Display the Forgot Pasword Section
    //        $("#forgot-password-container").addClass("in");
    //        $("#forgot-password-container").css('height', 'auto');
    //    }
    //}

    var loginController = (function () {

        function showSuccessModal() {
            var $modal = $("#success-modal");
            $modal.modal();
        };

        function addUser($modal) {
            var $form = $modal.find("form");
            var application_url = $('select[name="selectedcountry"] option:selected').data("url");
            var application_culture = $('select[name="selectedcountry"] option:selected').data("culture");
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".register-btn");

                TDPWeb.ExtraNet.checkCaptcha("#UserRegistrationFormCaptchaInput", $form, function () {
                    TDPWeb.ExtraNet.spinnerOn($button);
                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL("/Login/RegisterNewUser?applicationUrl="+ application_url + "&applicationCulture=" + application_culture),
                        type: 'POST',
                        data: $form.serialize()
                    }).done(function (data) {

                        TDPWeb.ExtraNet.spinnerOff($button);
                        TDPWeb.ExtraNet.cancelFormModal($modal);

                        setTimeout(function () {
                            showSuccessModal();
                        }, 350);

                    }).fail(function (data) {
                        TDPWeb.ExtraNet.spinnerOff($button);

                        if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                            TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $modal.find(".backEndErrors"));
                        }
                    });
                }, $button, $modal.find(".backEndErrors"));
            }
        };

        function showRegisterNewUserModal(html_content) {
            var $modal = $("#new-user-registration");

            $modal.find(".modal-content").html(html_content);
            TDPWeb.ExtraNet.resetForm($modal.find("form"));
            TDPWeb.ExtraNet.resetFormValidations($modal);
            TDPWeb.ExtraNet.setUpPopovers({ placement: "right" });

            $.each($modal.find("select"), function () {
                var $this = $(this);

                $this.select2({
                    placeholder: $this.data("placeholder"),
                    theme: 'bootstrap'
                });
            });

            $modal.modal();
        };

        function prepareRegisterNewuserModal() {
            showRegisterNewUserModal(TDPWeb.ExtraNet.emptySpinnerContainer);
            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Login/RegisterNewUser'),
                type: 'GET',
                data: {}
            }).done(function (html) {

                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                showRegisterNewUserModal(html);
            }).fail(function (error) {

                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });
        };

        function hidePasswordForm() {
            TDPWeb.ExtraNet.resetFormValidations($forgotPasswordForm);
        };

        function displayValidationMessage(elementId) {
            TDPWeb.ExtraNet.displayMessage({
                element: elementId
            });
        };

        function hideValidationMessage(elementId) {            
            $(elementId).slideUp(200);
        };

        function validateRequiredInputs($form, messageElementId) {
            var valid = true;
            hideValidationMessage(messageElementId);

            $form
                .find('input, select')
                .not('[disabled]')
                .each(function () {
                    var $this = $(this);

                    $this.removeClass('error');
                    if ($this.is('[required]') &&
                        $this.val().trim().length === 0) {
                        $this.addClass('error');
                        valid = false;
                    }
                });

            if (!valid) {
                displayValidationMessage(messageElementId);
            }

            return valid;
        }

        return {
            attachDomEvents: function () {
                var $body = $("body"),
                    $forgotPasswordForm = $("#ForgotPasswordForm");

                $("#ForgotPasswordForm .cancel-form-tab").click(hidePasswordForm);

                $forgotPasswordForm.submit(function (evt) {
                    evt.preventDefault();

                    if (!TDPWeb.ExtraNet.validateInput($forgotPasswordForm)) {
                        TDPWeb.ExtraNet.displayMessage({ element: $forgotPasswordForm.find(".formError") });
                    } else {
                        TDPWeb.ExtraNet.resetFormValidations($forgotPasswordForm);
                        var $sendButton = $(this).find(".request-password-btn");
                        TDPWeb.ExtraNet.spinnerOn($sendButton);

                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL($(this).attr("action")),
                            type: $(this).attr("method"),
                            data: $(this).serialize()
                        }).done(function (data) {
                            TDPWeb.ExtraNet.spinnerOff($sendButton);
                            TDPWeb.ExtraNet.resetFormValidations($forgotPasswordForm);
                            TDPWeb.ExtraNet.displayMessage({ element: $forgotPasswordForm.find(".formSuccess") });
                        }).fail(function (data) {
                            TDPWeb.ExtraNet.spinnerOff($sendButton);

                            if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                                TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $forgotPasswordForm, $("#ForgotPasswordForm .backEndErrors"));
                            }
                        });
                    }
                });

                $("#register-new-user-btn").on("click", function () {
                    prepareRegisterNewuserModal();
                });

                $("#confirmCancelButton").on("click", function () {
                    $('#confirmAccountForm').hide(150);
                    $('#confirmCancelAlert').show(300);
                });


                $body.delegate(".modal .register-btn", "click", function () {
                    addUser(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $("#notifyNoAccessExtranet").on("click", function () {
                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Login/NotifyNoAccessExtranet')
                    }).done(function (data) {
                        TDPWeb.ExtraNet.displayMessage({ element: $("#noAccessExtranetNotifiedMessage") });
                    }).fail(function (data) {
                        //maybe notify user
                    });
                });

				$(".success-validate-message a").click(function () {
                    $(".success-validate-message").fadeOut("slow");
				});

				$("#unsubscribeFailure a").click(function () {
				    $("#unsubscribeFailure").fadeOut("slow");
                });

                $("#confirmAccountForm button[type='submit']").click(function () {
                    if (!validateRequiredInputs($('#confirmAccountForm'), '#jobTitleAlert')) {
                        return false;
                    }    
                })
            }
        }
    })();

    $(function () {
        loginController.attachDomEvents();
    });
})(jQuery);