(function ($) {

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        //Hide the Choose File & Show the IE9
        $("#company-logo-file").hide();
        $("#spanIE9Dummy").show();
    }
    else {
        $("#company-logo-file").show();
        $("#spanIE9Dummy").hide();
    }

    //Display the IE9 error Message
    $("#spanIE9Dummy").click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $("#IE9BrowserVersion-modal").modal();
        return;
    });

    var MarketingResourcesController = (function () {
        var companyLogo = undefined,
            searchPagination,
            hasSearched = false,
            lastSearch = undefined,
            searchTimeout = null,
            actionTypeClicked = false,
            selectedDownloadLink = undefined,
            isImageType = false,
            fileTypeValue = $("#marketing-resources-download-links").data("imagetypes").split(",");

        function validateWizard(step) {
            switch (step) {
                case 1:
                    if (!$("#file-list .fileListItem").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#file-list-alert-msg" });
                        return false;
                    }
                    break;
            }
            return true;
        };

        function clearDownloadLinks() {
            $("#marketing-resources-download-links")[0].innerHTML = "";
        }

        function addDownloadLinks(files) {
            $container = $("#marketing-resources-download-links");
            $container[0].innerHTML = "";
            $template = $container.siblings(".template").first();

            $.each(files, function () {
                var $clone = $template.clone(),
                    url = $(this).data("url"),
                    isImageData = $(this).data("is-image"),
                    isImage = isImageData && isImageData.toString().toLowerCase() == 'true';

                $clone.show().removeClass("template");
                $.each($clone.find("a"), function () {
                    $(this).attr("href", url);
                    if (!$(this).hasClass("u-floatRight") && isImage) {
                        disableClickEvent($(this));
                    }
                });
                $clone.find("[data-text='title']").text($(this).data("title"));
                $clone.find("[data-text='subtitle']").text($(this).data("subtitle"));

                if ($(this).data("thumbnail")) {
                    $clone.find(".asset-img").css("background-image", 'url(' + $(this).data("thumbnail") + ')');
                }

                $container.append($clone);
            });
        }

        var change_timeout = undefined;

        function fileListChanged() {
            var fileListCount = $("#file-list-added-assets .fileListItem").length;
            $("#file-list-count").text(fileListCount);

            if (fileListCount) {
                $("#file-list").show();
                $("#file-list-empty-description").hide();
            }
            else {
                $("#file-list").hide();
                $("#file-list-empty-description").show();
            }

            if (change_timeout !== undefined) {
                clearTimeout(change_timeout);
            }

            change_timeout = setTimeout(function () {
                clearDownloadLinks();

                files = $("#file-list-added-assets .fileListItem");

                if (files.length > 0) {
                    addDownloadLinks(files);
                }

            }, 250);

            if (fileListCount > 1) {
                $("#download-all-btn").show();
            }
            else {
                $("#download-all-btn").hide();
            }
        }

        function sendFileList(evt) {
            evt.preventDefault();
            var $form = $("#marketing-resources-email-btn").closest("form");
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                var $button = $form.find("input[type=submit]"),
                    url = TDPWeb.ExtraNet.resolveURL("/MarketingResources/SendMarketingResourcesList"),
                    params = {};

                TDPWeb.ExtraNet.spinnerOn($button);

                $.each($form.serializeArray(), function (idx, elem) {
                    params[elem.name] = elem.value;
                });

                params.asset_names = $.map($("#file-list .fileListItem .asset-name"), function (val, i) {
                    return $(val).text();
                });
                params.asset_urls = $.map($("#marketing-resources-download-links .asset-module .u-floatRight"), function (val, i) {
                    return $(val).attr("href");
                });
                params.emailList = $form.find(".emailList").tagsinput("items");
                params.ccemailList = $form.find(".ccemailList").tagsinput("items");
                params.displayLogo = $("#enableLogo").length == 0 || $("#enableLogo").is(":checked");
                params.logo_data = companyLogo;
                params.logo_file_name = $("#company-logo-file").val() ? $("#company-logo-file").val().split('\\').pop() : "";
                params.displayDisclaimer = isImageType;

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: params
                }).done(function (data) {
                    TDPWeb.ExtraNet.reportEmailSentEvent();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageForm($form);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }
                });
            }
        }

        function update_company_logo(file) {
            if (file == undefined) {
                companyLogo = undefined;
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    companyLogo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                }
                reader.readAsDataURL(file);
            }
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function toggleAddToListLink($addLink, hide) {

            if (hide) {
                $addLink.hide();
                $addLink.siblings(".added-to-list-label").show();
            } else {
                $addLink.show();
                $addLink.siblings(".added-to-list-label").hide();
            }
        }

        function updateFilesAlreadyInList() {
            var assetNumbersInList = $("#file-list-body [data-asset-number]").map(function () {
                return $(this).data("asset-number");
            }).get();

            $(".asset-module ").each(function () {
                var currentAssetNumber = $(this).data("asset-number"),
                    alreadyInList = false;

                if (currentAssetNumber) {
                    alreadyInList = $.inArray(currentAssetNumber, assetNumbersInList) != -1;
                }

                if (alreadyInList) {
                    toggleAddToListLink($(this).find(".add-to-list"), true);
                }
            });
        }

        function searchAssets(searchTerm, serializedFormData, done) {
            var $searchResult = $("#search-result"),
                oldLastSearch = lastSearch;

            lastSearch = searchTerm;

            if (lastSearch === oldLastSearch) {
                done();
            }
            else if (searchTerm !== undefined) {
                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/MarketingResources/SearchAssets'),
                    type: 'POST',
                    data: serializedFormData + "&searchTerm=" + searchTerm
                }).done(function (html) {
                    hasSearched = true;
                    $searchResult.html(html);
                    $("#file-list-search-count").text($("#asset-results").data('totalCount'));
                    updateFilesAlreadyInList();
                    $('#file-list-content').find('[data-action="tab"]').navigation('setActiveLink', $('#file-list-content').find('[data-toggle-pane="#file-list-search-assets"]'));
                    TDPWeb.ExtraNet.scrollUpToTarget($searchResult);

                    done();
                }).fail(function (error) {
                    done();
                });
            }
            else {
                $searchResult.html("");
                $("#file-list-search-count").text(0);
                done();
            }
        }

        function addFileToList(assetName, assetNumber, assetType, assetDownloadURL, assetThumbnail, resolutions, isImage) {
            var list_files = $("#file-list-body [data-asset-number]").map(function () {
                return $(this).data("asset-number");
            }).get(),
                should_add = $.inArray(assetNumber, list_files) == -1;

            if (should_add) {
                var $cloneRowInstance = $("#file-list-body .fileListItemTemplate").clone(),
                    $titleLink = $cloneRowInstance.find(".asset-name").text(assetName);

                $cloneRowInstance.attr("data-asset-number", assetNumber);
                $cloneRowInstance.data("url", assetDownloadURL);
                $cloneRowInstance.data("is-image", isImage);
                $titleLink.attr("href", assetDownloadURL);
                if (isImage) {
                    disableClickEvent($titleLink);
                }
                $cloneRowInstance.data("title", assetName);
                $cloneRowInstance.find(".asset-type").text(assetType);
                $cloneRowInstance.data("subtitle", assetType);

                if (assetThumbnail) {
                    var $thumbnailLink = $cloneRowInstance.find(".asset-img").css("background-image", 'url(' + assetThumbnail + ')').show().closest("a");
                    $thumbnailLink.attr("href", assetDownloadURL);
                    if (isImage) {
                        disableClickEvent($thumbnailLink);
                    }
                    $cloneRowInstance.data("thumbnail", assetThumbnail);
                }

                $cloneRowInstance.addClass("fileListItem");
                $cloneRowInstance.removeClass("fileListItemTemplate");
                $cloneRowInstance.show();

                $cloneRowInstance.addClass("fileListItemAdded").insertAfter(".fileListItemTemplate");

                $.each(resolutions, function () {
                    $cloneRowInstance.attr(this.name, this.value);
                });

                setTimeout(function () {
                    $cloneRowInstance.removeClass("fileListItemAdded");
                }, 500);

                $asset = $("#search-result .asset-module[data-asset-number='" + assetNumber + "']");

                toggleAddToListLink($asset.find(".add-to-list"), true);

                fileListChanged();
            }
        }

        function removeFileFromList($asset) {
            var assetNumber = $asset.data("asset-number");

            $asset.remove();

            $searchAsset = $("#search-result .asset-module[data-asset-number='" + assetNumber + "']");

            $searchAsset.removeClass("included-in-list");

            toggleAddToListLink($searchAsset.find(".add-to-list"), false);
            fileListChanged();
        }

        function marketingAssetDisclaimer(event, actionType) {
            event.preventDefault();
            actionTypeClicked = actionType;

            //actionType :: 0: Download Icon click, 1: Email button Click, 2: Download All clicked

            $modal = $("#disclaimer");

            // Check for download not email
            if (isImageType && actionType != 1) {
                $modal.modal();
            }
            else {
                switch (actionType) {
                    case 0:
                        window.open(selectedDownloadLink);
                        break;
                    case 1:
                        var $form = $("#marketing-resources-email-btn").closest("form");
                        if (!TDPWeb.ExtraNet.validateInput($form)) {
                            TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
                        }
                        else if (isImageType) {
                            $modal.modal();
                        }
                        else {
                            sendFileList(event);
                        }
                        break;
                    case 2:
                        downloadAllMarketingAssets(event);
                        break;
                }
            }
        }

        function closeDisclaimerPopup(event) {
            switch (actionTypeClicked) {
                case 0:
                    window.open(selectedDownloadLink);
                    break;
                case 1:
                    sendFileList(event);
                    break;
                case 2:
                    downloadAllMarketingAssets(event);
                    break;
            }

        };

        function clearAllDocumentTypeSelection() {
            $("#divCheckBox input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        };

        function disableClickEvent(element) {
            element.removeAttr("href");
            element.attr('disabled', "");
            element.removeAttr("_target");
            element.addClass('u-noClick');
        };

        function downloadAllMarketingAssets(evt) {
            evt.preventDefault();

            var $button = $('#download-all-btn');
            TDPWeb.ExtraNet.spinnerOn($button);
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/MarketingResources/StoreDownloadAllResourceURLs'),
                type: 'POST',
                data: { urls: $.map($("#marketing-resources-download-links .asset-module .u-floatRight"), function (val, i) {
                    return $(val).attr("href");
                }),
                    assetType: 'Marketing'
                }
                     
            }).done(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.url);
                TDPWeb.ExtraNet.spinnerOff($button);
            });
            
        };

        function turnButtonOn() {

        };

        function turnButtonOff() {

        };

        return {
            attachDomEvents: function () {
                var $body = $("body"),
                    $companyLogo = $("#company-logo-file"),
                    $spinnerLocation = $("#company-logo-file"),
                    $defaultLogo = $("#defaultLogo");

                Wizard.initiateWizard(validateWizard);

                $("#marketing-resources-module select").select2({
                    minimumResultsForSearch: -1,
                    theme: 'bootstrap'
                });

                $body.delegate("#search-filter-content select, #search-filter-content input", "change", function () {
                    lastSearch = undefined;
                });

                $body.delegate(".add-to-list", "click", function () {
                    var $asset = $(this).closest(".asset-module"),
                        assetName = $asset.find("h3").first().text(),
                        assetNumber = $asset.data("asset-number"),
                        assetType = $asset.find("span[data-type='file-extension']").first().text(),
                        assetDownloadURL = $asset.data("asset-download-url"),
                        assetThumbnail = $asset.data("asset-thumbnail"),
                        resolutionPrefix = $asset.data("resolution-prefix") + "-",
                        isImageData = $asset.data("is-image"),
                        resolutions = [],
                        isImage = isImageData && isImageData.toLowerCase() == 'true';

                    $.each($asset[0].attributes, function () {
                        if (this.specified && this.name.match("^" + resolutionPrefix)) {
                            resolutions.push(this);
                        }
                    });

                    addFileToList(assetName, assetNumber, assetType, assetDownloadURL, assetThumbnail, resolutions, isImage);

                    // Push the Marketing asset info to GA.
                    window.dataLayer.push(
                        {
                            'event': 'gaEvent',
                            'eventCategory': 'Marketing assets download',
                            'eventAction': 'Add to list',
                            'eventLabel': assetName
                        });

                });

                $body.delegate(".delete-list-btn", "click", function () {
                    removeFileFromList($(this).closest(".fileListItem"));
                });

                $body.delegate("#search-result li[data-query], #search-result .tork-pagination a[data-query]", "click", function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    var $target = $(this),
                        $searchResult = $("#search-result"),
                        $torkPagination = $target.parents(".tork-pagination");

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/MarketingResources/SearchAssets'),
                        type: 'POST',
                        data: {
                            searchTerm: $(this).data("query"),
                            resolution: $("#image-resolution-select-container input:checked").val()
                        }
                    }).done(function (html) {
                        $searchResult.html(html);
                        updateFilesAlreadyInList();
                        TDPWeb.ExtraNet.scrollUpToTarget($searchResult);
                    });
                });

                $body.delegate("#image-resolution-select-container input", "change", function (e) {
                    var resolution = $(this).val(),
                        resolution_string = $(this).data("resolution-prefix") + "-" + resolution;

                    $.each($("#asset-results div[" + resolution_string + "]"), function () {
                        $(this).find("a[href]").attr("href", $(this).attr(resolution_string));
                        $(this).data("asset-download-url", $(this).attr(resolution_string));
                    });

                    $.each($("#file-list-body tr.fileListItem[" + resolution_string + "]"), function () {
                        $(this).find("a[href]").attr("href", $(this).attr(resolution_string));
                        $(this).data("url", $(this).attr(resolution_string));
                    });

                    fileListChanged();
                });

                function bindFileInput() {
                    $companyLogo.change(function () {
                        var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                        switch (ext) {
                            case 'jpg':
                            case 'jpeg':
                            case 'png':
                            case 'gif':
                                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                                var file = this.files[0];
                                update_company_logo(file);
                                displaySelectedFileName($companyLogo);
                                break;
                            default:
                                this.value = '';
                        }
                    });
                };

                bindFileInput();

                if ($defaultLogo.length) {
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });
                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                } else {
                    $companyLogo.preimage();
                }

                $("#preview-image").bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });

                $(".remove-logo-btn").on("click", function () {
                    $companyLogo.val("");
                    $("#preview-image").hide();
                    update_company_logo(undefined);
                    displaySelectedFileName($companyLogo);
                });

                $("#reset-logo-btn").on("click", function () {
                    TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                    $companyLogo.unbind('change');
                    bindFileInput();
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });

                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                });

                $body.delegate("#search-result-form .clear-search-field", "click", function () {
                    $input = $("#search-result-form #search-field");
                    $input.val('');
                    if (hasSearched) {
                        TDPWeb.ExtraNet.spinnerOn($input, true);
                        searchAssets(undefined, $(this).closest("form").serialize(), function () {
                            TDPWeb.ExtraNet.spinnerOff($input);
                        });
                    }
                });

                $body.delegate("#search-filter-toggle", "click", function () {
                    var $toggler = $(this),
                        $filterContent = $($toggler.data("target")),
                        $togglerIndicator = $toggler.find('[data-indicator]'),
                        indicatorStateUp = 'icon-arrow_right_borderless',
                        indicatorStateDown = 'icon-arrow_down_borderless';

                    if ($filterContent.is(":visible")) {
                        $filterContent
                            .hide()
                            .removeClass("open");

                        $togglerIndicator
                            .removeClass(indicatorStateDown)
                            .addClass(indicatorStateUp);
                    } else {
                        $filterContent
                            .show()
                            .addClass("open");

                        $togglerIndicator
                            .addClass(indicatorStateDown)
                            .removeClass(indicatorStateUp);
                    }

                    $(this).blur();
                });

                $("#search-result-form #search-field").keyup(function () {
                    if (searchTimeout != null) {
                        clearTimeout(searchTimeout);
                    }

                    $input = $(this);
                    var val = $input.val(),
                        showSpinner = true; // val.length > 0 && val != lastSearch;

                    searchTimeout = setTimeout(function () {
                        if (showSpinner) {
                            TDPWeb.ExtraNet.spinnerOn($input, true);
                        }

                        searchAssets($input.val(), $input.closest("form").serialize(), function () {
                            if (showSpinner) {
                                TDPWeb.ExtraNet.spinnerOff($input);
                            }
                        });
                    }, 500);
                });

                $('#search-result-form').submit(function (e) {
                    e.preventDefault();

                    if (searchTimeout != null) {
                        clearTimeout(searchTimeout);
                    }

                    $input = $("#search-result-form #search-field");
                    var val = $input.val();
                    var showSpinner = val != lastSearch;
                    $button = $("#search-submit");

                    if (showSpinner) {
                        TDPWeb.ExtraNet.spinnerOn($input, true);
                        $button.prop("disabled", true);
                    }

                    searchAssets(val, $(this).serialize(), function () {
                        if (showSpinner) {
                            TDPWeb.ExtraNet.spinnerOff($input);
                            $button.prop("disabled", false);
                        }
                    });
                });

                //Disclaimer popup on click of send email button
                $body.delegate("#marketing-resources-email-btn", "click", function (event) {
                    checkIfImagePresent();
                    marketingAssetDisclaimer(event, 1);
                });

                //Disclaimer popup on click of download icon
                $body.delegate("#marketing-resources-download-links a", "click", function (event) {
                    selectedDownloadLink = $(this).attr('href');
                    //Check matching File Type .If matching file type found it display the disclaimer popup    
                    isImageType = $.inArray($(this).parent().find("[data-text='subtitle']").text(), fileTypeValue) != -1;
                    marketingAssetDisclaimer(event, 0);
                });

                //Closing of disclaimer popup on click of close button & cross button
                $body.delegate("#close-disclaimer, #disclaimer-header-close", "click", function (event) {
                    closeDisclaimerPopup(event);
                });

                if ($("select[name='selectedcampaignandprograms'] option:selected").val() === $("#search-filter-content").data("torkready-facet")) {
                    $input = $("#search-result-form #search-field");
                    TDPWeb.ExtraNet.spinnerOn($input, true);
                    searchAssets("", $("select[name='selectedcampaignandprograms']").closest("form").serialize(), function () {
                        TDPWeb.ExtraNet.spinnerOff($input);
                    });
                }

                //Disclaimer pop-up on click of Download All 
                $body.delegate("#download-all-btn", "click", function () {
                    checkIfImagePresent();
                    marketingAssetDisclaimer(event, 2);
                });

                function checkIfImagePresent() {
                    // Check the File Type as TIF ,JPG,PNG,GIF
                    var fileTypes = $("#marketing-resources-download-links [data-text='subtitle']").map(function (index) {
                        return $(this).text();
                    });
                    //Check matching File Type .If matching file type found it display the disclaimer popup 
                    var intersectionValues = _.intersection(fileTypes, fileTypeValue).length;

                    if (intersectionValues > 0) {
                        isImageType = true;
                    } else { isImageType = false; }
                }

                // Tab toggling
                $('[data-action="tab"]').navigation({
                    linkSelector: '[data-toggle-pane]',
                    onSelect: function ($to, $from, event) {
                        // Don't allow to unset item
                        if (!$to.length) return false;
                    },
                    paneContainer: '#file-list-content',
                    currentPaneClass: 'visible-xs-block'
                });

                // Deselect all document type check boxes
                $body.delegate("#clearDocumentSelection", "click", function (event) {
                    clearAllDocumentTypeSelection();
                });

            }
        }
    })();

    (function () {
        MarketingResourcesController.attachDomEvents();
    })();

})(jQuery);