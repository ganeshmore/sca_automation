(function ($) {
    var currentPage = 1,
        hasSearched = false;

    function search(usingSearchInput, clearSearch) {
        var $spinnerLocation = $("#my-highlights-list"),
            useMiniSpinner = false;

        if (usingSearchInput) {
            $spinnerLocation = $("#search-field");
            useMiniSpinner = true;
        }

        TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

        var postParams = "searchTerm=" + $("#search-field").val() + "&currentPage=" + currentPage;

        $.ajax({
            type: "POST",
            url: TDPWeb.ExtraNet.resolveURL("/MyData/Search"),
            data: postParams,

        }).done(function (result) {
            hasSearched = !clearSearch;
            $("#my-highlights-list").html(result);
            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            TDPWeb.ExtraNet.scrollUpToTarget($(".main-content-container"));
        }).fail(function (error) {
            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
        });
    }

    function totalPages() {
        return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
    }

    function goToPage(n) {
        if (n == 0 || n > totalPages()) return;
        currentPage = n;
        search();
    }

    function updateNewsAlertSubscription($form) {
        var $button = $("#news-subscribe-button");
        TDPWeb.ExtraNet.spinnerOn($button);
        var isUserSubscribed = $("#subscribe-news").prop("checked");

        $.ajax({
            url: TDPWeb.ExtraNet.resolveURL('MyData/UpdateNewsAlertSubscription'),
            type: 'POST',
            data: { isUserSubscribed: isUserSubscribed }

        }).done(function (data) {
            if (data) {
                TDPWeb.ExtraNet.spinnerOff($button);
                TDPWeb.ExtraNet.successMessageForm($form);
                window.setTimeout(function () {
                    $('#news-alert-modal').modal('hide');
                }, 1500);
            }
        }).fail(function (e) {
            TDPWeb.ExtraNet.spinnerOff($button);
            if (e.hasOwnProperty('responseJSON')) {
                $('.alert-danger').slideDown("slow", function () { });
            }
        });
    };

    function loadSubscriptionPopUp() {
        var $button = $("#subscribe-button-modal");
        $.ajax({
            type: "POST",
            url: TDPWeb.ExtraNet.resolveURL("/MyData/GetSubscriptionStatus")
        }).done(function (data) {
            $("#subscribe-news").prop("checked", data);
            var $modal = $('#news-alert-modal');
            TDPWeb.ExtraNet.resetFormValidations($('#alerts-form'));
            $modal.modal();
        }).fail(function (error) {
            TDPWeb.ExtraNet.spinnerOff($button);
        });
    };

    var myHighlightsController = (function () {

        return {
            attachDomEvents: function () {
                var $body = $("body");

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"));
                });

                $("#search-form").submit(function (event) {
                    event.preventDefault();
                    search(true);
                });

                $body.delegate(".clear-search-field", "click", function () {
                    $("#search-field").val('');
                    if (hasSearched) {
                        search(true, true);
                    }
                });

                $body.delegate("#subscribe-button-modal", "click", function (event) {
                    loadSubscriptionPopUp();
                });

                $body.delegate("#news-subscribe-button", "click", function () {
                    updateNewsAlertSubscription($('.modal-footer'));
                });

                $body.delegate("#attachment .attachment-link", "click", function (event) {
                    event.preventDefault();
                    var selectedDownloadLink = $(this).attr('href');
                    window.open(selectedDownloadLink);
                });
            }
        };
    })();

    $(function () {
        myHighlightsController.attachDomEvents();
    });

})(jQuery);