(function ($) {

    var MyProductListsController = (function () {

        var sortOrder = "name",
            currentPage = 1,
            hasSearched = false;

        function searchProductLists(usingSearchInput, query, clearSearch) {
            var $searchField = $("#search-field"),
                $searchResults = $(".search-result"),
                $spinnerLocation = $("#my-product-lists-table"),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage;

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/MyData/SearchMyProductLists"),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });

            return false;
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchProductLists();
        }

        function sortByName() {
            sortByProperty("name", "name_desc");
        }

        function sortBySize() {
            sortByProperty("size", "size_desc");
        }

        function sortByDate() {
            sortByProperty("date", "date_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchProductLists(false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function setProductListNameHidden(hidden)
        {
            if (hidden)
            {
                $("input.product-list-name").prop("disabled", true);
                $(".save-product-list-btn").prop("disabled", true);
            }
            else
            {
                $("input.product-list-name").prop("disabled", false);
                $(".save-product-list-btn").prop("disabled", false);
            }
        }

        function toggleAddOrEdit(productListId) {
            if (productListId) {
                $("#add-title").hide();
                $("#edit-title").show();
                $(".delete-btn").show();
                setProductListNameHidden(false);
            } else {
                $("#add-title").show();
                $("#edit-title").hide();
                $(".delete-btn").hide();
                $(".confirm-delete-btn").hide();
                setProductListNameHidden(true);
            }
        }

        function showEditProductListModal(product_list_id) {
            $modal = $("#edit-product-list-modal");

            toggleAddOrEdit(product_list_id);

            $modal.find(".alert").hide();

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));
            $("#search-result-form .clear-search-field").hide()

            $modal.modal().addClass("fade");

            TDPWeb.ExtraNet.getProductListController().loadListFromExternalSource(product_list_id);
        }

        function saveProductList($modal) {
            TDPWeb.ExtraNet.getProductListController().saveListFromExternalButton($modal.find(".save-product-list-btn"), function () {
                toggleAddOrEdit(true);
                searchProductLists();
            });
        }

        function deleteProductList($modal) {
            var $button = $modal.find(".confirm-delete-product-list-btn"),
                $saveButton = $modal.find(".save-product-list-btn"),
                $deleteButton = $modal.find(".delete-product-list-btn");
            TDPWeb.ExtraNet.spinnerOn($button);
            $saveButton.prop("disabled", true);
            $deleteButton.prop("disabled", true);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/MyData/ProductList'),
                type: 'DELETE',
                data: { id: $modal.find("#product-list-header .product-list-name").data("id") }
            }).done(function (data) {
                searchProductLists();

                toggleAddOrEdit();
                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);
                TDPWeb.ExtraNet.spinnerOff($button);

                $modal.modal("hide");
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($button);
                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);

                $deleteButton.show();
                $button.hide();

                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                }
            });
        }

        return {
            attachDomEvents: function () {
                var $body = $("body"),
                    $searchResult = $(".search-result");

                $body.delegate(".table-head .name", "click", function () {
                    sortByName();
                });

                $body.delegate(".table-head .size", "click", function () {
                    sortBySize();
                });

                $body.delegate(".table-head .last-updated", "click", function () {
                    sortByDate();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $body.delegate(".modal .save-product-list-btn", "click", function () {
                    saveProductList(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $searchResult.delegate("td.show-edit-modal, .edit-managed-product-btn", "click", function (event) {
                    event.stopPropagation();
                    showEditProductListModal($(this).closest("tr").data("product-list-id"));
                });

                $body.delegate(".modal .confirm-delete-product-list-btn", "click", function () {
                    deleteProductList(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $("#my-product-list-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;
                    searchProductLists(true);
                });

                $("#edit-product-list-modal").on("hidden.bs.modal", function (e) {
                    TDPWeb.ExtraNet.getProductListController().resetProductList();
                });

                $("#add-product-list-btn").click(function () {
                    showEditProductListModal();
                });

                $body.delegate("#my-product-list-search-form .clear-search-field", "click", function () {
                    $("#search-field").val('');
                    if (hasSearched) {
                        searchProductLists(true, "", true);
                    }
                });
            },
            showEditProductListModal: showEditProductListModal
        }
    })();

    $(function () {
        MyProductListsController.attachDomEvents();

        if ($(".product-list.item").length === 0)
        {
            MyProductListsController.showEditProductListModal();
        }

    });

})(jQuery);
