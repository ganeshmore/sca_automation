(function ($) {
    var myProfileController = (function () {
        function saveUser($form) {
            if (!TDPWeb.ExtraNet.validateInput($form.find("#my-profile"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                var $button = $form.find("#save-my-profile");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/MyData/UpdateMyProfile'),
                    type: 'PUT',
                    data: $form.find("form").serialize()
                }).done(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    TDPWeb.ExtraNet.successMessageForm($form);
                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form.find("form"), $form.find(".backEndErrors"));
                    }
                });
            }
        }

        return {
            attachDomEvents: function () {
                var $body = $("body");

                $.each($body.find("select"), function () {
                    $(this).select2({
                        minimumResultsForSearch: -1,
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $body.delegate("#save-my-profile", "click", function () {
                    saveUser($("#my-profile-wrapper"));
                });
            }
        }
    })();

    $(function () {
        myProfileController.attachDomEvents();
        TDPWeb.ExtraNet.setUpPopovers();

        TDPWeb.ExtraNet.updateRequiredGroup("my-profile-phones");
    });

})(jQuery);
