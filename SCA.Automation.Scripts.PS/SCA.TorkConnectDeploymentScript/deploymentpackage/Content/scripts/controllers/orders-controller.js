(function ($) {

    var OrdersController = (function () {
        var pagination,
            ajaxCall,
            _orderNumber;

        $("#PrintOrderDetails").click(function () {
            $("#orderDetailsContent").print({});
            return false;
        });

        // Download the Order Details
        $("#order-download-btn").click(function (e) {
            e.stopPropagation();
            turnButtonsOff();
            getOrderExcel(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.downloadurl) + "&" + "orderNumber=" + _orderNumber;
                turnButtonsOn();
            }, function (error) {
                onExcelGenerationFailure();
            });
        });

        // Generate Order Excel
        function getOrderExcel(done, fail) {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Excel/GenerateOrderDetails"),
                type: "POST",
                data: { orderNumber: _orderNumber }
            }).done(done).fail(fail);
        }

        function turnButtonsOff() {
            var $downloadButton = $("#order-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'disable');
            TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getFileWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });
                }

            }, 15000);
        };

        function turnButtonsOn() {
            var $downloadButton = $("#order-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'enable');
            clearTimeout(timeOut);

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }
        };

        function onExcelGenerationFailure() {
            turnButtonsOn();
            TDPWeb.ExtraNet.displayMessage({ element: $("#getExcelError-msg") });
        };

        function searchOrders($form) {
            var $button = $form.find("[type='submit']");

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            }
            else {
                if (ajaxCall) {
                    ajaxCall.abort(); // if getLatestOrders is still pending
                }

                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.btnState($button, 'disable');

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Orders/SearchOrders'),
                    type: 'POST',
                    data: $form.serialize()
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($form);
                    TDPWeb.ExtraNet.btnState($button, 'enable');

                    $(".search-result").html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th").first(), false, true);
                    $(".search-result").find("[data-alert-type=search]").slideDown(200);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.btnState($button, 'enable');
                });
            }
        }

        function showOrderDetailsModal(content_html, animated) {
            $modal = $("#order-details");

            $modal.find(".modal-body").html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        return {
            getLatestOrders: function () {
                var $resultsContainer = $(".search-result");

                TDPWeb.ExtraNet.spinnerOn($resultsContainer);

                ajaxCall = $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Orders/FetchLatestOrders'),
                    type: 'POST',
                    data: {}
                }).done(function (html) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);

                    $resultsContainer.html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th.delivery-date").first(), true, true);
                    $(".search-result").find("[data-alert-type=search]").slideDown(200);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                });
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    getOrderDetails;

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                TDPWeb.ExtraNet.setUpDatepickerPair("#po-date");
                TDPWeb.ExtraNet.setUpDatepickerPair("#delivery-date", 1);

                $body.delegate("#orders-form", "submit", function (evt) {
                    evt.preventDefault();
                    searchOrders($(this));
                });

                $body.delegate("tr.order", "click", function (evt) {
                    evt.stopPropagation();
                    showOrderDetailsModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container"),
                        orderNumber = $(this).data("sca-order-number");
                        _orderNumber = orderNumber;
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    getOrderDetails = $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Orders/GetOrderDetails'),
                        type: 'POST',
                        data: { orderNumber: orderNumber }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showOrderDetailsModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $(".modal").on("hide.bs.modal", function () {
                    if (getOrderDetails != undefined) {
                        getOrderDetails.abort();
                    }
                });

                var $pagination = $("#search-orders-pagination"),
                    $resultsContainer = $(".search-result"),
                    $slim_pagination = $('[data-slim-pagination]');

                OrdersController.pagination = new Pagination();
                OrdersController.pagination.initialize($pagination, $resultsContainer, "tbody tr", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"), $slim_pagination);
                $body.delegate('[data-slim-pagination] [data-navigate]', 'click', function () {
                    var $clicked = $(this);
                    $pagination.pagination($clicked.data('navigate'));
                });
            }
        }
    })();

    $(function () {
        OrdersController.attachDomEvents();
        OrdersController.getLatestOrders();
    });
})(jQuery);
