(function ($) {

    // Check for the IE9 Browser
    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
        //Hide the Choose File & Show the IE9
        $("#company-logo-file").hide();
        $("#spanIE9Dummy").show();
    }
    else {
        $("#company-logo-file").show();
        $("#spanIE9Dummy").hide();
    }

    //Display the IE9 error Message
    $("#spanIE9Dummy").click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $("#IE9BrowserVersion-modal").modal();
        return;
    });

    var ProductAssetsDownloadController = (function () {
        var companyLogo = undefined,
            selectedDownloadLink = undefined,
            isImageType = false,
            actionTypeClicked = false;
           

        function validateWizard(step) {
            switch (step) {
                case 1:
                    if (!$("#product-list-added-products .productListItem").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-list-alert-msg" });
                        return false;
                    }
                    break;
                case 2:
                    if (!$("#product-assets-download-select-list input[type=checkbox]:checked").length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#product-assets-select-alert-msg" });
                        return false;
                    }
                    else {
                        $("#productasset-download-all-btn").hide();
                        var count = $("#product-assets-download-select-list input[type=checkbox]:checked").length;
                        if (count > 1)
                        {
                            $("#productasset-download-all-btn").show();
                        }
                    }
            }
            return true;
        };

        function clearDownloadLinks() {
            $("#product-assets-download-download-links")[0].innerHTML = "";
        }

        function addDownloadLinks() {
            $container = $("#product-assets-download-download-links");
            $container[0].innerHTML = "";
            $template = $container.siblings(".template").first();

            $.each($("#product-assets-download-select-list input[type=checkbox]:checked"), function () {
                var $clone = $template.clone(),
                    url = $(this).data("url")
                isImage = $(this).data("is-image"),
               isImageType = isImage && isImage.toLowerCase() == 'true';

                $clone.show().removeClass("template");
                $.each($clone.find("a"), function () {
                    $(this).attr("href", url);
                    $(this).data("is-image", isImageType);
                    if (!$(this).hasClass("download-icon-container") && isImageType) {
                        $(this).removeAttr("href");
                        $(this).attr('disabled', "");
                        $(this).removeAttr("_target");
                        $(this).addClass('u-noClick');
                    }
                });
                $clone.find("[data-text='title']").text($(this).data("title"));
                $clone.find("[data-text='subtitle']").text($(this).data("subtitle"));

                if ($(this).data("thumbnail")) {
                    $clone.find(".product-img").find("img").attr("src", $(this).data("thumbnail"));
                }

                $container.append($clone);
            });
        }

        function updateSelectedCheckboxesCount($containers) {
            clearDownloadLinks();

            if ($containers == undefined || $containers.length == 0) {
                $containers = $("#product-assets-download-select-list tr");
            }

            $.each($containers, function () {
                $selected_items = $(this).find($(".selected-items"));
                var count = $(this).find("input[type=checkbox]:checked").length;
                $selected_items.text($selected_items.text().replace(/\d+\/(\d+)/, count + "/$1"));
            });

            var total_count = $("#product-assets-download-select-list input[type=checkbox]:checked").length;
            var $deselect = $("#multiple-select-container [data-select-type=deselect]");
            setElementHidden($deselect, total_count == 0);
            $deselect.text($deselect.data("title") + " (" + total_count + ")")

            addDownloadLinks();
        }

        function setElementHidden($element, hidden) {
            if (hidden) {
                $element.hide();
            }
            else {
                $element.show();
            }
        }


        function updateAndShowSelectAll($selectAll, $selectList) {
            $selectAll.find(".links a:not(.template):not([data-select-type=deselect])").remove();
            var all_types = {};
            var $checkboxes = $selectList.find("input[type=checkbox]");
            $.each($checkboxes, function () {
                var $checkbox = $(this);
                var type = $checkbox.data("type");
                if (type !== undefined) {
                    if (all_types[type] === undefined) {
                        all_types[type] = { type: type, name: $checkbox.data("name"), count: 0, sort_order: $checkbox.data("sort-order"), sub_types: {} };
                    }
                    all_types[type].count += 1;

                    var sub_type = $checkbox.data("sub-type");
                    if (sub_type !== undefined) {
                        if (all_types[type].sub_types[sub_type] === undefined) {
                            all_types[type].sub_types[sub_type] = { type: sub_type, name: $checkbox.data("sub-type-name"), count: 0 };
                        }
                        all_types[type].sub_types[sub_type].count += 1;
                    }
                }
            });

            var $template = $selectAll.find(".template");
            var $clone = $template.clone();
            $clone.show().removeClass("template").text($clone.text() + " (" + $checkboxes.length + ")").data("select-type", "all");
            $template.after($clone);
            $.each($.map(all_types, function (value, index) { return [value]; }).sort(function (x, y) { var diff = x.sort_order - y.sort_order; return diff != 0 ? diff : (x.name < y.name) ? -1 : (x.name > y.name) ? 1 : 0; }), function () {
                var $clone = $template.clone();
                $clone.show().removeClass("template").text(this.name + " (" + this.count + ")").data("select-type", this.type);
                $template.siblings().last().after($clone);

                $.each($.map(this.sub_types, function (value, index) { return [value]; }).sort(function (x, y) { return (x.name < y.name) ? -1 : (x.name > y.name) ? 1 : 0; }), function () {
                    var $sub_clone = $template.clone();
                    $sub_clone.show().removeClass("template").addClass("sub").text(this.name + " (" + this.count + ")").data("select-type", this.type);
                    $template.siblings().last().after($sub_clone);
                });
            });

            setElementHidden($selectAll.find("[data-select-type=deselect]"), true);

            $selectAll.show();
        }

        var change_timeout = undefined;
        var last_xhr = undefined;
        function productListChanged(evt, articles) {
            if (change_timeout !== undefined) {
                clearTimeout(change_timeout);
            }

            change_timeout = setTimeout(function () {
                if (last_xhr) {
                    last_xhr.abort();
                    last_xhr = undefined;
                }

                var $selectAll = $("#multiple-select-container");
                $selectAll.hide();

                var $imageResolutionSelect = $("#image-resolution-select-container");
                $imageResolutionSelect.hide();

                var $selectList = $("#product-assets-download-select-list");

                $selectList[0].innerHTML = "";
                clearDownloadLinks();

                if (articles.length > 0) {
                    TDPWeb.ExtraNet.spinnerOn($selectList, false, "", { color: "#575757", zIndex: 15, top: "60%" });

                    last_xhr = $.ajax({
                        type: "POST",
                        url: TDPWeb.ExtraNet.resolveURL("/ProductData/ProductAssetsDownloadSelectBody"),
                        data: { articles: articles, resolution: $("#image-resolution-select-container input:checked").val() }
                    }).done(function (result) {
                        last_xhr = undefined;
                        $selectList.html(result);
                        updateAndShowSelectAll($selectAll, $selectList);
                        $imageResolutionSelect.show();
                    }).fail(function (error) {
                        last_xhr = undefined;
                        TDPWeb.ExtraNet.spinnerOff($selectList);
                    });
                }
            }, 1000);
        }

        function sendProductAssetsList(evt) {
            evt.preventDefault();
            var $form = $("#product-assets-download-email-btn").closest("form");
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                var $button = $form.find("input[type=submit]"),
                    url = TDPWeb.ExtraNet.resolveURL("/ProductData/SendProductAssetsDownloadList"),
                    params = {};

                TDPWeb.ExtraNet.spinnerOn($button);

                $.each($form.serializeArray(), function (idx, elem) {
                    params[elem.name] = elem.value;
                });

                params.product_names = $.map($("#product-assets-download-select-list input[type=checkbox]:checked"), function (val, i) { return $(val).data("subtitle"); });
                params.asset_names = $.map($("#product-assets-download-select-list input[type=checkbox]:checked"), function (val, i) { return $(val).data("title"); });
                params.asset_urls = $.map($("#product-assets-download-select-list input[type=checkbox]:checked"), function (val, i) { return $(val).data("url"); });
                params.emailList = $form.find(".emailList").tagsinput("items");
                params.ccemailList = $form.find(".ccemailList").tagsinput("items");
                params.displayLogo = $("#enableLogo").length == 0 || $("#enableLogo").is(":checked");
                params.logo_data = companyLogo;
                params.logo_file_name = $("#company-logo-file").val() ? $("#company-logo-file").val().split('\\').pop() : "";
                params.displayDisclaimer = isImageType;

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: params
                }).done(function (data) {
                    TDPWeb.ExtraNet.reportEmailSentEvent();
                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageForm($form);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }
                });
            }
        }

        function update_company_logo(file) {
            if (file == undefined) {
                companyLogo = undefined;
            } else {
                var reader = new FileReader();
                reader.onload = function (event) {
                    companyLogo = event.target.result.replace(/^data:image\/(.*?);base64,/, "");
                }
                reader.readAsDataURL(file);
            }
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function productAssetDisclaimer(event, actionType) {
            //actionType :: 0: Download Icon click, 1: Email button Click
            event.preventDefault();
            actionTypeClicked = actionType;
            $modal = $("#disclaimer");
            if (isImageType && actionType != 1) {
                $modal.modal();
            }
            else {
                switch (actionType) {
                    case 0:
                        window.open(selectedDownloadLink);
                        break;
                    case 1:
                        var $form = $("#product-assets-download-email-btn").closest("form");
                        if (!TDPWeb.ExtraNet.validateInput($form)) {
                            TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
                        }
                        else if (isImageType) {
                            $modal.modal();
                        }
                        else {
                            sendProductAssetsList(event);
                        }
                        break;
                    case 2:
                        downloadAllProductAssets();
                        break;
                }
            }
        }

        function closeDisclaimerPopup(event) {
            //actionType :: 0: Download Icon click, 1: Email button Click, 2: Download All click
            switch (actionTypeClicked) {
                case 0:
                    window.open(selectedDownloadLink);
                    break;
                case 1:
                    sendProductAssetsList(event);
                    break;
                case 2:
                    downloadAllProductAssets();
                    break;
            }
        }

        function downloadAllProductAssets() {
            var $button = $('#productasset-download-all-btn');

            TDPWeb.ExtraNet.spinnerOn($button);
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/MarketingResources/StoreDownloadAllResourceURLs'),
                type: 'POST',
                data: {
                    urls: $.map($("#product-assets-download-download-links .download-icon-container"), function (val, i) {
                        return $(val).attr("href");
                    }),
                    assetType: 'Product'
                }

            }).done(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.url);
                TDPWeb.ExtraNet.spinnerOff($button);
            });
        }


        return {
            attachDomEvents: function () {
                var $body = $("body"),
                    $companyLogo = $("#company-logo-file"),
                    $spinnerLocation = $("#company-logo-file"),
                    $defaultLogo = $("#defaultLogo");

                Wizard.initiateWizard(validateWizard);

                $body.delegate("#application-product-list", "product-list-changed", productListChanged);

                $body.delegate("#product-assets-download-select-list td>.panel-toggler", "click", function (e) {
                    $(this).siblings(".collapse").collapse("toggle");
                    $tr = $(this).closest("tr");
                    $tr.toggleClass("open");
                    TDPWeb.ExtraNet.loadDataSrcImageInContainer($tr);
                });

                $body.delegate("#product-assets-download-select-list input[type=checkbox]", "change", function (e) {
                    updateSelectedCheckboxesCount($(this).closest("tr"));
                });

                $body.delegate("#multiple-select-container .links a", "click", function (e) {
                    e.preventDefault();
                    var select_type = $(this).data("select-type");
                    if (select_type !== undefined) {
                        if (select_type === "all") {
                            $("#product-assets-download-select-list input[type=checkbox]").prop("checked", true);
                            updateSelectedCheckboxesCount();
                        }
                        else if (select_type === "deselect") {
                            $("#product-assets-download-select-list input[type=checkbox]").prop("checked", false);
                            updateSelectedCheckboxesCount();
                        }
                        else {
                            $("#product-assets-download-select-list input[type=checkbox][data-type='" + select_type + "']").prop("checked", true);
                            $("#product-assets-download-select-list input[type=checkbox][data-sub-type='" + select_type + "']").prop("checked", true);
                            updateSelectedCheckboxesCount();
                        }
                    }
                });

                $body.delegate(".tooltip-select-container [data-target=tooltip]", "mouseleave", function () {
                    $(this).removeClass("visible");
                });

                $body.delegate("#image-resolution-select-container input", "change", function (e) {
                    var resolution = $(this).val(),
                        resolution_string = $(this).data("resolution-prefix") + "-" + resolution;

                    $.each($("#product-assets-download-select-list input[" + resolution_string + "]"), function () {
                        $(this).closest(".single-container").find("a").attr("href", $(this).attr(resolution_string));
                        $(this).data("url", $(this).attr(resolution_string));
                    });

                    addDownloadLinks();
                });

                $body.delegate("#product-assets-download-email-btn", "click", function (event) {
                    isImageType = downloadAssetHasImage();
                    productAssetDisclaimer(event, 1);
                });

                $body.delegate("#product-assets-download-download-links a", "click", function (event) {
                    selectedDownloadLink = $(this).attr('href');
                    isImageType = $(this).data("is-image") == true;
                    productAssetDisclaimer(event, 0);
                });

                $body.delegate("#close-disclaimer, #disclaimer-header-close", "click", function (event) {
                    closeDisclaimerPopup(event);
                });

                $body.delegate("#productasset-download-all-btn", "click", function (event) {
                    isImageType = downloadAssetHasImage();
                    productAssetDisclaimer(event, 2);
                });

                function downloadAssetHasImage() {
                    var hasImage = false;
                    $("#product-assets-download-download-links .download-icon-container").each(function () {
                        if ($(this).data("is-image")) {
                            hasImage = true;
                            return false;
                        }
                    });
                    return hasImage;
                }

                function bindFileInput() {
                    $companyLogo.change(function () {
                        var ext = this.value.match(/\.([^\.]+)$/)[1].toLowerCase();
                        switch (ext) {
                            case 'jpg':
                            case 'jpeg':
                            case 'png':
                            case 'gif':
                                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                                var file = this.files[0];
                                update_company_logo(file);
                                displaySelectedFileName($companyLogo);
                                break;
                            default:
                                this.value = '';
                        }
                    });
                };

                bindFileInput();

                if ($defaultLogo.length) {
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });
                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                } else {
                    $companyLogo.preimage();
                }

                $("#preview-image").bind("imageLoaded", function () {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                });

                $(".remove-logo-btn").on("click", function () {
                    $companyLogo.val("");
                    $("#preview-image").hide();
                    update_company_logo(undefined);
                    displaySelectedFileName($companyLogo);
                });

                $("#reset-logo-btn").on("click", function () {
                    TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");
                    $companyLogo.unbind('change');
                    bindFileInput();
                    $companyLogo.preimage({
                        logoId: $defaultLogo.data("logo-id"),
                        name: $defaultLogo.data("name"),
                        file: $defaultLogo.data("file")
                    });

                    $("#selectedFileNamePlaceHolder .fileName").html($defaultLogo.data("name"));
                });
            }
        }
    })();

    (function () {
        ProductAssetsDownloadController.attachDomEvents();
    })();

})(jQuery);