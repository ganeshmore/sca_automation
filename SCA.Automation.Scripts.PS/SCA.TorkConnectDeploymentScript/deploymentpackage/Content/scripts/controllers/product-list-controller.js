(function ($) {

    var ProductListController = (function () {

        var getProductsCountURL = TDPWeb.ExtraNet.resolveURL('/Product/GetProductsCount');
        var searchProductsURL = TDPWeb.ExtraNet.resolveURL('/Product/SearchProducts');
        var tkadSearch = false;

        if (typeof TKAD_SEARCH != 'undefined' && TKAD_SEARCH) {
            tkadSearch = true;
        }

        if (typeof PRODUCT_SEARCH_URL != 'undefined' && PRODUCT_SEARCH_URL != null) {
            searchProductsURL = TDPWeb.ExtraNet.resolveURL(PRODUCT_SEARCH_URL);
        }

        if (typeof GET_PRODUCTS_COUNT_URL != 'undefined' && GET_PRODUCTS_COUNT_URL != null) {
            getProductsCountURL = TDPWeb.ExtraNet.resolveURL(GET_PRODUCTS_COUNT_URL);
        }

        var firstTimeSave = !/\/\d+/.test(window.location.href), // boolean returns true if url does not have a product list id
            unsavedChanges = false,
            loadModeOpen = true,
            productListPagination,
            hasSearched = false,
            lastSearch = "",
            searchTimeout = null,
            selectedTabId = null,
            customerSpecificModule = "Customer specific catalog",
            productAssetsDownloadModule = "Product assets download",
            customerSpecificSectionName = "customer-specific-catalog-module",
            productAssetsDownloadSectionName = "product-assets-download-module";

        function productListItems() {
            var $products = $("#application-product-list .productListItem"),
                listItems = [];

            $.each($products, function (i) {
                listItems[i] = {
                    productarticleid: $($products[i]).find(".article-number").text(),
                    sortorder: i + 1
                }
            });

            return listItems;
        }

        function getSoldTo() {
            if (tkadSearch) {
                return $('#tkad-distributor-data-sold-to').val();
            }

            return "";
        }

        function updateProductCount(delta) {
            $('.number-of-products').each(function () {
                var nProductsSpan = $(this),
                    nProducts = nProductsSpan.text();
                nProducts = parseInt(nProducts) + delta;
                nProductsSpan.text(nProducts).parent().toggleClass("u-xs-colorBlue", !!nProducts).toggleClass("u-xs-colorGray", !nProducts);
            });
        }

        function updateProductsAlreadyInList() {
            var articleNumbersInList = $("#application-product-list-body .article-number").map(function () {
                return $(this).text();
            }).get();

            if (window.location.href.indexOf("MyProductLists") == -1) {
                $(".product-img, .product-name").addClass('u-cursorPointer');

                $(".product-img, .product-name").click(function (event) {
                    event.stopPropagation();
                    showProductDetailModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var $spinner_position = $("#empty-spinner-container");
                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Product/ProductDetailModalBody'),
                        type: 'GET',
                        data: { articleNumber: $(this).data("articleNumber") }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showProductDetailModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });
            }

            $('.add-to-list[data-article-number]').each(function () {
                var currentArticleNumber = String($(this).data("articleNumber")),
                    alreadyInList = false;

                if (currentArticleNumber.length > 0) {
                    alreadyInList = $.inArray(currentArticleNumber, articleNumbersInList) != -1;
                }

                if (alreadyInList) {
                    var $product = $(this).closest(".product-module");

                    toggleAddToListLink($(this), true);
                    $product.addClass("included-in-list");
                }
            });
        }

        function showProductDetailModal(body_html, animated) {
            $modal = $("#productDetailModal");
            $modal.find(".alert").hide();
            $modal.find(".modal-body").html(body_html);
            initCarousel();

            selectedTabId = null;
            $modal.off('click', '.tab', tabClicked);
            $modal.on('click', '.tab', tabClicked);
            $modal.off('click', '#productDescriptionReadMore', productDescriptionReadMoreClicked);
            $modal.on('click', '#productDescriptionReadMore', productDescriptionReadMoreClicked);

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        function initCarousel() {
            var container = $(".productImageContainer.active");
            if (!container.hasClass("flexSliderExpanded")) {
                container.addClass("flexSliderExpanded");

                var imgContainer = container.find(".imgContainer");
                var thumbnails = container.find(".thumbnails").not(".excludeFromSlider");

                thumbnails.flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 60,
                    itemMargin: 5,
                    minItems: 1,
                    prevText: "",
                    nextText: "",
                    asNavFor: imgContainer
                });

                imgContainer.flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    minItems: 1,
                    prevText: "",
                    nextText: "",
                    sync: thumbnails
                });
            }
        }

        function tabClicked(evt) {
            // Taken from Tork Online
            evt.preventDefault();

            var tab = $(evt.currentTarget);
            var tabLink = tab.hasClass('tabLink') ? tab : tab.find('.tabLink');
            var tabId = tabLink.attr('href');
            var similarTabs = $modal.find("[href='" + tabId + "']");
            var tabContent = $(tabId);

            if (selectedTabId == null) {
                //Nothing selected. Just expand the new tab:
                selectedTabId = tabId;
                similarTabs.addClass("active");
                tabContent.stop().slideDown();
            } else if (selectedTabId == tabId) {
                // Collapse the selected tab:
                selectedTabId = null;
                $(".tabLink").removeClass("active");
                tabContent.stop().slideUp();
            } else {
                // Slide up the previous, change class on the tab and then slide down the new:
                var oldContent = $(selectedTabId);
                selectedTabId = tabId;
                oldContent.stop().slideUp(function () {
                    if (selectedTabId == tabId) {
                        $(".tabLink").removeClass("active");
                        similarTabs.addClass("active");
                        tabContent.stop().slideDown();
                    }
                });
            }
        }

        function productDescriptionReadMoreClicked (evt) {
            evt.preventDefault();

            var target = $($(evt.currentTarget).attr('href'));
            var readMore = $(evt.currentTarget).find("#readMore");
            var readLess = $(evt.currentTarget).find("#readLess");

            if (readMore.hasClass("hide")) {
                target.slideUp();
            } else {
                target.slideDown();
            }

            readMore.toggleClass("hide");
            readLess.toggleClass("hide");
        }

        function showProductDetailModal(body_html, animated) {
            $modal = $("#productDetailModal");
            $modal.find(".alert").hide();
            $modal.find(".modal-body").html(body_html);
            initCarousel();

            selectedTabId = null;
            $modal.off('click', '.tab', tabClicked);
            $modal.on('click', '.tab', tabClicked);
            $modal.off('click', '#productDescriptionReadMore', productDescriptionReadMoreClicked);
            $modal.on('click', '#productDescriptionReadMore', productDescriptionReadMoreClicked);

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");
        }

        function initCarousel() {
            var container = $(".productImageContainer.active");
            if (!container.hasClass("flexSliderExpanded")) {
                container.addClass("flexSliderExpanded");

                var imgContainer = container.find(".imgContainer");
                var thumbnails = container.find(".thumbnails").not(".excludeFromSlider");

                thumbnails.flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 60,
                    itemMargin: 5,
                    minItems: 1,
                    prevText: "",
                    nextText: "",
                    asNavFor: imgContainer
                });

                imgContainer.flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    minItems: 1,
                    prevText: "",
                    nextText: "",
                    sync: thumbnails
                });
            }
        }

        function tabClicked(evt) {
            // Taken from Tork Online
            evt.preventDefault();

            var tab = $(evt.currentTarget);
            var tabLink = tab.hasClass('tabLink') ? tab : tab.find('.tabLink');
            var tabId = tabLink.attr('href');
            var similarTabs = $modal.find("[href='" + tabId + "']");
            var tabContent = $(tabId);

            if (selectedTabId == null) {
                //Nothing selected. Just expand the new tab:
                selectedTabId = tabId;
                similarTabs.addClass("active");
                tabContent.stop().slideDown();
            } else if (selectedTabId == tabId) {
                // Collapse the selected tab:
                selectedTabId = null;
                $(".tabLink").removeClass("active");
                tabContent.stop().slideUp();
            } else {
                // Slide up the previous, change class on the tab and then slide down the new:
                var oldContent = $(selectedTabId);
                selectedTabId = tabId;
                oldContent.stop().slideUp(function () {
                    if (selectedTabId == tabId) {
                        $(".tabLink").removeClass("active");
                        similarTabs.addClass("active");
                        tabContent.stop().slideDown();
                    }
                });
            }
        }

        function productDescriptionReadMoreClicked (evt) {
            evt.preventDefault();

            var target = $($(evt.currentTarget).attr('href'));
            var readMore = $(evt.currentTarget).find("#readMore");
            var readLess = $(evt.currentTarget).find("#readLess");

            if (readMore.hasClass("hide")) {
                target.slideUp();
            } else {
                target.slideDown();
            }

            readMore.toggleClass("hide");
            readLess.toggleClass("hide");
        }

        function listChanged() {
            var $saveBtn = $('#save-btn')

            if ($("#product-list-added-products .productListItem").length) {
                $saveBtn.removeClass("disabled").addClass("u-cursorPointer");
                $("#application-product-list").show();
                $("#application-product-list-empty-description").hide();
            }
            else {
                $saveBtn.addClass("disabled").removeClass("u-cursorPointer");
                $("#application-product-list").hide();
                $("#application-product-list-empty-description").show();
            }

            $('.product-list-name').each(function () {
                var productListNameSpan = $(this),
                    productListName = productListNameSpan.text();
                if (!productListName.indexOf("*") == 0) {
                    productListNameSpan.text("*" + productListName);
                }
            });

            ProductListController.unsavedChanges = true;

            var articleNumbersInList = $("#application-product-list-body .productListItem .article-number").map(function () {
                return $(this).text();
            }).get();

            $("#application-product-list").trigger("product-list-changed", [articleNumbersInList]);
        }

        function resetListChanged() {
            $('#save-btn').removeClass("u-cursorPointer").addClass("disabled");

            $('.product-list-name').each(function () {
                var productListNameSpan = $(this),
                    productListName = productListNameSpan.text();
                if (productListName.indexOf("*") == 0) {
                    productListNameSpan.text(productListName.substring(1));
                }
            });
        }

        function addProductToList(articleName, articleNum, batch_adding, list_products, isRefill, unitOfMeasure) {            
            if (batch_adding === undefined) {
                batch_adding = false;
            }

            if (isRefill === undefined) {
                isRefill = true;
            }

            if (list_products === undefined) {
                list_products = $.map($(".productListItem .article-number"), function (val, i) {
                    return $(val).text();
                });
            }

            var should_add = $.inArray(articleNum, list_products) == -1;
            if (should_add) {
                updateProductCount(1);

                var $cloneRowInstance = $(".productListItemTemplate").clone();

                $cloneRowInstance.find(".article-name").text(articleName);
                $cloneRowInstance.find(".article-number").text(articleNum);

                $cloneRowInstance.addClass("productListItem");
                $cloneRowInstance.removeClass("productListItemTemplate");
                $cloneRowInstance.show();

                if (unitOfMeasure) {                    
                    $($cloneRowInstance.find(".article-name")).data('unitMeasure', unitOfMeasure);                    
                }

                $cloneRowInstance.addClass("productListItemAdded").insertAfter("tbody .productListItemTemplate");

                setTimeout(function () {
                    $cloneRowInstance.removeClass("productListItemAdded");
                }, 500);

                var $product = $(this).closest(".product-module");
                toggleAddToListLink($('.add-to-list[data-article-number="' + articleNum + '"]'), true);
                $product.addClass("included-in-list");

                if (!batch_adding) {
                    listChanged();
                }
            }

            $("input.product-list-name").removeAttr("disabled");
            $(".save-product-list-btn").removeAttr("disabled");
            // Add Price to the List
            addProductPriceToList(articleNum, articleName, isRefill);
        }

        // This is a bit of a hack to expose the add to list functionality to TKAD; this should probably be done as a view model relationship
        window.ProductListAddProductToList = addProductToList;

        function toggleAddToListLink($addLink, hide) {

            var $listElem = $addLink.closest("li");
            if (hide) {
                $listElem.hide();
                $listElem.siblings(".added-to-list-label").show();
            } else {
                $listElem.show();
                $listElem.siblings(".added-to-list-label").hide();
            }
        }

        function addToList() {
            var $product = $(this).closest(".product-module"),
                articleName = $product.find("h3").first().text(),
                articleNum = $product.find(".article-number").first().text(),
                isRefill = $($product.find(".product-meta").first()).data('is-refill').toLowerCase() == "true",
                unitOfMeasure = $product.find("h3").data('productUnitofmeasure');

            addProductToList(articleName, articleNum, undefined, undefined, isRefill, unitOfMeasure);

            // Push to GA
            addProductInfoToGA(articleName, articleNum);
        }


        // Add Price to the Product List
        function addProductPriceToList(articleNum, articleName, isRefill) {
            if ($(".productPriceListItemTemplate").length > 0) {
                var $clonePriceRowInstance = $(".productPriceListItemTemplate").clone();
                $clonePriceRowInstance.find(".article-name").text(articleName);
                $clonePriceRowInstance.find(".article-number").text(articleNum);

                if (isRefill) {
                    $clonePriceRowInstance.find("[type=checkbox]").hide();
                    $clonePriceRowInstance.find("label[for='chkTADP']").hide();
                }
                else {
                    $clonePriceRowInstance.find("[type=checkbox]").attr('id', 'chkTADP' + '_' + articleNum);
                    $clonePriceRowInstance.find("label[for='chkTADP']").attr('for', 'chkTADP' + '_' + articleNum);
                }

                $clonePriceRowInstance.addClass("productPriceListItem");
                $clonePriceRowInstance.removeClass("productPriceListItemTemplate");
                $clonePriceRowInstance.removeClass("hidden");

                $clonePriceRowInstance.insertAfter("tbody .productPriceListItemTemplate");
            }
        };

        // Add GA event
        function addProductInfoToGA(articleName, articleNum) {
            var moduleName = "";
            var sectionName = $("#search-submit").closest("section").filter(function () {
                return $(this).attr("role") == "main";
            }).attr("id");
            if (sectionName != undefined) {
                switch (sectionName) {
                    case customerSpecificSectionName:
                        {
                            moduleName = customerSpecificModule;
                            break;
                        }
                    case productAssetsDownloadSectionName:
                        {
                            moduleName = productAssetsDownloadModule;
                            break;
                        }
                }
            }

            if (moduleName.length > 0) {
                window.dataLayer.push(
                    {
                        'event': 'gaEvent',
                        'eventCategory': moduleName,
                        'eventAction': 'Add to list',
                        'eventLabel': articleName + " - " + articleNum
                    });
            }
        }


        function removeProductFromList($product) {
            var articleNum = $product.find(".article-number").text();

            $product.remove();
            updateProductCount(-1);

            $product = $(".product-module .article-number:contains('" + articleNum + "')")
                .closest(".product-module");

            //use following code in case we need to match article-number exactly
            //$product = $(".product-module .article-number")
            //                               .filter(function () {
            //                                   return $(this).text() == articleNum;
            //                               }).closest(".product-module");

            $product.removeClass("included-in-list");
            toggleAddToListLink($product.find(".action-links").find(".add-to-list"), false);

            // Delete From Add Price Tab
            $("#addtoprice-products-list .article-number:contains('" + articleNum + "')").closest('tr').remove();

            listChanged();
        }

        function removeFromList() {
            var $product = $(this).closest(".productListItem");

            removeProductFromList($product);

        }

        function toggleRelatedProducts($elem, show) {
            $elem.toggle();
            if (show) {
                $elem.next().toggle();
            } else {
                $elem.prev().toggle();
            }
        }

        function showRelatedProducts() {
            var $this = $(this),
                $product = $this.closest(".product-module");

            getRelatedProducts($product);
            toggleRelatedProducts($this.closest("li"), true);
        }

        function hideRelatedProducts() {
            var $this = $(this);
            toggleRelatedProducts($this.closest("li"), false);
            $this.closest(".product-module").children(".related-products-container").slideUp();
        }

        function toggleActionDialog(element) {
            var didOpen = false,
                openActionDialogs = $(".list-action").filter(":visible"),
                thisActionDialog = $('#' + element);

            if (thisActionDialog.is(":visible")) {
                thisActionDialog.slideUp(200);
            } else if (openActionDialogs.length) {
                didOpen = true;
                $(openActionDialogs).slideUp(200, function () {
                    thisActionDialog.slideDown(200);
                });
            } else {
                didOpen = true;
                thisActionDialog.slideDown(200);
            }
            return didOpen;
        }

        function saveAs() {
            $("#save-as-save-btn").prop('disabled', true);
            toggleActionDialog("save-as-confirmation");
            ProductListController.unsavedChanges = false;
        }

        function saveAsCancel() {
            $('#save-as-confirmation').slideUp(200);
        }

        function saveAsSave() {
            $saveBtn = $("#save-as-save-btn");
            TDPWeb.ExtraNet.spinnerOn($saveBtn, true);
            saveList(null, $('#productListNameTextField').val(), function (result) {
                var newProductListName = $('#productListNameTextField').val();
                $('#productListNameTextField').val("");

                firstTimeSave = false;
                $('#save-as-confirmation').hide();
                resetListChanged();

                $('.product-list-name').text(newProductListName);

                $('.product-list-name').data("id", result.product_list_id);

                TDPWeb.ExtraNet.spinnerOff($saveBtn);

                $('#save-confirmation').show().delay(2000).slideUp(200);
            }, function (error) {
                TDPWeb.ExtraNet.spinnerOff($saveBtn);

                $('#save-failed').show().delay(2000).slideUp(200);
            });
        }

        function getList(list_id, done, fail) {

            var getListRequest = $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/Product/GetProductList"),
                data: { id: list_id }
            }).done(done).fail(fail);

            $("#load-cancel-btn").click(function () {
                getListRequest.abort();
            });

            $(".modal").on("hide.bs.modal", function () {
                getListRequest.abort();
                $(".move-aside").removeClass("move-aside");
                $("#search-filter-content").hide();
            });
        }

        function saveList(listID, listName, done, fail) {
            var listItems = productListItems(),
                data = null;

            data = {
                id: listID,
                name: listName,
                listitems: listItems
            };

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/Product/SaveProductList"),
                data: data
            }).done(done).fail(fail);
        }

        function save($externalButton, success) {
            var $non_external_spinner_element = $('.product-list-name');
            var external_button_clicked = $externalButton !== undefined;
            if (firstTimeSave && !external_button_clicked && !$("#expired-products-list").is(":visible")) {
                saveAs();
            }
            else {
                if (external_button_clicked || $('#save-btn').hasClass('u-cursorPointer') || $("#expired-products-list").is(":visible")) {
                    $('#save-as-confirmation').hide();
                    hideConfirmation();

                    var $product_list_name_input = $("#product-list-header input.product-list-name"),
                        product_list_name = $product_list_name_input.val();

                    if (product_list_name !== undefined && !TDPWeb.ExtraNet.validateInput($product_list_name_input.parent())) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#save-failed" });
                    }
                    else {
                        $deleteBtn = $(".delete-product-list-btn");
                        $saveBtn = $(".save-product-list-btn");

                        if (external_button_clicked) {
                            $deleteBtn.prop("disabled", true);
                            $saveBtn.prop("disabled", true);
                            TDPWeb.ExtraNet.spinnerOn($externalButton);
                        } else {
                            TDPWeb.ExtraNet.spinnerOn($non_external_spinner_element, true);
                        }

                        saveList($(".product-list-name").data("id"), product_list_name, function (result) {
                            resetListChanged();

                            $('.product-list-name').data("id", result.product_list_id);

                            if (external_button_clicked) {
                                $deleteBtn.prop("disabled", false);
                                $saveBtn.prop("disabled", false);
                                TDPWeb.ExtraNet.spinnerOff($externalButton);
                            } else {
                                TDPWeb.ExtraNet.spinnerOff($non_external_spinner_element);
                            }

                            TDPWeb.ExtraNet.displayMessage({
                                element: "#save-confirmation",
                                type: 'success'
                            });

                            $('#save-confirmation').delay(2000).slideUp(200);

                            if (success !== undefined) {
                                success();
                            }
                        }, function (error) {

                            if (external_button_clicked) {
                                $deleteBtn.prop("disabled", false);
                                $saveBtn.prop("disabled", false);
                                TDPWeb.ExtraNet.spinnerOff($externalButton);
                            }

                            TDPWeb.ExtraNet.displayMessage({
                                element: "#save-failed",
                                type: 'danger'
                            });
                        });
                    }
                }
                ProductListController.unsavedChanges = false;
            }
        }

        function loadProductLists() {
            $select = $("#load-list-select");
            $select.prop("disabled", true);
            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/Product/GetProductLists")
            }).done(function (result) {
                var html = "<option></option>";

                $.each(result.lists, function (i, list) {
                    html += "<option value='" + list.id + "'>" + list.name + "</option>";
                });

                $select.html(html);

                $select.select2({
                    minimumResultsForSearch: -1,
                    placeholder: $select.data("placeholder"),
                    language: {
                        noResults: function (params) {
                            var noresult = $select.data("noresulttext");
                            return noresult;
                        }
                    },
                    theme: 'bootstrap'
                }).unbind("change.toggleLoadButton").on("change.toggleLoadButton", function () {
                    if (parseInt($select.val()) > 0) {
                        enableLoadButton();
                    }
                    else {
                        disableLoadButton();
                    }
                });

                $select.prop("disabled", false);
            }).fail(function (error) {
                $select.prop("disabled", false);
            });
        }

        function load() {
            disableLoadButton();

            if (toggleActionDialog("load-confirmation")) {
                loadProductLists();
            }
        }

        function resetProductList() {

            var $product = $("#search-result .product-module");

            $('.number-of-products').text("0").parent().removeClass("u-xs-colorBlue").addClass("u-xs-colorGray");
            $(".productListItem").remove();

            $product.removeClass("included-in-list");

            toggleAddToListLink($product.find(".action-links").find(".add-to-list"), false);

            $('.product-list-name').text("");
            $('.product-list-name').val("");
            $('.product-list-name').data("id", "");

            $("#application-product-list-empty-description").show();
            $("#application-product-list").hide();

            // Reset Price list.
            $("#addtoprice-products-list .article-number").closest('tr:not(.hidden)').remove();
        }

        function saveListFromExternalButton($button, success) {
            save($button, success);
        }

        function loadListFromExternalSource(list_id) {
            resetProductList();
            loadListWithId(list_id, undefined, true);
        }

        function toggleModalSpinner(toggle) {
            var $spinner_position = $("#product-list-spinner-container");
            if (toggle) {
                $("#product-list-header").hide();
                $("#product-list-content").hide();

                $spinner_position.show();
                TDPWeb.ExtraNet.spinnerOn($spinner_position);
            }
            else {
                $("#product-list-header").show();
                $("#product-list-content").show();

                $spinner_position.hide();
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            }
        }

        function displayExpiredProductsIfAny(result) {
            if (result.expiredList.length > 0) {
                $("#expired-products-list p").text((result.expiredList.join(", ")));
                $("#expired-products-list").show();

            } else {
                $("#expired-products-list").hide();
            }
        }

        function loadListWithId(list_id, $spinner_button, force_open) {
            if (list_id !== undefined) {
                if ($spinner_button !== undefined) {
                    TDPWeb.ExtraNet.spinnerOn($spinner_button, true);
                }

                var in_modal = $("#product-list-header").closest(".modal").length > 0;

                if (in_modal && (force_open || replaceListUponLoad())) {
                    toggleModalSpinner(true);
                    $(".save-product-list-btn").hide();
                }

                if (force_open === undefined) {
                    force_open = false;
                }

                getList(list_id, function (result) {
                    firstTimeSave = false;
                    if ($spinner_button !== undefined) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_button);
                    }

                    hideConfirmation();

                    displayExpiredProductsIfAny(result);

                    if (force_open || replaceListUponLoad()) {
                        resetProductList();

                        $('.product-list-name')
                            .text(result.list.name)
                            .val(result.list.name)
                            .data("id", result.list.id);

                        listChanged();
                    }

                    var products_array = $.map($(".productListItem .article-number"), function (val, i) {
                        return $(val).text();
                    });

                    $.each(result.list.items.reverse(), function (i, item) {
                        addProductToList(item.name, item.article_number, true, products_array, item.is_refill);

                        products_array.push(item.article_number);
                    });

                    listChanged();

                    if (force_open || replaceListUponLoad()) {
                        resetListChanged();
                        ProductListController.unsavedChanges = false;
                    }

                    ProductListController.productListPagination.resetPagination();

                    toggleModalSpinner(false);
                    $(".save-product-list-btn").show();

                }, function (error) {
                    if ($spinner_button !== undefined) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_button);
                    }
                    hideConfirmation();

                    toggleModalSpinner(false);
                });
            }
        }

        function loadList() {
            var $list_select = $('#load-list-select');

            if (parseInt($list_select.val()) > 0) {
                loadListWithId($list_select.val(), $(this));
            }
        }

        function loadCancel() {
            hideConfirmation();
        }

        function enableLoadButton() {
            $("#load-load-btn").removeClass("disabled");
        }

        function disableLoadButton() {
            $("#load-load-btn").addClass("disabled");
        }

        function hideConfirmation() {
            $('#load-confirmation').slideUp(200);
        }

        function replaceListUponLoad() {
            return ProductListController.loadModeOpen;
        }

        function filterOptionsCheck() {
            var productCount = $(".product-module").length,
                filterCount = $(".filter-content .category-group").length,
                $toggler = $("#search-filter-toggle");

            if (productCount > 0 && filterCount > 0) {
                $toggler.prop("disabled", false);
            }
            else {
                $toggler.prop("disabled", true);
                $(".move-aside").removeClass("move-aside");
            }
        }

        function searchProductByCategory(query, hideShowMoreCategories, done) {

            var requestData = { searchTerm: query, categorySearch: true, hideShowMoreCategories: hideShowMoreCategories };

            var soldTo = getSoldTo();
            if (soldTo) {
                requestData.soldToNumber = soldTo;
            }

            var hasProductList = ($("#product-list-content").length > 0);
            if (!hasProductList) {
                requestData.hideRelatedProducts = true;
                requestData.hideListAdd = true;
            }

            $.ajax({
                url: searchProductsURL,
                type: 'POST',
                data: requestData
            }).done(function (html) {
                var $searchResult = $("#search-result");
                $searchResult.html(html);
                updateProductsAlreadyInList();
                filterOptionsCheck();
                done();
            }).fail(function (error) {
                filterOptionsCheck();
                done();
            });
        }

        function searchProducts(searchTerm, page, done, clearSearch, forceSearch) {
            var $searchResult = $("#search-result"),
                oldLastSearch = lastSearch,
                force = forceSearch || false;


            lastSearch = searchTerm;

            if (!force && lastSearch == oldLastSearch && searchTerm.length > 0) {
                done();
            }
            else {
                var requestData = { searchTerm: searchTerm, currentPage: page };

                var soldTo = getSoldTo();
                if (soldTo) {
                    requestData.soldToNumber = soldTo;
                }

                var hasProductList = ($("#product-list-content").length > 0);
                if (!hasProductList) {
                    requestData.hideRelatedProducts = true;
                    requestData.hideListAdd = true;
                }

                $.ajax({
                    url: searchProductsURL,
                    type: 'POST',
                    data: requestData
                }).done(function (html) {
                    hasSearched = !clearSearch;
                    $searchResult.html(html);
                    updateProductsAlreadyInList();
                    if ($searchResult.is(':visible')) {
                        TDPWeb.ExtraNet.scrollUpToTarget($searchResult);
                    }
                    filterOptionsCheck();
                    done();
                }).fail(function (error) {
                    filterOptionsCheck();
                    done();
                });
            }
            
        };

        function getRelatedProducts($product) {

            var $relatedProducts = $product.children(".related-products-container"),
                emptyContainer = TDPWeb.ExtraNet.emptySpinnerContainer,
                articleNumber = $product.find(".article-number").first().text();

            if ($relatedProducts.html().trim() === "<p></p>") {

                $relatedProducts.html(emptyContainer);
                var $spinner_position = $relatedProducts.find("#empty-spinner-container");

                $relatedProducts.slideDown(200, TDPWeb.ExtraNet.spinnerOn($spinner_position));

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Product/GetRelatedProducts'),
                    type: 'GET',
                    data: { articleNumber: articleNumber }
                }).done(function (html) {
                    setTimeout(function () {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        $relatedProducts.html(html);
                        updateProductsAlreadyInList();
                    }, 500);

                }).fail(function (error) {

                    TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    $relatedProducts.html("<p></p>");
                });
            } else {
                $relatedProducts.slideDown();
            }
            ;
        }

        function totalPages() {
            return parseInt($('#search-result .tork-pagination ul li:last-child')[0].textContent);
        }

        function loadAll() {
            $button = $("#add-all-available-products");
            if ($button.hasClass("disabled")) {
                return;
            }

            TDPWeb.ExtraNet.spinnerOn($button, true, "left");

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Product/GetAllProducts'),
                type: 'POST'
            }).done(function (result) {
                var products_array = $.map($(".productListItem .article-number"), function (val, i) {
                    return $(val).text();
                });
                $.each(result.items, function (i, item) {
                    addProductToList(item.name, item.article_number, true, products_array, item.is_refill);

                    products_array.push(item.article_number);
                });

                TDPWeb.ExtraNet.spinnerOff($button);
                listChanged();
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($button);

            });
        }

        function setUnsavedChangesTrue() {
            ProductListController.unsavedChanges = true;
        };

        function setUnsavedChangesFalse() {
            ProductListController.unsavedChanges = false;
        };

        function numberOfItems() {
            return $("#product-list-added-products .productListItem").length;
        };

        function leaveWarning() {
            return $("[data-warning='product-list-added-products']").text();
        };

        function toggleSearchFilter() {
            var $productListContent = $('#product-list-content'),
                $toggler = $('#search-filter-toggle'),
                $filterContent = $($toggler.data('target')),
                $togglerIndicator = $toggler.find('[data-indicator]'),
                indicatorStateUp = 'icon-arrow_right_borderless',
                indicatorStateDown = 'icon-arrow_down_borderless';

            if ($filterContent.is(":visible")) {
                $filterContent
                    .hide()
                    .removeClass("open");

                $productListContent
                    .parent("div")
                    .removeClass("move-aside");

                $togglerIndicator
                    .removeClass(indicatorStateDown)
                    .addClass(indicatorStateUp);
            } else {
                $filterContent
                    .show()
                    .addClass("open");

                $productListContent
                    .parent("div")
                    .addClass("move-aside");

                $togglerIndicator
                    .addClass(indicatorStateDown)
                    .removeClass(indicatorStateUp);
            }
        }

        function submitSearch() {
            $('#search-result-form').submit();
        }

        function resetSearch(searchUrl, countUrl) {
            $("#search-result").empty();

            if (countUrl) {
                getProductsCountURL = TDPWeb.ExtraNet.resolveURL(countUrl);
            }
            setProductSearchPlaceholderCount();

            $input = $("#search-result-form #search-field");
            $input.val('');

            $("#search-result-form .clear-search-field").hide();

            if (searchUrl) {
                searchProductsURL = TDPWeb.ExtraNet.resolveURL(searchUrl);
            }
            TDPWeb.ExtraNet.spinnerOn($input, true);
            searchProducts("", 1, function () {
                TDPWeb.ExtraNet.spinnerOff($input);
            }, true);
        }

        function setProductSearchPlaceholderCount() {
            if (getProductsCountURL) {
                var soldTo = getSoldTo();
                var requestData = (soldTo) ? { soldToNumber: soldTo } : null;
                var $searchField = $(".product-list-headline #search-field");

                $searchField.attr("placeholder", $searchField.data('placeholder-default'));

                $.ajax({
                    url: getProductsCountURL,
                    data: requestData,
                    type: 'POST'
                }).done(function (html) {
                    if (html.length > 0) {
                        var text = $searchField.data('placeholder-default');
                        text = text + " (" + html + ")";
                        $searchField.attr("placeholder", text);
                    }
                });
            }
        }

        return {
            unsavedChanges: unsavedChanges,
            firstTimeSave: firstTimeSave,
            productListPagination: productListPagination,

            attachDomEvents: function () {
                var $body = $("body");

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        theme: 'bootstrap'
                    });
                });

                $("#application-product-list").disableSelection();

                $body.delegate(".add-to-list", "click", addToList);
                $body.delegate(".delete-list-btn", "click", removeFromList);

                $body.delegate(".show-related-products", "click", showRelatedProducts);
                $body.delegate(".hide-related-products", "click", hideRelatedProducts);

                $("#save-btn").click(function () { save(); });
                $("#save-as-btn").click(saveAs);
                $("#load-list-btn").click(load);
                $("#save-as-save-btn").click(saveAsSave);
                $("#save-as-cancel-btn").click(saveAsCancel);
                $("#load-load-btn").click(loadList);
                $("#load-cancel-btn").click(loadCancel);
                $("#add-all-available-products").click(loadAll);

                $body.delegate("#remove-expired-products-btn", "click", function () {
                    save();
                    $("#expired-products-list").hide();
                });

                $body.delegate("#search-filter-toggle", "click", function () {
                    toggleSearchFilter();
                });

                $('#search-result-form').submit(function (e) {
                    e.preventDefault();

                    if (searchTimeout != null) {
                        clearTimeout(searchTimeout);
                    }

                    $input = $("#search-result-form #search-field");
                    var val = $input.val();
                    var showSpinner = val.length > 0 && val != lastSearch;
                    $button = $("#search-submit");

                    if (showSpinner) {
                        TDPWeb.ExtraNet.spinnerOn($input, true);
                        $button.prop("disabled", true);
                    }

                    searchProducts($input.val(), 1, function () {
                        if (showSpinner) {
                            TDPWeb.ExtraNet.spinnerOff($input);
                            $button.prop("disabled", false);
                        }
                    });
                });

                $("#search-result-form #search-field").keyup(function () {
                    if (searchTimeout != null) {
                        clearTimeout(searchTimeout);
                    }

                    $input = $(this);
                    var val = $input.val(),
                        showSpinner = val.length > 0 && val != lastSearch;

                    searchTimeout = setTimeout(function () {
                        if (showSpinner) {
                            TDPWeb.ExtraNet.spinnerOn($input, true);
                        }

                        searchProducts($input.val(), 1, function () {
                            if (showSpinner) {
                                TDPWeb.ExtraNet.spinnerOff($input);
                            }
                        });
                    }, 500);
                });

                $('#productListNameTextField').on('input', function (e) {
                    $("#save-as-save-btn").prop('disabled', $('#productListNameTextField').val().match(/^ *$/) !== null);
                });

                $body.delegate("#search-result-form .clear-search-field", "click", function () {
                    $input = $("#search-result-form #search-field");
                    $input.val('');
                    if (hasSearched) {
                        TDPWeb.ExtraNet.spinnerOn($input, true);
                        searchProducts("", 1, function () {
                            TDPWeb.ExtraNet.spinnerOff($input);
                        }, true);
                    }
                });

                $body.delegate("#search-result li[data-query], #search-result .tork-pagination a[data-query]", "click", function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    var $target = $(this),
                        $searchResult = $("#search-result"),
                        $categoryGroup = $target.parents(".category-group"),
                        $torkPagination = $target.parents(".tork-pagination"),
                        $spinnerObject = $target.find('.icon');

                    $target.addClass('loading');
                    TDPWeb.ExtraNet.spinnerOn($spinnerObject, true, '');

                    searchProductByCategory($(this).data("query"), $("#product-results").data("hide-show-more-categories"), function () {
                        var $filters = $("#search-filter-content");
                        var moveAside = $(".move-aside").length > 0 ? true : false;

                        TDPWeb.ExtraNet.spinnerOff($spinnerObject);

                        // Todo: refactor this; quickfix to prevent panel from closing when selecting filters
                        if ($categoryGroup.length) {
                            $filters.show().addClass("open");
                            $filters.siblings("div").addClass("move-aside");
                        }
                        else if ($torkPagination.length && moveAside) {
                            $filters.show().addClass("open");
                            $filters.siblings("div").addClass("move-aside");
                        }
                        else if ($searchResult.is(':visible')) {
                            TDPWeb.ExtraNet.scrollUpToTarget($searchResult);
                        }
                    });
                });

                $body.delegate("#search-result .back-to-result", "click", function () {
                    $input = $("#search-result-form #search-field");
                    $(".move-aside").removeClass("move-aside");
                    $("")
                    lastSearch = "";

                    $("#search-result .back-to-result").addClass('loading');
                    TDPWeb.ExtraNet.spinnerOn($("#search-result .back-to-result .icon"), true, '');

                    searchProducts($input.val(), 1, function () {

                    }, true);
                });

            },

            getList: getList,
            addProductToList: addProductToList,
            resetListChanged: resetListChanged,
            loadProductLists: loadProductLists,
            listChanged: listChanged,
            setUnsavedChangesTrue: setUnsavedChangesTrue,
            setUnsavedChangesFalse: setUnsavedChangesFalse,
            numberOfItems: numberOfItems,
            leaveWarning: leaveWarning,
            loadModeOpen: loadModeOpen,
            loadListFromExternalSource: loadListFromExternalSource,
            saveListFromExternalButton: saveListFromExternalButton,
            resetProductList: resetProductList,
            displayExpiredProductsIfAny: displayExpiredProductsIfAny,
            searchProducts: searchProducts,
            getSoldTo: getSoldTo,
            submitSearch: submitSearch,
            resetSearch: resetSearch,
            setProductSearchPlaceholderCount: setProductSearchPlaceholderCount
        }
    })();

    TDPWeb.ExtraNet.registerProductListController(ProductListController);


    $(function () {
        $('[data-toggle-product-list]').on('click', function () {
            $('#application-product-list').toggleClass('xs-in');
        });

        ProductListController.attachDomEvents();

        $("#add-all-available-products").show();

        var $pagination = $("#product-list-pagination"),
            $resultsContainer = $("#application-product-list-body");

        ProductListController.productListPagination = new Pagination();
        ProductListController.productListPagination.initialize($pagination, $resultsContainer, ".productListItem", $pagination.data("items-per-page"));

        if (typeof PRODUCT_LIST_INIT_PLACEHOLDER_COUNT == 'undefined' || PRODUCT_LIST_INIT_PLACEHOLDER_COUNT) {
            ProductListController.setProductSearchPlaceholderCount();
        }

        ProductListController.loadProductLists();

        var $productListContent = $("#product-list-content");

        ProductListController.loadModeOpen = $productListContent.data("load-mode") === "open";

        var product_list_id = $productListContent.data("product-list");
        if (product_list_id !== undefined && product_list_id.toString().length > 0) {

            var $productListEmptyDescription = $("#application-product-list-empty-description"),
                $spinner_position = $("#product-list-added-products"),
                $loadBtn = $('#load-list-btn'),
                $saveAsBtn = $('#save-as-btn');

            $productListEmptyDescription.hide();
            TDPWeb.ExtraNet.spinnerOn($spinner_position, false, "", { top: '30px' });

            $loadBtn.addClass("disabled").removeClass("u-cursorPointer");
            $saveAsBtn.addClass("disabled").removeClass("u-cursorPointer");

            ProductListController.getList(product_list_id, function (result) {

                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $('.product-list-name').text(result.list.name);
                $('.product-list-name').data("id", result.list.id);

                ProductListController.displayExpiredProductsIfAny(result);

                $.each(result.list.items.reverse(), function (i, item) {
                    ProductListController.addProductToList(item.name, item.article_number, true, undefined, item.is_refill);
                });

                ProductListController.listChanged();
                ProductListController.resetListChanged();
                ProductListController.setUnsavedChangesFalse();

                $loadBtn.removeClass("disabled").addClass("u-cursorPointer");
                $saveAsBtn.removeClass("disabled").addClass("u-cursorPointer");

                ProductListController.productListPagination.resetPagination();
            }, function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $productListEmptyDescription.show();

                $loadBtn.removeClass("disabled").addClass("u-cursorPointer");
                $saveAsBtn.removeClass("disabled").addClass("u-cursorPointer");
            });
        }
    });

})(jQuery);
