(function ($) {

    var ProductTranslatorController = (function () {
        var exportLink = "/Excel/ExportProductTranslations/",
            sortOrder = "date_desc",
            currentPage = 1,
            hasSearched = false;

        function exportTranslations($form) {
            var $accountSelect = $form.find("select[name=accountid]");

            if (!TDPWeb.ExtraNet.validateInput($accountSelect.parent())) {
                TDPWeb.ExtraNet.displayMessage({ element: $("#account-select-alert-msg") });
            }
            else {
                var fullLink = exportLink;

                if ($accountSelect.val() !== undefined) {
                    fullLink = fullLink + $accountSelect.val();
                }

                window.location = TDPWeb.ExtraNet.resolveURL(fullLink);
                TDPWeb.ExtraNet.resetFormValidations($form);
            }
        }

        function toggleProductInfo($tr) {
            var showInfo = true,
                torkArticleNumber = $tr.find(".tork-article-number").text().trim();

            if ($("#productInfoBoxId")) {
                showInfo = $("#productInfoBoxId").data("tork-article-number") != torkArticleNumber
                $("#productInfoBoxId").remove();
            }

            if (showInfo) {
                var $productInfoBox = $("<tr id='productInfoBoxId' data-tork-article-number='" + torkArticleNumber + "' class='no-hover'><td class='p-0' colspan='100%'></td></tr>");
                $productInfoBox.insertAfter($tr);
                var accountId = $("#account-select").val(),
                    $spinner_position = $productInfoBox;
                TDPWeb.ExtraNet.spinnerOn($spinner_position, false, "other", { top: '28px', left: '175%' });

                $.ajax({
                    type: "POST",
                    url: TDPWeb.ExtraNet.resolveURL("/ProductData/Translate"),
                    data: { accountid: accountId, searchtype: 1, productarticlenumber: torkArticleNumber }
                }).done(function (result) {
                    TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    if (result.tork_product_html !== undefined && result.tork_product_html.length > 0) {
                        $productInfoBox.find("td").html(result.tork_product_html);
                        $productInfoBox.find("select").select2({
                            minimumResultsForSearch: -1,
                            placeholder: $(this).data("placeholder"),
                            theme: 'bootstrap'
                        });
                    }
                    else {
                        $productInfoBox.find("td").html("");
                        if (result.errors !== undefined) {
                            var $errorsWrapper = $("#expand-errors-wrapper").clone();
                            for (var i = 0; i < result.errors.length; i++) {
                                $errorsWrapper.append("<p>" + result.errors[i]["Message"] + "</p>")
                            }

                            $errorsWrapper.show();
                            $productInfoBox.find("td").append($errorsWrapper);
                        }
                    }

                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinner_position);
                });
            }
        }

        function searchTranslations(usingSearchInput, query, done, clearSearch) {
            var $form = $("#translations-search-form");

            if (!TDPWeb.ExtraNet.validateInput($("#account-select").parent())) {
                TDPWeb.ExtraNet.displayMessage({ element: $("#account-select-alert-msg") });
            }
            else {
                if (query === undefined) {
                    query = $("#translations-search-form input[name=searchTerm]")[0].value;
                }

                var $searchResults = $(".search-result"),
                    account_id = $("#account-select").val();

                if (account_id === undefined || account_id.length === 0) {
                    account_id = -1;
                }

                var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage + "&accountId=" + account_id + "&extraParams={accountId:" + account_id + "}";

                var $spinnerLocation = $("#product-translations-table"),
                    $search_input = $("#search-field"),
                    useMiniSpinner = false;

                if (usingSearchInput) {
                    $spinnerLocation = $search_input;
                    useMiniSpinner = true;
                }

                TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

                $.ajax({
                    type: "POST",
                    url: TDPWeb.ExtraNet.resolveURL("/ProductData/SearchProductTranslations"),
                    data: postParams
                }).done(function (result) {
                    hasSearched = !clearSearch;
                    $searchResults.html(result);
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    TDPWeb.ExtraNet.scrollUpToTarget($searchResults);

                    if (done !== undefined) {
                        done();
                    }
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);

                    if (done !== undefined) {
                        done();
                    }
                });

                TDPWeb.ExtraNet.resetFormValidations($form);

                return false;
            }
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchTranslations(false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function sortByColumn($th, query) {
            var column_sort_order = $th.data("sort-order");
            if (sortOrder.indexOf(column_sort_order) === -1) {
                sortOrder = column_sort_order;
            } else {
                sortOrder = sortOrder === column_sort_order ? column_sort_order + "_desc" : column_sort_order;
            }

            searchTranslations(false, query);
        }

        function addTranslation($form) {
            var $button = $("#add-translation-modal-btn"),
                $modal = $form.closest(".modal");


            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            }
            else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ProductData/ProductTranslation'),
                    type: 'POST',
                    data: $form.serialize() + "&accountid=" + $("#add-account-select").val()
                }).done(function (data) {
                    dataLayer.push({
                        'event': 'successfully_added_translation_event'
                    });

                    if ($("#account-select").val() == $("#add-account-select").val()) {
                        searchTranslations();
                    }

                    TDPWeb.ExtraNet.spinnerOff($button);
                    TDPWeb.ExtraNet.successMessageModal($modal);
                    $form.find("input[type='text']").val("");
                    TDPWeb.ExtraNet.cleanForm($form);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function saveTranslation($form) {
            var $modal = $form.closest(".modal");
            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            }
            else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                var $saveButton = $("#save-translation-btn"),
                    $deleteButton = $("#delete-translation-btn");
                    
                TDPWeb.ExtraNet.spinnerOn($saveButton);
                $deleteButton.prop("disabled", true);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ProductData/ProductTranslation'),
                    type: 'PUT',
                    data: $form.serialize()
                }).done(function (data) {
                    if ($form.data("prevent-search-after-save") !== "1") {
                        searchTranslations();
                    }

                    TDPWeb.ExtraNet.spinnerOff($saveButton);
                    $deleteButton.prop("disabled", false);
                    TDPWeb.ExtraNet.successMessageModal($modal);
                    TDPWeb.ExtraNet.cleanForm($form);
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($saveButton);
                    $deleteButton.prop("disabled", false);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function deleteTranslation($form) {
            TDPWeb.ExtraNet.resetFormValidations($form);

            var $confirmDeleteButton = $("#confirm-delete-translation-btn"),
                $saveButton = $("#save-translation-btn"),
                $deleteButton = $("#delete-translation-btn");

            TDPWeb.ExtraNet.spinnerOn($confirmDeleteButton);
            $saveButton.prop("disabled", true);
            $deleteButton.prop("disabled", true);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/ProductData/ProductTranslation'),
                type: 'DELETE',
                data: { id: $form.find("input[name=id]").val() }
            }).done(function (data) {
                searchTranslations(false, undefined, function () {
                    TDPWeb.ExtraNet.restoreDeleteButton($deleteButton, $confirmDeleteButton);

                    TDPWeb.ExtraNet.spinnerOff($confirmDeleteButton);
                    $saveButton.prop("disabled", false);
                    $deleteButton.prop("disabled", false);

                    $modal.modal("hide");
                });
            }).fail(function (data) {
                TDPWeb.ExtraNet.spinnerOff($confirmDeleteButton);
                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);

                $deleteButton.show();
                $confirmDeleteButton.hide();

                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.closest(".modal").find(".backEndErrors"));
                }
            });
        }

        function displaySelectedFileName($fileButton) {
            var fileName = $fileButton.val().split('\\').pop(),
                $placeHolder = $("#selectedFileNamePlaceHolder .fileName"),
                noFileSelected = $placeHolder.data("placeholder");

            if (fileName && fileName.length) {
                $placeHolder.html(fileName);
            }
            else {
                $placeHolder.html(noFileSelected);
            }
        }

        function resetFileInput() {
            $button = $("#choose-file-btn-id");
            $button.val("");

            // Empty file input for IE
            if (TDPWeb.ExtraNet.isOnIE()) {
                $button.wrap('<form>').closest('form').get(0).reset();
                $button.unwrap();
            }

            displaySelectedFileName($button);
        }

        function importFile() {
            var $resultsDiv = $("#results"),
                $spinnerLocation = $("#selectedFileNamePlaceHolder");
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, "left");

            // Delay in order to show spinner first
            setTimeout(function () {
                $form = $("#upload-form");
                var $accountSelect = $("#accountsDropdown"),
                    $deleteTranslation = $("#chk-Delete-Product-Translation").is(":checked"), //----Added to check the whether to delete all product translation or not
                    formData = new FormData($form[0]);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL("/Import/ImportTranslations?id=" + $accountSelect.val() + "&removeExistingTranslation=" + $deleteTranslation),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($("#import-translations-module"));
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html(html);

                    resetFileInput();
                }).fail(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                    $resultsDiv.html("");
                    TDPWeb.ExtraNet.displayMessage({ element: $(".formError") });
                });
            }, 100);
        }

        function updateFileUpload() {
            var isDisabled = !$("#accountsDropdown").val();

            $(".Upload-button.btn.btn-primary").prop("disabled", isDisabled);

            if (isDisabled) {
                $(".Upload-button.btn.btn-primary").addClass("disabled");
            } else {
                $(".Upload-button.btn.btn-primary").removeClass("disabled");
            }
        }

        function displayImportModal(account_id) {
            $modal = $("#import-translations-modal");
            $importModule = $("#import-translations-module");
            $importModule.html(TDPWeb.ExtraNet.emptySpinnerContainer);
            $modal.modal();

            var $spinner_position = $("#empty-spinner-container");
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/Import/ImportTranslationsModalBody'),
                data: { accountid: account_id },
                type: 'GET'
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                $importModule.html(html);

                $.each($importModule.find("select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $importModule.find("#accountsDropdown").change(function () {
                    updateFileUpload();
                });
                updateFileUpload();

                // This onchange work around is required because the normal .change() make IE 11 fire submit twice.
                $importModule.find('#choose-file-btn-id').click(function () {
                    $(this).off('change'); //---required for firing on change function only once.
                    $(this).one(
                         'change',
                         function () {
                             displaySelectedFileName($(this));
                             $("#upload-form").submit();
                         }
                    )
                });

                $importModule.find("#upload-form").submit(function (ev) {
                    ev.preventDefault();
                    importFile();
                });

                $importModule.delegate("#import-user-button", "click", function () {
                    resetFileInput();
                    importFile();
                });

            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });;
        }

        function displayAddModal(account_id) {
            $modal = $("#add-translation-modal");
            $form = $modal.find("form");
            $modal.find(".alert").hide();

            $select = $modal.find("#add-account-select");
            if (account_id === undefined) {
                $select.select2("val", "");
            }
            else {
                $select.select2("val", account_id);
            }

            TDPWeb.ExtraNet.cleanForm($form);
            $modal.modal();
        }

        function displayEditModal(translation_id, torkArticle, distributorArticle, productPrice, priceLabel) {
            $modal = $("#edit-translation-modal");
            $form = $modal.find("form");
            $modal.find(".alert").hide();
            $form.find("input[name=id]").val(translation_id);
            $form.find("input[name=productarticlenumber]").val(torkArticle);
            $form.find("input[name=distributorarticlenumber]").val(distributorArticle);
            $form.find("input[name=productpriceinput]").val(productPrice);
            $form.find(".price-header").text(priceLabel);
            TDPWeb.ExtraNet.cleanForm($form);
            $modal.modal();
        }

        function addItemToList(torkArtNo, listId) {
            var $addButton = $(".add-to-list-button");
            TDPWeb.ExtraNet.spinnerOn($addButton, true, "left");

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL("/ProductData/AddItemToProductList"),
                data: { torkArtNo: torkArtNo, listId: listId },
            }).done(function (result) {
                TDPWeb.ExtraNet.spinnerOff($addButton);

                var $select = $addButton.parent().parent().find('select');
                $select.find('option:selected').remove();
                $select.select2('val', '');

                $addButton.parents(".add-to-list").find(".add-to-list-button").hide();
                $addButton.parent().parent().find("#alert-info").slideDown().delay(1000).slideUp();
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($addButton);
            });

            return false;
        }

        return {

            attachDomEvents: function () {
                var $body = $("body"),
                    $searchResult = $(".search-result");

                $.each($body.find("select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $body.delegate("#add-translation-btn", "click", function (evt) {
                    displayAddModal($("#account-select").val());
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $body.delegate("#translations-search-form", "submit", function (evt) {
                    evt.preventDefault();
                    currentPage = 1;
                    searchTranslations(true);
                });

                $searchResult.delegate(".edit-translation-link", "click", function () {
                    var distributorArticle,
                        torkArticle,
                        translation_id = $(this).closest("tr").data("translation-id"),
                        distributorArticle = $(this).closest("tr").find(".distributor-article-number").text().trim(),
                        priceHeader =$(this).closest("table").find('.product-price .Sort-label').text(),
                        torkArticle = $(this).closest("tr").find(".tork-article-number").text().trim();
                    var productPrice = $(this).closest("tr").find(".product-price").text().trim();
                    displayEditModal(translation_id, torkArticle, distributorArticle, productPrice, priceHeader);
                });

                $body.delegate("#edit-translation-btn", "click", function () {
                    $(this).hide();
                    $("#edit-translation-actions").show();
                    $(".to-article-number").prop("readonly", false);
                });

                $("#export-translations-btn-translate").on("click", function () {
                    $form = $("#translations-search-form");

                    exportTranslations($form);
                });

                $("#choose-file-btn-id").on("click", function (event) {
                    $("#importSuccessMessage").slideUp(200);
                    var $form = $("#add-translation-form"),
                        $accountSelect = $("#account-select");

                    if (!TDPWeb.ExtraNet.validateInput($accountSelect.parent())) {
                        TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
                        event.preventDefault();
                    }
                    else {
                        TDPWeb.ExtraNet.resetFormValidations($form);
                    }
                });

                $("#choose-file-btn-id").on("change", function (event) {
                    var $form = $("#add-translation-form"),
                        $deleteTranslation = $("#chk-Delete-Product-Translation").is(":checked"), //----Added to check the whether to delete all product translation or not
                        $accountSelect = $("#account-select");
                    TDPWeb.ExtraNet.spinnerOn($("#selectedFileSpinner"), true, "left");

                    displaySelectedFileName($(this));

                    var formData = new FormData($("#upload-form")[0]);

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL("/Import/ImportTranslations?id=" + $accountSelect.val() + "&removeExistingTranslation=" + $deleteTranslation),
                        type: 'POST',
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false
                    }).done(function (html) {
                        TDPWeb.ExtraNet.resetFormValidations($form);
                        TDPWeb.ExtraNet.spinnerOff($("#selectedFileSpinner"));

                        TDPWeb.ExtraNet.successMessageForm($form);
                    }).fail(function (data) {
                        TDPWeb.ExtraNet.spinnerOff($("#selectedFileSpinner"));
                        TDPWeb.ExtraNet.displayMessage({ element: $("#importErrorMessage") });

                    }).always(function () {
                        $("#choose-file-btn-id").closest('form').trigger('reset');
                    });
                });

                $body.delegate(".add-to-list-select", "change", function () {
                    var $this = $(this);
                    if ($this.val()) {
                        $this.parents(".add-to-list").find(".add-to-list-button").show();
                    }
                    else {
                        $this.parents(".add-to-list").find(".add-to-list-button").hide();
                    }
                });

                $body.delegate(".add-to-list-button", "click", function () {
                    var $wrapperDiv = $(this).parent().parent(),
                        torkArtNo = $wrapperDiv.attr("data-tork-art-no"),
                        listId = $wrapperDiv.find("select").val();
                    addItemToList(torkArtNo, listId);
                });

                $body.delegate(".Sort-header", "click", function () {
                    sortByColumn($(this), $(this).closest("table").data("query"));
                });

                $body.delegate("#product-translations-table td:not(.Settings-container, .p-0)", "click", function () {
                    toggleProductInfo($(this).closest("tr"));
                });

                $body.delegate("#delete-translation-btn", "click", function () {
                    TDPWeb.ExtraNet.confirmDelete($(this), $("#confirm-delete-translation-btn"));
                });

                $body.delegate("#confirm-delete-translation-btn", "click", function () {
                    deleteTranslation($("#edit-translation"));
                });

                $body.delegate("#save-translation-btn", "click", function () {
                    saveTranslation($("#edit-translation"));
                });

                $body.delegate("#add-translation-modal-btn", "click", function () {
                    addTranslation($("#add-translation"));
                });

                $("#import-translations-btn").on("click", function () {
                    // Check for IE9 Browser Version
                    if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                        //Display IE9 message to User
                        $("#IE9BrowserVersion-modal").modal();
                        return;
                    }
                    displayImportModal($("#account-select").val());
                });

                $body.delegate(".clear-search-field", "click", function () {
                    $("#search-field").val('');
                    if (hasSearched) {
                        searchTranslations(false, "", function () { }, true);
                    }
                });

                $body.delegate("#add-account-select", "change", function () {
                    var account = $("#add-account-select").val();
                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/ProductData/GetPriceHeader'),
                        type: 'POST',
                        data: { accountid: $("#add-account-select").val() }
                    }).done(function (data) {
                        $("#add-translation-modal .price-header").text(data);
                    });
                });
            }
        }
    })();

    $(function () {
        ProductTranslatorController.attachDomEvents();
    });
})(jQuery);
