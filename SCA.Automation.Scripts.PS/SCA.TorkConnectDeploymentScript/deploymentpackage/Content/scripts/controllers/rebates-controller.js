(function ($) {

    var RebatesController = (function () {
        var pagination,
            contractsListPagination,
            ajaxCall;

        $("#PrintRebateDetails").click(function () {

            if ($(".rebate-details-content .panel:hidden").length > 0) {

                // Show all the Rebates int the List
                $(".rebate-details-content .panel:hidden").addClass("needtoResetHidden");
                $(".rebate-details-content .panel:hidden").show();

                // Print the Rebates
                $("#rebatedetailscontent").print({});

                // Hide the Rebates which are not require to show
                $(".rebate-details-content .panel.needtoResetHidden").hide();
                $(".rebate-details-content .panel.needtoResetHidden").removeClass("needtoResetHidden");
            }
            else {
                // Print the Rebates
                $("#rebatedetailscontent").print({});
            }
            return false;

        });

        function searchRebates($form) {
            var $button = $form.find("input[type='submit']");

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            }
            else {
                if (ajaxCall) {
                    ajaxCall.abort();
                }

                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/Rebates/SearchRebates'),
                    type: 'POST',
                    data: $form.serialize()
                }).done(function (html) {
                    TDPWeb.ExtraNet.resetFormValidations($form);
                    TDPWeb.ExtraNet.spinnerOff($button);

                    $('.search-result').html(html);
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th").first(), false, true);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($button);
                });
            }
        }

        function showRebateDetailsModal(content_html, animated) {
            $modal = $("#rebate-details");

            $modal.find(".rebate-details-content").html(content_html);

            TDPWeb.ExtraNet.cleanForm($modal.find("form"));

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");

            $("#pod-date").datepicker();

            //Set customer info from drop down list in search form
            var soldTo = $("#sold-to option:selected").text();
            $("#customerInfo .customer").text(soldTo);
			
			$("#contracts-list").on('change keyup', 'input, select, textarea', function () {
                $(this).closest('.panel').addClass("modified");
            });
        }

        function showMaintenanceModal() {
            $modal = $("#maintenance");

            $modal.modal().addClass("fade");
        }

        function addAdditionalItemRow() {
            var $rowTemplate = $("#additionalItemsTable tbody tr:first").clone();
            $rowTemplate.show().addClass("newItem");

            $index = $("#additionalItemsTable tbody tr").length;
            $rowTemplate.find("label[for*='box']").attr("for", "box_" + $index);
            $rowTemplate.find("input[id*='box']").attr("id", "box_" + $index);

            $rowTemplate.insertAfter("#additionalItemsTable tbody tr:last");
        }

        function addAdditionalItemRows() {
            for (i = 1; i <= 4 ; i++) {
                addAdditionalItemRow();
            }
        }

        function saveRebateDetailsModal($modal, submit) {
            var confirmQuestion = submit ? "Are you sure you want to submit the rebate details?" : "Are you sure you want to save the changes?";
            var r = confirm(confirmQuestion);
            if (r != true) return;

            if (!TDPWeb.ExtraNet.validateInput($modal)) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                var $button = $modal.find(".save-btn");
                TDPWeb.ExtraNet.resetFormValidations($modal);
                TDPWeb.ExtraNet.spinnerOn($button);

                setTimeout(function () {
                    // Change index on added rebate items
                    $items = $("#additionalItemsTable tbody").find(".newItem");

                    console.log($items);

                    $.each($items, function (index, item) {

                        $.each($(item).find("td > input, td > select"), function (i, input) {
                            $(input)
                                .attr('name', $(input).attr('name').replace("[100]", "[" + index + "]"));
                        });
                    });
                    // Change index on modified rebate items
                    $edited = $(".panel.modified");

                    $.each($edited, function (index, item) {

                        $.each($(item).find("td > input, td > select"), function (i, input) {
                            $(input)
                                .attr('name', $(input).attr('name').replace("[x]", "[" + index + "]"));
                        });
                    });

                    $modal.find("form .modified").each(function (index, item) {
                        var elementsWithName = $(item).find(" [name^=ContractRebate]");

                        elementsWithName.each(function (_, elementWithName) {
                            var oldName = $(elementWithName).attr('name');
                            var newName = oldName.replace(/(\d+)/, index);
                            $(elementWithName).attr('name', newName);
                        });
                    });

                    var formData = $modal.find("form :not(#contracts-list *), form .modified *").serializeArray();
                    formData.push({ name: "submit", value: submit });

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Rebates/SaveRebateDetails'),
                        type: 'POST',
                        data: $.param(formData),
                    }).done(function (data) {

                        showRebateDetailsModal(data, false);
                        TDPWeb.ExtraNet.successMessageModal($modal);
                        $modal.find("form .panel").removeClass("modified");
                        RebatesController.contractsListPagination.refreshPage();
                    }).fail(function (data) {

                        TDPWeb.ExtraNet.spinnerOff($button);

                        if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                            TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                        }
                    });
                }, 0);
            }
        }

        function filterPanels(e) {
            var $target = $(e.target).closest(".filter-option");
            var filter = $target.data("filter");

            var $panels = $("#contracts-list").find(".panel");

            $panels.find(".panel-content").removeClass("in")
            $panels.find(".panel-toggler").find(".icon-minus").removeClass("icon-minus")
                                                              .addClass("icon-plus");

            var filterClass = "isError";

            switch (filter) {
                case "error":
                    $panels.each(function () {
                        p = $(this);


                        if (p.data("filter") === "error") {
                            p.addClass(filterClass);
                            p.show();
                        } else {
                            p.removeClass(filterClass);
                            p.hide();
                        }
                    });
                    RebatesController.contractsListPagination.filterList(filterClass);
                    break;
                default:
                    $panels.removeClass("isError");
                    p.show();
                    RebatesController.contractsListPagination.useUnfilteredList();
                    break;
            }
        }

        return {
            attachDomEvents: function () {
                var $body = $("body"),
                    getRebateDetails;

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                TDPWeb.ExtraNet.setUpDatepickerPair("#shipping-date");

                $body.delegate("#rebates-form", "submit", function (evt) {
                    evt.preventDefault();
                    searchRebates($(this));
                });

                $body.delegate("#maintenance-btn", "click", function (evt) {
                    showMaintenanceModal();
                });

                $("#export-range-btn").on("click", function () {
                    window.location = TDPWeb.ExtraNet.resolveURL("/Excel/ExportRebates") + "?" + $("#export-range").serialize();
                    $("#export-range input[type=text]").val("");
                });

                $("#downloadRebateClaims").on("click", function (e) {
                    e.stopPropagation();
                    selected_rebate_number = $("input[name='Rebate_Number']").val();
                    window.location = TDPWeb.ExtraNet.resolveURL("/Excel/ExportRebateClaims") + "?" + "rebateNumber=" + selected_rebate_number;
                });

                $("#delete-rebate-btn").on("click", function (evt) {
                    evt.stopPropagation();
                    var r = confirm("Are you sure you want to delete the rebate details?");
                    if (r != true) return;

                    $modal = $("#maintenance");

                    $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Rebates/DeleteRebate'),
                        type: 'POST',
                        data: $("#delete-rebate").serialize()
                    }).done(function (html) {
                        TDPWeb.ExtraNet.successMessageModal($modal);
                        $("#rebateNumber").val("");
                    }).fail(function (error) {
                        // Fail message
                    });
                    return false;
                });

                $body.delegate("tr.rebate", "click", function (evt) {
                    evt.stopPropagation();
                    showRebateDetailsModal(TDPWeb.ExtraNet.emptySpinnerContainer, true);
                    var rebateNumber = $(this).data("rebate-number"),
                        $spinner_position = $("#empty-spinner-container");

                    TDPWeb.ExtraNet.spinnerOn($spinner_position);

                    getRebateDetails = $.ajax({
                        url: TDPWeb.ExtraNet.resolveURL('/Rebates/GetRebateDetails'),
                        type: 'POST',
                        data: { rebateNumber: rebateNumber }
                    }).done(function (html) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                        showRebateDetailsModal(html, true);
                    }).fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($spinner_position);
                    });
                });

                $(".modal").on("hide.bs.modal", function (e) {
                    if (TDPWeb.ExtraNet.discardFormChanges() === false) {
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        return false;
                    } else if (getRebateDetails != null) {
                        getRebateDetails.abort();
                    }

                });

                $body.delegate(".rebate-items-filter", "click", function (e) {
                    filterPanels(e);
                });

                $body.delegate(".collapsibles .panel-content", "show.bs.collapse", function () {
                    $this = $(this);
                    var rebateNumber = $("input[name='Rebate_Number']").val(),
                        contractNumber = $this.attr("id").split(/[- ]+/).pop();

                    if ($this.has("table").length == 0) {

                        TDPWeb.ExtraNet.spinnerOn($this);

                        $.ajax({
                            url: TDPWeb.ExtraNet.resolveURL('/Rebates/GetContractRebateDetails'),
                            type: 'GET',
                            data: { rebateNumber: rebateNumber, contractNumber: contractNumber }
                        }).done(function (html) {
                            $this.html(html);

                            $select = $this.find("select").not(".excludeFromSel2Init");
                            $.each($select, function () {
                                $this.select2({
                                    minimumResultsForSearch: -1,
                                    placeholder: $this.data("placeholder"),
                                    theme: 'bootstrap'
                                });
                            });

                            TDPWeb.ExtraNet.spinnerOff($this);
                        }).fail(function (error) {
                            TDPWeb.ExtraNet.spinnerOff($this);
                        });
                    }

                });

                $("#contracts-list").on('change keyup', 'input, select, textarea', function () {
                    $(this).closest('.panel').addClass("modified");
                });

                $body.delegate("#addItemsToContractsBtn", "click", function () {
                    var $container = $("#addItemsToContractsContainer");
                    if ($container.hasClass("in")) { return; }

                    // if no visible rows
                    if ($container.find("#additionalItemsTable tbody tr").length <= 1) {
                        addAdditionalItemRows();
                    }
                    $container.slideDown();
                });

                $body.delegate("#addItemsToContractsClose", "click", function (evt) {
                    evt.preventDefault();

                    var $container = $("#addItemsToContractsContainer");
                    if ($container.is(":hidden")) { return; }

                    //TDPWeb.ExtraNet.resetFormValidations($("#rebate-details"));
                    $container.slideUp(200);
                });

                $body.delegate("#addMoreItems", "click", function () {
                    addAdditionalItemRows();
                });

                $body.delegate("#submitAdditionalItems", "click", function () {
                    // todo: add additional items to contract(s)
                });

                $body.delegate("#quickSaveRebate", "click", function () {
                    saveRebateDetailsModal($("#rebate-details"), false);
                });

                $body.delegate("#submitRebate", "click", function () {
                    saveRebateDetailsModal($("#rebate-details"), true);
                });

                $body.delegate("input.key-event", "keydown", function (e) {
                    if (e.keyCode == 40) {
                        var $nextRow = $(this).parent().parent().next();

                        if ($nextRow.is("tr")) {
                            $nextRow.find("input[type='text']:first").focus();
                        }
                    } else if (e.keyCode == 13) {
                        saveRebateDetailsModal($("#rebate-details"), false);
                    }
                });

                $body.on("focus", "input.lookSelected", function () {
                    var saveThis = $(this);
                    setTimeout(function () { saveThis.select(); }, 0);
                });

                var $pagination = $("#search-rebates-pagination"),
                    $resultsContainer = $(".search-result"),
                    $contractsListPagination = $("#rebate-contracts-list-pagination"),
                    $contractsListContainer = $(".rebate-details-content");

                RebatesController.pagination = new Pagination();
                RebatesController.pagination.initialize($pagination, $resultsContainer, "tbody tr", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"));

                RebatesController.contractsListPagination = new Pagination();
                RebatesController.contractsListPagination.initialize($contractsListPagination, $contractsListContainer, ".panel", $contractsListPagination.data("items-per-page"), $contractsListPagination.data("previous-text"), $contractsListPagination.data("next-text"));
            }
        }
    })();

    $(function () {
        RebatesController.attachDomEvents();
    });
})(jQuery);
