(function ($) {

    var resetPasswordController = (function () {

        function showTermsModal() {
            var $modal = $("#terms-modal");
            $modal.modal();
        }
        
        function resetPassword($form) {

            var verifypassword = $("#verifypassword-field"),
                newpassword = $("#newpassword-field"),
                $updateButton = $("#update-button");

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($updateButton);

                var hash = $form.find("#hash");

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/ResetPassword/' + hash.val()),
                    type: 'POST',
                    data: $form.serialize()

                }).done(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($updateButton);
                    $("#reset-password-fields").remove();
                    $updateButton.remove();
                    TDPWeb.ExtraNet.resetFormValidations($form);
                    TDPWeb.ExtraNet.cleanForm($form);
                    TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formSuccess") });

                }).fail(function (data) {
                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find(".backEndErrors"));
                    }

                }).always(function (data) {
                    TDPWeb.ExtraNet.spinnerOff($updateButton);
                });
            }
        }

        return {
            attachDomEvents: function () {
                var $body = $("body");

                $body.delegate("#update-button", "click", function () {
                    resetPassword($(this).parents("form").first());
                });

                $body.delegate("#open-terms-link", "click", function (e) {
                    showTermsModal();
                    return false;
                });
            }
        }
    })();

    $(function () {
        resetPasswordController.attachDomEvents();
    });

})(jQuery);
