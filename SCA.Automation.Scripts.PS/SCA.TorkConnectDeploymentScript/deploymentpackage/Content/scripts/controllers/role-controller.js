(function ($) {

    var controller = (function () {

        var sortOrder = "name",
            currentPage = 1,
            hasSearched = false;

        function getControllerPath(endPoint) {
            return "/" + (endPoint.indexOf("External") > -1 ? "ExternalRoles" : "InternalRoles") + "/";
        }

        function searchRoles(usingSearchInput, query, done, clearSearch) {
            var $searchField = $('#search-field'),
                $searchResults = $(".search-result"),
                $spinnerLocation = $('#roles-table'),
                useMiniSpinner = false;

            if (query === undefined) {
                query = $searchField[0].value;
            }

            if (usingSearchInput) {
                $spinnerLocation = $searchField;
                useMiniSpinner = true;
            }

            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, useMiniSpinner);

            var postParams = "searchTerm=" + query + "&sortOrder=" + sortOrder + "&currentPage=" + currentPage,
                end_point = $searchResults.data("end-point");

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL(getControllerPath(end_point) + "Search"),
                data: postParams
            }).done(function (result) {
                hasSearched = !clearSearch;
                $searchResults.html(result);
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);

                if (done !== undefined) {
                    done();
                }
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);

                if (done !== undefined) {
                    done();
                }
            });

            return false;
        }

        function sortByProperty(ascending, descending) {
            if (sortOrder != ascending) {
                sortOrder = ascending;
            }
            else {
                sortOrder = descending;
            }

            searchRoles();
        }

        function sortByName() {
            sortByProperty("name", "name_desc");
        }

        function sortByUsers() {
            sortByProperty("users", "users_desc");
        }

        function sortByDate() {
            sortByProperty("date", "date_desc");
        }

        function goToPage(n, query) {
            if (n == 0 || n > totalPages()) return;

            currentPage = n;
            searchRoles(false, query);
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function showEditRoleModal(body_html, animated) {
            $modal = $("#edit-role-modal");

            $modal.find(".alert").hide();

            $modal.find(".modal-body").html(body_html);

            TDPWeb.ExtraNet.cleanForm($modal.find('form'));

            if (!animated) {
                $modal.removeClass("fade");
            }

            $modal.modal().addClass("fade");

            return $modal;
        }

        function openEditRoleModal(id, animated) {
            var $manageButtons = $(".save-role-btn, .delete-role-btn"),
                $edit_modal = showEditRoleModal(TDPWeb.ExtraNet.emptySpinnerContainer, animated),
                $spinner_position = $("#empty-spinner-container"),
                end_point = $edit_modal.data("end-point");

            $manageButtons.hide(0);
            TDPWeb.ExtraNet.spinnerOn($spinner_position);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL(getControllerPath(end_point) + "EditModalBody"),
                type: 'GET',
                data: { id: id }
            }).done(function (html) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
                showEditRoleModal(html, false);

                if (!$(".input-role-name").is(':disabled')) {
                    $manageButtons.show(0);
                }
            }).fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($spinner_position);
            });
        }

        function addRole($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("form"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var roleName = $modal.find(".input-role-name").val(),
                    $button = $modal.find(".add-role-btn"),
                    end_point = $modal.data("end-point");

                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL(getControllerPath(end_point) + "Create"),
                    type: 'POST',
                    data: { name: roleName }
                }).done(function (data) {
                    if (data.id !== undefined) {
                        searchRoles();
                        TDPWeb.ExtraNet.spinnerOff($button);
                        $modal.removeClass("fade").modal("hide").addClass("fade");

                        openEditRoleModal(data.id, false);
                    }
                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function saveRole($modal) {
            if (!TDPWeb.ExtraNet.validateInput($modal.find("form"))) {
                TDPWeb.ExtraNet.displayMessage({ element: $modal.find(".formError") });
            } else {
                TDPWeb.ExtraNet.resetFormValidations($modal);
                var $button = $modal.find(".save-role-btn"),
                    end_point = $modal.data("save-end-point");
                TDPWeb.ExtraNet.spinnerOn($button);

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL(getControllerPath(end_point) + "Update"),
                    type: 'PUT',
                    data: $modal.find("form").serialize()
                }).done(function (data) {
                    searchRoles();

                    TDPWeb.ExtraNet.spinnerOff($button);

                    TDPWeb.ExtraNet.successMessageModal($modal);
                }).fail(function (data) {

                    TDPWeb.ExtraNet.spinnerOff($button);

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                    }
                });
            }
        }

        function deleteRole($modal) {
            var $button = $modal.find(".confirm-delete-role-btn"),
                $saveButton = $modal.find(".save-role-btn"),
                $deleteButton = $modal.find(".delete-role-btn"),
                end_point = $modal.data("delete-end-point");

            TDPWeb.ExtraNet.spinnerOn($button);
            $saveButton.prop("disabled", true);
            $deleteButton.prop("disabled", true);

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL(getControllerPath(end_point) + "Delete"),
                type: 'DELETE',
                data: { id: $modal.find("form input[name=id]").val() }
            }).done(function (data) {
                searchRoles(false, undefined, function () {
                    $saveButton.prop("disabled", false);
                    $deleteButton.prop("disabled", false);
                    TDPWeb.ExtraNet.spinnerOff($button);

                    $modal.modal("hide");
                });
            }).fail(function (data) {

                TDPWeb.ExtraNet.spinnerOff($button);
                $saveButton.prop("disabled", false);
                $deleteButton.prop("disabled", false);

                $deleteButton.show();
                $button.hide();

                if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty("errors")) {
                    TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $modal.find("form"), $modal.find(".backEndErrors"));
                }
            });
        }

        function deSelectSubTools(element) {
            if (!$(element).is(":checked")) {
                var $subTools = $(element).closest(".panel").find(".sub-tool");
                $subTools.prop("checked", false);
            }
        }

        function selectParentTool(element) {
            if ($(element).is(":checked")) {
                var $parentTool = $(element).closest(".panel").find(".parent-tool");
                $parentTool.prop("checked", true);
            }
        }

        return {
            focusOnSearchField: function () {
                $("#search-field").focus();
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    $searchResult = $(".search-result");

                $searchResult.show();

                $("#role-search-form").submit(function (e) {
                    e.preventDefault();
                    currentPage = 1;

                    $(e.target).parents("#search-input") ? searchRoles(true) : searchRoles();
                });

                $(".clear-search-field").on("click", function () {
                    var $searchField = $("#search-field");
                    if ($searchField.val().length != 0) {
                        $searchField.val("");
                        if (hasSearched) {
                            currentPage = 1;
                            $searchResult.has("table").length != 0 ? searchRoles(false, "", undefined, true) : searchRoles(true, "", undefined, true);
                        }
                    }
                });

                $body.delegate(".table-head .name", "click", function () {
                    sortByName();
                });

                $body.delegate(".table-head .users", "click", function () {
                    sortByUsers();
                });

                $body.delegate(".table-head .last-updated", "click", function () {
                    sortByDate();
                });

                $body.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                    goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
                });

                $searchResult.delegate("tr.role", "click", function () {
                    openEditRoleModal($(this).data("role-id"), true);
                });

                $body.delegate(".modal .save-role-btn", "click", function () {
                    saveRole(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal #show-details-btn", "click", function (e) {
                    e.preventDefault();
                    $(".subtool-container").slideDown();
                    $(this).hide();
                    $("#hide-details-btn").show();
                });

                $body.delegate(".modal #hide-details-btn", "click", function (e) {
                    e.preventDefault();
                    $(".subtool-container").slideUp();
                    $(this).hide();
                    $("#show-details-btn").show();
                });

                $body.delegate("#add-role-btn", "click", function () {
                    var $modal = $("#new-role");
                    TDPWeb.ExtraNet.resetForm($modal.find("form"));
                    TDPWeb.ExtraNet.resetFormValidations($modal);
                    $modal.modal();
                });

                $body.delegate(".modal .add-role-btn", "click", function () {
                    addRole(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal .confirm-delete-role-btn", "click", function () {
                    deleteRole(TDPWeb.ExtraNet.getModalInstance(this));
                });

                $body.delegate(".modal .parent-tool", "change", function () {
                    deSelectSubTools(this);
                });

                $body.delegate(".modal .sub-tool", "change", function () {
                    selectParentTool(this);
                });
            }
        }
    })();

    $(function () {
        controller.focusOnSearchField();
        controller.attachDomEvents();
    });

})(jQuery);
