(function ($) {
    var $wizard = $('#sample-and-spare-parts-order-wizard');

    var ProductListController = TDPWeb.ExtraNet.getProductListController();

    var SampleAndSparePartsOrderWizardValidator = (function () {
        function clearAlertMessages() {
            $wizard.find('#alert-container .alert').slideUp();
        }

        function requireField($field, isRequired) {
            $field.prop('required', isRequired);

            var $requiredIndicator = $('label[for=' + $field.attr("id") + '] .required-indicator');
            if (isRequired) {
                $requiredIndicator.show();
            }
            else {
                $requiredIndicator.hide();
                $field.removeClass('error');
            }
        }

        function validateRequiredFields($form) {
            var $requiredFields = $form.find('input, select').not(':disabled'),
                valid = true;

            $requiredFields.each(function (index, element) {
                var $field = $(element);

                $field.removeClass('error');
                if ($field.is('[required]') && $field.val().trim().length === 0) {
                    $field.addClass('error');
                    valid = false;
                }
            });

            if (!valid) {
                TDPWeb.ExtraNet.displayMessage({ element: $('#alert-required-fields') });
            }

            var endCustomerStreetNumber = $('#end-customer-street-number');
            var endCustomerPostalCode = $('#end-customer-postal-code');
            var pattern = new RegExp("^[a-zA-Z0-9 ]+$");

            if (valid && (!$('#end-customer-data-not-available').prop('checked'))) {
                if (!pattern.test(endCustomerStreetNumber.val())) {
                    TDPWeb.ExtraNet.displayMessage({ element: $('#allowed-characters-letters-digits-spaces') });
                    valid = false;
                    endCustomerStreetNumber.addClass('error');
                }

                if (!pattern.test(endCustomerPostalCode.val())) {
                    TDPWeb.ExtraNet.displayMessage({ element: $('#allowed-characters-letters-digits-spaces') });
                    valid = false;
                    endCustomerPostalCode.addClass('error');
                }
            }

            return valid;
        }

        function validateProductSelection() {
            var valid = SampleAndSparePartsOrderWizardController.hasSelectedProducts();
            if (!valid) {
                TDPWeb.ExtraNet.displayMessage({ element: $('#alert-sample-or-spare-part-selection-required') });
            }

            return valid;
        }

        function validateWizardStep(step) {
            var $form = $wizard.find('.wizard-pane[data-index="' + step + '"]'),
                valid = true;

            clearAlertMessages();
            switch (step) {
                case 1:
                    valid = validateRequiredFields($form);
                    break;
                case 2:
                    valid = validateProductSelection();
                    break;
                case 3:
                    valid = validateRequiredFields($form);
            }

            return valid;
        }

        return {
            clearAlertMessages: clearAlertMessages,
            requireField: requireField,
            validateWizardStep: validateWizardStep,
            validateRequiredFields: validateRequiredFields
        }
    })();

    var SampleAndSparePartsOrderWizardController = (function () {
        var selectedProducts = { samples: [], spareparts: [] };
        var deliveryAddresses = [];

        if (chosenSampleParts) {
            if (chosenSampleParts.length > 0) {
                chosenSampleParts.forEach(function (e, i) {
                    addSelectedProduct('samples', e.ArticleNumber, e.ArticleName, e.UnitOfMeasure, e.Quantity);
                });
            }
        }

        if (chosenSpareParts) {
            if (chosenSpareParts.length > 0) {
                $('#select-spare-parts .badge').text(chosenSpareParts.length);

                chosenSpareParts.forEach(function (e, i) {
                    addSelectedProduct('spareparts', e.ArticleNumber, e.ArticleName, e.UnitOfMeasure, e.Quantity);
                });
            }
        }

        function hasSelectedProducts() {
            return ((selectedProducts.samples.length + selectedProducts.spareparts.length) > 0);
        }

        function getSelectedProducts(productType) {
            return selectedProducts[productType];
        }

        function addSelectedProduct(productType, articleNumber, articleName, uniteOfMeasure, quantity) {
            return selectedProducts[productType].push({ articleNumber: articleNumber, articleName: articleName, uniteOfMeasure: uniteOfMeasure, quantity: quantity });
        }

        function removeSelectedProduct(productType, articleNumber) {
            $.each(selectedProducts[productType], function (index, product) {
                if (product.articleNumber == articleNumber) {
                    selectedProducts[productType].splice(index, 1);
                    return false;
                }
            });
        }

        function reloadSelectSamplesAndSparePartsProductList(productType) {
            var $productList = $('#select-samples-and-spare-parts-product-list'),
                searchUrl = null,
                countUrl = null;

            if (productType) {
                $productList.data('product-type', productType);
            }
            else {
                productType = $productList.data('product-type');
            }

            if (productType == 'samples') {
                searchUrl = '/Product/SearchSamples';
                countUrl = '/Product/GetSamplesCount';
            }
            else if (productType == 'spareparts') {
                searchUrl = '/Product/SearchSpareParts';
                countUrl = '/Product/GetSparePartsCount';
            }

            $productList.fadeTo('fast', 0, function () {
                ProductListController.resetSearch(searchUrl, countUrl);

                ProductListController.resetProductList();
                $.each(getSelectedProducts(productType), function (index, product) {
                    ProductListController.addProductToList(product.articleName, product.articleNumber, true);
                });
                ProductListController.listChanged();

                $productList.fadeTo('fast', 1);
            });
        }

        function soldToAccountChanged(event) {
            var $deliveryAddressSelect = $wizard.find('#ship-to-delivery-address'),
                $spinnerTarget = $deliveryAddressSelect.data('select2').$selection,
                selectedSapIdentifier = $(event.target).val(),
                optionClassName = 'address';

            SampleAndSparePartsOrderWizardValidator.clearAlertMessages();

            // clear delivery address options
            $deliveryAddressSelect.prop('disabled', true);
            $deliveryAddressSelect.find('option.' + optionClassName).remove();

            if (selectedSapIdentifier) {
                TDPWeb.ExtraNet.spinnerOn($spinnerTarget, true);
                $.ajax({
                    type: 'POST',
                    data: { sapIdentifier: selectedSapIdentifier },
                    url: TDPWeb.ExtraNet.resolveURL("/SampleAndSparePartsOrdering/GetDeliveryAddresses")
                }).done(function (result) {
                    deliveryAddresses = result;
                    $.each(result, function (i, address) {
                        var option = new Option(address.FullAddress, address.SapIdentifier);
                        option.className = optionClassName;
                        $deliveryAddressSelect.append(option);
                    });
                }).fail(function (result) {
                    var $alert = $wizard.find('#alert-get-delivery-addresses-failed');
                    TDPWeb.ExtraNet.displayMessage({ element: $alert });
                    TDPWeb.ExtraNet.scrollUpToTarget($alert);
                }).always(function (result) {
                    TDPWeb.ExtraNet.spinnerOff($spinnerTarget);
                    $deliveryAddressSelect.prop('disabled', false);
                });
            }
        }

        function shipToDeliveryAddressChanged(event) {
            var $endCustomerDataNotAvailable = $('#end-customer-data-not-available'),
                selectedShipToDeliveryAddress = $(event.target).val(),
                isEndCustomerDataRequired = (selectedShipToDeliveryAddress == 'END_CUSTOMER');

            if (isEndCustomerDataRequired) {
                $endCustomerDataNotAvailable.prop('checked', false).change();
                $endCustomerDataNotAvailable.parent('div').slideUp();
            }
            else {
                $endCustomerDataNotAvailable.parent('div').slideDown();
            }
        }

        function endCustomerDataNotAvailableChanged(event) {
            var $endCustomerDataFields = $('#end-customer-data input'),
                endCustomerDataNotAvailableChecked = $(event.target).is(':checked');

            $endCustomerDataFields.prop('disabled', endCustomerDataNotAvailableChecked);
            if (endCustomerDataNotAvailableChecked) {
                $endCustomerDataFields.val(null);
            }

            $endCustomerDataFields.each(function (index, element) {
                SampleAndSparePartsOrderWizardValidator.requireField($(element), !endCustomerDataNotAvailableChecked);
            });
        }

        function selectSamplesAndSparePartsProductListChanged(event) {
            var $productList = $(event.delegateTarget),
                $productListItems = $productList.find('.productListItem'),
                productType = $productList.data('product-type');
            // update count
            $('#select-samples-and-spare-parts .nav-tabs a[data-product-type="' + productType + '"] .badge').text($productListItems.length);

            var currentSelectedArticleNumbers = getSelectedProducts(productType).map(function (product) { return product.articleNumber; });
            $productListItems.each(function (index, element) {
                var $product = $(element),
                    articleNumber = $product.find('.article-number').text(),
                    articleName = $product.find('.article-name').text(),
                    uniteOfMeasure = $product.find('.article-name').data('unitMeasure');

                var currentIndex = $.inArray(articleNumber, currentSelectedArticleNumbers);
                if (currentIndex >= 0) {
                    currentSelectedArticleNumbers.splice(currentIndex, 1);
                }
                else {
                    addSelectedProduct(productType, articleNumber, articleName, uniteOfMeasure, 1)
                }
            });

            $.each(currentSelectedArticleNumbers, function (index, articleNumber) {
                removeSelectedProduct(productType, articleNumber);
            });

            if ($wizard.data('editOrder') != '') {
                checkProductList();
            }
        }

        function checkProductList() {
            var selectedSamples = selectedProducts.samples;
            var selectedSpareparts = selectedProducts.spareparts;
            var diff = false;

            if (chosenSampleParts != null && chosenSpareParts != null) {
                if (selectedSamples.length > 0 && selectedSamples.length == chosenSampleParts.length) {
                    $(selectedSamples).each(function (index, element) {
                        var selectedNumber = element.articleNumber;
                        var quantity = element.quantity;

                        chosenSampleParts.filter(el => {
                            if (el.articleNumber == selectedNumber && el.quantity != quantity) {
                                diff = false;
                            }
                        });
                    });
                } else if (selectedSamples.length > 0 && selectedSamples.length != chosenSampleParts.length) {
                    diff = true;
                }

                if (selectedSpareparts.length > 0 && selectedSpareparts.length == chosenSpareParts.length) {
                    $(selectedSpareparts).each(function (index, element) {
                        var selectedNumber = element.articleNumber;
                        var quantity = element.quantity;

                        chosenSpareParts.filter(el => {
                            if (el.articleNumber == selectedNumber && el.quantity != quantity) {
                                diff = false;
                            }
                        });
                    });
                } else if (selectedSpareparts.length > 0 && selectedSpareparts.length != chosenSpareParts.length) {
                    diff = true;
                }
            }

            if (!diff) {
                ProductListController.setUnsavedChangesFalse();
            }
        }

        function selectSamplesAndSparePartsTabClicked(event) {
            var productType = $(event.currentTarget).data('product-type');
            reloadSelectSamplesAndSparePartsProductList(productType);
        }

        function selectSamplesAndSparePartsAddProductToQuantityList(articleName, articleNum, productType, unitOfMeasure, quantity) {
            var $cloneRowInstance = $('.addQuantityProductListItemTemplate').clone();
            $cloneRowInstance.find('.article-name').text(articleName);
            $cloneRowInstance.find('.article-number').text(articleNum);
            $cloneRowInstance.find('.unit-of-measure').text(unitOfMeasure);

            $cloneRowInstance.addClass('quantityProductListItem');
            $cloneRowInstance.removeClass('addQuantityProductListItemTemplate');
            $cloneRowInstance.removeClass('hidden');
            $cloneRowInstance.data('productId', articleNum);
            $cloneRowInstance.data('productType', productType);
            $cloneRowInstance.find('.quantity').val(quantity);
            $cloneRowInstance.find('.quantity').prop('required', true);
            $wizard.find('#wizard-pane-add-quantities .' + productType).append($cloneRowInstance);

            var $cloneDataRowInstance = $('.displayDataProductListItemTemplate').clone();
            $cloneDataRowInstance.find('.article-name').text(articleName);
            $cloneDataRowInstance.find('.article-number').text(articleNum);
            $cloneDataRowInstance.find('.unit-of-measure').text(unitOfMeasure);
            $cloneDataRowInstance.find('.display-quantity').text(1);

            $cloneDataRowInstance.addClass('dataProductListItem');
            $cloneDataRowInstance.removeClass('displayDataProductListItemTemplate');
            $cloneDataRowInstance.removeClass('hidden');
            $cloneDataRowInstance.addClass('article-number-' + articleNum);
            $wizard.find('#wizard-pane-confirm-order .' + productType).append($cloneDataRowInstance);
        }

        function attachDomControls() {
            // init wizard and define validation function
            Wizard.initiateWizard(SampleAndSparePartsOrderWizardValidator.validateWizardStep);

            $wizard.on('click', '.wizard-item, #nextButton-top, #nextButton-bottom, #previousButton-top, #previousButton-bottom, .Arrange .btn-link', checkStep);

            // init select2 drop downs
            $(".main-content-container select").each(function () {
                $(this).select2({
                    theme: 'bootstrap',
                    width: '100%'
                });
            });
        }

        function samplesAndSparePartsQuantityChanged(e) {
            var sum = 0;

            ProductListController.setUnsavedChangesTrue();

            if (e) {
                var field = $(e.currentTarget);
                field.removeClass('error');

                if (field.is('[required]') && field.val().trim().length === 0) {
                    field.addClass('error');
                }
            }

            $wizard.find('#wizard-pane-add-quantities .quantityProductListItem .quantity').each(function (index, element) {
                var value = parseInt($(element).val())
                if (value <= 0) {
                    value = 1;
                    $(element).val(value);
                }
                var type = $(element).closest('.quantityProductListItem').data('productType');
                var productId = $(element).closest('.quantityProductListItem').data('productId');
                selectedProducts[type].filter(function (el, i) {
                    if (el.articleNumber == productId) {
                        selectedProducts[type][i]['quantity'] = value;
                    }
                });

                $('#wizard-pane-confirm-order .article-number-' + productId).find('.display-quantity').text(value);

                sum += value;
            });

            $wizard.find('#wizard-pane-add-quantities').find('.total').val(sum);
        }

        function samplesAndSparePartsQuantityReset(e) {
            e.preventDefault();
            $wizard.find('#wizard-pane-add-quantities .quantity').val(1);
            samplesAndSparePartsQuantityChanged();
        }

        function displaySampleAndSpareParts() {
            $wizard.find('.quantityProductListItem, .dataProductListItem').remove();
            $.each(selectedProducts, function (productType, products) {
                $.each(products, function (productIndex, product) {
                    selectSamplesAndSparePartsAddProductToQuantityList(product.articleName, product.articleNumber, productType, product.uniteOfMeasure, product.quantity);
                });
            })

            samplesAndSparePartsQuantityChanged();

            if (ProductListController.ProductListController != null && ProductListController.ProductListController.unsavedChanges == true) {
                ProductListController.setUnsavedChangesTrue();
            } else {
                ProductListController.setUnsavedChangesFalse();
            }
        }

        function checkStep(step) {
            var step = Wizard.getCurrentWizardTab();

            if (step == 3) {
                displaySampleAndSpareParts();
                $('#reset-quantity').removeClass('d-none');
            } else {
                $('#reset-quantity').addClass('d-none');
            }

            if (step == 4) {
                displayContactData()
                $('#confirm-button').removeClass('d-none');
                $('#nextButton-bottom').addClass('d-none');
            } else {
                $('#confirm-button').addClass('d-none');
                $('#nextButton-bottom').removeClass('d-none');
            }
        }

        function displayContactData() {
            var data = {};
            var paneData = $wizard.find('#wizard-pane-customer-data');
            var dataView = $wizard.find('#wizard-pane-confirm-order')
            var deliveryAddress = getDeliveryAddress(paneData.find('#ship-to-delivery-address').val())[0];

            if (deliveryAddress) {
                $('.delivery-data').show();
                dataView.find('#data-view-delivery-company').text(deliveryAddress.CompanyName);
                dataView.find('#data-view-delivery-street-name').text(deliveryAddress.StreetNumberAndName);
                dataView.find('#data-view-delivery-postal-code').text(deliveryAddress.PostalCode);
                dataView.find('#data-view-delivery-city').text(deliveryAddress.City);
                dataView.find('#data-view-delivery-state').text(deliveryAddress.StateProvinceCountry);
            } else {
                $('.delivery-data').hide();
            }

            dataView.find('#data-view-sold-to').text(paneData.find('#sold-to-account option:selected').text());
            dataView.find('#data-view-contact-name').text(paneData.find('#user').val());
            dataView.find('#data-view-customer-street-name').text(paneData.find('#end-customer-street-name').val());
            dataView.find('#data-view-customer-street-number').text(paneData.find('#end-customer-street-number').val());
            dataView.find('#data-view-customer-postal-code').text(paneData.find('#end-customer-postal-code').val());
            dataView.find('#data-view-customer-city').text(paneData.find('#end-customer-city').val());
            dataView.find('#data-view-customer-state').text(paneData.find('#end-customer-state-province-country').val());
            dataView.find('#data-view-company-name').text(paneData.find('#end-customer-name').val());
            dataView.find('#data-view-comment').text(paneData.find('#order-comment').val());

            if ($('#end-customer-data-not-available').prop('checked')) {
                $('.end-customer-data').hide();
            } else {
                $('.end-customer-data').show();
            }

            $('.products-wrapper').each(function () {
                if ($(this).find('.dataProductListItem').length < 1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        }

        function getDeliveryAddress(sapIdentifier) {
            return deliveryAddresses.filter(function (e, i) {
                if (e.SapIdentifier == sapIdentifier) {
                    return e;
                }
            });
        }

        function createOrder() {
            var data = {
                "Id": $("#sample-and-spare-parts-order-wizard").data("editOrder"),
                "SoldTo": $('#data-view-sold-to').text(),
                "ContactName": $('#data-view-contact-name').text(),
                "CompanyName": $('#data-view-delivery-company').text(),
                "StreetNameNumber": $('#data-view-delivery-street-name').text(),
                "PostalCode": $('#data-view-delivery-postal-code').text(),
                "City": $('#data-view-delivery-city').text(),
                "State": $('#data-view-delivery-state').text(),
                "Country": $('#data-view-delivery-country').text(),
                "EndCustomerDataCompanyName": $('#data-view-company-name').text(),
                "EndCustomerDataStreetName": $('#data-view-customer-street-name').text(),
                "EndCustomerDataStreetNumber": $('#data-view-customer-street-number').text(),
                "EndCustomerDataPostalCode": $('#data-view-customer-postal-code').text(),
                "EndCustomerDataCity": $('#data-view-customer-city').text(),
                "EndCustomerDataCountry": $('#data-view-customer-state').text(),
                "OrderComment": $('#data-view-comment').text(),
                "SampleParts": selectedProducts.samples,
                "SpareParts": selectedProducts.spareparts
            };

            $.ajax({
                type: 'POST',
                data: data,
                url: TDPWeb.ExtraNet.resolveURL("/SampleAndSparePartsOrdering/CreateOrder")
            }).always(function (result) {
                resetWizardFields();
                window.location = TDPWeb.ExtraNet.resolveURL("/SampleAndSparePartsOrdering");
            });
        }

        function saveDraftOrder($saveDraft) {
            if (!$saveDraft.hasClass('disabled')) {
                $saveDraft.addClass('disabled');
                $saveDraft.text($saveDraft.data('save-draft-in-progress'));

                var paneData = $wizard.find('#wizard-pane-customer-data');
                var data = {
                    "Id": $("#sample-and-spare-parts-order-wizard").data("editOrder"),
                    "SoldTo": paneData.find('#sold-to-account option:selected').text(),
                    "ContactName": paneData.find('#user').val(),
                    "DeliverTo": paneData.find('#ship-to-delivery-address option:selected').val(),
                    "EndCustomerDataNotAvailableCheckBox": $('#end-customer-data-not-available').is(":checked"),
                    "EndCustomerDataCompanyName": $('#end-customer-name').val(),
                    "EndCustomerDataStreetName": $('#end-customer-street-name').val(),
                    "EndCustomerDataStreetNumber": $('#end-customer-street-number').val(),
                    "EndCustomerDataPostalCode": $('#end-customer-postal-code').val(),
                    "EndCustomerDataCity": $('#end-customer-city').val(),
                    "EndCustomerDataCountry": $('#end-customer-state-province-country').val(),
                    "OrderComment": $('#order-comment').val(),

                    "SampleParts": selectedProducts.samples,
                    "SpareParts": selectedProducts.spareparts
                };

                $.ajax({
                    type: 'POST',
                    data: data,
                    url: TDPWeb.ExtraNet.resolveURL("/SampleAndSparePartsOrdering/SaveDraftOrder")
                }).done(function (data) {
                    $("#sample-and-spare-parts-order-wizard").data("editOrder", data.Id);
                    ProductListController.setUnsavedChangesFalse();
                    $saveDraft.text($saveDraft.data('save-draft-completed'));
                    setTimeout(function () {
                        $saveDraft.text($saveDraft.data('save-draft-text'));
                    }, 5000);
                }).fail(function () {
                    $saveDraft.text($saveDraft.data('save-draft-failed'));
                    setTimeout(function () {
                        $saveDraft.text($saveDraft.data('save-draft-text'));
                    }, 5000);
                }).always(function (result) {
                    TDPWeb.ExtraNet.cleanForm($('#sample-and-spare-parts-order-wizard'));
                    setTimeout(function () {
                        $saveDraft.removeClass('disabled');
                    }, 5000);
                });
            }
        }

        function resetWizardFields() {
            $wizard.find('.delete-list-btn').click()
        }

        function attachDomEvents() {
            // Step 1: Customer Data
            $wizard.find('#sold-to-account').change(soldToAccountChanged);
            $wizard.find('#ship-to-delivery-address').change(shipToDeliveryAddressChanged);
            $wizard.find('#end-customer-data-not-available').change(endCustomerDataNotAvailableChanged);
            $wizard.find('#end-customer-data-not-available').change();

            // Step 2: Select Samples and Spare Parts
            $wizard.find('#select-samples-and-spare-parts').on('click', '.nav-tabs li:not(.active) a', selectSamplesAndSparePartsTabClicked);
            $wizard.find('#select-samples-and-spare-parts-product-list').on('product-list-changed', selectSamplesAndSparePartsProductListChanged);

            // Step 3: Add Quantities
            $wizard.find('#wizard-pane-add-quantities').on('keyup change', '.quantity', samplesAndSparePartsQuantityChanged);
            $wizard.find('.Arrange ').on('click', '#reset-quantity', samplesAndSparePartsQuantityReset)

            // Step4: Data View
            $("#confirm-button").on("click", function (e) {
                e.preventDefault();
                // Check for IE9 Browser Version
                if (TDPWeb.ExtraNet.ie9BrowserCheck()) {
                    //Display IE9 message to User
                    $("#IE9BrowserVersion-modal").modal();
                    return;
                }

                $('#data-confirm-modal').modal('show');
            });

            $('#save-draft-btn').on('click', function (e) {
                e.preventDefault();
                saveDraftOrder($(this));
            });

            $('#export-user-btn').on('click', function () {
                createOrder();
            });
        }

        return {
            attachDomControls: attachDomControls,
            attachDomEvents: attachDomEvents,
            displayContactData: displayContactData,
            hasSelectedProducts: hasSelectedProducts,
            reloadSelectSamplesAndSparePartsProductList: reloadSelectSamplesAndSparePartsProductList
        }
    })();

    $(function () {
        SampleAndSparePartsOrderWizardController.attachDomControls();
        SampleAndSparePartsOrderWizardController.attachDomEvents();

        SampleAndSparePartsOrderWizardController.reloadSelectSamplesAndSparePartsProductList();
    });
})(jQuery);