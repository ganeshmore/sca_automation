(function ($) {
    var SampleAndSparePartsOrderingController = (function () {
        var $searchResults = $("#sample-and-spare-parts-orders #search-results"),
            $searchForm = $("#sample-and-spare-parts-orders #search-form"),
            currentSortOrder = $searchResults.find("#sort-order").val(),
            currentPage = 1;

        function clearAlertMessages() {
            $('#sample-and-spare-parts-orders #alert-container .alert').slideUp();
        }

        function searchOrders(sortOrder) {
            var accountId = $searchForm.find("#account").find(":selected").val(),
                createdBy = $searchForm.find("#created-by").val();

            clearAlertMessages();
            TDPWeb.ExtraNet.spinnerOn($searchResults);

            var params = { accountId: accountId, createdBy: createdBy, sortOrder: (sortOrder || currentSortOrder || ''), currentPage: currentPage };
            var url = "/SampleAndSparePartsOrdering/Search";

            return $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL(url),
                data: params
            }).done(function (result) {
                $searchResults.html(result);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            })
                .fail(function (error) {
                    var $alert = $('#alert-search-failed');
                    TDPWeb.ExtraNet.displayMessage({ element: $alert });
                    TDPWeb.ExtraNet.scrollUpToTarget($alert);
                }).always(function () {
                    TDPWeb.ExtraNet.spinnerOff($searchResults);
                });
        }

        function goToPage(n) {
            if (n == 0 || n > totalPages()) return;
            currentPage = n;
            searchOrders();
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function searchFormSubmitted(e) {
            e.preventDefault();
            currentPage = 1;
            searchOrders();
        }

        function deleteOrderConfirmedClicked(e) {
            var $modal = $('#confirm-delete-order-modal'),
                $buttonDismiss = $modal.find('[data-dismiss="modal"]'),
                $button = $(this),
                url = $button.data('href');

            $buttonDismiss.attr("disabled", true);
            TDPWeb.ExtraNet.spinnerOn($button);
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL(url),
                type: 'POST'
            }).fail(function (data) {
                var $alert = $('#alert-delete-failed');
                TDPWeb.ExtraNet.displayMessage({ element: $alert });
                TDPWeb.ExtraNet.scrollUpToTarget($alert);
            }).then(function (data) {
                return searchOrders();
            }).always(function () {
                $buttonDismiss.attr("disabled", false);
                TDPWeb.ExtraNet.spinnerOff($button);
                $modal.modal('hide');
            });
        }

        function displayOrderDetails(e) {
            e.preventDefault();
            var hash = $(e.target).data('hash'),
                $modal = $('#preview-modal'),
                url = '/SampleAndSparePartsOrdering/ViewSampleAndSparePartsAgreement?hash=' + hash;

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL(url), success: function (result) {
                    $(".modal-body").html(result);
                }
            });

            $modal.modal();
        }

        function attachDomControls() {
            $(".main-content-container select").each(function () {
                $(this).select2({
                    allowClear: true,
                    placeholder: $(this).data("placeholder"),
                    theme: 'bootstrap',
                    width: '100%'
                });
            });
        }

        function attachDomEvents() {
            $searchForm.on('submit', searchFormSubmitted);

            $searchResults.delegate(".Sort-header", "click", function () {
                var sortAsc = $(this).attr("data-sort-order-asc");
                var sortDesc = $(this).attr("data-sort-order-desc");

                currentSortOrder = (currentSortOrder == sortDesc ? sortAsc : sortDesc);

                searchOrders(currentSortOrder);
            });

            $searchResults.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
            });

            $('#confirm-delete-order-modal').on('show.bs.modal', function (e) {
                var $target = $(e.relatedTarget),
                    $modal = $(this),
                    $confirmButton = $modal.find('#delete-order-confirmed');

                $confirmButton.data('href', $target.data('href'));
            });

            $('#delete-order-confirmed').on('click', deleteOrderConfirmedClicked);
            $('tr.sample-and-spare-parts-order a').on('click', clearAlertMessages);
            $('#sample-and-spare-parts-ordering-module').on('click', '.order-preview', displayOrderDetails);
        }

        return {
            attachDomControls: attachDomControls,
            attachDomEvents: attachDomEvents
        }
    })();

    $(function () {
        SampleAndSparePartsOrderingController.attachDomControls();
        SampleAndSparePartsOrderingController.attachDomEvents();
    });
})(jQuery);