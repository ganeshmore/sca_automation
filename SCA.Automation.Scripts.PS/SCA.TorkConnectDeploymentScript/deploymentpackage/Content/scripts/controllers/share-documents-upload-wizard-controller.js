(function ($) {
    var ShareDocumentsUploadWizardController = (function () {
        var selectRole = null,
            selectRoleClone = null,
            addedAccountsAndRoles = [],
            deletedAccountsAndRoles = [],
            productListPagination;

        moment.locale($("#share-document-hidden-expiration-date").data("locale"));

        function checkAndAddToList() {
            var account = $("#share-document-upload-wizard-form #account").find(":selected"),
                role = $("#share-document-upload-wizard-form #role").find(":selected"),
                roleId = role.val(),
                roleId = !isNaN(parseFloat(roleId)) && isFinite(roleId) ? Number(roleId) : roleId;

            var accountAndRoleList = $.map($(".productListItem .article-number"), function (element) {
                var $element = $(element);
                return { AccountId: $element.data('account-id'), RoleId: $element.data('role-id') };
            });

            if (roleId == 0) {
                var addedRolesForAccount = accountAndRoleList.filter(function (accountAndRole) {
                    var accountId = account.val(),
                        accountId = !isNaN(parseFloat(accountId)) && isFinite(accountId) ? Number(accountId) : accountId;
                    return accountAndRole.AccountId == accountId;
                });

                if (addedRolesForAccount.length > 0) {
                    showAddAllRolesConfirmation();
                    return;
                }
            }

            addToList();
        }

        function addToList() {
            var account = $("#share-document-upload-wizard-form #account").find(":selected"),
                accountId = account.val(),
                accountId = !isNaN(parseFloat(accountId)) && isFinite(accountId) ? Number(accountId) : accountId,
                role = $("#share-document-upload-wizard-form #role").find(":selected"),
                roleId = role.val(),
                roleId = !isNaN(parseFloat(roleId)) && isFinite(roleId) ? Number(roleId) : roleId,
                accountAndRole = { AccountId: accountId, RoleId: roleId };

            var accountAndRoleList = $.map($(".productListItem .article-number"), function (element) {
                var $element = $(element);
                return { AccountId: $element.data('account-id'), RoleId: $element.data('role-id') };
            });

            if (_.findIndex(accountAndRoleList, accountAndRole) == -1) {
                var $cloneRowInstance = $(".productListItemTemplate").clone(true, true);

                $cloneRowInstance.find(".article-name").text(account.text());
                var roleCloneInstance = $cloneRowInstance.find(".article-number");
                roleCloneInstance.text(role.text());
                roleCloneInstance.data('account-id', accountAndRole.AccountId).data('role-id', accountAndRole.RoleId);

                $cloneRowInstance.addClass("productListItem");
                $cloneRowInstance.removeClass("productListItemTemplate");
                $cloneRowInstance.show();

                $cloneRowInstance.addClass("productListItemAdded").insertAfter("tbody .productListItemTemplate");

                setTimeout(function () {
                    $cloneRowInstance.removeClass("productListItemAdded");
                }, 500);

                if (_.remove(deletedAccountsAndRoles, accountAndRole).length == 0) {
                    addedAccountsAndRoles.push(accountAndRole);
                };

                var $accountAndRole = $(this).closest(".product-module");
                $accountAndRole.addClass("included-in-list");

                selectRole.val(null).trigger("change");
                $("#accountsAndRolesInformation").show();
            }
        }

        function removeAddedRolesForCurrentAccount() {
            var account = $("#share-document-upload-wizard-form #account").find(":selected").val(),
                account = !isNaN(parseFloat(account)) && isFinite(account) ? Number(account) : account;

            $(".productListItem .article-number").each(function (index, element) {
                var $element = $(element),
                    itemToRemove = $element.closest(".productListItem"),
                    accountAndRoleToRemove = { AccountId: $element.data('account-id'), RoleId: $element.data('role-id') };

                if (accountAndRoleToRemove.AccountId == account) {
                    if (_.remove(addedAccountsAndRoles, accountAndRoleToRemove).length == 0) {
                        deletedAccountsAndRoles.push(accountAndRoleToRemove);
                    };

                    itemToRemove.remove();
                }
            });

            addToList();
        }

        function showAddAllRolesConfirmation() {
            var $modal = $('#add-all-roles-confirmation');

            $modal.off('click', '#confirm-add-all-roles', removeAddedRolesForCurrentAccount);
            $modal.on('click', '#confirm-add-all-roles', removeAddedRolesForCurrentAccount);

            $modal.modal().addClass("fade");
        }

        function removeFromList() {
            var $accountAndRoleItem = $(this).closest(".productListItem"),
                $accountAndRoleElement = $accountAndRoleItem.find(".article-number"),
                accountAndRole = { AccountId: $accountAndRoleElement.data('account-id'), RoleId: $accountAndRoleElement.data('role-id') },
                selectAccountVal = $("#share-document-upload-wizard-form #account").find(":selected").val(),
                selectAccountVal = !isNaN(parseFloat(selectAccountVal)) && isFinite(selectAccountVal) ? Number(selectAccountVal) : selectAccountVal,
                selectRoleVal = $("#share-document-upload-wizard-form #role").find(":selected").val(),
                selectRoleVal = !isNaN(parseFloat(selectRoleVal)) && isFinite(selectRoleVal) ? Number(selectRoleVal) : selectRoleVal;

            $accountAndRoleItem.remove();

            if (_.remove(addedAccountsAndRoles, accountAndRole).length == 0) {
                deletedAccountsAndRoles.push(accountAndRole);
            };

            if (accountAndRole == selectAccountVal + '-' + selectRoleVal || (accountAndRole.AccountId == selectAccountVal && accountAndRole.RoleId == 0)) {
                $('#add-to-list').show();
                $('#added-to-list').hide();
                $('#all-added-to-list').hide();
            }
        }

        function toggleAddToListLink() {
            var account = $("#share-document-upload-wizard-form #account").find(":selected").val(),
                account = !isNaN(parseFloat(account)) && isFinite(account) ? Number(account) : account,
                role = $("#share-document-upload-wizard-form #role").find(":selected").val(),
                role = !isNaN(parseFloat(role)) && isFinite(role) ? Number(role) : role,
                accountAndRole = { AccountId: account, RoleId: role };

            var accountAndRoleList = $.map($(".productListItem .article-number"), function (element) {
                var $element = $(element);
                return { AccountId: $element.data('account-id'), RoleId: $element.data('role-id') };
            });

            if (_.findIndex(accountAndRoleList, accountAndRole) > -1) {
                $('#add-to-list').hide();
                $('#added-to-list').show();
                $('#all-added-to-list').hide();
            } else if (_.findIndex(accountAndRoleList, { AccountId: account, RoleId: 0 }) > -1) {
                $('#add-to-list').hide();
                $('#added-to-list').hide();
                $('#all-added-to-list').show();
            } else {
                $('#add-to-list').show();
                $('#added-to-list').hide();
                $('#all-added-to-list').hide();
            }
        }

        function showSaveWithoutSoldTosConfirmation() {
            var $modal = $('#save-without-sold-tos-confirmation');

            $modal.off('click', '#confirm-save-without-sold-tos', save);
            $modal.on('click', '#confirm-save-without-sold-tos', save);

            $modal.modal().addClass("fade");
        }

        function validateAndSave() {
            TDPWeb.ExtraNet.resetFormValidations($('#share-document-upload-wizard-form'));

            var accountAndRoleListItems = $(".productListItem .article-number");

            if ($('#shared-document-id').val() == 0 && accountAndRoleListItems.length == 0) {
                TDPWeb.ExtraNet.displayMessage({ element: "#share-document-upload-account-role-required-error" });
                return;
            } else if ($('#shared-document-id').val() != 0 && accountAndRoleListItems.length == 0) {
                showSaveWithoutSoldTosConfirmation();
                return;
            }

            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'sharing tool',
                'eventAction': 'internal',
                'eventLabel': 'save'
            });

            save();
        }

        function save() {
            var $spinnerLocation = $('#share-document-upload-file-name-placeholder'),
                $uploadFormSuccessMessage = $('#share-document-upload-success'),
                $uploadFormErrorMessage = $('#share-document-upload-error'),
                $uploadForm = $('#share-document-upload-wizard-form'),
                $uploadFormInputs = $uploadForm.find(':input'),
                uploadFormData = new FormData($uploadForm[0]);

            $uploadFormSuccessMessage.hide();
            $uploadFormErrorMessage.hide();
            $uploadFormInputs.prop('disabled', true);
            $uploadFormInputs.addClass('disabled');
            TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, 'right');

            uploadFormData = AppendAccountRoleArrayToFormData(uploadFormData, addedAccountsAndRoles, 'AddedAccountsAndRoles');
            uploadFormData = AppendAccountRoleArrayToFormData(uploadFormData, deletedAccountsAndRoles, 'DeletedAccountsAndRoles');

            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/SharedDocuments/UploadDocument'),
                type: 'POST',
                data: uploadFormData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (data) {
                $uploadFormSuccessMessage.show();
                setTimeout(function () {
                    window.location = TDPWeb.ExtraNet.resolveURL('/SharedDocuments/Manage');
                }, 5000);
            }).fail(function (data) {
                $uploadFormInputs.prop('disabled', false);
                $uploadFormInputs.removeClass('disabled');
                $uploadFormErrorMessage.show();
            }).always(function () {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
        }

        function AppendAccountRoleArrayToFormData(formData, accountAndRoleArray, name) {
            var appenedFormData = formData;

            accountAndRoleArray.forEach(function (accountAndRole, index) {
                appenedFormData.append(name + '[' + index + '][AccountId]', accountAndRole.AccountId);
                appenedFormData.append(name + '[' + index + '][RoleId]', accountAndRole.RoleId);
            });

            return appenedFormData;
        }

        function validateWizard(step) {
            TDPWeb.ExtraNet.resetFormValidations($('#share-document-upload-wizard-form')),
                $filenamePlaceHolder = $('#share-document-upload-file-name-placeholder .selected-file-name'),
                noFileSelected = $filenamePlaceHolder.data('default-text')

            switch (step) {
                case 1:
                    if ($filenamePlaceHolder.text().trim() == noFileSelected && !$("#share-document-upload-file-input")[0].files.length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#share-document-upload-file-empty-error" });
                        return false;
                    }

                    var description = $("#share-document-upload-description");
                    description.removeClass('error');
                    if (!description.val().trim().length) {
                        TDPWeb.ExtraNet.displayMessage({ element: "#share-document-upload-input-required-error" });
                        description.addClass('error');
                        return false;
                    }
                    break;
            }
            return true;
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function attachDomControls() {
            Wizard.initiateWizard(validateWizard);

            var $expirationDate = $("#share-document-hidden-expiration-date"),
                $displayExpirationDate = $("#share-document-upload-expiration-date");
            var expirationDateValue = $expirationDate.val();
            if (expirationDateValue && expirationDateValue.length > 0) {
                $displayExpirationDate.val(moment.utc(expirationDateValue).local().format("l"))
            }
            $displayExpirationDate.datepicker({ dateFormat: $displayExpirationDate.data("date-format") || window.shortDateFormat });

            $("#share-document-upload-wizard-form select").each(function () {
                $(this).select2({
                    placeholder: $(this).data("placeholder"),
                    theme: 'bootstrap',
                    width: '100%'
                });
            });

            selectRole = $("#share-document-upload-wizard-form #role");
            selectRoleClone = selectRole.clone();

            var $pagination = $("#product-list-pagination"),
                $resultsContainer = $("#application-product-list-body");

            productListPagination = new Pagination();
            productListPagination.initialize($pagination, $resultsContainer, ".productListItem", $pagination.data("items-per-page"));
            productListPagination.updatePagination();
        }

        function attachDomEvents() {
            $('#share-document-upload-file-input').on('change', function () {
                var fileName = $(this).val().split('\\').pop(),
                    $placeHolder = $('#share-document-upload-file-name-placeholder .selected-file-name'),
                    noFileSelected = $placeHolder.data('default-text');

                if (fileName && fileName.length) {
                    $placeHolder.text(fileName);
                } else {
                    $placeHolder.text(noFileSelected);
                }
            });

            $("#share-document-upload-expiration-date").on("change", function () {
                if (this.value) {
                    $("#share-document-hidden-expiration-date").val(moment(this.value, moment.localeData().longDateFormat("l")).add(1, 'days').subtract(1, 'seconds').toISOString());
                }
            });

            $("#share-document-upload-wizard-form #account").on("change", function () {
                var roleOptions = selectRoleClone.find("option"),
                    inUseRoleIds = $("#share-document-upload-wizard-form #account").find(":selected").data("roleids");

                selectRole.find("option").remove();
                selectRole.append(selectRoleClone.find("option[value='']").clone());
                selectRole.append(selectRoleClone.find("option[value='0']").clone());

                for (i = 0; i < inUseRoleIds.length; i++) {
                    var options = selectRoleClone.find("option[value='" + inUseRoleIds[i] + "']").clone();
                    if (options) selectRole.append(options);
                }

                selectRole.val(null).trigger("change");
                selectRole.prop("disabled", false);
            });

            $("#share-document-upload-wizard-form #role").on("change", function () {
                var $userList = $("#share-document-upload-wizard-form .user-list-container"),
                    accountId = $("#share-document-upload-wizard-form #account").find(":selected").val(),
                    accountId = !isNaN(parseFloat(accountId)) && isFinite(accountId) ? Number(accountId) : accountId,
                    roleId = $("#share-document-upload-wizard-form #role").find(":selected").val(),
                    roleId = !isNaN(parseFloat(roleId)) && isFinite(roleId) ? Number(roleId) : roleId,
                    url = "/SharedDocuments/Users";

                $userList.html("");

                if (this.selectedIndex > 0) {
                    $('#add-to-list').removeClass('disabled');
                } else {
                    $('#add-to-list').addClass('disabled');
                    return;
                }

                TDPWeb.ExtraNet.spinnerOn($userList);

                var params = "accountId=" + accountId + "&roleId=" + roleId;

                $.ajax({
                    type: "POST",
                    url: TDPWeb.ExtraNet.resolveURL(url),
                    data: params
                })
                    .done(function (result) {
                        $userList.html(result);
                        TDPWeb.ExtraNet.spinnerOff($userList);
                        TDPWeb.ExtraNet.scrollUpToTarget($userList);
                    })
                    .fail(function (error) {
                        TDPWeb.ExtraNet.spinnerOff($userList);
                    });

                toggleAddToListLink();

                return false;
            });

            $('#add-to-list').on('click', function (e) { e.preventDefault(); checkAndAddToList(); });
            $('.delete-list-btn').click(removeFromList);

            $('#share-document-upload-submit').on('click', function (e) {
                e.preventDefault();
                validateAndSave();
            });
        }

        return {
            attachDomControls: attachDomControls,
            attachDomEvents: attachDomEvents
        }
    })();

    $(function () {
        ShareDocumentsUploadWizardController.attachDomControls();
        ShareDocumentsUploadWizardController.attachDomEvents();
    });

})(jQuery);