(function ($) {

    var SharedDocumentsController = (function () {

        var currentSortOrder = $("#shared-documents #search-results #sort-order").val(),
            currentPage = 1;

        moment.locale($("#date-format").val());

        function adjustDatesFromUTCToLocal() {
            var dates = $("td[class=UploadDate],td[class=ExpirationDate]");
            dates.each(function (index, element) {
                var $element = $(element);
                var dateText = $element.data("upload-date") || $element.data("expiration-date");
                if (dateText && dateText.length > 0) {
                    $element.text(moment.utc(dateText).local().format("l")); //Formats date to local time and display format (Date Only Displayed).
                }
            });
        }
            
        function searchSharedDocuments(sortOrder) {
            var $searchResults = $("#shared-documents #search-results"),
                accountId = $("#shared-documents #search-form #account").find(":selected").val();

            TDPWeb.ExtraNet.spinnerOn($searchResults);

            var params = "accountId=" + accountId + "&sortOrder=" + (sortOrder || currentSortOrder) + "&currentPage=" + currentPage;

            var url = "/SharedDocuments/";
            if ($searchResults.parents("#shared-documents-manage").length == 1) {
                url += "SearchAll";
            }
            else {
                url += "SearchActive";
            }

            $.ajax({
                type: "POST",
                url: TDPWeb.ExtraNet.resolveURL(url),
                data: params
            })
            .done(function (result) {
                $searchResults.html(result);
                adjustDatesFromUTCToLocal();
                TDPWeb.ExtraNet.spinnerOff($searchResults);
                TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
            })
            .fail(function (error) {
                TDPWeb.ExtraNet.spinnerOff($searchResults);
            });

            return false;
        }

        function goToPage(n) {
            if (n == 0 || n > totalPages()) return;
            currentPage = n;
            searchSharedDocuments();
        }

        function totalPages() {
            return parseInt($('.tork-pagination ul li:last-child')[0].textContent);
        }

        function attachDomControls() {

            $('#shared-documents #search-results').on('click', '.shared-documents-download', downloadGATracking);

            $(".main-content-container select").each(function () {
                $(this).select2({
                    placeholder: $(this).data("placeholder"),
                    theme: 'bootstrap',
                    width: '100%'
                });
            });

            adjustDatesFromUTCToLocal();
        }

        function attachDomEvents() {
            var $searchResults = $("#shared-documents #search-results");

            $("#shared-documents #search-form #account").on("change", function () {
                currentPage = 1;
                searchSharedDocuments();
            });

            $searchResults.delegate(".Sort-header", "click", function () {
                var sortAsc = $(this).attr("data-sort-order-asc");
                var sortDesc = $(this).attr("data-sort-order-desc");

                currentSortOrder = (currentSortOrder == sortDesc ? sortAsc : sortDesc);

                searchSharedDocuments(currentSortOrder);
            });

            $searchResults.delegate(".tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next", "click", function () {
                goToPage($(this).data("page-number"), $(this).closest(".tork-pagination").data("query"));
            });
        }

        function downloadGATracking(e) {
            var account = $('#account option:selected').text();
            var action = $('#shared-documents-manage').length > 0 ? 'internal' : 'external';

            dataLayer.push({
                'event': 'gaEvent',
                'eventCategory': 'sharing tool',
                'eventAction': action,
                'eventLabel': 'download - ' + account
            });
        }

        return {
            attachDomControls: attachDomControls,
            attachDomEvents: attachDomEvents
        }
    })();

    $(function () {
        SharedDocumentsController.attachDomControls();
        SharedDocumentsController.attachDomEvents();
    });

})(jQuery);