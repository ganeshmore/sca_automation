(function ($) {

    var StockPricingController = (function () {
        var pagination,
            ajaxCall,
            SCAMaterialNumber, soldTo, validOn;

        // Print Entire Stock Pricing
        $("#PrintStockPriceDetails").click(function () {
            // Display all Stock Pricing
            if ($(".search-result .item.u-cursorPointer.hidden").length > 0) {

                // Show all the Stock price int the List
                $(".search-result .item.u-cursorPointer.hidden").addClass("needtoResetHidden");
                $(".search-result .item.u-cursorPointer.hidden").removeClass("hidden");

                // Print the Stock price
                $(".search-result").print({});

                // Hide the Stock price which are not require to show
                $(".search-result .item.needtoResetHidden").addClass("hidden");
                $(".search-result .item.needtoResetHidden").removeClass("needtoResetHidden");
            }
            else {
                $(".search-result").print({});
            }
            return false;
        });

        // Download the Stock Price Details
        $("#stock-price-download-btn").click(function (e) {
            e.stopPropagation();
            turnButtonsOff();
            getStockPriceExcel(function (data) {
                window.location = TDPWeb.ExtraNet.resolveURL(data.downloadurl) + "&" + "sapAccountNumber=" + data.sapAccountNumber;
                turnButtonsOn();
            }, function (error) {
                onExcelGenerationFailure();
            });
        });

        // Generate Stock Price Excel
        function getStockPriceExcel(done, fail) {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL("/Excel/GenerateStockPriceDetails"),
                type: "POST",
                data: { soldTo: soldTo, ValidOn: validOn, SCAMaterialNumber: SCAMaterialNumber }
            }).done(done).fail(fail);
        }

        function turnButtonsOff() {
            var $downloadButton = $("#stock-price-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'disable');
            TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });

            timeOut = setTimeout(function () {
                if ($("#getFileWait-msg").is(":visible")) {
                    TDPWeb.ExtraNet.displayMessage({ element: $("#getFileWait-msg") });
                }

            }, 15000);
        };

        function turnButtonsOn() {
            var $downloadButton = $("#stock-price-download-btn");

            TDPWeb.ExtraNet.btnState($downloadButton, 'enable');
            clearTimeout(timeOut);

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }

            if ($("#getFileWait-msg").is(":visible")) {
                $("#getFileWait-msg").slideUp();
            }
        };

        function onExcelGenerationFailure() {
            turnButtonsOn();
            TDPWeb.ExtraNet.displayMessage({ element: $("#getExcelError-msg") });
        };

        function showHidePrintDownload()
        {
            if ($(".stock.price.item.u-cursorPointer").length > 0) {
                // Display Print and Download
                $("#stockpricing-download-section").show();
            }
            else
            {
                // Hide Print and Download
                $("#stockpricing-download-section").hide();
            }
        }

        function loadSearchResult($button, $form, html)
        {
            TDPWeb.ExtraNet.resetFormValidations($form);
            TDPWeb.ExtraNet.spinnerOff($button);
            soldTo = $("#sold-to option:selected").val();
            validOn = $("#valid-date-on").val();
            SCAMaterialNumber = $("#sca-material-number").val();
            $(".search-result").html(html);
            showHidePrintDownload();
            TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th").first(), false, true);
            $(".search-result").find("[data-alert-type=search]").slideDown(200);
        }

        // Search for Stock Price based on inputs
        function searchStockPricing($form) {
            var $button = $form.find("[type='submit']");

            if (!TDPWeb.ExtraNet.validateInput($form)) {
                TDPWeb.ExtraNet.displayMessage({ element: $form.find(".formError") });
            }
            else {
                if (ajaxCall) {
                    ajaxCall.abort(); // if Latest Stock Pricing is still pending
                }

                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.btnState($button, 'disable');

                $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/StockPricing/SearchStockPricing'),
                    type: 'POST',
                    data: $form.serialize()
                }).done(function (html) {
                    loadSearchResult($button, $form, html);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.btnState($button, 'enable');
                });
            }
        }

        return {
            getLatestStockPricing: function () {
                var $resultsContainer = $(".search-result");

                TDPWeb.ExtraNet.spinnerOn($resultsContainer);

                ajaxCall = $.ajax({
                    url: TDPWeb.ExtraNet.resolveURL('/StockPricing/FetchLatestStockPrice'),
                    type: 'POST',
                    data: {}
                }).done(function (html) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                    soldTo = $("#sold-to option:selected").val();
                    validOn = $("#valid-date-on").val();
                    SCAMaterialNumber = $("#sca-material-number").val();
                    $resultsContainer.html(html);
                    showHidePrintDownload();
                    TDPWeb.ExtraNet.sortTableByColumn($(".search-result table thead th.delivery-date").first(), true, true);
                    $(".search-result").find("[data-alert-type=search]").slideDown(200);
                }).fail(function (error) {
                    TDPWeb.ExtraNet.spinnerOff($resultsContainer);
                });
            },

            attachDomEvents: function () {
                var $body = $("body"),
                    getStockPriceDetails;

                $.each($body.find(".main-content-container select"), function () {
                    $(this).select2({
                        placeholder: $(this).data("placeholder"),
                        theme: 'bootstrap'
                    });
                });

                $("#valid-date-on").datepicker();

                $body.delegate("#stock-price-form", "submit", function (evt) {
                    evt.preventDefault();
                    searchStockPricing($(this));
                });

                var $pagination = $("#search-stock-price-pagination"),
                    $resultsContainer = $(".search-result"),
                    $slim_pagination = $('[data-slim-pagination]');
                StockPricingController.pagination = new Pagination();
                StockPricingController.pagination.initialize($pagination, $resultsContainer, "tbody tr", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"), $slim_pagination);

                showHidePrintDownload();

                $body.delegate('[data-slim-pagination] [data-navigate]', 'click', function () {
                    var $clicked = $(this);
                    $pagination.pagination($clicked.data('navigate'));
                });
            }
        }
    })();

    $(function () {
        StockPricingController.attachDomEvents();
        StockPricingController.getLatestStockPricing();
    });
})(jQuery);
