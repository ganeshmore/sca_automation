
function TKADValidator($) {
    function displayValidationMessage(elementId) {
        TDPWeb.ExtraNet.displayMessage({
            element: elementId
        });
    };

    function validateRequiredInputs($form, messageElementId) {
        var valid = true;

        $form
            .find('input, select')
            .not('[disabled]')
            .each(function () {
                var $this = $(this);

                $this.removeClass('error');
                if ($this.is('[required]') &&
                    $this.val().trim().length === 0) {
                    $this.addClass('error');
                    valid = false;
                }
            });

        if (!valid) {
            displayValidationMessage(messageElementId);
        }

        return valid;
    }

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailAddress);
    }

    function validateEmailDomain($emailAddress, domainName, messageElementId) {
        var valid = true;

        if ($emailAddress.length > 0) {
            $emailAddress.removeClass('error');

            if (!($emailAddress.val().indexOf(domainName) > 0)) {
                $emailAddress.addClass('error');
                valid = false;
            }

            if (!valid) {
                displayValidationMessage(messageElementId);
            }
        }

        return valid;
    }

    function validateEmailFormat($emailAddress, messageElementId) {
        var valid = true;

        $emailAddress
            .find("input[type='email']")
            .not('[disabled]')
            .each(function () {
                var $this = $(this);

                $this.removeClass('error');
                if ($this.is('[required]') &&
                    !isValidEmailAddress($this.val())) {
                    $this.addClass('error');
                    valid = false;
                }
            });

        if (!valid) {
            displayValidationMessage(messageElementId);
        }

        return valid;
    }

    function compareEmailInputs($emailAddress, $emailAddressConfirmation, messageElementId, shouldMatch) {
        if (shouldMatch === undefined) {
            shouldMatch = true;
        }

        $emailAddress.removeClass('error');
        $emailAddressConfirmation.removeClass('error');

        if ((shouldMatch && $emailAddress.val() !== $emailAddressConfirmation.val()) ||
            (!shouldMatch && $emailAddress.val() === $emailAddressConfirmation.val())) {
            $emailAddress.addClass('error');
            $emailAddressConfirmation.addClass('error');

            displayValidationMessage(messageElementId);

            return false;
        }

        return true;
    }

    function validateAgreementStartAndEndDates($agreementStartDate, $agreementEndDate) {
        // Not checking for null as these are required fields and would have been validated earlier
        var startDate = $agreementStartDate.datepicker('getDate'),
            endDate = $agreementEndDate.datepicker('getDate');

        $agreementStartDate.removeClass('error');
        $agreementEndDate.removeClass('error');

        if (endDate <= startDate) {
            $agreementEndDate.addClass('error');
            displayValidationMessage('#agreement-end-date-invalid-alert-msg');
            return false;
        }

        if (self.localeIsNorthAmerica && startDate.getDate() !== 1) {
            $agreementStartDate.addClass('error');
            displayValidationMessage('#agreement-start-date-invalid-alert-msg');
            return false;
        }

        return true;
    }

    function validateIntegerInputs($inputs, messageElementId, valueGreaterThanZero) {
        var valid = true,
            numbers = /^-?[0-9]+$/;

        $inputs
            .each(function () {
                var $this = $(this);
                $this.removeClass('error');
                if (!numbers.test(this.value) || (valueGreaterThanZero && this.value <= 0)) {
                    $this.addClass('error');
                    valid = false;
                }
            });

        if (!valid) {
            displayValidationMessage(messageElementId);
        }

        return valid;
    }

    function validateServiceFee($inputs, messageElementId) {
        var valid = true;
        //Checks for digits,dot or comma
        var pat = new RegExp(/^[0-9]+([,.][0-9]+)?$/);
        $inputs
            .each(function () {
                var $this = $(this);
                if (this.value.length > 0 && pat.test(this.value)) {
                    floatValue = parseFloat(this.value);

                    $this.removeClass('error');
                    if (!$.isNumeric(floatValue) || floatValue < 0) {
                        $this.addClass('error');
                        valid = false;
                    }
                }
                else { $this.addClass('error'); valid = false; }
            });

        if (!valid) {
            displayValidationMessage(messageElementId);
        }

        return valid;

    }

    function validateProductList() {
        if (!$('#product-list-added-products .productListItem').length) {
            displayValidationMessage('#product-list-alert-msg');
            return false;
        }

        return true;
    }

    function validatePaperContractNumber() {
        var $paperContractNumber = $('#tkad-paper-contract-number'),
            messageElementId = '#paper-contract-number-invalid-alert-msg',
            alphanumeric = /^[a-zA-Z0-9]+$/;

        $paperContractNumber.removeClass('error');

        if (!alphanumeric.test($paperContractNumber.val())) {
            $paperContractNumber.addClass('error');
            displayValidationMessage(messageElementId);
            return false;
        }

        return true;
    }

    function validateEndCustomerDataAndAgreementDetails() {
        var $endCustomerForm = $('#end-customer'),
            $fscBscForm = $('#add-fsc-bsc'),
            $emailAddress = $endCustomerForm.find('#tkad-end-customer-email'),
            $emailAddressConfirmation = $endCustomerForm.find('#tkad-end-customer-email-confirmation'),
            $agreementStartDate = $endCustomerForm.find('#tkad-end-customer-agreement-start-date'),
            $agreementEndDate = $endCustomerForm.find('#tkad-end-customer-agreement-end-date'),
            $paperContract = $('#tkad-paper-contract'),
            $fscBscAdded = $fscBscForm.find('#tkad-fsc-bsc-expanded');

        // Check if all form inputs are valid. Short circuit on the first false.
        var valid = validateRequiredInputs($endCustomerForm, '#end-customer-required-alert-msg') &&
            validateEmailFormat($endCustomerForm, '#end-customer-email-format-alert-msg') &&
            compareEmailInputs($emailAddress, $emailAddressConfirmation, '#end-customer-email-match-alert-msg') &&
            validateAgreementStartAndEndDates($agreementStartDate, $agreementEndDate);

        if (valid && $paperContract.is(':checked')) {
            valid = validatePaperContractNumber();
        }

        if (valid && $fscBscAdded.val() === 'true') {
            var $fscEmailAddress = $fscBscForm.find('#tkad-fsc-email'),
                $fscEmailAddressConfirmation = $fscBscForm.find('#tkad-fsc-email-confirmation');

            valid = validateRequiredInputs($fscBscForm, '#end-customer-required-alert-msg') &&
                validateEmailFormat($fscBscForm, '#end-customer-email-format-alert-msg') &&
                compareEmailInputs($fscEmailAddress, $fscEmailAddressConfirmation, '#end-customer-email-match-alert-msg') &&
                compareEmailInputs($fscEmailAddress, $emailAddress, '#end-customer-fsc-email-match-alert-msg', false);
        }

        return valid;
    }

    function validateDistributorData() {
        var $distributorDataForm = $('#distributor-data'),
            $fscBscForm = $('#add-fsc-bsc'),
            $subjobberForm = $('#add-subjobber-ado'),
            $endCustomerEmailAddress = $('#tkad-end-customer-email'),
            $distributorEmailAddress = $distributorDataForm.find('#tkad-distributor-data-email'),
            $salesRepEmailAddress = $distributorDataForm.find('#tkad-distributor-data-sca-sales-rep-email'),
            $fscBscAdded = $fscBscForm.find('#tkad-fsc-bsc-expanded'),
            $subjobberAdded = $subjobberForm.find('#tkad-subjobber-ado-expanded');

        var valid = validateRequiredInputs($distributorDataForm, '#distributor-data-required-alert-msg') &&
            validateEmailFormat($distributorDataForm, '#distributor-data-email-format-alert-msg') &&
            compareEmailInputs($endCustomerEmailAddress, $distributorEmailAddress, '#end-customer-email-match-distributor-alert-msg', false) &&
            validateEmailDomain($salesRepEmailAddress, $salesRepEmailAddress.data("domain"), '#distributor-data-sales-rep-email-format-alert-msg');

        if (valid && $fscBscAdded.val() === 'true') {
            var $fscEmailAddress = $fscBscForm.find('#tkad-fsc-email');

            valid = compareEmailInputs($fscEmailAddress, $distributorEmailAddress, '#end-customer-email-match-distributor-alert-msg', false);
        }

        if (valid && $subjobberAdded.val() === 'true') {
            var $subjobberEmailAddress = $subjobberForm.find('#tkad-subjobber-email'),
                $subjobberEmailAddressConfirmation = $subjobberForm.find('#tkad-subjobber-email-confirmation');

            valid = validateRequiredInputs($subjobberForm, '#distributor-data-required-alert-msg') &&
                validateEmailFormat($subjobberForm, '#distributor-data-email-format-alert-msg') &&
                compareEmailInputs($subjobberEmailAddress, $subjobberEmailAddressConfirmation, '#subjobber-email-match-alert-msg') &&
                compareEmailInputs($subjobberEmailAddress, $distributorEmailAddress, '#distributor-data-subjobber-email-match-alert-msg', false) &&
                compareEmailInputs($endCustomerEmailAddress, $subjobberEmailAddress, '#end-customer-email-match-distributor-alert-msg', false);

            if (valid && $fscBscAdded.val() === 'true') {
                var $fscEmailAddress = $fscBscForm.find('#tkad-fsc-email');

                valid = compareEmailInputs($fscEmailAddress, $subjobberEmailAddress, '#end-customer-email-match-distributor-alert-msg', false);
            }
        }

        return valid;
    }

    function validateProducts() {

        var valid = validateProductList();

        return valid;
    }

    function validateQuantities() {
        var $quantityProductList = $('#quantity-product-list'),
            $quantityInputs = $('.quantityProductListItem .quantity-text-area'),
            $feeInputs = $('.quantityProductListItem .fee-text-area');

        var valid = validateRequiredInputs($quantityProductList, '#add-quantity-required-alert-msg') &&
            validateIntegerInputs($quantityInputs, '#interger-quantity-required-alert-msg', true) &&
            validateServiceFee($feeInputs, '#check-inputs-alert-msg')

        return valid;
    }

    // Returning an object with each step in the TKAD process
    // set as a property referencing its validation function
    return {
        1: validateEndCustomerDataAndAgreementDetails,
        2: validateDistributorData,
        3: validateProducts,
        4: validateQuantities
    };
}

function TKADController($jQuery, options) {
    'use strict';
    options = options || {};

    // Shared
    this.$ = $jQuery;
    this.localeIsNorthAmerica = options.localeIsNorthAmerica || false;
    this.$signatureControl = null;
    this.productListController = options.productListController || TDPWeb.ExtraNet.getProductListController();
    this.dateFormat = options.dateFormat || window.shortDateFormat;

    // Properties for index view
    this.currentPage = 1;
    this.sortOrder = 'latest_update_desc';
    this.oldFormData = null;

    // Properties for Wizard or Accept Page/Modal
    this.isWizard = typeof Wizard !== 'undefined';
    this.tkadValidator = new TKADValidator($jQuery);
    this.showSignatureControl = typeof options.showSignature == 'undefined' ? true : options.showSignature;
    this.currentStep = 1;
    this.agreementIsViewed = false;
    this.agreementIsSent = false;
    this.agreementIsSigned = false;
    this.savedAgreement = options.savedAgreement;

    // Loading Draft floag
    this.loadingDraft = false;
    this.dispenserssaved = false;
}

// Initialization Functions
////////////////////////////////////////////////

TKADController.prototype._ieShim = function () {
    var $ = this.$;

    $('#spanIE9Dummy').click(function (e) {
        e.preventDefault();
        //Display IE9 message to User
        $('#IE9BrowserVersion-modal').modal();
        return;
    });
};

TKADController.prototype._attachDomEvents = function () {
    'use strict';
    var self = this,
        $ = self.$,
        productListController = self.productListController,
        $body = $('body'),
        $noTouchSupport = $('.no-touch'),
        $touchSupport = $('.touch');

    // Index/Search Functions
    ////////////////////////////////////////////////

    function tkadListViewSearchForAgreements(useOldFormData) {
        var $form = $('#tkad-filter-form');
        var $button = $form.find("[type='submit']");

        TDPWeb.ExtraNet.resetFormValidations($form);
        TDPWeb.ExtraNet.spinnerOn($button, true, 'right');

        var data = $form.serialize();

        if (useOldFormData) {
            data = self.oldFormData;
        } else {
            self.currentPage = 1;
            self.oldFormData = data;
        }

        data += '&sortOrder=' + self.sortOrder + '&currentPage=' + self.currentPage;

        $.ajax({
            url: TDPWeb.ExtraNet.resolveURL('/TKAD/Search'),
            type: 'POST',
            data: data
        }).done(function (html) {
            TDPWeb.ExtraNet.resetFormValidations($form);

            $('#agreements-table').html(html);
        }).fail(function (error) {

        }).always(function () {
            TDPWeb.ExtraNet.spinnerOff($button);
        });
    }

    function tkadListViewDeleteDraftClicked(e) {
        e.preventDefault();
        var $button = $(this),
            agreementId = $button.data('agreement-id'),
            confirmMessage = $button.data('confirm-delete-message'),
            url = TDPWeb.ExtraNet.resolveURL('/TKAD/DeleteAgreement'),
            $form = $('#tkad-filter-form');

        if (!$button.hasClass('disabled')) {
            if (confirm(confirmMessage)) {
                $button.addClass('disabled');
                TDPWeb.ExtraNet.resetFormValidations($form);

                TDPWeb.ExtraNet.spinnerOn($button, true, 'right');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        id: agreementId
                    }
                }).done(function (data) {
                    tkadListViewSearchForAgreements();
                }).fail(function (data) {
                    $button.removeClass('disabled');
                    TDPWeb.ExtraNet.spinnerOff($button);
                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty('errors')) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find('.backEndErrors'));
                    }
                });
            }
        }
    }

    function tkadListViewResendPdfAgreementClicked(e) {
        e.preventDefault();

        var $button = $(this),
            agreementId = $button.data('agreement-id'),
            confirmMessage = $button.data('confirm-resend-message'),
            url = TDPWeb.ExtraNet.resolveURL('/PDF/ResendTKADAgreementForSigning'),
            $form = $('#tkad-filter-form');

        if (!$button.hasClass('disabled')) {

            if (confirm(confirmMessage)) {
                $button.addClass('disabled');
                TDPWeb.ExtraNet.resetFormValidations($form);
                TDPWeb.ExtraNet.spinnerOn($button, true, 'right');
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        id: agreementId
                    }
                }).fail(function (data) {

                    if (data.hasOwnProperty('responseJSON') && data.responseJSON.hasOwnProperty('errors')) {
                        TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $form, $form.find('.backEndErrors'));
                    }
                }).always(function () {
                    $button.removeClass('disabled');
                    TDPWeb.ExtraNet.spinnerOff($button);
                });
            }
        }
    }

    function tkadListViewSearchFormSubmitted(e) {
        e.preventDefault();
        tkadListViewSearchForAgreements();
    }

    function tkadlistViewAgreementsTableChangeSortOrder(ascending, descending) {
        if (self.sortOrder !== ascending) {
            self.sortOrder = ascending;
        } else {
            self.sortOrder = descending;
        }

        tkadListViewSearchForAgreements(true);
    }

    function tkadListViewAgreementsTableSortableHeaderClicked() {
        tkadlistViewAgreementsTableChangeSortOrder($(this).data('sort-property'), $(this).data('sort-property-desc'));
    }

    function tkadListViewAgreementsTableGoToPage(n) {
        var totalPages = parseInt($('#agreements-table .tork-pagination ul li:last-child')[0].textContent);
        if (n === 0 || n > totalPages) {
            return;
        }

        self.currentPage = n;
        tkadListViewSearchForAgreements(true);
    }

    function tkadListViewAgreementsTablePaginationClicked() {
        tkadListViewAgreementsTableGoToPage($(this).data('page-number'));
    }

    function reportDispensorContractToGA(event, dispenserCheckout, description) {
        dataLayer.push({
            'event': event,
            'dispenser checkout': dispenserCheckout,
            'checkout description': description
        });
    }

    function reportDetailToGA(step) {
        var toolType = 'dispenser tool';
        switch (step) {
            case 0:
                reportDispensorContractToGA(toolType, 'step 1', 'create-agreement');
                break;
            case 1:
                reportDispensorContractToGA(toolType, 'step 2', 'end-customer-data');
                break;
            case 2:
                reportDispensorContractToGA(toolType, 'step 3', 'distributor-data');
                break;
            case 3:
                reportDispensorContractToGA(toolType, 'step 4', 'select-dispensers');
                break;
            case 4:
                reportDispensorContractToGA(toolType, 'step 5', 'add-quantity');
                break;
            case 5:
                reportDispensorContractToGA(toolType, 'step 6', 'agreement-acceptance');
                break;
            case '6a':
                reportDispensorContractToGA(toolType, 'step 6b', 'sign right now');
                break;
            case '6b':
                reportDispensorContractToGA(toolType, 'step 6b', 'send for signing');
                break;
            case '7a':
                reportDispensorContractToGA(toolType, 'order-complete', 'direct');
                break;
            case '7b':
                reportDispensorContractToGA(toolType, 'order-complete', 'mail');
                break;
        }
    }
    // Wizard/Modal Functions
    ////////////////////////////////////////////////
    function tkadAgreementWizardStepValidation(step) {
        // TODO: optimize where this is loaded from
        var validationStep = self.tkadValidator[step];

        if (validationStep) {
            return validationStep();
        }

        return true;
    }

    function tkadAgreementWizardNextPreviousButtonClicked() {
        if (self.currentStep !== Wizard.getCurrentWizardTab()) {
            self.currentStep = Wizard.getCurrentWizardTab();
            //Send info to the google Analytic
            reportDetailToGA(self.currentStep);
        }
        // Ensure that any generated PDF (in a touch environment) gets cleared out if we go back in the form)
        var $generateAndDownloadPdfButtons = $('.generate-and-download-pdf-btn');
        $generateAndDownloadPdfButtons.data('generated-download-url', null);
        $generateAndDownloadPdfButtons.each(function () {
            var $e = $(this);
            $e.val($e.data('original-text'));
        });
    }

    function tkadAgreementWizardUpdateRequiredGroups(id) {
        var $inputs = $("[data-required-group='" + id + "']"),
            anyNonEmpty = false;

        $.each($inputs, function () {
            if ($(this).val().trim().length > 0) {
                anyNonEmpty = true;
                return false;
            }
        });

        $.each($inputs, function () {
            $(this).prop('required', !anyNonEmpty);
            $(this).parent().siblings().children('label').find('.mandatory').text(anyNonEmpty ? '' : '*');
        });
    }

    function tkadAgreementWizardInputsChanged() {
        $('#terms-and-conditions-checkbox').prop('checked', false);
        $('#terms-and-conditions-checkbox').prop('disabled', true);
        $('#submit-for-approval-btn').prop('disabled', true);
    }

    function tkadAgreementWizardRequiredGroupChanged() {
        tkadAgreementWizardUpdateRequiredGroups($(this).data('required-group'));
    }

    function tkadAgreementWizardUpdateQuantityAndFees() {
        var quantityTotal = 0,
            feeTotal = 0;

        var cultureCode = $('#tkad-culture-code').val();

        $('.quantityProductListItem').each(function () {
            var $i = $(this),
                rawFee = $i.find('.fee-text-area').val(),
                fee = typeof rawFee != 'undefined' ? parseFloat(rawFee.replace(/,/g, '.')) : '',
                rawQuantity = $i.find('.quantity-text-area').val(),
                quantity = parseFloat(rawQuantity, 10),
                lineTotal = fee * quantity,
                roundedLineTotal = Number(Math.round(lineTotal + 'e2') + 'e-2');

            if (self.localeIsNorthAmerica) {
                if (!isNaN(quantity)) {
                    // Update totals
                    quantityTotal += quantity;
                }
            }
            else {
                if (!isNaN(fee) && !isNaN(quantity) && !isNaN(lineTotal)) {
                    $i.find('.row-total-text-area').val(roundedLineTotal.toLocaleString(cultureCode, {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2,
                    }));

                    // Update totals
                    quantityTotal += quantity;
                    feeTotal += lineTotal;
                } else {
                    $i.find('.row-total-text-area').val('');
                }
            }
        });

        $('#quantityTotalTextArea').val(quantityTotal);

        if (!self.localeIsNorthAmerica) {
            $('#productTotalTextArea').val(Number(Math.round(feeTotal + 'e2') + 'e-2').toLocaleString(cultureCode, {
                maximumFractionDigits: 2,
                minimumFractionDigits: 2,
            }));
        }
    }

    function tkadAgreementWizardQuantityProductListItemKeyUp() {
        var $input = $(this),
            value = this.value,
            integerValue = parseInt(value, 10),
            numbers = /^-?[0-9]+$/;

        $input.removeClass('error');

        if (!numbers.test(value) || integerValue <= 0) {
            $input.addClass('error');
        }

        tkadAgreementWizardUpdateQuantityAndFees();
    }


    function tkadAgreementWizardFeeProductListItemKeyUp() {
        var $input = $(this),
            value = this.value,
            floatValue = parseFloat(value);

        $input.removeClass('error');

        if (!$.isNumeric(floatValue) || floatValue < 0) {
            $input.addClass('error');
        }

        tkadAgreementWizardUpdateQuantityAndFees();
    }

    function tkadAgreementWizardGetExistingProducts() {
        return $('#quantity-product-list-body .quantityProductListItem .article-number').map(function () {
            return $(this).text();
        }).get();
    }

    function tkadAgreementWizardAddProductToQuantityList(articleName, articleNum) {
        var $cloneRowInstance = $('.quantityProductListItemTemplate').clone();

        $cloneRowInstance.prop('id', 'quantity-' + articleNum);
        $cloneRowInstance.find('.article-name').text(articleName);
        $cloneRowInstance.find('.article-number').text(articleNum);

        $cloneRowInstance.find('.quantity-text-area').prop('required', true);
        $cloneRowInstance.find('.quantity-text-area').val(1);
        $cloneRowInstance.find('.fee-text-area').prop('required', true);
        $cloneRowInstance.find('.row-total-text-area').prop('required', true);
        $cloneRowInstance.find('.row-total-text-area').val(0);

        $cloneRowInstance.addClass('quantityProductListItem');
        $cloneRowInstance.removeClass('quantityProductListItemTemplate');
        $cloneRowInstance.removeClass('hidden');
        $cloneRowInstance.insertAfter('.quantityProductListItemTemplate');


        if (!self.localeIsNorthAmerica && !self.loadingDraft) {
            setTimeout(function () {
                var url = TDPWeb.ExtraNet.resolveURL('/TKADproducts/GetProductServiceFees'),
                    $feeInput = $cloneRowInstance.find('.fee-text-area');

                // If there is already a numeric value, then a draft previously loaded; do not get new fees
                if (!$.isNumeric($feeInput.val())) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            products: [articleNum]
                        }
                    }).done(function (data) {
                        var serviceFee = 0, currency = '';
                        if (data) {
                            currency = data.currency;
                            if (data.serviceFees && data.serviceFees.length > 0 && data.serviceFees[0].serviceFee != undefined) {
                                var cultureCode = $('#tkad-culture-code').val();
                                serviceFee = data.serviceFees[0].serviceFee.toString();
                            }
                            else {
                                serviceFee = '';
                            }
                        }

                        $feeInput.val(serviceFee);
                        $('#quantity-product-list-currency').text(currency);
                    }).fail(function () {
                        // If the service fails to get data, present an empty dialog,
                        $feeInput.val('');
                    }).always(function () {
                        tkadAgreementWizardUpdateQuantityAndFees();
                    });
                }
            }, 100);
        }
    }

    function tkadAgreementWizardProductListChanged(e, articles) {
        // Get Products Listed on Quantity Step
        var existingProducts = tkadAgreementWizardGetExistingProducts();

        // Check for Added Items
        $('.productListItem').each(function () {

            var $article = $(this);
            var articleNumber = $article.find('.article-number').text();
            var articleName = $article.find('.article-name').text();

            if (!($.inArray(articleNumber, existingProducts) > -1)) {
                tkadAgreementWizardAddProductToQuantityList(articleName, articleNumber);
                //Report Article Name and ID to GA 
                if (self.currentStep === 3) {
                    reportDispensorContractToGA('dispenser tool', 'add dispensers', articleName + ' ' + articleNumber);
                }
            }
        });

        // Check for Removed Items
        $('.quantityProductListItem').each(function () {
            var articleNumber = $(this).find('.article-number').text();

            if (!($.inArray(articleNumber, articles) > -1)) {
                $(this).remove();
            }
        });

        // Update Totals
        tkadAgreementWizardUpdateQuantityAndFees();
    }

    function tkadAgreementWizardSerializeFormData() {
        var form = $('#tkad-agreement-wizard :input:not(:checkbox, .hasDatepicker)').serializeObject();
        var $checkboxes = $('#tkad-agreement-wizard :input:checkbox');
        var $datePickers = $('#tkad-agreement-wizard .hasDatepicker');

        $datePickers.each(function () {
            var $datepicker = $(this),
                name = $datepicker.prop('name'),
                date = $datepicker.datepicker("getDate") != null ? $datepicker.datepicker("getDate").toISOString() : null;
            form[name] = date;
        });

        $checkboxes.each(function () {
            var $checkbox = $(this),
                name = $checkbox.prop('name'),
                checked = $checkbox.is(':checked');

            form[name] = checked;
        });

        // add additional items
        form.SelectDispensers = [];

        var $products = $('#quantity-product-list-body .quantityProductListItem');
        var articleCurrency = $('#quantity-product-list-currency').text();
        var cultureCode = $('#tkad-culture-code').val();
        $products.each(function () {
            var $product = $(this),
                articleNumber = $product.find('.article-number').text(),
                articleName = $product.find('.article-name').text(),
                articleFee = $product.find('.fee-text-area').val() || '0',
                //articleTotal = $product.find('.row-total-text-area').val() || '0',
                articleQuantity = $product.find('.quantity-text-area').val() || '0';

            if (articleName && articleNumber) {
                form.SelectDispensers.push({
                    SelectDispensersArticleNumber: articleNumber,
                    SelectDispensersArticleName: articleName,
                    SelectDispensersArticleQuantity: articleQuantity,
                    SelectDispensersFee: parseFloat(articleFee.replace(/,/g, '.'), 10).toLocaleString(cultureCode, {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2,
                    }),
                    SelectDispensersCurrency: articleCurrency
                });
            }
        });

        // Saved form
        form.SelectedDispenserList = $('#load-list-select').val();

        return form;
    }

    function tkadAgreementWizardSetFormAsClean() {
        productListController.setUnsavedChangesFalse();
        TDPWeb.ExtraNet.cleanForm($('#tkad-agreement-wizard'));
    }

    function tkadAgreementWizardSendPdfAgreementClicked(e) {
        e.preventDefault();

        var $button = $(this),
            $form = $('#accept-agreement-form');

        TDPWeb.ExtraNet.spinnerOn($button, true, 'right');

        var url = TDPWeb.ExtraNet.resolveURL('/PDF/SendTKADAgreementForSigning');

        var data = tkadAgreementWizardSerializeFormData();
        data.emailAddress = $('#agreementAcceptanceEmail').text();
        data.message = $('#tkad-agreement-acceptance-email-message').val();

        $.ajax({
            url: url,
            type: 'POST',
            data: data
        }).done(function () {
            reportDetailToGA('7b');
            TDPWeb.ExtraNet.reportEmailSentEvent();
            TDPWeb.ExtraNet.successMessageForm($form);
            tkadAgreementWizardSetFormAsClean();

            self.agreementIsSent = true;
            $('#submit-for-approval-btn').prop('disabled', true);
            $('#sign-agreement-now').removeAttr('target');
        }).fail(function (d) {
            if (d.hasOwnProperty('responseJSON') && d.responseJSON.hasOwnProperty('errors')) {
                TDPWeb.ExtraNet.displayBackendErrors(d.responseJSON.errors, $form, $form.find('.backEndErrors'));
            }
        }).always(function () {
            TDPWeb.ExtraNet.spinnerOff($button);
        });

        reportDetailToGA('6b');
    }

    function tkadAgreementWizardEndCustomerAgreementStartDateChanged(e) {
        var currentDate = $('#tkad-end-customer-agreement-start-date').datepicker('getDate');

        if (currentDate) {
            // NA can only be start of month; if it's not, push it to the start of next month
            if (self.localeIsNorthAmerica) {
                if (currentDate.getDate() !== 1) {
                    currentDate.setMonth(currentDate.getMonth() + 1);
                    currentDate.setDate(1);
                    $('#tkad-end-customer-agreement-start-date').datepicker('setDate', currentDate);
                }
            }
            else {
                $('#tkad-end-customer-agreement-start-date').datepicker('setDate', currentDate);
            }

            currentDate.setYear(currentDate.getFullYear() + 3);
            currentDate.setDate(currentDate.getDate() - 1);
            $('#tkad-end-customer-agreement-end-date').datepicker('setDate', currentDate);

            // We don't want to bubble this and risk a continuous loop
            e.preventDefault();

            $(this).blur();
        }

        return false;
    }

    function tkadAgreementWizardEndCustomerAgreementEndDateChanged(e) {
        var currentDate = $('#tkad-end-customer-agreement-end-date').datepicker('getDate');

        if (currentDate) {

            $('#tkad-end-customer-agreement-end-date').datepicker('setDate', currentDate);
        }

        // We don't want to bubble this and risk a continuous loop
        e.preventDefault();

        $(this).blur();

        return false;
    }




    function tkadAgreementWizardClearProductCount() {
        var $searchField = $('.product-list-headline #search-field');
        $searchField.attr('placeholder', '');
    }

    function tkadAgreementWizardDistributorDataSoldToChanged() {
        productListController.resetProductList();

        var $input = $('#search-result-form #search-field');
        $input.val('');

        TDPWeb.ExtraNet.spinnerOn($input, true, 'right');
        productListController.searchProducts('', 1, function () {
            TDPWeb.ExtraNet.spinnerOff($input);
        }, true, true);

        tkadAgreementWizardClearProductCount();
    }

    function tkadAgreementWizardSignRightNowCollapsed() {

        $('#sign-right-now-expander').removeClass('icon-minus');
        $('#sign-right-now-expander').addClass('icon-plus');

        if (!self.agreementIsSent) {
            if (!self.agreementIsSigned) {
                $('.send-pdf-agreement').prop('disabled', false);
            }
        }
    }

    function tkadAgreementWizardRegisterSignatureControl() {
        var $tkadAgreementSignature = $('#tkad-agreement-signature');

        // This is a shared controller, check if jSignature is even available on this page
        if ($.fn.jSignature && self.showSignatureControl) {
            // If there are any children, remove them:
            $('#tkad-agreement-signature').children().remove();

            // jSignature has to be applied a little later to allow all sizes to be fully adjusted
            setTimeout(function () {
                // Load jSignature, grabbing sizing from container
                $tkadAgreementSignature.jSignature({
                    width: $tkadAgreementSignature.width(),
                    height: $tkadAgreementSignature.outerHeight() + 'px'
                });
                $tkadAgreementSignature.jSignature('disable');
            }, 500);
        }
    }

    function tkadAgreementWizardSignRightNowExpanded() {

        $('#sign-right-now-expander').removeClass('icon-plus');
        $('#sign-right-now-expander').addClass('icon-minus');
        reportDetailToGA('6a');
        if (!self.agreementIsSent) {

            $('.send-pdf-agreement').prop('disabled', true);

            // A bit of a hack to get jSignature working as expected; we
            // have to remove and re-add it back:
            tkadAgreementWizardRegisterSignatureControl();
        }
    }

    function tkadAgreementWizardEndCustomerFirstNameChanged() {
        if (!$('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceContactFirstName').text(this.value);
        }
    }

    function tkadAgreementWizardEndCustomerLastNameChanged() {
        if (!$('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceContactLastName').text(this.value);
        }
    }

    function tkadAgreementWizardEndCustomerEmailChanged() {
        if (!$('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceEmail').text(this.value);
        }
    }

    function tkadAgreementWizardFscFirstNameChanged() {
        if ($('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceContactFirstName').text(this.value);
        }
    }

    function tkadAgreementWizardFscLastNameChanged() {
        if ($('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceContactLastName').text(this.value);
        }
    }

    function tkadAgreementWizardFscEmailChanged() {
        if ($('#tkad-fsc-contractor').is(':checked')) {
            $('#agreementAcceptanceEmail').text(this.value);
        }
    }

    function tkadAgreementWizardUpdateEndCustomerFscContractorInformation() {
        var $isContractor = $('#tkad-fsc-contractor');

        if ($isContractor.is(':checked')) {
            $('#agreementAcceptanceEmail').text($('#tkad-fsc-email').val());
            $('#agreementAcceptanceContactFirstName').text($('#tkad-fsc-first-name').val());
            $('#agreementAcceptanceContactLastName').text($('#tkad-fsc-last-name').val());
        } else {
            $('#agreementAcceptanceEmail').text($('#tkad-end-customer-email').val());
            $('#agreementAcceptanceContactFirstName').text($('#tkad-end-customer-first-name').val());
            $('#agreementAcceptanceContactLastName').text($('#tkad-end-customer-last-name').val());
        }
    }

    function tkadAgreementWizardFscContractorClicked() {
        $('#contractor-notification-modal').modal();
        tkadAgreementWizardUpdateEndCustomerFscContractorInformation();
    }

    function tkadAgreementWizardGetPdf(save, done, fail) {
        var data = tkadAgreementWizardSerializeFormData();
        data.save = save;
        $.ajax({
            method: 'POST',
            dataType: 'json',
            data: data,
            url: TDPWeb.ExtraNet.resolveURL('/PDF/GenerateTKADAgreement')
        }).done(done)
            .fail(fail);
    }

    function tkadAgreementWizardTurnButtonsOff($button) {
        $button.prop('disabled', true);

        var $waitMessage = '';
        var $waitMoreMessage;

        if ($button.is('#pdf-preview-btn')) {
            $waitMessage = $('#preview-getPDFWait-msg');
            $waitMoreMessage = $('#preview-getPDFWaitMore-msg');
        } else if ($button.is('.view-agreement-btn')) {
            $waitMessage = $('#view-getPDFWait-msg');
            $waitMoreMessage = $('#view-getPDFWaitMore-msg');
        }

        TDPWeb.ExtraNet.displayMessage({ element: $waitMessage });

        setTimeout(function () {
            if ($waitMessage.is(':visible')) {
                TDPWeb.ExtraNet.displayMessage({ element: $waitMoreMessage });
            }

        }, 15000);
    }

    function tkadAgreementWizardTurnButtonsOn($button) {

        $button.prop('disabled', false);

        var $waitMessage = '';
        var $waitMoreMessage = '';

        if ($button.is('#pdf-preview-btn')) {
            $waitMessage = $('#preview-getPDFWait-msg');
            $waitMoreMessage = $('#preview-getPDFWaitMore-msg');
        } else if ($button.is('.view-agreement-btn')) {
            $waitMessage = $('#view-getPDFWait-msg');
            $waitMoreMessage = $('#view-getPDFWaitMore-msg');
        }

        if ($waitMessage.is(':visible')) {
            $waitMessage.slideUp();
        }

        if ($waitMoreMessage.is(':visible')) {
            $waitMoreMessage.slideUp();
        }
    }

    function tkadAgreementWizardPdfGenerationFailed($button) {
        tkadAgreementWizardTurnButtonsOn($button);

        var $errorMessage = '';

        if ($button.is('#pdf-preview-btn')) {
            $errorMessage = $('#preview-getPDFError-msg');
        } else if ($button.is('.view-agreement-btn')) {
            $errorMessage = $('#view-getPDFError-msg');
        }

        TDPWeb.ExtraNet.displayMessage({ element: $errorMessage });
    }

    function tkadAgreementWizardShowPreviewModal($element, save) {
        var $modal = $('#pdf-preview-modal'),
            $object = $modal.find('object'),
            attribute = 'data';
        // IE need to use iframe instead of object
        if (TDPWeb.ExtraNet.isOnIE()) {
            $object = $modal.find('iframe');
            attribute = 'src';
        }
        tkadAgreementWizardTurnButtonsOff($element);

        tkadAgreementWizardGetPdf(save, function (data) {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL(data.checkurl),
                type: 'GET'
            }).done(function (result) {
                if (result === 'True') {
                    $('#modal-pdf-download-btn').data('download-url', TDPWeb.ExtraNet.resolveURL(data.downloadurl));
                    tkadAgreementWizardTurnButtonsOn($element);

                    if (!TDPWeb.ExtraNet.isOnIE() || getAcrobatInfo().acrobat) {
                        $object.prop(attribute, TDPWeb.ExtraNet.resolveURL(data.url));
                        $modal.find('.modal-body').html($object.clone());
                        $modal.modal();
                    } else {
                        window.open(TDPWeb.ExtraNet.resolveURL(data.url));
                    }
                    if ($element.is('.view-agreement-btn')) {
                        self.agreementIsViewed = true;

                        if (!self.agreementIsSent) {
                            $('#terms-and-conditions-checkbox').removeAttr('disabled');
                        }
                    }
                } else {
                    tkadAgreementWizardPdfGenerationFailed($element);
                }
            }).fail(function () {
                tkadAgreementWizardPdfGenerationFailed($element);
            });
        }, function () {
            tkadAgreementWizardPdfGenerationFailed($element);
        });
    }

    function tkadAgreementWizardNoTouchPdfPreviewButtonClicked() {
        tkadAgreementWizardShowPreviewModal($(this));
    }

    function tkadAgreementWizardDownloadPdf(e, $element, save) {
        if (e.target.id === 'modal-pdf-download-btn') {
            // Delay in order to let disableFormConfirm be set to true
            setTimeout(function () {
                window.location = $('#modal-pdf-download-btn').data('download-url');
            }, 10);
        } else {

            // View\Preivew Button on Touch
            tkadAgreementWizardTurnButtonsOff($element);

            var downloadUrl = $element.data('download-url');

            if (self.isWizard) {
                var generatedDownloadUrl = $element.data('generated-download-url');

                if (generatedDownloadUrl) {
                    var resolvedDownloadUrl = TDPWeb.ExtraNet.resolveURL(generatedDownloadUrl);
                    window.open(resolvedDownloadUrl);
                    tkadAgreementWizardTurnButtonsOn($element);

                    if ($element.is('.view-agreement-btn')) {
                        self.agreementIsViewed = true;

                        if (!self.agreementIsSent) {
                            $('#terms-and-conditions-checkbox').removeAttr('disabled');
                        }
                    }
                } else {
                    tkadAgreementWizardGetPdf(save, function (data) {
                        $element.data('generated-download-url', data.downloadurl);
                        $element.text($element.data('download-pdf-text'));
                        tkadAgreementWizardTurnButtonsOn($element);
                    }, function () {
                        tkadAgreementWizardPdfGenerationFailed($element);
                    });
                }
            }
            else {
                // View Agreement Button on Touch, No Wizard
                window.open(downloadUrl);
                tkadAgreementWizardTurnButtonsOn($element);

                if ($element.is('.view-agreement-btn')) {
                    self.agreementIsViewed = true;

                    if (!self.agreementIsSent) {
                        $('#terms-and-conditions-checkbox').removeAttr('disabled');
                    }
                }
            }
        }
    }

    function tkadAgreementWizardTouchPdfPreviewButtonClicked(e) {
        tkadAgreementWizardDownloadPdf(e, $(this), true);
    }

    function tkadAgreementWizardFscBscToggleAndDelete($icon, $addMessage, $removeMessage, isAdd) {
        if (isAdd) {
            $icon.toggleClass('icon-trashcan', true);
            $icon.toggleClass('icon-plus', false);
            $addMessage.toggle(false);
            $removeMessage.toggle(true);
        } else {
            $icon.toggleClass('icon-trashcan', false);
            $icon.toggleClass('icon-plus', true);
            $addMessage.toggle(true);
            $removeMessage.toggle(false);
        }
    }

    function tkadAgreementWizardFscBscCollapseFormItem($panel, $expanded, $icon, $addMessage, $removeMessage) {
        $panel.collapse('hide');
        $expanded.val(false);
        tkadAgreementWizardFscBscToggleAndDelete($icon, $addMessage, $removeMessage, false);

        $panel.find(':input:not([type=hidden])').val('');
        $panel.find('input[type=checkbox]:checked').prop('checked', false);
    }

    function tkadAgreementWizardFscBscExpandFormItem($panel, $expanded, $icon, $addMessage, $removeMessage) {
        $panel.collapse('show');
        $expanded.val(true);
        tkadAgreementWizardFscBscToggleAndDelete($icon, $addMessage, $removeMessage, true);
    }

    function tkadAgreementWizardAddFscBscClicked() {
        var $expanded = $('#tkad-fsc-bsc-expanded'),
            $panel = $('#add-fsc-bsc'),
            $icon = $('#add-fsc-bsc-icon'),
            $addMessage = $('#add-fsc-bsc-add-message'),
            $removeMessage = $('#add-fsc-bsc-remove-message');

        if ($expanded.val() === 'true') {
            $('#confirm-FSCBSC-removal-modal').modal();
        } else {
            tkadAgreementWizardFscBscExpandFormItem($panel, $expanded, $icon, $addMessage, $removeMessage);
        }
    }

    function tkadAgreementWizardAddSubjobberClicked() {
        var $expanded = $('#tkad-subjobber-ado-expanded'),
            $panel = $('#add-subjobber-ado'),
            $icon = $('#add-subjobber-ado-icon'),
            $addMessage = $('#add-subjobber-ado-add-message'),
            $removeMessage = $('#add-subjobber-ado-remove-message');

        if ($expanded.val() === 'true') {
            $('#confirm-Subjobber-ADO-removal-modal').modal();
        } else {
            tkadAgreementWizardFscBscExpandFormItem($panel, $expanded, $icon, $addMessage, $removeMessage);
        }
    }

    function tkadAgreementWizardFscBscConfirmRemovalClicked() {
        var $expanded = $('#tkad-fsc-bsc-expanded'),
            $panel = $('#add-fsc-bsc'),
            $icon = $('#add-fsc-bsc-icon'),
            $addMessage = $('#add-fsc-bsc-add-message'),
            $removeMessage = $('#add-fsc-bsc-remove-message');

        tkadAgreementWizardFscBscCollapseFormItem($panel, $expanded, $icon, $addMessage, $removeMessage);

        tkadAgreementWizardUpdateEndCustomerFscContractorInformation();
    }

    function tkadAgreementWizardSubjobberConfirmRemovalClicked() {
        var $expanded = $('#tkad-subjobber-ado-expanded'),
            $panel = $('#add-subjobber-ado'),
            $icon = $('#add-subjobber-ado-icon'),
            $addMessage = $('#add-subjobber-ado-add-message'),
            $removeMessage = $('#add-subjobber-ado-remove-message');

        tkadAgreementWizardFscBscCollapseFormItem($panel, $expanded, $icon, $addMessage, $removeMessage);
    }

    function tkadAgreementWizardLoadSavedFormDataTriggerUpdatedElement($element, element) {

        // DatePickers need to be set differently
        if ($element.hasClass('hasDatepicker') && element) {

            $element.datepicker('option', 'dateFormat', self.dateFormat);
            $element.datepicker('setDate', new Date(element));

            // Don't fire onchange events since we do auto date selection when dates change
        } else {
            $element.val(element);
            $element.trigger('change');
            $element.trigger('keyup');
        }

    }

    function tkadAgreementWizardLoadSavedFormData(savedAgreementData) {

        self.loadingDraft = true;

        // Product list gets special handling
        var selectedDispensers = savedAgreementData.SelectDispensers,
            selectedDispenserList = savedAgreementData.SelectedDispenserList;
        delete savedAgreementData.SelectDispensers;
        delete savedAgreementData.SelectedDispenserList;

        // Everything else should have a key to name mapping of html content
        for (var key in savedAgreementData) {
            if (savedAgreementData.hasOwnProperty(key)) {
                var element = savedAgreementData[key];
                if (element) {
                    var lookupSelector = '#tkad-agreement-wizard [name=' + key + ']',
                        $element = $(lookupSelector);
                    if ($element && $element.length > 0) {
                        if ($element.prop('type') === 'checkbox') {
                            $element.prop('checked', element);
                        } else {
                            tkadAgreementWizardLoadSavedFormDataTriggerUpdatedElement($element, element);
                        }
                    }
                }
            }
        }

        if (selectedDispenserList) {
            productListController.loadListFromExternalSource(selectedDispenserList);
        }

        var $productQuantities = $('#quantity-product-list-body');

        for (var i = 0; i < selectedDispensers.length; i++) {
            var product = selectedDispensers[i],
                articleName = product.SelectDispensersArticleName,
                articleNumber = product.SelectDispensersArticleNumber,
                articleFee = product.SelectDispensersFee,
                articleCurrency = product.SelectDispensersCurrency,
                articleQuantity = product.SelectDispensersArticleQuantity || 0;

            if (articleName && articleNumber) {
                if (!selectedDispenserList) {
                    productListController.addProductToList(articleName, articleNumber);
                }

                // After the product is added, we have to find it on the quantity page
                var $quantity = $productQuantities.find('#quantity-' + articleNumber);
                if ($quantity) {
                    $quantity.find('.quantity-text-area').val(articleQuantity);
                    $quantity.find('.fee-text-area').val(articleFee);
                }
                if (articleCurrency) {
                    $('#quantity-product-list-currency').text(articleCurrency);
                }
            }
        }

        if (savedAgreementData.PaperContract) {
            tkadAgreementWizardPaperContractExpandCollapse(true);
        }

        if (savedAgreementData.FscBscExpanded) {
            tkadAgreementWizardFscBscExpandFormItem($('#add-fsc-bsc'), $('#tkad-fsc-bsc-expanded'), $('#add-fsc-bsc-icon'), $('#add-fsc-bsc-add-message'), $('#add-fsc-bsc-remove-message'));
        }

        if (savedAgreementData.SubjobberAdoExpanded) {
            tkadAgreementWizardFscBscExpandFormItem($('#add-subjobber-ado'), $('#tkad-subjobber-ado-expanded'), $('#add-subjobber-ado-icon'), $('#add-subjobber-ado-add-message'), $('#add-subjobber-ado-remove-message'));
        }

        tkadAgreementWizardUpdateEndCustomerFscContractorInformation();
        tkadAgreementWizardUpdateQuantityAndFees();
        tkadAgreementWizardSetFormAsClean();
        if (selectedDispensers.length > 0) {
            self.dispenserssaved = true;
        }

        self.loadingDraft = false;
    }

    function tkadAgreementWizardRegisterFormOptions($tkadForm) {
        // Copy ids to names on form fields
        $tkadForm.find('input, select, textarea').each(function () {
            var $element = $(this),
                id = $element.prop('id');

            // only process ids that start with tkad (this basically limits only those forms to be submitted
            if (id.substring(0, 4) === 'tkad') {
                // Name is basically the field id sans "tkad", removed hyphens, and title case.
                id = id.substring(4).replace(/\-/g, ' ').replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                }).replace(/[ ]/g, '');
                $element.prop('name', id);
            }
        });

        // Check to see if save data was presented
        if (self.savedAgreement) {
            tkadAgreementWizardLoadSavedFormData(self.savedAgreement);
        }

        $('.save-draft').on('click', function (e) {
            e.preventDefault();
            var $saveDraftButton = $(this);
            if (!$saveDraftButton.hasClass('disabled')) {
                $saveDraftButton.addClass('disabled');
                $saveDraftButton.text($saveDraftButton.data('save-draft-in-progress'));
                // Grab form data
                var form = tkadAgreementWizardSerializeFormData();

                // Send to endpoint for save
                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    data: form,
                    url: TDPWeb.ExtraNet.resolveURL('/TKAD/AgreementWizardSaveDraft')
                }).done(function () {
                    // Save completed
                    $saveDraftButton.text($saveDraftButton.data('save-draft-completed'));
                    tkadAgreementWizardSetFormAsClean();

                    setTimeout(function () {
                        $saveDraftButton.text($saveDraftButton.data('save-draft-text'));
                    }, 5000);
                }).fail(function () {
                    $saveDraftButton.text($saveDraftButton.data('save-draft-failed'));
                    setTimeout(function () {
                        $saveDraftButton.text($saveDraftButton.data('save-draft-text'));
                    }, 5000);
                }).always(function () {
                    setTimeout(function () {
                        $saveDraftButton.removeClass('disabled');
                    }, 5000);
                });
            }
        });
    }

    function tkadAgreementWizardPaperContractClicked() {
        var $paperContract = $('#tkad-paper-contract'),
            $paperContractNotification = $('#paper-contract-notification-modal');

        if ($paperContract.is(':checked')) {
            tkadAgreementWizardPaperContractExpandCollapse(true);
            $paperContractNotification.modal();
        } else {
            tkadAgreementWizardPaperContractExpandCollapse(false);
        }
    }

    function tkadAgreementWizardPaperContractExpandCollapse(expanded) {
        var $paperContractNumber = $('#tkad-paper-contract-number'),
            $paperContractNumberPanel = $('#paper-contract-number-collapsible');

        if (!expanded) {
            $paperContractNumber.val(null);
        }

        $paperContractNumber.prop('required', expanded);
        $paperContractNumberPanel.collapse(expanded ? 'show' : 'hide');
    }

    // Shared Functions
    ////////////////////////////////////////////////
    function tkadSharedAgreementTermsAndConditionsChanged() {
        if (this.checked && self.agreementIsViewed && !self.agreementIsSent && !self.agreementIsSigned) {
            if (self.showSignatureControl) {
                $('#tkad-agreement-signature').jSignature('enable');
                $('#tkad-agreement-signature-parent').removeClass('disabled');
            } else {
                $('#submit-for-approval-btn').prop('disabled', false);
            }

        } else if (!this.checked && !self.agreementIsSigned) {
            if (self.showSignatureControl) {
                $('#tkad-agreement-signature').jSignature('reset');
                $('#tkad-agreement-signature').jSignature('disable');
                $('#tkad-agreement-signature-parent').addClass('disabled');
            } else {
                $('#submit-for-approval-btn').prop('disabled', true);
            }
        }
    }

    function tkadSharedAgreementSignatureChanged() {
        // see if there's data
        var signatureData = $('#tkad-agreement-signature').jSignature('getData', 'native');
        if (signatureData && signatureData.length > 0 && self.agreementIsViewed && !self.agreementIsSent && !self.agreementIsSigned) {
            $('#submit-for-approval-btn').prop('disabled', false);
        } else if ((!signatureData || signatureData.length === 0) && !self.agreementIsSigned) {
            $('#submit-for-approval-btn').prop('disabled', true);
        }
    }

    function tkadSharedAgreementSignatureClearButtonClicked() {
        $('#tkad-agreement-signature').jSignature('reset');
    }

    function tkadSharedAgreementShowViewModal(hash) {
        var $modal = $('#pdf-preview-modal'),
            $object = $modal.find('object'),
            attribute = 'data';
        // IE need to use iframe instead of object
        if (TDPWeb.ExtraNet.isOnIE()) {
            $object = $modal.find('iframe');
            attribute = 'src';
        }

        $('#modal-pdf-download-btn').data('download-url', TDPWeb.ExtraNet.resolveURL('/PDF/ViewTKADAgreement?hash=' + hash + '&preview=0'));
        var url = '/PDF/ViewTKADAgreement?hash=' + hash;
        if (!TDPWeb.ExtraNet.isOnIE() || getAcrobatInfo().acrobat) {
            $object.prop(attribute, TDPWeb.ExtraNet.resolveURL(url));
            $modal.find('.modal-body').html($object.clone());
            $modal.modal();
        } else {
            window.open(TDPWeb.ExtraNet.resolveURL(url));
        }

        self.agreementIsViewed = true;
        $('#terms-and-conditions-checkbox').removeAttr('disabled');
    }

    function tkadSharedAgreementNoTouchViewAgreementButtonClicked(e) {
        e.preventDefault();
        if (self.isWizard) {
            tkadAgreementWizardShowPreviewModal($(this), true);
        } else {
            var hash = $('#submit-for-approval-btn').data('hash') || $(this).data('hash');
            tkadSharedAgreementShowViewModal(hash);
        }
    }

    function tkadSharedAgreementTouchViewAgreementButtonClicked(e) {
        e.preventDefault();
        tkadAgreementWizardDownloadPdf(e, $(this), true);
    }

    function tkadSharedAgreementDownloadButtonClicked(e) {
        tkadAgreementWizardDownloadPdf(e, $(this));
    }

    function tkadSharedAgreementSubmitForApprovalButtonClicked(e) {
        var signatureData = $('#tkad-agreement-signature').jSignature('getData', 'native');
        if ($('#terms-and-conditions-checkbox').prop('checked') && self.agreementIsViewed && ((self.showSignatureControl && signatureData && signatureData.length > 0) || !self.showSignatureControl) && !self.agreementIsSigned && !self.agreementIsSent) {
            var $button = $(this);
            $('#submit-for-approval-success').hide();
            $('#submit-for-approval-error').hide();
            $('.view-agreement-btn').prop('disabled', true);
            $('.send-pdf-agreement').prop('disabled', true);
            $('#tkad-agreement-signature').addClass('disabled');
            $('#tkad-agreement-signature-clear-btn').prop('disabled', true);
            $('#terms-and-conditions-checkbox').prop('disabled', true);
            TDPWeb.ExtraNet.spinnerOn($button, true, 'right');

            e.preventDefault();
            var signatureData = '',
                signatureDataUrl = '';

            if (self.showSignatureControl) {
                $('#tkad-agreement-signature').jSignature('disable');
                signatureData = $('#tkad-agreement-signature').jSignature('getData', 'svgbase64');
                signatureDataUrl = 'data:' + signatureData.join(',');
            }

            $.ajax({
                method: 'POST',
                dataType: 'json',
                data: {
                    token: $(this).data('token'),
                    hash: $(this).data('hash'),
                    signature: signatureDataUrl
                },
                url: TDPWeb.ExtraNet.resolveURL('/TKAD/SubmitForApproval')
            }).done(function () {
                self.agreementIsSigned = true;
                $('#submit-for-approval-btn').removeClass('btn-primary');
                $('#submit-for-approval-success').show();
                TDPWeb.ExtraNet.spinnerOff($button);

                if (self.isWizard) {
                    tkadAgreementWizardSetFormAsClean();
                }
                $('#submit-for-approval-btn').prop('disabled', true);
                reportDetailToGA('7a');
            }).fail(function () {
                $('#submit-for-approval-error').show();
                TDPWeb.ExtraNet.spinnerOff($button);
            });
        }
    }

    // Index/Search Event Bindings/Initialization
    ////////////////////////////////////////////////
    if (!self.isWizard) {
        $body.on('click', '.delete-draft-btn', tkadListViewDeleteDraftClicked);
        $body.on('click', '.resend-pdf-agreement', tkadListViewResendPdfAgreementClicked);
        $body.on('submit', '#tkad-filter-form', tkadListViewSearchFormSubmitted);
        $body.on('click', '#agreements-table [data-sort-property]', tkadListViewAgreementsTableSortableHeaderClicked);
        $body.on('click', '#agreements-table .tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next', tkadListViewAgreementsTablePaginationClicked);
        reportDetailToGA(0);
    }
    // Wizard/Modal Event Bindings/Initialization
    ////////////////////////////////////////////////
    else {
        // Initialize form wizard
        reportDetailToGA(1);
        Wizard.initiateWizard(tkadAgreementWizardStepValidation);
        self.currentStep = Wizard.getCurrentWizardTab();

        // Styling hack
        $('.wizard-item').each(function () {
            var $e = $(this),
                width = $e.outerWidth() * 0.8,
                $label = $e.find('.wizardLabel');
            $label.width(width);
        });

        // Initialize additional controls
        $('#tkad-end-customer-agreement-start-date').datepicker();
        $('#tkad-end-customer-agreement-end-date').datepicker();

        // Set events on body
        $body.on('click', '.wizard-item, #nextButton-top, #nextButton-bottom, #previousButton-top, #previousButton-bottom', tkadAgreementWizardNextPreviousButtonClicked);
        $body.on('change', '[data-required-group]', tkadAgreementWizardRequiredGroupChanged);
        $body.on('keyup change', '.quantityProductListItem .quantity-text-area, .quantityProductListItem', tkadAgreementWizardQuantityProductListItemKeyUp);
        $body.on('keyup', '.fee-text-area', tkadAgreementWizardFeeProductListItemKeyUp);
        $body.on('product-list-changed', '#application-product-list', tkadAgreementWizardProductListChanged);
        $body.on('click', '.send-pdf-agreement', tkadAgreementWizardSendPdfAgreementClicked);
        $body.on('change', '#tkad-end-customer-agreement-start-date', tkadAgreementWizardEndCustomerAgreementStartDateChanged);
        $body.on('change', '#tkad-end-customer-agreement-end-date', tkadAgreementWizardEndCustomerAgreementEndDateChanged);
        $body.on('change', '#tkad-distributor-data-sold-to', tkadAgreementWizardDistributorDataSoldToChanged);
        $body.on('hide.bs.collapse', '#sign-agreement-now', tkadAgreementWizardSignRightNowCollapsed);
        $body.on('show.bs.collapse', '#sign-agreement-now', tkadAgreementWizardSignRightNowExpanded);
        $body.on('change', '#tkad-end-customer-first-name', tkadAgreementWizardEndCustomerFirstNameChanged);
        $body.on('change', '#tkad-end-customer-last-name', tkadAgreementWizardEndCustomerLastNameChanged);
        $body.on('change', '#tkad-end-customer-email', tkadAgreementWizardEndCustomerEmailChanged);
        $body.on('change', '#tkad-fsc-first-name', tkadAgreementWizardFscFirstNameChanged);
        $body.on('change', '#tkad-fsc-last-name', tkadAgreementWizardFscLastNameChanged);
        $body.on('change', '#tkad-fsc-email', tkadAgreementWizardFscEmailChanged);
        $body.on('click', '#tkad-fsc-contractor', tkadAgreementWizardFscContractorClicked);
        $body.on('click', '#add-fsc-bsc-expand-collapse', tkadAgreementWizardAddFscBscClicked);
        $body.on('click', '#add-subjobber-ado-expand-collapse', tkadAgreementWizardAddSubjobberClicked);
        $body.on('click', '#confirm-fsc-bsc-removal', tkadAgreementWizardFscBscConfirmRemovalClicked);
        $body.on('click', '#confirm-subjobber-ado-removal', tkadAgreementWizardSubjobberConfirmRemovalClicked);
        $body.on('click', '#tkad-paper-contract', tkadAgreementWizardPaperContractClicked);
        $body.on('change', '#step1, #step2, #step3, #step4, #tkad-agreement-acceptance-email-message, #add-subjobber-ado-collapsibles', tkadAgreementWizardInputsChanged);
        $body.on('click', '#step1 .icon-calendar, #tkad-end-customer-agreement-start-date, #tkad-end-customer-agreement-end-date, #add-fsc-bsc-remove-message, #product-results .add-to-list, #application-product-list-body .delete-list-btn, #add-subjobber-ado-add-message, #add-subjobber-ado-remove-message', tkadAgreementWizardInputsChanged);


        // Set specific events for touch devices
        $touchSupport.on('click', '#pdf-preview-btn', tkadAgreementWizardTouchPdfPreviewButtonClicked);

        // set specific events for non-touch devices
        $noTouchSupport.on('click', '#pdf-preview-btn', tkadAgreementWizardNoTouchPdfPreviewButtonClicked);

        // Finalize setup
        tkadAgreementWizardRegisterFormOptions($('#tkad-agreement-wizard'));

        // Run initially since some data is filled in from the Model
        tkadAgreementWizardUpdateRequiredGroups('distributor-data-phones');

        //Display the Dispensers for External user having single account.
        if (!self.dispenserssaved) {
            tkadAgreementWizardDistributorDataSoldToChanged();
        }
    }

    // Shared Event Bindings
    // TODO: Are these actually shared? Make sure to move into appropriate area
    ////////////////////////////////////////////////
    $body.on('change', '#terms-and-conditions-checkbox', tkadSharedAgreementTermsAndConditionsChanged);
    $body.on('click', '#pdf-download-btn, #pdf-download-link, #modal-pdf-download-btn', tkadSharedAgreementDownloadButtonClicked);
    $body.on('click', '#submit-for-approval-btn', tkadSharedAgreementSubmitForApprovalButtonClicked);

    if (self.showSignatureControl) {
        $body.on('change', '#tkad-agreement-signature', tkadSharedAgreementSignatureChanged);
        $body.on('click', '#tkad-agreement-signature-clear-btn', tkadSharedAgreementSignatureClearButtonClicked);
    }

    $noTouchSupport.on('click', '.view-agreement-btn', tkadSharedAgreementNoTouchViewAgreementButtonClicked);

    $touchSupport.on('click', '.view-agreement-btn', tkadSharedAgreementTouchViewAgreementButtonClicked);

    // Shared Initialization
    // TODO: Are these actually shared? Make sure to move into appropriate area
    ////////////////////////////////////////////////
    if (self.showSignatureControl) {
        tkadAgreementWizardRegisterSignatureControl();
    }
    $('#submit-for-approval-btn').prop('disabled', true);
};

TKADController.prototype._attachDomControls = function () {
    'use strict';
    var self = this,
        $ = self.$;

    // Control Registration Functions
    ////////////////////////////////////////////////
    function registerEnhancedSelectControls() {
        $.each($('.main-content-container select, .modal select'), function () {
            var opt = {
                placeholder: $(this).data('placeholder'),
                theme: 'bootstrap',
                allowClear: true
            };

            if ($(this).data('hide-search') === 1) {
                opt.minimumResultsForSearch = -1;
            }

            $(this).select2(opt);
        });
    }

    // Invoke Registration Functions
    ////////////////////////////////////////////////
    registerEnhancedSelectControls();
}

TKADController.prototype.initialize = function () {
    'use strict';

    this._ieShim();
    this._attachDomEvents();
    this._attachDomControls();

    TDPWeb.ExtraNet.setUpPopovers();
};