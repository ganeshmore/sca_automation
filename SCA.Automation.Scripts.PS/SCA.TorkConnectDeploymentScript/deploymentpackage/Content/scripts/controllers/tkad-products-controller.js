function TKADProductsController($jQuery) {
    'use strict';

    // Capturing jQuery to make sure we're always using that opposed to in-browser $'s
    this.$ = $jQuery;
    this.currentPage = 1;
    this.sortOrder = 'base_materials_number';
}

TKADProductsController.prototype._attachDomEvents = function () {
    'use strict';
    var self = this,
        $ = self.$,
        $body = $('body');

    // Shared Products Functions
    //////////////////////////////////////////
    function tkadProductsExport(country) {
        var exportUrl = TDPWeb.ExtraNet.resolveURL('/TKADProducts/Export/');
        exportUrl += '?country=' + country;
        window.open(exportUrl);
    }

    // Export Products Functions
    //////////////////////////////////////////
    function tkadProductsExportResetModalFields() {
        $('#tkad-products-export-country').val('');
        $('#tkad-products-export-country').trigger('change');
        $('#tkad-products-export-btn').prop('disabled', true);
    }

    function tkadProductsExportCancelClicked(e) {
        e.preventDefault();

        tkadProductsExportResetModalFields();
        $('#tkad-products-export-modal').modal('hide');
    }

    function tkadProductsExportCountryChanged(e) {
        var selectedValue = $(this).val();

        $('#tkad-products-export-btn').prop('disabled', !selectedValue);
    }

    function tkadProductsExportButtonClicked(e) {
        e.preventDefault();

        // Open the export controller to begin a download of the current products
        var selectedCountry = $('#tkad-products-export-country').val();
        tkadProductsExport(selectedCountry);

        setTimeout(function () {
            tkadProductsExportCancelClicked(e);
        }, 500);
    }

    // Import Products Functions
    //////////////////////////////////////////
    function tkadProductsImportResetModalFields() {
        $('#tkad-products-import-form').prop('disabled', false);
        $('#tkad-products-import-form').removeClass('disabled');
        $('#tkad-products-import-form :input').prop('disabled', false);
        $('#tkad-products-import-form :input').removeClass('disabled');
        TDPWeb.ExtraNet.resetFormValidations($('#tkad-products-import-form'));

        $('#tkad-products-import-success').hide();

        $('#tkad-products-import-country').val('');
        $('#tkad-products-import-country').trigger('change');
        $('#tkad-products-import-backup-current-products-btn').prop('disabled', true);
        $('#tkad-products-import-btn').prop('disabled', true);

        var $tkadProductsImportFileNamePlaceHolder = $('#tkad-products-import-file-name-placeholder .selected-file-name');
        $tkadProductsImportFileNamePlaceHolder.html($tkadProductsImportFileNamePlaceHolder.data('default-text'));

        var $tkadProductsImportFile = $('#tkad-products-import-file');
        $tkadProductsImportFile.prop('disabled', true);
        $tkadProductsImportFile.val('');
        $tkadProductsImportFile.parent().addClass('disabled');
    }

    function tkadProductsImportCountryChanged() {
        var selectedValue = $(this).val();
        // TODO: On change, we should clear the form

        $('#tkad-products-import-file').prop('disabled', true);
        $('#tkad-products-import-file').parent().addClass('disabled');
        $('#tkad-products-import-backup-current-products-btn').prop('disabled', !selectedValue);
    }

    function tkadProductsImportBackupCurrentProductsClicked(e) {
        e.preventDefault();

        // Open the export controller to begin a download of the current products
        var selectedCountry = $('#tkad-products-import-country').val();
        tkadProductsExport(selectedCountry);

        // set a small delay to allow the browser to generate the file, then enable
        // the import process
        // TODO: this does not handle a failed generation, consider using something
        // like the jQuery File Download Plugin: http://johnculviner.com/jquery-file-download-plugin-for-ajax-like-feature-rich-file-downloads/
        setTimeout(function () {
            $('#tkad-products-import-file').prop('disabled', false);
            $('#tkad-products-import-file').parent().removeClass('disabled');
        }, 500);
    }

    function tkadProductsImportCancelClicked(e) {
        e.preventDefault();

        tkadProductsImportResetModalFields();
        $('#tkad-products-import-modal').modal('hide');
    }

    function tkadProductsImportFileChanged() {
        var regExMatch = (this.value || '').match(/\.([^\.]+)$/),
            ext = (regExMatch && regExMatch.length > 0) ? regExMatch[1].toLowerCase() : '',
            fileName = $(this).val().split('\\').pop(),
            $placeHolder = $('#tkad-products-import-file-name-placeholder .selected-file-name'),
            noFileSelected = $placeHolder.data('default-text');

        switch (ext) {
            case 'xlsx':
            case 'xls':
                if (fileName && fileName.length) {
                    $placeHolder.html(fileName);
                } else {
                    $placeHolder.html(noFileSelected);
                }

                $('#tkad-products-import-btn').prop('disabled', false);

                break;
            default:
                $placeHolder.html(noFileSelected);
                this.value = '';
        }
    }

    function tkadProductsImportFormSubmitted(e) {
        e.preventDefault();

        var $spinnerLocation = $('#tkad-products-import-file-spinner-placeholder'),
            $uploadFormSuccessMessage = $('#tkad-products-import-success'),
            $uploadForm = $(this),
            $uploadFormInputs = $uploadForm.find(':input'),
            uploadFormData = new FormData($uploadForm[0]);

        $uploadFormSuccessMessage.hide();
        $uploadFormInputs.prop('disabled', true);
        $uploadFormInputs.addClass('disabled');
        $('#tkad-products-import-file').parent().addClass('disabled');


        TDPWeb.ExtraNet.resetFormValidations($uploadForm);
        TDPWeb.ExtraNet.spinnerOn($spinnerLocation, true, 'right');

        setTimeout(function () {
            $.ajax({
                url: TDPWeb.ExtraNet.resolveURL('/TKADProducts/Import'),
                type: 'POST',
                data: uploadFormData,
                async: false,
                cache: false,
                contentType: false,
                processData: false
            }).done(function () {
                // Reset form values
                $uploadFormSuccessMessage.show();
                setTimeout(function () {
                    tkadProductsImportCancelClicked(e);
                    tkadProductSearch();
                }, 5000);
            }).fail(function (data) {
                var $uploadFormErrors = $uploadForm.find('.form-errors');

                TDPWeb.ExtraNet.displayBackendErrors(data.responseJSON.errors, $uploadForm, $uploadFormErrors);
                $uploadFormInputs.prop('disabled', false);
                $uploadFormInputs.removeClass('disabled');
                $('#tkad-products-import-file').parent().removeClass('disabled');
            }).always(function () {
                TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            });
        }, 100);
    }

    // Product List Functions
    //////////////////////////////////////////
    function tkadProductsImportProducts(e) {
        e.preventDefault();

        tkadProductsImportResetModalFields();

        $('#tkad-products-import-modal').modal();
    }

    function tkadProductsExportProducts(e) {
        e.preventDefault();

        tkadProductsExportResetModalFields();

        $('#tkad-products-export-modal').modal();
    }

    function tkadProductSearch() {
        var $searchResults = $('.search-result'),
            query = $('#search-field').val() || '',
            $spinnerLocation = $('#tkad-products-table'),
            countryFilter = $('#country-filter').val();

        TDPWeb.ExtraNet.spinnerOn($spinnerLocation, false);

        var data = {
            searchTerm: query,
            sortOrder: self.sortOrder,
            currentPage: self.currentPage,
            countryFilter: countryFilter
        };
        $.ajax({
            type: 'POST',
            url: TDPWeb.ExtraNet.resolveURL('/TKADProducts/Search'),
            data: data
        }).done(function (result) {
            $searchResults.html(result);
            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
            TDPWeb.ExtraNet.scrollUpToTarget($searchResults);
        }).fail(function () {
            TDPWeb.ExtraNet.spinnerOff($spinnerLocation);
        });

        return false;
    }

    function tkadProductTableSortByProperty(ascending, descending) {
        if (self.sortOrder !== ascending) {
            self.sortOrder = ascending;
        } else {
            self.sortOrder = descending;
        }

        tkadProductSearch();
    }

    function tkadProductTableGoToPage(n) {
        var totalPages = parseInt($('.tork-pagination ul li:last-child')[0].textContent, 10);
        if (n === 0 || n > totalPages) {
            return;
        }

        self.currentPage = n;
        tkadProductSearch();
    }

    function tkadProductsSearchInputSubmitted(e) {
        e.preventDefault();

        self.currentPage = 1;

        tkadProductSearch();
    }

    function tkadProductTableSortableColumnClicked() {
        var $this = $(this);
        tkadProductTableSortByProperty($this.data('sort-property'), $this.data('sort-property-desc'));
    }

    function tkadProductTablePaginationClicked() {
        var $this = $(this);
        tkadProductTableGoToPage($this.data('page-number'));
    }

    // DOM Event Wiring for Each Section
    //////////////////////////////////////////

    // Product List DOM Events
    $body.on('click', '#tkad-products-import-products-btn', tkadProductsImportProducts);
    $body.on('click', '#tkad-products-export-products-btn', tkadProductsExportProducts);
    $('#tkad-products-search-input').on('submit', tkadProductsSearchInputSubmitted);
    $body.on('click', '.table-head .Sort-header', tkadProductTableSortableColumnClicked);
    $body.on('click', '.tork-pagination ul li, .tork-pagination .prev, .tork-pagination .next', tkadProductTablePaginationClicked);

    // Export Products DOM Events
    $body.on('change', '#tkad-products-export-country', tkadProductsExportCountryChanged);
    $body.on('click', '#tkad-products-export-btn', tkadProductsExportButtonClicked);
    $body.on('hidden.bs.modal', '#tkad-products-export-modal', tkadProductsExportCancelClicked);

    // Import Products DOM Events
    $body.on('change', '#tkad-products-import-country', tkadProductsImportCountryChanged);
    $body.on('click', '#tkad-products-import-backup-current-products-btn', tkadProductsImportBackupCurrentProductsClicked);
    $body.on('click', '#tkad-products-import-cancel-btn', tkadProductsImportCancelClicked);
    $body.on('change', '#tkad-products-import-file', tkadProductsImportFileChanged);
    $body.on('submit', '#tkad-products-import-form', tkadProductsImportFormSubmitted);
};

TKADProductsController.prototype._attachDomControls = function () {
    'use strict';
    var self = this,
        $ = self.$;

    $.each($('body select'), function () {
        var opt = {
            placeholder: $(this).data('placeholder'),
            theme: 'bootstrap',
            allowClear: true
        };
        
        if ($(this).data('hide-search') === 1) {
            opt.minimumResultsForSearch = -1;
        }

        $(this).select2(opt);
    });
}

TKADProductsController.prototype.initialize = function () {
    'use strict';
    this._attachDomEvents();
    this._attachDomControls();
    TDPWeb.ExtraNet.setUpPopovers();
};


(function ($) {
    'use strict';
    var tkadProductsController = new TKADProductsController($);
    tkadProductsController.initialize();
})(jQuery);