(function ($) {
    var TorkReadyController = (function () {

        var videoListPagination;

        return {
            videoListPagination: videoListPagination,

            init: function () {

                var $pagination = $("#video-list-pagination"),
                    $resultsContainer = $("#video-list-table");

                TorkReadyController.videoListPagination = new Pagination();
                TorkReadyController.videoListPagination.initialize($pagination, $resultsContainer, ".tork-challenge-item", $pagination.data("items-per-page"), $pagination.data("previous-text"), $pagination.data("next-text"));
                TorkReadyController.videoListPagination.refreshPage();
                TorkReadyController.attachDomEvents();

            },

            getchallengeVideoDetail: function (isInitialization) {
                $.ajax({
                    type: "GET",
                    url: TDPWeb.ExtraNet.resolveURL("/TorkReady/GetCompletedLoIds"),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                }).done(function (result) {
                    if (result.length > 0) {
                        $(".tork-challenge-box .cs_video_LoId").each(function () {
                            if (result.indexOf($(this).val()) !== -1) {
                                if (!$(this).parent().find('.icon').hasClass('icon-checkbox')) {
                                    $(this).parent().find('.icon').addClass("icon-checkbox").removeClass("icon-circle");
                                }
                            }
                        });
                    }
                })
            },


            attachDomEvents: function () {
                var $body = $("body");
                var isAutoRefreshEnabled = false;
                $body.delegate("[data-start]", "click", function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var videoUrl = $(this).prop('href');
                    var cornerStoneStudyWindow = window.open(videoUrl, '_blank')
                    
                    var closeIntermitPageTime = $('#cs_intermitpage_closetime').val();
                    if (!($.isNumeric(closeIntermitPageTime)))
                    {
                        closeIntermitPageTime = 13000
                    }
                    setTimeout(function () { cornerStoneStudyWindow.close(); }, closeIntermitPageTime);

                    //Tork challenge video get auto refreshed once in every 1 minute. Since video is getting opened in new popwindow in corner stone side, not possible to know the completion of study to change the status.
                    if (isAutoRefreshEnabled == false) {
                        setInterval(function () {
                            TorkReadyController.getchallengeVideoDetail(false);
                        }, 60 * 1000);
                        isAutoRefreshEnabled = true;
                    }
                });
            }
        }
    })();

    $(function () {
        TorkReadyController.init();
    });
})(jQuery);