Pagination = function () {
    var currentPage = 1,
        currentListLength = 0,
        $pagination,
        $list,
        $initialiList,
        item_class,
        items_per_page,
        usingFilteredList = false,
        $slim_pagination,
        $nextButton,
        $prevButton;

    function initialize($p, $l, i_class, i_per_page, prev_text, next_text, $slim_p) {
        prev_text = prev_text == undefined ? "" : prev_text
        next_text = next_text == undefined ? "" : next_text

        $pagination = $p;
        $list = $l;
        $initialiList = $l;
        $list.addClass("pagination-list-container");
        item_class = i_class;
        items_per_page = i_per_page;
        $slim_pagination = $slim_p || $();
        $nextButton = $slim_pagination.find('[data-navigate="nextPage"]');
        $prevButton = $slim_pagination.find('[data-navigate="prevPage"]');

        $pagination.pagination({
            items: listLength(),
            itemsOnPage: items_per_page,
            edges: 1,
            prevText: "<span class='icon-arrow_left'></span>" + prev_text,
            nextText: next_text + "<span class='icon-arrow_right'></span>",
            onInit: function () {
                if (noPagination()) {
                    $pagination.hide();
                }
                if (currentListLength === 0) {
                    $slim_pagination.hide();
                }
            },
            onPageClick: function (pageNumber) {
                setCurrentPage(pageNumber);
                TDPWeb.ExtraNet.scrollUpToTarget($list, { topMargin: 45 });
            }
        });

        var classes = $list.attr("class");
        $list.observe('childlist subtree', function (record) {
            // Observe changes in the subtree
            updatePagination();
        })
        .observe({ attributes: true }, function (record) {
            // Observe changes in attribute class on $list
            var new_classes = $list.attr("class");
            if (new_classes !== classes)
            {
                classes = new_classes;
                updatePagination();
            }
        });
    }

    function listLength() {
        return $list.find(item_class).not(".ui-sortable-placeholder, .ui-sortable-helper").length;
    }

    function lastPage() {
        return Math.ceil(currentListLength / items_per_page);
    }

    function setCurrentPage(pageNumber) {
        currentPage = pageNumber;
        $pagination.pagination('drawPage', pageNumber);
        showItemsInPage(pageNumber);
        if ($prevButton.length) {
            var count = $pagination.pagination('getPagesCount');
            var current = $pagination.pagination('getCurrentPage');

            $prevButton.prop('disabled', current === 1);
            $nextButton.prop('disabled', current === count);
        }
    }

    function refreshPage() {
        setCurrentPage(currentPage);
    }

    function resetPagination() {
        setCurrentPage(1);
    }

    var filterList = function (filterClass) {
        $list = $list.find("." + filterClass);
        usingFilteredList = true;
        updatePagination();
        setCurrentPage(1);
    };

    var useUnfilteredList = function () {
        $list = $initialiList;
        usingFilteredList = false;
        updatePagination();
        setCurrentPage(1);
    };

    var updatePagination = function () {
        var oldListLength = currentListLength;
        currentListLength = usingFilteredList ? $list.length : listLength();

        var ignoreChanges = $list.hasClass("no-pagination-change"),
            resorted = $list.hasClass("resorted");
        if (!ignoreChanges && (oldListLength !== currentListLength || resorted)) {
            $list.removeClass("resorted");
            $pagination.pagination('updateItems', currentListLength);

            if (currentListLength > oldListLength) {
                // item was added
                setCurrentPage(1);
            }
            else {
                // item was removed
                var last = lastPage();
                if (currentPage > last) {
                    // goto last page
                    setCurrentPage(last);
                }
                else {
                    setCurrentPage(currentPage);
                }
            }

            if (noPagination()) {
                $pagination.hide();
            }
            else {
                $pagination.show();
            }

            if (currentListLength === 0) {
                $slim_pagination.hide();
            } else {
                $slim_pagination.show();
            }
        }
    }

    function noPagination() {
        return currentListLength <= items_per_page;
    }

    function lastItemInLastPage() {
        var rem,
            isLastPage;
        rem = currentListLength % items_per_page;

        isLastPage = lastPage() === currentPage;

        return rem == 1 && isLastPage;
    }

    function showItemsInPage(pageNumber) {
        var $items = usingFilteredList ? $list : $list.find(item_class);

        //hide all items
        for (var i = 0; i < $items.length ; i++) {
            $($items[i]).hide();
        }

        //show items only in this pageNumber
        var start_item = items_per_page * (pageNumber - 1),
            next_page_first_item = items_per_page * pageNumber;
        for (var i = start_item; i < next_page_first_item && i < $items.length; i++) {
            var $item = $($items[i]);
            $item.show();
            TDPWeb.ExtraNet.loadDataSrcImageInContainer($item);
        }
    }

    return {
        initialize: initialize,
        setCurrentPage: setCurrentPage,
        resetPagination: resetPagination,
        refreshPage: refreshPage,
        filterList: filterList,
        useUnfilteredList: useUnfilteredList,
        updatePagination: updatePagination
    }
};