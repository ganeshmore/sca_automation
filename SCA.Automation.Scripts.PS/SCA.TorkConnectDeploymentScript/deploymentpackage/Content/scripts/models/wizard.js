Wizard = (function () {
    var wizardValidatons;
    var $wizardNext;
    var $wizardPrev;
    var $wizard;
    var $tabItems;
    var $tabTitle;
    var $progressBar = $('.ProgressBar-indicator');

    function initiateWizard(wizardValidatorFunction) {
        wizardValidatons = wizardValidatorFunction;
        $wizardNext = $('[data-wizard="nextTab"]');
        $wizardPrev = $('[data-wizard="prevTab"]');
        $wizard = $(".wizard");
        $tabItems = $wizard.find(".wizard-item");
        $tabTitle = $('.tab-title');

        $("[id^=nextButton]").click(function () {
            var currentTab = getCurrentWizardTab();
            setProgress(currentTab + 1);
        });

        $("[id^=previousButton]").click(function () {
            var currentTab = getCurrentWizardTab();
            setProgress(currentTab - 1);
        });

        $('.wizard').on("click", ".selectable", function (evt) {
            clickedIndex = parseInt($(evt.currentTarget).attr("data-index"));
            setProgress(clickedIndex);
        });

        $wizardNext.click(function () {
            var tab = getCurrentWizardTab() + 1;
            setProgress(tab);
        });

        $wizardPrev.click(function () {
            var tab = getCurrentWizardTab() - 1;
            setProgress(tab);
        });

        setProgress(1);
    };

    function setProgress(tab) {
        var didChange = selectWizardTab(tab);

        if (didChange) {
            var $current = $tabItems.filter('.current');
            var title = $current.find('.wizardLabel').text();
            $tabTitle.text(title);
            var newTitle;

            if (tab < $tabItems.length) {
                newTitle = $current.next().find('.wizardLabel').text();
            } else if (tab === $tabItems.length) {
                newTitle = '';
            }

            $wizardNext.find('[data-text-content]').text(newTitle);

            $progressBar.animate({ 'width': tab / $tabItems.length * 100 + '%' }, 150);

            $wizardNext.toggleClass('disabled', tab === $tabItems.length);
            $wizardPrev.toggleClass('disabled', tab === 1);
        }
    };

    function getCurrentWizardTab () {
        return parseInt($(".wizard").find(".current").attr("data-index"));
    };

    function selectWizardTab (index) {
        var $wizard = $(".wizard");
        var $items = $wizard.find(".wizard-item");

        if (index >= 1 && index <= $items.length) {
            // Get current selected index:
            var currentIndex = -1;
            for (var i = 0; i < $items.length; i++) {
                if ($($items[i]).hasClass("current")) {
                    currentIndex = i + 1;
                    break;
                }
            }

            if (currentIndex != -1 && index > currentIndex) {
                // Going forward in the wizard. Validate the current step: 
                if (wizardValidatons) {
                    if (!wizardValidatons(currentIndex)) {
                        // Not allowed to select anything.
                        return false;
                    }
                }
            }

            for (var i = 0; i < $items.length; i++) {
                var item = $($items[i]);
                if ((i + 1) == index) {
                    item.addClass("current");
                } else {
                    item.removeClass("current");
                }

                if (i <= index) {
                    item.addClass("selectable");
                } else {
                    item.removeClass("selectable");
                }

                if (i < index - 1) {
                    item.addClass("previous");
                } else {
                    item.removeClass("previous");
                }
            }

            // Move the arrow:
            targetWidth = (100 * index / $items.length) + "%";
            var $wizardProgress = $wizard.find(".wizard-progress");
            $wizardProgress.animate({ width: targetWidth }, 200);
            if ($items.length == index) {
                $wizardProgress.addClass("finished");
            } else {
                $wizardProgress.removeClass("finished");
            }

            if (index == 1) {
                $("[id^=previousButton]").addClass('disabled');
            } else {
                $("[id^=previousButton]").removeClass('disabled');
            }

            if (index == $items.length) {
                $("[id^=nextButton]").addClass('disabled');
            } else {
                $("[id^=nextButton]").removeClass('disabled');
            }

            $(".wizard-alert-msg, .wizard-alert-indicator").hide();

            var wizardPanes = $(".wizard-content").find(".wizard-pane");
            wizardPanes.hide();
            $(wizardPanes[index - 1]).show();

            $(".wizard-content").trigger("wizard-page-changed");
            return true;
        }
    };

    return {
        initiateWizard: initiateWizard,
        getCurrentWizardTab: getCurrentWizardTab,
        selectWizardTab: selectWizardTab
    }
})();