(function ($, window) {

    'use strict';

    if (typeof $.pluginFactory !== 'function') {
        return;
    }

    // args: name, defaultOptions, constructor, methods
    $.pluginFactory(
        'Navigation',
        {
            linkSelector: 'a',
            selectedClass: 'is-active',
            parentLink: function ($link) {
                return $link.parent(this.linkSelector);
            },
            currentPaneClass: 'u-block',
        },
        function() {
            var _self = this;

            if (_self.options.paneContainer) {
                _self.$paneContainer = $(_self.options.paneContainer);
            }

            _self.$element.on('click.navigation', _self.options.linkSelector, _self.handleClick.bind(this));
        },
        {
            /**
             * Set referens to 'selected' by traversing the DOM tree and look for selectedClass
             */
            getStateFromDOM: function() {
                var _self = this;
                if (!_self.$selected || !_self.$selected.closest('body').length) {

                    // Get all selected items
                    var $selectedCollection = _self.$element.find('.' + _self.options.selectedClass);
                    var $target = $selectedCollection.last();

                    _self.$selected = $target;
                }

                if (!_self.$currentPane && _self.$paneContainer) {
                    _self.$currentPane = _self.$paneContainer.find(_self.$selected.data('togglePane')).addClass(_self.options.currentPaneClass);
                }
            },

            /**
             * Handle click on link
             */
            handleClick: function(e) {
                var $target = $(e.currentTarget);

                this.setActiveLink($target, e);

                if ($target.data('togglePane')) {
                    // Don't use href when toggling pane
                    e.preventDefault();
                }
            },

            /**
             * Handle click on link
             */
            setActiveLink: function($target, e) {
                var _self = this;

                // Wire up current selected if one exists in DOM
                _self.getStateFromDOM();

                var $selectedParents = _self.getSelectedParents(_self.$selected);
                var $targetParents = _self.getSelectedParents($target);

                // When target is tha same as selected, unset target
                if ($target.is(_self.$selected)) {
                    $targetParents = $targetParents.slice(0, -1);
                    $target = $targetParents.last();
                }

                // Callback
                if (typeof _self.options.onSelect === 'function') {
                    if (_self.options.onSelect($targetParents, $selectedParents, e) === false) {
                        return;
                    }
                }
                
                // Remove classes on current selected (old)
                $selectedParents.each(function(index, item) { 
                    var $item = $(item);
                    $item.removeClass($item.data('selectedClass') + ' ' + _self.options.selectedClass);
                });

                // Add classes on target (new)
                $targetParents.each(function(index, item) {
                    var $item = $(item);
                    $item.addClass(($item.data('selectedClass') || '') + ' ' + _self.options.selectedClass);
                });

                // Toggle content?
                if ($target.data('togglePane')) {
                    // Hide current pane
                    _self.$currentPane.removeClass(_self.options.currentPaneClass);

                    // Show target pane
                    _self.$currentPane = _self.$paneContainer.find($target.data('togglePane')).addClass(_self.options.currentPaneClass);
                }

                // Make target current selected
                _self.$selected = $target;
            },

            getActive: function() {
                var _self = this;

                // Wire up current selected if one exists in DOM
                _self.getStateFromDOM();

                return _self.$selected;
            },

            /**
             * Traverse up the DOM to toggle active class on all parent links
             */
            getSelectedParents: function($node) {
                var _self = this;

                // Start with empty jQuery list
                var $nodes = $([]);

                // Continiously loop while there are parent links
                do {
                    // Add node to list
                    $nodes = $nodes.add($node);
                    $node = _self.options.parentLink($node);

                } while ($node.length);

                return $nodes;
            },
        }
    );

})(jQuery, window);