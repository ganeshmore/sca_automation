/*  Modified by Christian Fynbo, Altea for better user experience.
    Preview slides down after an image has been selected.

    Previous functionality always required the preview container to be displayed.

    Modified by Shan Huang, Alten for allowing a default img. 2015-08-26
*/

(function ($) {
    var settings = {
        'scale': 'contain', // cover
        'prefix': 'prev_',
        'prefix_wrapper': 'prev-wrapper_',
        'types': ['image/gif', 'image/png', 'image/jpeg'],
        'mime': { 'jpe': 'image/jpeg', 'jpeg': 'image/jpeg', 'jpg': 'image/jpeg', 'gif': 'image/gif', 'png': 'image/png', 'x-png': 'image/png', 'tif': 'image/tiff', 'tiff': 'image/tiff' }
    };

    function renderPreview(id, imgData) {
        var $previewWrapper = $('.' + settings['prefix_wrapper'] + id);

        $previewWrapper
             .find(".progress")
             .hide();

        $previewWrapper.find("#preview-image").hide();

        $previewWrapper.slideDown();
        $previewWrapper
            .find(".progress")
            .show();

        $previewWrapper
            .find(".progress-bar")
            .animate({ "width": "100%" }, 2000, function () {
                $previewWrapper.find("#preview-image").show();
                $previewWrapper.find("#preview-image").trigger("imageLoaded");
                $('<div />')
                    .css({ 'background-image': ('url(' + imgData + ')'), 'background-repeat': 'no-repeat', 'background-size': settings['scale'] })
                    .addClass(settings['prefix'] + 'thumb').appendTo($('#' + settings['prefix'] + id));

                $previewWrapper
                    .find(".progress")
                    .delay(500)
                    .slideUp(200);

                setTimeout(function () {
                    $previewWrapper
                    .find(".progress-bar")
                    .css({ "width": 0 })
                }, 1000);
            });
    }

    var methods = {
        init: function (defaultImg, options) {
            settings = $.extend(settings, options);

            if (defaultImg) {
                var fileNameParts = defaultImg.name.split(".");
                var fileExtension = fileNameParts[fileNameParts.length - 1];
                var imgSrc = "data:image/" + fileExtension + ";base64," + defaultImg.file;

                renderPreview(defaultImg.logoId, imgSrc);
            }

            return this.each(function () {
                $(this).bind('change', methods.change);
                $('#' + settings['prefix'] + this.id).html('').addClass(settings['prefix'] + 'container');
            });
        },
        destroy: function () {
            return this.each(function () {
                $(this).unbind('change');
            })
        },
        change: function (event) {
            var id = this.id

            $('#' + settings['prefix'] + id).html('');


            if (window.FileReader) {
                for (i = 0; i < this.files.length; i++) {
                    if (!$.inArray(this.files[i].type, settings['types']) == -1) {
                        window.alert("File of not allowed type");
                        return false
                    }
                }

                for (i = 0; i < this.files.length; i++) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        renderPreview(id, e.target.result);
                    };
                    reader.readAsDataURL(this.files[i]);
                }
            } else {
                //if(window.confirm('Internet Explorer do not support required HTML5 features. \nPleas, download better browser - Firefox, Google Chrome, Opera... \nDo you want to download and install Google Chrome now?')){ window.location("//google.com/chrome"); }
            }
        }
    };

    $.fn.preimage = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.preimage');
        }

    };

})(jQuery);

