﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using NuGet;

namespace WindowsFormsNugetApp
{

    public partial class NugetApp : Form
    {
        string sSelectedfolder;
        string[] fileArray;
        DialogResult result;
        string logPath = ConfigurationManager.AppSettings["logPath"].ToString();
        public NugetApp()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    sSelectedfolder = folderBrowserDialog1.SelectedPath.ToString();
                    fileArray = Directory.GetFiles(sSelectedfolder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith("packages.config")).ToArray(); ;
                    ExportNuget.Enabled = true;
                    ExportNuget.Visible = true;
                    PackageVersions.DataSource = null;
                    PackageVersions.Rows.Clear();
                    PackageVersions.Refresh();
                    PackageVersions.Visible = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                Log.write("FileNotFoundException exception in the getting file path method, thus no extract list would be extract", ex, logPath);
            }
            catch (PathTooLongException ex)
            {
                Log.write("PathTooLongException exception in the SendPatchingEmail method, thus no notification email would be sent", ex, logPath);
            }
            catch (Exception ex)
            {
                Log.write("Excception occured in exporting excel sheet", ex, logPath);
            }
        }

        private void NugetPanel_Paint(object sender, PaintEventArgs e)
        {
            outputmsg.Text = "please select root folder of solution";
            //exportnuget.enabled = false;

            if (!File.Exists(logPath))
            {
                using (FileStream fs = File.Create(logPath))
                {
                    // Add some text to file
                    Byte[] title = new UTF8Encoding(true).GetBytes("Log File is get created..");
                    fs.Write(title, 0, title.Length);
                    byte[] author = new UTF8Encoding(true).GetBytes("Ganesh More");
                    fs.Write(author, 0, author.Length);
                }

            }
        }
        public static class Log
        {
            public static void write(string message, Exception ex, string logPath)
            {
                string text = ex == null ? message : ex.ToString();
                using (StreamWriter sw = new StreamWriter(logPath, true))
                {
                    sw.WriteLine(text);
                }
            }
        }

        private void ExportNuget_Click(object sender, EventArgs e)
        {
            ExportNuget.Enabled = false;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = true;
            outputmsg.Text = "Loading....Exporting Nuget package for given solution.";
            Log.write("/*****************NuGet Package List extraction started  " + DateTime.Now.ToString() + "*****************/", null, logPath);
            try
            {
                Library.NugetExportList nel = new Library.NugetExportList();
                DataTable dt = nel.NugetListExtract(fileArray);
                PackageVersions.DataSource = dt;
            
                if (PackageVersions.Rows.Count > 1)
                {
                    Microsoft.Office.Interop.Excel.Application xlApp;
                    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;

                    xlApp = new Microsoft.Office.Interop.Excel.Application();
                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    for (j = 0; j <= 2; j++)
                    {
                        Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                        rng.Font.Bold = true;
                        rng.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                        rng.BorderAround();
                    }
                    i = 0;
                    j = 0;
                    xlWorkSheet.Cells[i + 1, j + 1] = "Package Name";
                    xlWorkSheet.Cells[i + 1, j + 2] = "Installed Version";
                    xlWorkSheet.Cells[i + 1, j + 3] = "Latest Version";

                    for (i = 1; i <= PackageVersions.RowCount; i++)
                    {
                        for (j = 0; j <= PackageVersions.ColumnCount - 1; j++)
                        {
                            Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i, j + 1];
                            rng.BorderAround();
                            xlWorkSheet.Columns.AutoFit();
                            xlWorkSheet.Rows.AutoFit();
                            DataGridViewCell cell = PackageVersions[j, i - 1];
                            if (j > 1)
                            {
                                if (PackageVersions[j - 1, i - 1].Value != null && PackageVersions[j, i - 1].Value != null)
                                {
                                    if (PackageVersions[j - 1, i - 1].Value.ToString() == PackageVersions[j, i - 1].Value.ToString())
                                    {
                                        Microsoft.Office.Interop.Excel.Range rng1 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                                        Microsoft.Office.Interop.Excel.Range rng2 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j];
                                        rng2.Font.Bold = rng1.Font.Bold = true;
                                        rng2.Interior.Color = rng1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                        rng1.BorderAround();
                                    }
                                }
                            }
                            xlWorkSheet.Cells[i + 1, j + 1] = cell.Value != null ? cell.Value.ToString() : "";
                        }
                    }
                    string lastFolderName = Path.GetFileName(Path.GetDirectoryName(sSelectedfolder));
                    string filename = "H:\\Nuget_List_" + lastFolderName + "_" + System.DateTime.Now.ToString("dd-MMM-yyyy_hh-mm");

                    xlWorkBook.SaveAs(filename, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);

                    outputmsg.Text = "From Given Solution " + (PackageVersions.RowCount -1) + " Nuget packages exported in Excel file, you can find the file in H drive as filename as" + filename;
                    result = MessageBox.Show("From Given Solution " + (PackageVersions.RowCount -1) + " NuGet packages exported in Excel file, you can find the file in H drive as filename as" + filename);
                    Log.write("From Given Solution " + (PackageVersions.RowCount-1) + " NuGet packages exported in Excel file, you can find the file in H drive as filename as" + filename, null, logPath);
                }

                else
                {
                    result = MessageBox.Show("No NuGet Package used in given solution!");
                    Log.write("No NuGet Package used in given solution!", null, logPath);
                }

                if (result == DialogResult.OK)
                {
                    ResetNuget();
                }
            }
            catch (Exception ex)
            {
                Log.write("Excception occured in exporting excel sheet", ex, logPath);
                MessageBox.Show(ex.Message);
            }
        }
        public void ResetNuget()
        {
            PackageVersions.DataSource = null;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = false;
            ExportNuget.Visible = false;
            outputmsg.Text = "Please select root folder of solution.";
        }
        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {


        }

        private void btnSelectFiles_Click(object sender, EventArgs e)
        {
            string path = null;
            Reset();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                path = folderBrowserDialog1.SelectedPath.ToString();
                string[] fileArray = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".config") || s.EndsWith(".xml")).ToArray(); ;
                lstFile_Paths.DataSource = fileArray;
                Select_All();
                chkSelectAll_lblTotalCount_settings();
            }
        }
        public void Validate(string[] fileArray)
        {
            dgvError_List.Visible = true;
            string error = null;
            int error_count = 0;
            for (int i = 0; i < fileArray.Length; i++)
            {
                if (fileArray[i] != " ")
                {
                    try
                    {
                        using (var readers = XmlReader.Create(fileArray[i].ToString()))

                        {
                            while (readers.Read()) { }
                            readers.Close();
                            dgvError_List.Rows.Add(Path.GetFileName(fileArray[i].ToString()), fileArray[i].ToString(), "No Error");
                        }
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        dgvError_List.Rows.Add(Path.GetFileName(fileArray[i].ToString()), fileArray[i].ToString(), error);
                        error = null;
                        error_count++;
                    }
                }
            }
            lblTotalCount.ForeColor = error_count != 0 ? Color.Red : Color.GreenYellow;
            lblTotalCount.Text = error_count != 0 ? error_count.ToString() + "/" + lstFile_Paths.SelectedItems.Count.ToString() : lstFile_Paths.SelectedItems.Count.ToString();
        }
        public void Select_All()
        {
            for (int i = 0; i < lstFile_Paths.Items.Count; i++)
            {
                lstFile_Paths.SetSelected(i, true);
            }
        }
        public void Reset()
        {
            lstFile_Paths.DataSource = null;
            dgvError_List.Rows.Clear();
            dgvError_List.Refresh();
            dgvError_List.Visible = false;
            chkSelectAll.Visible = false;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            dgvError_List.Rows.Clear();
            dgvError_List.Refresh();
            dgvError_List.Visible = false;
            string[] fileArray = new string[lstFile_Paths.Items.Count];
            if (lstFile_Paths.SelectedItems.Count != 0)
            {
                for (int i = 0; i < lstFile_Paths.Items.Count; i++)
                {
                    fileArray[i] = lstFile_Paths.GetSelected(i) == true ? lstFile_Paths.Items[i].ToString() : " ";
                }
                Validate(fileArray);
                dgvError_List.Visible = true;
            }
            else
            {
                MessageBox.Show("Please select a file!");
            }
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (dgvError_List.Rows.Count > 1)
            {
                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                int i = 0;
                int j = 0;
                for (j = 0; j <= 2; j++)
                {
                    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                    rng.Font.Bold = true;
                    rng.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                    rng.BorderAround();
                }
                i = 0;
                j = 0;
                xlWorkSheet.Cells[i + 1, j + 1] = "File Name";
                xlWorkSheet.Cells[i + 1, j + 2] = "Path";
                xlWorkSheet.Cells[i + 1, j + 3] = "Error";


                for (i = 1; i <= dgvError_List.RowCount; i++)
                {
                    for (j = 0; j <= dgvError_List.ColumnCount - 1; j++)
                    {
                        Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i, j + 1];
                        rng.BorderAround();
                        xlWorkSheet.Columns.AutoFit();
                        xlWorkSheet.Rows.AutoFit();
                        DataGridViewCell cell = dgvError_List[j, i - 1];
                        xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
                    }
                }

                xlWorkBook.SaveAs("H:\\XML_Validator_Result_" + System.DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss"), Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show("Data exported in Excel file, you can find the file in H drive!");
            }
            else
            {
                MessageBox.Show("No data to export!");
            }
        }
        int flag = 0;
        private void btnReset_Click(object sender, EventArgs e)
        {
            if (lstFile_Paths.Items.Count != 0 || dgvError_List.Rows.Count != 0)
            {
                if (flag == 1)
                {
                    lstFile_Paths.Items.Clear();
                    flag = 0;
                }
                Reset();
                lblTotalCount.Text = lstFile_Paths.Items.Count.ToString();
                lblTotalCount.ForeColor = Color.GreenYellow;
            }
            else
            {
                MessageBox.Show("No Items to Clear!");
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSelectAll.CheckState == CheckState.Checked)
            {
                Select_All();
            }
            else if (chkSelectAll.CheckState == CheckState.Unchecked)
            {
                lstFile_Paths.ClearSelected();
            }
        }
        private void lstFile_Paths_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTotalCount.ForeColor = Color.GreenYellow;
            lblTotalCount.Text = lstFile_Paths.SelectedItems.Count.ToString();
            chkSelectAll.CheckState = lstFile_Paths.Items.Count == lstFile_Paths.SelectedItems.Count ? CheckState.Checked : CheckState.Unchecked;
        }

        public void chkSelectAll_lblTotalCount_settings()
        {
            chkSelectAll.Visible = true;
            chkSelectAll.CheckState = CheckState.Checked;
            lblTotalCount.Visible = true;
            lblTotalCount.Text = lstFile_Paths.SelectedItems.Count.ToString();
        }

        private void lstFile_Paths_DragDrop(object sender, DragEventArgs e)
        {
            int j = 0;
            string[] path = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            StringBuilder non_XML = new StringBuilder();
            if (path[j].EndsWith(".config") || path[j].EndsWith(".xml"))
            {
                for (j = 0; j < path.Count(); j++)
                {
                    if (path[j].EndsWith(".config") || path[j].EndsWith(".xml"))
                    {
                        lstFile_Paths.Items.Add(path[j].ToString());
                    }
                    else
                    {
                        non_XML.Append(Path.GetFileName(path[j].ToString()) + ", ");
                    }
                }
                flag = 1;
                MessageBox.Show("The File/Files : " + non_XML + "is neither an XML file nor a Config file");
            }
            else
            {
                string[] fileArray = Directory.GetFiles(path[0], "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".config") || s.EndsWith(".xml")).ToArray();
                lstFile_Paths.DataSource = fileArray;
            }
            Select_All();
            chkSelectAll_lblTotalCount_settings();
        }

        private void xMLValidatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            NugetPanel.Visible = false;
        }

        private void nugetExtractListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NugetPanel.Visible = true;
            panel1.Visible = false;
        }
    }
}
