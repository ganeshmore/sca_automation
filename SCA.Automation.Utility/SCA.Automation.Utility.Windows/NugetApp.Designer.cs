﻿namespace SCA.Automation.Utility.Windows
{
    partial class NugetApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.xMLValidatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nugetExtractListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NugetPanel = new System.Windows.Forms.Panel();
            this.outputmsg = new System.Windows.Forms.Label();
            this.ExportNuget = new System.Windows.Forms.Button();
            this.SelectFolder = new System.Windows.Forms.Button();
            this.PackageVersions = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.StepsPanel = new System.Windows.Forms.Panel();
            this.ConfigValidatorSteps = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            this.dgvError_List = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstFile_Paths = new System.Windows.Forms.ListBox();
            this.btnValidate = new System.Windows.Forms.Button();
            this.lblTotalCount = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnExportToExcel = new System.Windows.Forms.Button();
            this.btnSelectFiles = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.NugetPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackageVersions)).BeginInit();
            this.panel1.SuspendLayout();
            this.StepsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvError_List)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xMLValidatorToolStripMenuItem,
            this.nugetExtractListToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1272, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // xMLValidatorToolStripMenuItem
            // 
            this.xMLValidatorToolStripMenuItem.Name = "xMLValidatorToolStripMenuItem";
            this.xMLValidatorToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.xMLValidatorToolStripMenuItem.Text = "XML Validator";
            this.xMLValidatorToolStripMenuItem.Click += new System.EventHandler(this.xMLValidatorToolStripMenuItem_Click);
            // 
            // nugetExtractListToolStripMenuItem
            // 
            this.nugetExtractListToolStripMenuItem.Name = "nugetExtractListToolStripMenuItem";
            this.nugetExtractListToolStripMenuItem.Size = new System.Drawing.Size(111, 20);
            this.nugetExtractListToolStripMenuItem.Text = "Nuget Extract List";
            this.nugetExtractListToolStripMenuItem.Click += new System.EventHandler(this.nugetExtractListToolStripMenuItem_Click);
            // 
            // NugetPanel
            // 
            this.NugetPanel.Controls.Add(this.outputmsg);
            this.NugetPanel.Controls.Add(this.ExportNuget);
            this.NugetPanel.Controls.Add(this.SelectFolder);
            this.NugetPanel.Controls.Add(this.PackageVersions);
            this.NugetPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NugetPanel.Location = new System.Drawing.Point(0, 24);
            this.NugetPanel.Name = "NugetPanel";
            this.NugetPanel.Size = new System.Drawing.Size(1272, 631);
            this.NugetPanel.TabIndex = 1;
            this.NugetPanel.Visible = false;
            this.NugetPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.NugetPanel_Paint);
            // 
            // outputmsg
            // 
            this.outputmsg.AutoSize = true;
            this.outputmsg.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputmsg.Location = new System.Drawing.Point(63, 126);
            this.outputmsg.Name = "outputmsg";
            this.outputmsg.Size = new System.Drawing.Size(52, 17);
            this.outputmsg.TabIndex = 7;
            this.outputmsg.Text = "label1";
            // 
            // ExportNuget
            // 
            this.ExportNuget.Enabled = false;
            this.ExportNuget.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExportNuget.Location = new System.Drawing.Point(495, 85);
            this.ExportNuget.Name = "ExportNuget";
            this.ExportNuget.Size = new System.Drawing.Size(189, 23);
            this.ExportNuget.TabIndex = 4;
            this.ExportNuget.Text = "Export NugetList";
            this.ExportNuget.UseVisualStyleBackColor = true;
            this.ExportNuget.Click += new System.EventHandler(this.ExportNuget_Click);
            // 
            // SelectFolder
            // 
            this.SelectFolder.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectFolder.Location = new System.Drawing.Point(66, 85);
            this.SelectFolder.Name = "SelectFolder";
            this.SelectFolder.Size = new System.Drawing.Size(189, 23);
            this.SelectFolder.TabIndex = 6;
            this.SelectFolder.Text = "Select Root Folder of Solution";
            this.SelectFolder.UseVisualStyleBackColor = true;
            this.SelectFolder.Click += new System.EventHandler(this.Button2_Click);
            // 
            // PackageVersions
            // 
            this.PackageVersions.AllowUserToOrderColumns = true;
            this.PackageVersions.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PackageVersions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PackageVersions.Location = new System.Drawing.Point(27, 142);
            this.PackageVersions.Name = "PackageVersions";
            this.PackageVersions.ReadOnly = true;
            this.PackageVersions.RowHeadersVisible = false;
            this.PackageVersions.Size = new System.Drawing.Size(746, 199);
            this.PackageVersions.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.StepsPanel);
            this.panel1.Controls.Add(this.dgvError_List);
            this.panel1.Controls.Add(this.lstFile_Paths);
            this.panel1.Controls.Add(this.btnValidate);
            this.panel1.Controls.Add(this.lblTotalCount);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnExportToExcel);
            this.panel1.Controls.Add(this.btnSelectFiles);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1272, 631);
            this.panel1.TabIndex = 2;
            this.panel1.Visible = false;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // StepsPanel
            // 
            this.StepsPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.StepsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StepsPanel.Controls.Add(this.ConfigValidatorSteps);
            this.StepsPanel.Controls.Add(this.label2);
            this.StepsPanel.Location = new System.Drawing.Point(917, 61);
            this.StepsPanel.Name = "StepsPanel";
            this.StepsPanel.Size = new System.Drawing.Size(327, 219);
            this.StepsPanel.TabIndex = 30;
            // 
            // ConfigValidatorSteps
            // 
            this.ConfigValidatorSteps.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ConfigValidatorSteps.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfigValidatorSteps.Location = new System.Drawing.Point(3, 38);
            this.ConfigValidatorSteps.Multiline = true;
            this.ConfigValidatorSteps.Name = "ConfigValidatorSteps";
            this.ConfigValidatorSteps.Size = new System.Drawing.Size(306, 182);
            this.ConfigValidatorSteps.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(125, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "STEPS";
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSelectAll.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.chkSelectAll.Location = new System.Drawing.Point(58, 3);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(80, 23);
            this.chkSelectAll.TabIndex = 29;
            this.chkSelectAll.Text = "Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.Click += new System.EventHandler(this.chkSelectAll_Click);
            // 
            // dgvError_List
            // 
            this.dgvError_List.AllowUserToAddRows = false;
            this.dgvError_List.AllowUserToOrderColumns = true;
            this.dgvError_List.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvError_List.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvError_List.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvError_List.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvError_List.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvError_List.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvError_List.ColumnHeadersVisible = false;
            this.dgvError_List.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvError_List.GridColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvError_List.Location = new System.Drawing.Point(242, 286);
            this.dgvError_List.Name = "dgvError_List";
            this.dgvError_List.ReadOnly = true;
            this.dgvError_List.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvError_List.RowHeadersVisible = false;
            this.dgvError_List.RowHeadersWidth = 45;
            this.dgvError_List.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvError_List.Size = new System.Drawing.Size(1002, 294);
            this.dgvError_List.TabIndex = 28;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "File Name";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 79;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Path";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 54;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "Error";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 54;
            // 
            // lstFile_Paths
            // 
            this.lstFile_Paths.AllowDrop = true;
            this.lstFile_Paths.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstFile_Paths.FormattingEnabled = true;
            this.lstFile_Paths.ItemHeight = 15;
            this.lstFile_Paths.Location = new System.Drawing.Point(242, 98);
            this.lstFile_Paths.Name = "lstFile_Paths";
            this.lstFile_Paths.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstFile_Paths.Size = new System.Drawing.Size(669, 184);
            this.lstFile_Paths.TabIndex = 27;
            this.lstFile_Paths.SelectedIndexChanged += new System.EventHandler(this.lstFile_Paths_SelectedIndexChanged);
            this.lstFile_Paths.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstFile_Paths_DragDrop);
            this.lstFile_Paths.DragEnter += new System.Windows.Forms.DragEventHandler(this.lstFile_Paths_DragEnter);
            // 
            // btnValidate
            // 
            this.btnValidate.AutoSize = true;
            this.btnValidate.CausesValidation = false;
            this.btnValidate.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnValidate.Location = new System.Drawing.Point(27, 172);
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.Size = new System.Drawing.Size(154, 89);
            this.btnValidate.TabIndex = 26;
            this.btnValidate.Text = "Validate";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // lblTotalCount
            // 
            this.lblTotalCount.AutoSize = true;
            this.lblTotalCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCount.Location = new System.Drawing.Point(61, 520);
            this.lblTotalCount.Name = "lblTotalCount";
            this.lblTotalCount.Size = new System.Drawing.Size(238, 42);
            this.lblTotalCount.TabIndex = 25;
            this.lblTotalCount.Text = "lblTotalCount";
            this.lblTotalCount.Visible = false;
            // 
            // btnReset
            // 
            this.btnReset.AutoSize = true;
            this.btnReset.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(27, 395);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(154, 89);
            this.btnReset.TabIndex = 24;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.AutoSize = true;
            this.btnExportToExcel.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportToExcel.Location = new System.Drawing.Point(27, 286);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(154, 89);
            this.btnExportToExcel.TabIndex = 23;
            this.btnExportToExcel.Text = "Export to Excel";
            this.btnExportToExcel.UseVisualStyleBackColor = true;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // btnSelectFiles
            // 
            this.btnSelectFiles.AutoSize = true;
            this.btnSelectFiles.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFiles.Location = new System.Drawing.Point(27, 63);
            this.btnSelectFiles.Name = "btnSelectFiles";
            this.btnSelectFiles.Size = new System.Drawing.Size(154, 89);
            this.btnSelectFiles.TabIndex = 22;
            this.btnSelectFiles.Text = "Select Files";
            this.btnSelectFiles.UseVisualStyleBackColor = true;
            this.btnSelectFiles.Click += new System.EventHandler(this.BtnSelectFiles_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(543, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Config Validator";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.chkSelectAll);
            this.panel2.Location = new System.Drawing.Point(242, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(669, 29);
            this.panel2.TabIndex = 31;
            // 
            // NugetApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1272, 655);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NugetPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "NugetApp";
            this.Text = "SCA Automations";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.NugetPanel.ResumeLayout(false);
            this.NugetPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PackageVersions)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.StepsPanel.ResumeLayout(false);
            this.StepsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvError_List)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem xMLValidatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nugetExtractListToolStripMenuItem;
        private System.Windows.Forms.Panel NugetPanel;
        private System.Windows.Forms.Label outputmsg;
        private System.Windows.Forms.Button ExportNuget;
        private System.Windows.Forms.Button SelectFolder;
        private System.Windows.Forms.DataGridView PackageVersions;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkSelectAll;
        private System.Windows.Forms.DataGridView dgvError_List;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.ListBox lstFile_Paths;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Label lblTotalCount;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnExportToExcel;
        private System.Windows.Forms.Button btnSelectFiles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel StepsPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ConfigValidatorSteps;
        private System.Windows.Forms.Panel panel2;
    }
}

