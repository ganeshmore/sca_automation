﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;


namespace SCA.Automation.Utility.Windows
{

    public partial class NugetApp : Form
    {
        string selectedfolder;
        string[] fileArray;
        DialogResult dialogResult;
        string logPath = ConfigurationManager.AppSettings[Library.Constants.LogPath].ToString();
        public NugetApp()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    selectedfolder = folderBrowserDialog1.SelectedPath.ToString();
                    fileArray = Directory.GetFiles(selectedfolder, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(Library.Constants.PackageConfig)).ToArray(); ;
                    ExportNuget.Enabled = true;
                    ExportNuget.Visible = true;
                    PackageVersions.DataSource = null;
                    PackageVersions.Rows.Clear();
                    PackageVersions.Refresh();
                    PackageVersions.Visible = true;
                }
            }
            catch (FileNotFoundException ex)
            {
                Log.Write(Library.Constants.FileNotFoundException, ex, logPath);
            }
            catch (PathTooLongException ex)
            {
                Log.Write(Library.Constants.PathTooLongException, ex, logPath);
            }
            catch (Exception ex)
            {
                Log.Write(Library.Constants.GeneralException, ex, logPath);
            }
        }

        private void NugetPanel_Paint(object sender, PaintEventArgs e)
        {
            outputmsg.Text = Library.Constants.SelectFolder;

            if (!File.Exists(logPath))
            {
                using (FileStream fileStream = File.Create(logPath))
                {
                    // Add some text to file
                    Byte[] logTitle = new UTF8Encoding(true).GetBytes(Library.Constants.InitialLogText);
                    fileStream.Write(logTitle, 0, logTitle.Length);
                    byte[] author = new UTF8Encoding(true).GetBytes(Library.Constants.AuthorName);
                    fileStream.Write(author, 0, author.Length);
                }
            }
        }
        public static class Log
        {
            public static void Write(string logMessage, Exception exception, string logPath)
            {
                string logText = exception == null ? logMessage : exception.ToString();
                using (StreamWriter streamWriter = new StreamWriter(logPath, true))
                {
                    streamWriter.WriteLine(logText);
                }
            }
        }

        private void ExportNuget_Click(object sender, EventArgs e)
        {
            ExportNuget.Enabled = false;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = true;
            outputmsg.Text = Library.Constants.LoadingText;
            Log.Write(string.Format(Library.Constants.ListExtractionStarted, DateTime.Now.ToString()), null, logPath);

            SCA.Automation.Library.NugetExportList nugetList = new SCA.Automation.Library.NugetExportList();
            DataTable nugetPkgDataTable = nugetList.NugetListExtract(fileArray);

            PackageVersions.DataSource = nugetPkgDataTable;
            try
            {
                if (PackageVersions.Rows.Count > 1)
                {
                    Microsoft.Office.Interop.Excel.Application xlApp;
                    Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                    Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                    object misValue = System.Reflection.Missing.Value;

                    xlApp = new Microsoft.Office.Interop.Excel.Application();
                    xlWorkBook = xlApp.Workbooks.Add(misValue);
                    xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    for (j = 0; j <= 2; j++)
                    {
                        Microsoft.Office.Interop.Excel.Range excelRange = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                        excelRange.Font.Bold = true;
                        excelRange.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                        excelRange.BorderAround();
                    }
                    i = 0;
                    j = 0;
                    xlWorkSheet.Cells[i + 1, j + 1] = Library.Constants.PackageName;
                    xlWorkSheet.Cells[i + 1, j + 2] = Library.Constants.InstalledVersion;
                    xlWorkSheet.Cells[i + 1, j + 3] = Library.Constants.LatestVersion;

                    for (i = 1; i <= PackageVersions.RowCount; i++)
                    {
                        for (j = 0; j <= PackageVersions.ColumnCount - 1; j++)
                        {
                            Microsoft.Office.Interop.Excel.Range excelRange = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i, j + 1];
                            excelRange.BorderAround();
                            xlWorkSheet.Columns.AutoFit();
                            xlWorkSheet.Rows.AutoFit();
                            DataGridViewCell cell = PackageVersions[j, i - 1];
                            if (j > 1 && PackageVersions[j - 1, i - 1].Value != null && PackageVersions[j, i - 1].Value != null && PackageVersions[j - 1, i - 1].Value.ToString() == PackageVersions[j, i - 1].Value.ToString())
                            {
                                Microsoft.Office.Interop.Excel.Range excelRange1 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                                Microsoft.Office.Interop.Excel.Range excelRange2 = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j];
                                excelRange2.Font.Bold = excelRange1.Font.Bold = true;
                                excelRange2.Interior.Color = excelRange1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                                excelRange1.BorderAround();
                            }
                            xlWorkSheet.Cells[i + 1, j + 1] = cell.Value != null ? cell.Value.ToString() : "";
                        }
                    }
                    string lastFolderName = Path.GetFileName(Path.GetDirectoryName(selectedfolder));
                    string exportNugetFilePath = ConfigurationManager.AppSettings[Library.Constants.ExportNugetFilePath].ToString();
                    string fileName = string.Format(exportNugetFilePath, lastFolderName, System.DateTime.Now.ToString("dd-MMM-yyyy_hh-mm"));
                    xlWorkBook.SaveAs(fileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                    xlWorkBook.Close(true, misValue, misValue);
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);

                    outputmsg.Text = string.Format(Library.Constants.NumberOfNugetPkgsText, PackageVersions.RowCount - 1, fileName);
                    dialogResult = MessageBox.Show(string.Format(Library.Constants.NumberOfNugetPkgsText, PackageVersions.RowCount - 1, fileName));
                    Log.Write(string.Format(Library.Constants.NumberOfNugetPkgsText, PackageVersions.RowCount - 1, fileName), null, logPath);
                }

                else
                {
                    dialogResult = MessageBox.Show(Library.Constants.NoNugetPkgsInSolution);
                    Log.Write(Library.Constants.NoNugetPkgsInSolution, null, logPath);
                }

                if (dialogResult == DialogResult.OK)
                {
                    ResetNuget();
                }
            }
            catch (Exception ex)
            {
                Log.Write(Library.Constants.GeneralException, ex, logPath);
            }
        }
        public void ResetNuget()
        {
            PackageVersions.DataSource = null;
            PackageVersions.Rows.Clear();
            PackageVersions.Refresh();
            PackageVersions.Visible = false;
            ExportNuget.Visible = false;
            outputmsg.Text = Library.Constants.SelectFolder;
        }

        private void BtnSelectFiles_Click(object sender, EventArgs e)
        {
            string filePath = null;
            Reset();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                filePath = folderBrowserDialog1.SelectedPath.ToString();
                string[] fileArrayList = Directory.GetFiles(filePath, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(Library.Constants.Config) || s.EndsWith(Library.Constants.XML)).ToArray(); ;
                lstFile_Paths.DataSource = fileArrayList;
                SelectAll();
                ChkSelectAlllblTotalCountSettings();
            }
        }
        public void Validate(string[] fileArray)
        {
            dgvError_List.Visible = true;
            string validationError = null;
            int errorCount = 0;
            for (int i = 0; i < fileArray.Length; i++)
            {
                if (fileArray[i] != " ")
                {
                    try
                    {
                        using (var readers = XmlReader.Create(fileArray[i].ToString()))

                        {
                            while (readers.Read())
                            {
                                //reading the file 
                            }
                            readers.Close();
                            dgvError_List.Rows.Add(Path.GetFileName(fileArray[i].ToString()), fileArray[i].ToString(), Library.Constants.NoError);
                        }
                    }
                    catch (Exception ex)
                    {
                        validationError = ex.Message;
                        dgvError_List.Rows.Add(Path.GetFileName(fileArray[i].ToString()), fileArray[i].ToString(), validationError);
                        errorCount++;
                    }
                }
            }
            lblTotalCount.ForeColor = errorCount != 0 ? Color.Red : Color.GreenYellow;
            lblTotalCount.Text = errorCount != 0 ? errorCount.ToString() + "/" + lstFile_Paths.SelectedItems.Count.ToString() : lstFile_Paths.SelectedItems.Count.ToString();
        }
        public void SelectAll()
        {
            for (int i = 0; i < lstFile_Paths.Items.Count; i++)
            {
                lstFile_Paths.SetSelected(i, true);
            }
        }
        public void Reset()
        {
            lstFile_Paths.DataSource = null;
            dgvError_List.Rows.Clear();
            dgvError_List.Refresh();
            dgvError_List.Visible = false;
            chkSelectAll.Visible = false;
        }

        private void btnValidate_Click(object sender, EventArgs e)
        {
            dgvError_List.Rows.Clear();
            dgvError_List.Refresh();
            dgvError_List.Visible = false;
            string[] fileArrayList = new string[lstFile_Paths.Items.Count];
            if (lstFile_Paths.SelectedItems.Count != 0)
            {
                for (int i = 0; i < lstFile_Paths.Items.Count; i++)
                {
                    fileArrayList[i] = lstFile_Paths.GetSelected(i) == true ? lstFile_Paths.Items[i].ToString() : " ";
                }
                Validate(fileArrayList);
                dgvError_List.Visible = true;
            }
            else
            {
                MessageBox.Show(Library.Constants.SelectFile);
            }
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (dgvError_List.Rows.Count >= 1)
            {
                Microsoft.Office.Interop.Excel.Application xlApp;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                int i = 0;
                int j = 0;
                for (j = 0; j <= 2; j++)
                {
                    Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                    range.Font.Bold = true;
                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                    range.BorderAround();
                }
                i = 0;
                j = 0;
                xlWorkSheet.Cells[i + 1, j + 1] = Library.Constants.FileName;
                xlWorkSheet.Cells[i + 1, j + 2] = Library.Constants.FilePath;
                xlWorkSheet.Cells[i + 1, j + 3] = Library.Constants.FileError;


                for (i = 1; i <= dgvError_List.RowCount; i++)
                {
                    for (j = 0; j <= dgvError_List.ColumnCount - 1; j++)
                    {
                        Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)xlApp.Cells[i + 1, j + 1];
                        range.BorderAround();
                        xlWorkSheet.Columns.AutoFit();
                        xlWorkSheet.Rows.AutoFit();
                        DataGridViewCell cell = dgvError_List[j, i - 1];
                        xlWorkSheet.Cells[i + 1, j + 1] = cell.Value;
                    }
                }
                string xmlValidatorPath = ConfigurationManager.AppSettings[Library.Constants.XMLValidatorPath].ToString();
                xlWorkBook.SaveAs(xmlValidatorPath + System.DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss"), Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                MessageBox.Show(Library.Constants.XlFilePath);
            }
            else
            {
                MessageBox.Show(Library.Constants.NoDataToExport);
            }
        }
        int flag = 0;
        private void btnReset_Click(object sender, EventArgs e)
        {
            if (lstFile_Paths.Items.Count != 0 || dgvError_List.Rows.Count != 0)
            {
                if (flag == 1)
                {
                    lstFile_Paths.Items.Clear();
                    flag = 0;
                }
                Reset();
                lblTotalCount.Text = lstFile_Paths.Items.Count.ToString();
                lblTotalCount.ForeColor = Color.GreenYellow;
            }
            else
            {
                MessageBox.Show(Library.Constants.NoItemToClear);
            }
        }

        private void chkSelectAll_Click(object sender, EventArgs e)
        {
            if (chkSelectAll.CheckState == CheckState.Checked)
            {
                SelectAll();
            }
            else if (chkSelectAll.CheckState == CheckState.Unchecked)
            {
                lstFile_Paths.ClearSelected();
            }
        }
        public void ChkSelectAlllblTotalCountSettings()
        {
            chkSelectAll.Visible = true;
            chkSelectAll.CheckState = CheckState.Checked;
            lblTotalCount.Visible = true;
            lblTotalCount.Text = lstFile_Paths.SelectedItems.Count.ToString();
        }

        private void lstFile_Paths_DragDrop(object sender, DragEventArgs e)
        {
            int j = 0;
            string[] path = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            StringBuilder nonXML = new StringBuilder();
            if (path[j].EndsWith(Library.Constants.Config) || path[j].EndsWith(Library.Constants.XML))
            {
                for (j = 0; j < path.Count(); j++)
                {
                    if (path[j].EndsWith(Library.Constants.Config) || path[j].EndsWith(Library.Constants.XML))
                    {
                        lstFile_Paths.Items.Add(path[j].ToString());
                    }
                    else
                    {
                        nonXML.Append(Path.GetFileName(path[j].ToString()) + ", ");
                    }
                }
                flag = 1;
                MessageBox.Show(string.Format(Library.Constants.NoXMlNoConfig, nonXML));
            }
            else
            {
                string[] fileArrayList = Directory.GetFiles(path[0], "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(Library.Constants.Config) || s.EndsWith(".xml")).ToArray();
                lstFile_Paths.DataSource = fileArrayList;
            }
            SelectAll();
            ChkSelectAlllblTotalCountSettings();
        }

        private void xMLValidatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
            NugetPanel.Visible = false;
        }

        private void nugetExtractListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NugetPanel.Visible = true;
            panel1.Visible = false;
        }

        private void lstFile_Paths_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblTotalCount.ForeColor = Color.GreenYellow;
            lblTotalCount.Text = lstFile_Paths.SelectedItems.Count.ToString();
            chkSelectAll.CheckState = lstFile_Paths.Items.Count == lstFile_Paths.SelectedItems.Count ? CheckState.Checked : CheckState.Unchecked;
        }
        private void lstFile_Paths_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = e.Data.GetDataPresent(DataFormats.FileDrop, false) ? DragDropEffects.All : DragDropEffects.None;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            string validationSteps = "1.  Please select a folder which contains config files by clicking on 'Select Files' button. <br><br> 2. Click on 'Validate' button to validate the selected config files. <br><br> 3. Click on 'Export to Excel' button to export the validated data in excel file.";
            ConfigValidatorSteps.Text = validationSteps.Replace("<br>", Environment.NewLine);
            dgvError_List.Columns[1].Width = 408;
            dgvError_List.Columns[2].Width =
                dgvError_List.Width
                - dgvError_List.Columns[0].Width
                - dgvError_List.Columns[1].Width;
        }
    }
}
